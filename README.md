# 博客系统多国语言版v1.0

> 支持 133 种语言文字，自带本地运行环境 nginx1.20.2+php5.4.9+mysql5.5.62 精简最少体积占用，运行最小内存占用（1GB内存，512MB内存照样能跑），兼容老爷机或老服务器，一切大道从简。


## 简单使用

> 原生手写MVC（兼容PHP5.4+），它简单、易用、轻量，并且易于扩展，支持133种语言文字。

start.bat 一键开启
stop.bat 一键关闭

默认匿名用户，输入验证码直接登录
(匿名用户属于游客角色，只能看，不能增删改和使用上传接口)
用户名 anonymous
密码 123456

管理员用户
(管理员角色开放所有权限)
用户名 admin
密码 admin

管理员用户登录后台 http://localhost/admin/ （或前端点击右上角控制台）
后台点击右上角三个点，点开后，点击重置系统，恢复出厂设置。


## 演示地址

> https://yesu.co/


## 支持语言

> 繁体中文使用字典替换不写数据库

| 语言码   | 语言名称                                        |
| -------- | ----------------------------------------------- |
| co       | Corsu (科西嘉语)                                |
| gn       | guarani (瓜拉尼语)                              |
| rw       | Kinyarwanda (卢旺达语)                          |
| ha       | Hausa (豪萨语)                                  |
| no       | Norge (挪威语)                                  |
| af       | Suid-Afrikaanse Dutch taal (布尔语(南非荷兰语)) |
| yo       | Yoruba (约鲁巴语)                               |
| en       | English (英语)                                  |
| gom      | गोंगेन हें नांव (贡根语)                             |
| la       | Latina (拉丁语)                                 |
| ne       | नेपालीName (尼泊尔语)                            |
| fr       | Français (法语)                                 |
| cs       | čeština (捷克语)                                |
| haw      | ʻŌlelo Hawaiʻi (夏威夷语)                       |
| ka       | ჯორჯიანიName (格鲁吉亚语)                       |
| ru       | Русский язык (俄语)                             |
| zh-CN    | 简体中文 (中文（简体）)                         |
| fa       | فارسی (波斯语)                                  |
| bho      | भोजपुरी (博杰普尔语)                             |
| hi       | हिंदी (印地语)                                   |
| be       | беларускі (白俄罗斯语)                          |
| sw       | Kiswahili (斯瓦希里语)                          |
| is       | ÍslandName (冰岛语)                             |
| yi       | ייַדיש (意第绪语)                                |
| ak       | tur (契维语)                                    |
| ga       | Gaeilge (爱尔兰语)                              |
| gu       | ગુજરાતી (古吉拉特语)                             |
| km       | ខ្មែរKCharselect unicode block name (高棉语)     |
| sk       | Slovenská (斯洛伐克语)                          |
| iw       | היברית (希伯来语)                               |
| kn       | ಕನ್ನಡ್Name (卡纳达语)                             |
| hu       | Magyar (匈牙利语)                               |
| ta       | தாமில் (泰米尔语)                                |
| ar       | بالعربية (阿拉伯语)                             |
| bn       | বাংলা (孟加拉语)                                |
| az       | Azərbaycan (阿塞拜疆语)                         |
| sm       | lifiava (萨摩亚语)                              |
| su       | IndonesiaName (印尼巽他语)                      |
| da       | dansk (丹麦语)                                  |
| sn       | Shona (修纳语)                                  |
| bm       | Bamanankan (班巴拉语)                           |
| lt       | Lietuva (立陶宛语)                              |
| vi       | Tiếng Việt (越南语)                             |
| mt       | Malti (马耳他语)                                |
| tk       | Türkmençe (土库曼语)                            |
| as       | অসমীয়া (阿萨姆语)                               |
| ca       | català (加泰罗尼亚语)                           |
| si       | සිංගාපුර් (僧伽罗语)                               |
| ceb      | Cebuano (宿务语)                                |
| gd       | Gàidhlig na h-Alba (苏格兰盖尔语)               |
| sa       | Sanskrit (梵语)                                 |
| pl       | Polski (波兰语)                                 |
| gl       | galego (加利西亚语)                             |
| lv       | latviešu (拉脱维亚语)                           |
| uk       | УкраїнськаName (乌克兰语)                       |
| tt       | Татар (鞑靼语)                                  |
| cy       | Cymraeg (威尔士语)                              |
| ja       | 日本語 (日语)                                   |
| tl       | Pilipino (菲律宾语)                             |
| ay       | Aymara (艾马拉语)                               |
| lo       | ກະຣຸນາ (老挝语)                                  |
| te       | తెలుగుQFontDatabase (泰卢固语)                   |
| ro       | Română (罗马尼亚语)                             |
| ht       | a n:n (海地克里奥尔语)                          |
| doi      | डोग्रिड ने दी (多格来语)                          |
| sv       | Svenska (瑞典语)                                |
| mai      | मरातिलीName (迈蒂利语)                          |
| th       | ภาษาไทย (泰语)                                  |
| hy       | հայերեն (亚美尼亚语)                            |
| my       | ဗာရမ် (缅甸语)                                  |
| ps       | پښتوName (普什图语)                             |
| hmn      | Hmoob (苗语)                                    |
| dv       | ދިވެހި (迪维希语)                                  |
| zh-TW    | 繁體中文 (中文（繁体）)                         |
| lb       | LëtzebuergeschName (卢森堡语)                   |
| sd       | سنڌي (信德语)                                   |
| ku       | Kurdî (库尔德语（库尔曼吉语）)                  |
| tr       | Türkçe (土耳其语)                               |
| mk       | Македонски (马其顿语)                           |
| bg       | български (保加利亚语)                          |
| ms       | Malay (马来语)                                  |
| lg       | luganda (卢干达语)                              |
| mr       | मराठीName (马拉地语)                            |
| et       | eesti keel (爱沙尼亚语)                         |
| ml       | മലമാലം (马拉雅拉姆语)                           |
| de       | Deutsch (德语)                                  |
| sl       | slovenščina (斯洛文尼亚语)                      |
| ur       | اوردو (乌尔都语)                                |
| pt       | Português (葡萄牙语)                            |
| ig       | igbo (伊博语)                                   |
| ckb      | کوردی-سۆرانی (库尔德语（索拉尼）)               |
| om       | adeta (奥罗莫语)                                |
| el       | Ελληνικά (希腊语)                               |
| es       | español (西班牙语)                              |
| fy       | Frysk (弗里西语)                                |
| so       | Soomaali (索马里语)                             |
| am       | አማርኛ (阿姆哈拉语)                               |
| ny       | potakuyan (齐切瓦语)                            |
| pa       | ਪੰਜਾਬੀName (旁遮普语)                            |
| eu       | euskara (巴斯克语)                              |
| it       | Italiano (意大利语)                             |
| sq       | albanian (阿尔巴尼亚语)                         |
| ko       | 한어 (韩语)                                     |
| tg       | ТаjikӣName (塔吉克语)                           |
| fi       | Suomalainen (芬兰语)                            |
| ky       | Кыргыз тили (吉尔吉斯语)                        |
| ee       | Eʋegbe (埃维语)                                 |
| hr       | Hrvatski (克罗地亚语)                           |
| qu       | Quechua (克丘亚语)                              |
| bs       | bosanski (波斯尼亚语)                           |
| mi       | Maori (毛利语)                                  |
| or       | ଓଡିଆ (奥利亚语)                                  |
| ti       | ትግናን (蒂格尼亚语)                               |
| kk       | қазақ (哈萨克语)                                |
| nl       | Nederlands (荷兰语)                             |
| kri      | Krio we dɛn kɔl Krio (克里奥尔语)               |
| ln       | Lingala (林格拉语)                              |
| mg       | Malagasy (马尔加什语)                           |
| mn       | Монгол (蒙古语)                                 |
| lus      | Mizo tawng (米佐语)                             |
| xh       | IsiXhosa saseMzantsi Afrika (南非科萨语)        |
| zu       | Zulu, eNingizimu Afrika (南非祖鲁语)            |
| sr       | Српски (塞尔维亚语)                             |
| nso      | Sepeti (塞佩蒂语)                               |
| st       | Sesotho (塞索托语)                              |
| eo       | Esperanto (世界语)                              |
| mni-Mtei | แบบไทย (梅泰语（曼尼普尔语）)                   |
| ug       | ئۇيغۇر (维吾尔语)                               |
| uz       | o'zbek (乌兹别克语)                             |
| ilo      | Ilocano (伊洛卡诺语)                            |
| id       | bahasa Indonesia (印尼语)                       |
| jw       | Basa Jawa Indonesia (印尼爪哇语)                |
| ts       | Xitsonga (宗加语)                               |


## 配置信息

> html/config.php

### 本地调试

```php
<?php
return [
    // 设置时区
    'timezone' => 'Asia/Shanghai',

    // 数据库配置信息
    'db' => [
        'type' => 'mysql',
        'host' => '127.0.0.1',
        'dbname' => 'mvc',
        'port' => '3306',
        'charset' => 'utf8',
        'username' => 'root',
        'password' => '',
    ],

    // 调试模式
    'debug' => true,

    // 记录操作日志
    'log' => false,

    // 数据库零查询静态缓存周期(秒，0关闭)
    'cache' => 0,
```

### 线上设置

> 根据服务器所在国家设置时区，数据库配置信息，关闭调试模式，打开记录操作日志，启用数据库零查询静态缓存（示例为5分钟）

```php
<?php
return [
    // 设置时区
    'timezone' => 'Asia/Shanghai',

    // 数据库配置信息
    'db' => [
        'type' => 'mysql',
        'host' => '127.0.0.1',
        'dbname' => 'mvc',
        'port' => '3306',
        'charset' => 'utf8',
        'username' => 'root',
        'password' => '',
    ],

    // 调试模式
    'debug' => false,

    // 记录操作日志
    'log' => true,

    // 数据库零查询静态缓存周期(秒，0关闭)
    'cache' => 300,
```


## 注意事项

* 重置系统，恢复出厂设置，删除用户数据，恢复系统到一个初始状态。
* 如果系统破坏，导入数据库`html/mvc.sql`恢复系统到一个初始状态。


## 前端定制开发

### 定制样式

> html/static/css/custom.css

修改 css 样式，修改后，运行中会自动检测与其它 css 合并压缩到 `html/static/upload/style.css` 一个文件，减少 http 请求次数

### 定制动作

> html/static/js/custom.js

修改 js 文件，修改后，运行中会自动检测与其它 js 合并压缩到 `html/static/upload/script.js` 一个文件，减少 http 请求次数

### 定制控制器

> html/controller/index/Index.php

### 定制视图

> html/view/index/*

### 定制接口

> html/controller/api/Index.php


## mvc 使用命名空间

> PHP版本 5.4+

* config.php 配置文件（如连接数据库等）
* index.php 入口文件，可以用 nginx 或者 apache 隐藏入口
* autoload.php 类自动加载器
* mvc.sql 初始化数据库导入
* mvc/ 迷你mvc目录
    + Cache.php 数据库零查询静态缓存
    + Controller.php 基础控制器
    + Facade.php 门面静态化
    + Model.php 模型类（已门面）
    + View.php 视图类（已门面）
    + Util.php 实用工具（已门面）
    + cache_str/ 单点登录保存串
    + locale/ 本地国际化多语目录
    + tpl/ 模板编译目录
* controller/ 控制器目录
* model/ 模型目录
* view/ 视图目录
* facade/ 门面静态化目录
* static/ 静态文件目录
* util/ 扩展工具目录


## nginx 隐藏入口重写规则

> nginx.htaccess

```shell
location / {
  if (!-e $request_filename){
      rewrite ^/(.*)$ /index.php/$1 last;
  }
}
```


## apache 隐藏入口重写规则

> .htaccess

```shell
<IfModule mod_rewrite.c>
  RewriteEngine On
  RewriteBase /
  RewriteRule ^index\.php$ - [L]
  RewriteCond %{REQUEST_FILENAME} !-f
  RewriteCond %{REQUEST_FILENAME} !-d
  RewriteRule . /index.php [L]
</IfModule>
```