<?php

namespace model;

defined('IA_ROOT') || exit();

use mvc\Model;

// 模型类
class TestModel extends Model
{
    public function getData()
    {
        $data = [
            ['id' => 1, 'name' => 'test1'],
            ['id' => 2, 'name' => 'test2']
        ];
        return $data;
    }
}
