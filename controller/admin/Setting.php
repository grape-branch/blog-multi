<?php

namespace controller\admin;

defined('IA_ROOT') || exit();

use facade\Model;
use facade\View;
use facade\Util;

// 系统设置
class Setting extends Base
{
    // 网站设置
    public function index()
    {
        View::assign('row', $this->setting());
        View::assign('tran', [
            'basic' => Util::tran('基础设置'),
            'advanced' => Util::tran('高级设置'),
            'other' => Util::tran('其他设置'),
            'weblogo' => Util::tran('站标'),
            'webname' => Util::tran('网站名称'),
            'input_webname' => Util::tran('请输入网站名称'),
            'title' => Util::tran('标题'),
            'suggest' => Util::tran('建议'),
            'keywords_suggest' => Util::tran('关键词建议'),
            'description' => Util::tran('描述'),
            'upload_type' => Util::tran('上传类型'),
            'allow_upload_type' => Util::tran('允许上传的文件类型'),
            'limit_size' => Util::tran('限制大小'),
            'input_limit_size' => Util::tran('请输入限制大小'),
            'unit' => Util::tran('单位'),
            'default_max' => Util::tran('默认最大'),
            'robot_noindex' => Util::tran('蜘蛛不索引'),
            'single_login' => Util::tran('单点登录'),
            'copyr_info' => Util::tran('版权信息'),
            'input_copyr_info' => Util::tran('请输入版权信息'),
            'web_icp' => Util::tran('备案号'),
            'input' => Util::tran('请输入'),
            'carousel' => Util::tran('轮播图'),
            'link_address' => Util::tran('链接地址'),
            'hot_article' => Util::tran('热门文章'),
            'input_num' => Util::tran('请输入数量'),
            'bar_show' => Util::tran('条显示'),
            'home_comment' => Util::tran('首页评论'),
            'content_related' => Util::tran('内容相关'),
            'picture_per_page' => Util::tran('图片分页'),
            'friend_link' => Util::tran('友情链接'),
            'voice_reader' => Util::tran('语音朗读'),
            'comment_login' => Util::tran('评论登录'),
            'comment_pending' => Util::tran('评论待审'),
            'speed' => Util::tran('加速'),
            'save' => Util::tran('保存'),
            'reset' => Util::tran('重置'),
            'upload' => Util::tran('上传'),
        ]);
        View::display('admin/setting/index.html');
    }

    // 网站保存
    public function save_index()
    {
        if (Util::isAjax()) {
            $setting = Model::fetch('SELECT * FROM `setting` WHERE id=:id', [':id' => 1]);
            $param = Util::param(array_keys($this->setting('default')));
            $param = json_encode($param);
            // 写入或更新数据库记录日志
            if ($setting) {
                Model::update('setting', ['value' => $param], ['id' => 1]);
                // 计算媒体状态
                $paths = [0 => [], 1 => [], 2 => []];
                $values = json_decode($setting['value'], true);
                if ($values) {
                    foreach ($values as $value) {
                        if (preg_match('#/static/upload/.+?\.[gif|jpg|jpeg|png|bmp|webp|psd|svg|tiff]#', $value)) {
                            if (is_file(IA_ROOT . str_replace($this->base_url, '', $value))) {
                                $paths[1][] = str_replace($this->base_url, '', $value);
                            } else {
                                $paths[2][] = str_replace($this->base_url, '', $value);
                            }
                        }
                    }
                }
                $values = json_decode($param, true);
                if ($values) {
                    foreach ($values as $value) {
                        if (preg_match('#/static/upload/.+?\.[gif|jpg|jpeg|png|bmp|webp|psd|svg|tiff]#', $value)) {
                            if (is_file(IA_ROOT . str_replace($this->base_url, '', $value))) {
                                $paths[0][] = str_replace($this->base_url, '', $value);
                            } else {
                                $paths[2][] = str_replace($this->base_url, '', $value);
                            }
                        }
                    }
                }
                $paths[1] = array_filter($paths[1], function ($item) use ($paths) {
                    return !in_array($item, $paths[0]);
                });
                $paths[1] && Model::update('media', ['status' => 1], ['path' => array_unique($paths[1])]);
                $paths[0] && Model::update('media', ['status' => 0], ['path' => array_unique($paths[0])]);
                $paths[2] && Model::update('media', ['status' => 2], ['path' => array_unique($paths[2])]);
                $this->log(Util::tran('基础设置'), Util::tran('保存基础设置成功'), 0);
            } else {
                Model::insert('setting', ['name' => 'setting', 'value' => $param, 'time' => time()]);

                // 计算媒体状态
                $paths = [0 => [], 2 => []];
                $values = json_decode($param, true);
                if ($values) {
                    foreach ($values as $value) {
                        if (preg_match('#/static/upload/.+?[gif|jpg|jpeg|png|bmp|webp|psd|svg|tiff]#', $value)) {
                            if (is_file(IA_ROOT . str_replace($this->base_url, '', $value))) {
                                $paths[0][] = str_replace($this->base_url, '', $value);
                            } else {
                                $paths[2][] = str_replace($this->base_url, '', $value);
                            }
                        }
                    }
                }
                $paths[0] && Model::update('media', ['status' => 0], ['path' => $paths[0]]);
                $paths[2] && Model::update('media', ['status' => 2], ['path' => $paths[2]]);
                $this->log(Util::tran('基础设置'), Util::tran('首次基础设置成功'), 0);
            }
            is_file(IA_ROOT .'/static/upload/style.css') && @unlink(IA_ROOT .'/static/upload/style.css');
            is_file(IA_ROOT .'/static/upload/script.js') && @unlink(IA_ROOT .'/static/upload/script.js');
            Util::delDir(IA_ROOT . '/mvc/tpl/', true);
            exit(json_encode(['code' => 0, 'msg' => Util::tran('保存成功')]));
        }
    }

    // 恢复出厂
    public function recovery()
    {
        if (Util::isAjax()) {
            Model::beginTransaction();
            try {
                // 权限管理
                // 用户表
                Model::exec("DELETE FROM `user`");
                Model::exec("ALTER TABLE `user` AUTO_INCREMENT = 1");
                Model::exec("INSERT INTO `user` VALUES ('1', '1', 'admin', '', '', 'db69fc039dcbd2962cb4d28f5891aae1', '', '" . time() . "', '" . time() . "', '" . time() . "', '[\"co\",\"gn\",\"rw\",\"ha\",\"no\",\"af\",\"yo\",\"en\",\"gom\",\"la\",\"ne\",\"fr\",\"cs\",\"haw\",\"ka\",\"ru\",\"zh-CN\",\"fa\",\"bho\",\"hi\",\"be\",\"sw\",\"is\",\"yi\",\"ak\",\"ga\",\"gu\",\"km\",\"sk\",\"iw\",\"kn\",\"hu\",\"ta\",\"ar\",\"bn\",\"az\",\"sm\",\"su\",\"da\",\"sn\",\"bm\",\"lt\",\"vi\",\"mt\",\"tk\",\"as\",\"ca\",\"si\",\"ceb\",\"gd\",\"sa\",\"pl\",\"gl\",\"lv\",\"uk\",\"tt\",\"cy\",\"ja\",\"tl\",\"ay\",\"lo\",\"te\",\"ro\",\"ht\",\"doi\",\"sv\",\"mai\",\"th\",\"hy\",\"my\",\"ps\",\"hmn\",\"dv\",\"zh-TW\",\"lb\",\"sd\",\"ku\",\"tr\",\"mk\",\"bg\",\"ms\",\"lg\",\"mr\",\"et\",\"ml\",\"de\",\"sl\",\"ur\",\"pt\",\"ig\",\"ckb\",\"om\",\"el\",\"es\",\"fy\",\"so\",\"am\",\"ny\",\"pa\",\"eu\",\"it\",\"sq\",\"ko\",\"tg\",\"fi\",\"ky\",\"ee\",\"hr\",\"qu\",\"bs\",\"mi\",\"or\",\"ti\",\"kk\",\"nl\",\"kri\",\"ln\",\"mg\",\"mn\",\"lus\",\"xh\",\"zu\",\"sr\",\"nso\",\"st\",\"eo\",\"mni-Mtei\",\"ug\",\"uz\",\"ilo\",\"id\",\"jw\",\"ts\"]', '0', '')");
                Model::exec("INSERT INTO `user` VALUES ('2', '2', 'anonymous', '匿名', '', '87d9bb400c0634691f0e3baaf1e2fd0d', '', '" . time() . "', '" . time() . "', '" . time() . "', '', '0', '')");
                // 角色表
                Model::exec("DELETE FROM `role`");
                Model::exec("ALTER TABLE `role` AUTO_INCREMENT = 1");
                Model::exec("INSERT INTO `role` VALUES ('1', '管理员', '0', '[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,88,89,90,91,92,93,94,95,96,97,98,99,100,101,102,103,104,105,106,107,108,109,110,111,112,113,114]', '0', '" . time() . "')");
                Model::exec("INSERT INTO `role` VALUES ('2', '游客', '0', '[1,2,3,4,5,6,8,12,13,14,16,20,21,22,24,28,29,30,32,36,37,38,40,44,45,46,48,52,55,56,57,59,63,64,65,70,71,72,74,78,79,80,82,87,89,90,93,95,96,97,99,100,102,103,104,106,107,108,110,111,112,114]', '0', '" . time() . "')");
                Model::exec("INSERT INTO `role` VALUES ('3', '翻译', '0', '[1,2,3,4,5,6,8,12,13,14,16,20,21,22,24,28,29,30,32,36,37,38,40,44,45,46,48,52,55,56,57,59,63,64,65,70,71,72,74,78,79,80,82,87,89,90,93,95,96,97,98,99,100,101,102,103,104,105,106,107,108,109,110,111,112,113,114]', '0', '" . time() . "')");
                // 权限表
                Model::exec("DELETE FROM `auth`");
                Model::exec("ALTER TABLE `auth` AUTO_INCREMENT = 1");
                Model::exec("INSERT INTO `auth` VALUES ('1', '0', '后台首页', 'Index', 'index', 'layui-icon layui-icon-home', '0', '1', '0', '" . time() . "')");
                Model::exec("INSERT INTO `auth` VALUES ('2', '1', '欢迎页面', 'Index', 'welcome', '', '0', '1', '0', '" . time() . "')");
                Model::exec("INSERT INTO `auth` VALUES ('3', '0', '权限管理', '', '', 'layui-icon layui-icon-auz', '0', '0', '0', '" . time() . "')");
                Model::exec("INSERT INTO `auth` VALUES ('4', '3', '用户管理', 'User', 'index', '', '0', '0', '0', '" . time() . "')");
                Model::exec("INSERT INTO `auth` VALUES ('5', '4', '查看', 'User', 'index', '', '0', '1', '0', '" . time() . "')");
                Model::exec("INSERT INTO `auth` VALUES ('6', '4', '创建', 'User', 'create', '', '0', '1', '0', '" . time() . "')");
                Model::exec("INSERT INTO `auth` VALUES ('7', '4', '保存', 'User', 'save', '', '0', '1', '0', '" . time() . "')");
                Model::exec("INSERT INTO `auth` VALUES ('8', '4', '编辑', 'User', 'edit', '', '0', '1', '0', '" . time() . "')");
                Model::exec("INSERT INTO `auth` VALUES ('9', '4', '更新', 'User', 'update', '', '0', '1', '0', '" . time() . "')");
                Model::exec("INSERT INTO `auth` VALUES ('10', '4', '修改属性', 'User', 'modify', '', '0', '1', '0', '" . time() . "')");
                Model::exec("INSERT INTO `auth` VALUES ('11', '4', '删除', 'User', 'delete', '', '0', '1', '0', '" . time() . "')");
                Model::exec("INSERT INTO `auth` VALUES ('12', '3', '角色管理', 'Role', 'index', '', '0', '0', '0', '" . time() . "')");
                Model::exec("INSERT INTO `auth` VALUES ('13', '12', '查看', 'Role', 'index', '', '0', '1', '0', '" . time() . "')");
                Model::exec("INSERT INTO `auth` VALUES ('14', '12', '创建', 'Role', 'create', '', '0', '1', '0', '" . time() . "')");
                Model::exec("INSERT INTO `auth` VALUES ('15', '12', '保存', 'Role', 'save', '', '0', '1', '0', '" . time() . "')");
                Model::exec("INSERT INTO `auth` VALUES ('16', '12', '编辑', 'Role', 'edit', '', '0', '1', '0', '" . time() . "')");
                Model::exec("INSERT INTO `auth` VALUES ('17', '12', '更新', 'Role', 'update', '', '0', '1', '0', '" . time() . "')");
                Model::exec("INSERT INTO `auth` VALUES ('18', '12', '修改属性', 'Role', 'modify', '', '0', '1', '0', '" . time() . "')");
                Model::exec("INSERT INTO `auth` VALUES ('19', '12', '删除', 'Role', 'delete', '', '0', '1', '0', '" . time() . "')");
                Model::exec("INSERT INTO `auth` VALUES ('20', '3', '权限菜单', 'Auth', 'index', '', '0', '0', '0', '" . time() . "')");
                Model::exec("INSERT INTO `auth` VALUES ('21', '20', '查看', 'Auth', 'index', '', '0', '1', '0', '" . time() . "')");
                Model::exec("INSERT INTO `auth` VALUES ('22', '20', '创建', 'Auth', 'create', '', '0', '1', '0', '" . time() . "')");
                Model::exec("INSERT INTO `auth` VALUES ('23', '20', '保存', 'Auth', 'save', '', '0', '1', '0', '" . time() . "')");
                Model::exec("INSERT INTO `auth` VALUES ('24', '20', '编辑', 'Auth', 'edit', '', '0', '1', '0', '" . time() . "')");
                Model::exec("INSERT INTO `auth` VALUES ('25', '20', '更新', 'Auth', 'update', '', '0', '1', '0', '" . time() . "')");
                Model::exec("INSERT INTO `auth` VALUES ('26', '20', '修改属性', 'Auth', 'modify', '', '0', '1', '0', '" . time() . "')");
                Model::exec("INSERT INTO `auth` VALUES ('27', '20', '删除', 'Auth', 'delete', '', '0', '1', '0', '" . time() . "')");
                Model::exec("INSERT INTO `auth` VALUES ('28', '0', '分类管理', '', '', 'layui-icon layui-icon-app', '0', '0', '0', '" . time() . "')");
                Model::exec("INSERT INTO `auth` VALUES ('29', '28', '分类列表', 'Category', 'index', '', '0', '0', '0', '" . time() . "')");
                Model::exec("INSERT INTO `auth` VALUES ('30', '28', '创建', 'Category', 'create', '', '0', '0', '0', '" . time() . "')");
                Model::exec("INSERT INTO `auth` VALUES ('31', '28', '保存', 'Category', 'save', '', '0', '1', '0', '" . time() . "')");
                Model::exec("INSERT INTO `auth` VALUES ('32', '28', '编辑', 'Category', 'edit', '', '0', '1', '0', '" . time() . "')");
                Model::exec("INSERT INTO `auth` VALUES ('33', '28', '更新', 'Category', 'update', '', '0', '1', '0', '" . time() . "')");
                Model::exec("INSERT INTO `auth` VALUES ('34', '28', '修改属性', 'Category', 'modify', '', '0', '1', '0', '" . time() . "')");
                Model::exec("INSERT INTO `auth` VALUES ('35', '28', '删除', 'Category', 'delete', '', '0', '1', '0', '" . time() . "')");
                Model::exec("INSERT INTO `auth` VALUES ('36', '0', '标签管理', '', '', 'layui-icon layui-icon-note', '0', '0', '0', '" . time() . "')");
                Model::exec("INSERT INTO `auth` VALUES ('37', '36', '标签列表', 'Tag', 'index', '', '0', '0', '0', '" . time() . "')");
                Model::exec("INSERT INTO `auth` VALUES ('38', '36', '创建', 'Tag', 'create', '', '0', '0', '0', '" . time() . "')");
                Model::exec("INSERT INTO `auth` VALUES ('39', '36', '保存', 'Tag', 'save', '', '0', '1', '0', '" . time() . "')");
                Model::exec("INSERT INTO `auth` VALUES ('40', '36', '编辑', 'Tag', 'edit', '', '0', '1', '0', '" . time() . "')");
                Model::exec("INSERT INTO `auth` VALUES ('41', '36', '更新', 'Tag', 'update', '', '0', '1', '0', '" . time() . "')");
                Model::exec("INSERT INTO `auth` VALUES ('42', '36', '修改属性', 'Tag', 'modify', '', '0', '1', '0', '" . time() . "')");
                Model::exec("INSERT INTO `auth` VALUES ('43', '36', '删除', 'Tag', 'delete', '', '0', '1', '0', '" . time() . "')");
                Model::exec("INSERT INTO `auth` VALUES ('44', '0', '文章管理', '', '', 'layui-icon layui-icon-template-1', '0', '0', '0', '" . time() . "')");
                Model::exec("INSERT INTO `auth` VALUES ('45', '44', '文章列表', 'Article', 'index', '', '0', '0', '0', '" . time() . "')");
                Model::exec("INSERT INTO `auth` VALUES ('46', '44', '创建', 'Article', 'create', '', '0', '0', '0', '" . time() . "')");
                Model::exec("INSERT INTO `auth` VALUES ('47', '44', '保存', 'Article', 'save', '', '0', '1', '0', '" . time() . "')");
                Model::exec("INSERT INTO `auth` VALUES ('48', '44', '编辑', 'Article', 'edit', '', '0', '1', '0', '" . time() . "')");
                Model::exec("INSERT INTO `auth` VALUES ('49', '44', '更新', 'Article', 'update', '', '0', '1', '0', '" . time() . "')");
                Model::exec("INSERT INTO `auth` VALUES ('50', '44', '修改属性', 'Article', 'modify', '', '0', '1', '0', '" . time() . "')");
                Model::exec("INSERT INTO `auth` VALUES ('51', '44', '软删除', 'Article', 'delete', '', '0', '1', '0', '" . time() . "')");
                Model::exec("INSERT INTO `auth` VALUES ('52', '44', '回收站', 'Article', 'recycle', '', '0', '0', '0', '" . time() . "')");
                Model::exec("INSERT INTO `auth` VALUES ('53', '44', '还原', 'Article', 'restore', '', '0', '1', '0', '" . time() . "')");
                Model::exec("INSERT INTO `auth` VALUES ('54', '44', '彻底删除', 'Article', 'destroy', '', '0', '1', '0', '" . time() . "')");
                Model::exec("INSERT INTO `auth` VALUES ('55', '0', '页面管理', '', '', 'layui-icon layui-icon-file', '0', '0', '0', '" . time() . "')");
                Model::exec("INSERT INTO `auth` VALUES ('56', '55', '页面列表', 'Page', 'index', '', '0', '0', '0', '" . time() . "')");
                Model::exec("INSERT INTO `auth` VALUES ('57', '55', '创建', 'Page', 'create', '', '0', '0', '0', '" . time() . "')");
                Model::exec("INSERT INTO `auth` VALUES ('58', '55', '保存', 'Page', 'save', '', '0', '1', '0', '" . time() . "')");
                Model::exec("INSERT INTO `auth` VALUES ('59', '55', '编辑', 'Page', 'edit', '', '0', '1', '0', '" . time() . "')");
                Model::exec("INSERT INTO `auth` VALUES ('60', '55', '更新', 'Page', 'update', '', '0', '1', '0', '" . time() . "')");
                Model::exec("INSERT INTO `auth` VALUES ('61', '55', '修改属性', 'Page', 'modify', '', '0', '1', '0', '" . time() . "')");
                Model::exec("INSERT INTO `auth` VALUES ('62', '55', '删除', 'Page', 'delete', '', '0', '1', '0', '" . time() . "')");
                Model::exec("INSERT INTO `auth` VALUES ('63', '0', '媒体管理', '', '', 'layui-icon layui-icon-camera', '0', '0', '0', '" . time() . "')");
                Model::exec("INSERT INTO `auth` VALUES ('64', '63', '媒体列表', 'Media', 'index', '', '0', '0', '0', '" . time() . "')");
                Model::exec("INSERT INTO `auth` VALUES ('65', '63', '上传文件', 'Media', 'file', '', '0', '0', '0', '" . time() . "')");
                Model::exec("INSERT INTO `auth` VALUES ('66', '63', '上传接口', 'Media', 'upload', '', '0', '1', '0', '" . time() . "')");
                Model::exec("INSERT INTO `auth` VALUES ('67', '63', '重命名', 'Media', 'rename', '', '0', '1', '0', '" . time() . "')");
                Model::exec("INSERT INTO `auth` VALUES ('68', '63', '删除', 'Media', 'delete', '', '0', '1', '0', '" . time() . "')");
                Model::exec("INSERT INTO `auth` VALUES ('69', '63', '计算孤立文件', 'Media', 'calc', '', '0', '1', '0', '" . time() . "')");
                Model::exec("INSERT INTO `auth` VALUES ('70', '0', '链接管理', '', '', 'layui-icon layui-icon-link', '0', '0', '0', '" . time() . "')");
                Model::exec("INSERT INTO `auth` VALUES ('71', '70', '链接列表', 'Link', 'index', '', '0', '0', '0', '" . time() . "')");
                Model::exec("INSERT INTO `auth` VALUES ('72', '70', '创建', 'Link', 'create', '', '0', '0', '0', '" . time() . "')");
                Model::exec("INSERT INTO `auth` VALUES ('73', '70', '保存', 'Link', 'save', '', '0', '1', '0', '" . time() . "')");
                Model::exec("INSERT INTO `auth` VALUES ('74', '70', '编辑', 'Link', 'edit', '', '0', '1', '0', '" . time() . "')");
                Model::exec("INSERT INTO `auth` VALUES ('75', '70', '更新', 'Link', 'update', '', '0', '1', '0', '" . time() . "')");
                Model::exec("INSERT INTO `auth` VALUES ('76', '70', '修改属性', 'Link', 'modify', '', '0', '1', '0', '" . time() . "')");
                Model::exec("INSERT INTO `auth` VALUES ('77', '70', '删除', 'Link', 'delete', '', '0', '1', '0', '" . time() . "')");
                Model::exec("INSERT INTO `auth` VALUES ('78', '0', '评论管理', '', '', 'layui-icon layui-icon-dialogue', '0', '0', '0', '" . time() . "')");
                Model::exec("INSERT INTO `auth` VALUES ('79', '78', '评论列表', 'Comment', 'index', '', '0', '0', '0', '" . time() . "')");
                Model::exec("INSERT INTO `auth` VALUES ('80', '78', '回复', 'Comment', 'reply', '', '0', '1', '0', '" . time() . "')");
                Model::exec("INSERT INTO `auth` VALUES ('81', '78', '保存', 'Comment', 'save_reply', '', '0', '1', '0', '" . time() . "')");
                Model::exec("INSERT INTO `auth` VALUES ('82', '78', '编辑', 'Comment', 'edit', '', '0', '1', '0', '" . time() . "')");
                Model::exec("INSERT INTO `auth` VALUES ('83', '78', '更新', 'Comment', 'update', '', '0', '1', '0', '" . time() . "')");
                Model::exec("INSERT INTO `auth` VALUES ('84', '78', '待审', 'Comment', 'pending', '', '0', '1', '0', '" . time() . "')");
                Model::exec("INSERT INTO `auth` VALUES ('85', '78', '审核通过', 'Comment', 'passed', '', '0', '1', '0', '" . time() . "')");
                Model::exec("INSERT INTO `auth` VALUES ('86', '78', '垃圾评论', 'Comment', 'spam', '', '0', '1', '0', '" . time() . "')");
                Model::exec("INSERT INTO `auth` VALUES ('87', '78', '机器评论', 'Comment', 'robot', '', '0', '0', '0', '" . time() . "')");
                Model::exec("INSERT INTO `auth` VALUES ('88', '78', '删除', 'Comment', 'delete', '', '0', '1', '0', '" . time() . "')");
                Model::exec("INSERT INTO `auth` VALUES ('89', '0', '系统管理', '', '', 'layui-icon layui-icon-set', '0', '0', '0', '" . time() . "')");
                Model::exec("INSERT INTO `auth` VALUES ('90', '89', '系统设置', 'Setting', 'index', '', '0', '0', '0', '" . time() . "')");
                Model::exec("INSERT INTO `auth` VALUES ('91', '89', '保存设置', 'Setting', 'save_index', '', '0', '1', '0', '" . time() . "')");
                Model::exec("INSERT INTO `auth` VALUES ('92', '89', '恢复出厂', 'Setting', 'recovery', '', '0', '1', '0', '" . time() . "')");
                Model::exec("INSERT INTO `auth` VALUES ('93', '89', '操作日志', 'Log', 'index', '', '0', '0', '0', '" . time() . "')");
                Model::exec("INSERT INTO `auth` VALUES ('94', '89', '删除日志', 'Log', 'delete', '', '0', '1', '0', '" . time() . "')");
                Model::exec("INSERT INTO `auth` VALUES ('95', '0', '国际化', '', '', 'layui-icon layui-icon-util', '0', '0', '0', '" . time() . "')");
                Model::exec("INSERT INTO `auth` VALUES ('96', '95', '语言包', 'I18n', 'index', '', '0', '0', '0', '" . time() . "')");
                Model::exec("INSERT INTO `auth` VALUES ('97', '95', '提取语言包', 'I18n', 'fetch', '', '0', '1', '0', '" . time() . "')");
                Model::exec("INSERT INTO `auth` VALUES ('98', '95', '生成语言包', 'I18n', 'generate', '', '0', '1', '0', '" . time() . "')");
                Model::exec("INSERT INTO `auth` VALUES ('99', '95', '分类', 'I18n', 'category', '', '0', '0', '0', '" . time() . "')");
                Model::exec("INSERT INTO `auth` VALUES ('100', '95', '分类国际化', 'I18n', 'category_i18n', '', '0', '1', '0', '" . time() . "')");
                Model::exec("INSERT INTO `auth` VALUES ('101', '95', '保存分类国际化', 'I18n', 'save_category', '', '0', '1', '0', '" . time() . "')");
                Model::exec("INSERT INTO `auth` VALUES ('102', '95', '分类国际化帮助', 'I18n', 'category_help', '', '0', '1', '0', '" . time() . "')");
                Model::exec("INSERT INTO `auth` VALUES ('103', '95', '标签', 'I18n', 'tag', '', '0', '0', '0', '" . time() . "')");
                Model::exec("INSERT INTO `auth` VALUES ('104', '95', '标签国际化', 'I18n', 'tag_i18n', '', '0', '1', '0', '" . time() . "')");
                Model::exec("INSERT INTO `auth` VALUES ('105', '95', '保存标签国际化', 'I18n', 'save_tag', '', '0', '1', '0', '" . time() . "')");
                Model::exec("INSERT INTO `auth` VALUES ('106', '95', '标签国际化帮助', 'I18n', 'tag_help', '', '0', '1', '0', '" . time() . "')");
                Model::exec("INSERT INTO `auth` VALUES ('107', '95', '文章', 'I18n', 'article', '', '0', '0', '0', '" . time() . "')");
                Model::exec("INSERT INTO `auth` VALUES ('108', '95', '文章国际化', 'I18n', 'article_i18n', '', '0', '1', '0', '" . time() . "')");
                Model::exec("INSERT INTO `auth` VALUES ('109', '95', '保存文章国际化', 'I18n', 'save_article', '', '0', '1', '0', '" . time() . "')");
                Model::exec("INSERT INTO `auth` VALUES ('110', '95', '文章国际化帮助', 'I18n', 'article_help', '', '0', '1', '0', '" . time() . "')");
                Model::exec("INSERT INTO `auth` VALUES ('111', '95', '页面', 'I18n', 'page', '', '0', '0', '0', '" . time() . "')");
                Model::exec("INSERT INTO `auth` VALUES ('112', '95', '页面国际化', 'I18n', 'page_i18n', '', '0', '1', '0', '" . time() . "')");
                Model::exec("INSERT INTO `auth` VALUES ('113', '95', '保存页面国际化', 'I18n', 'save_page', '', '0', '1', '0', '" . time() . "')");
                Model::exec("INSERT INTO `auth` VALUES ('114', '95', '页面国际化帮助', 'I18n', 'page_help', '', '0', '1', '0', '" . time() . "')");
                // 分类表
                Model::exec("DELETE FROM `category`");
                Model::exec("ALTER TABLE `category` AUTO_INCREMENT = 1");
                Model::exec("INSERT INTO `category` VALUES ('1', '0', '分类目录', 'categories', '0', '0', '', '', '0', '', '', '0', '" . time() . "', 'zh-CN')");
                Model::exec("INSERT INTO `category` VALUES ('2', '1', '未分类', 'uncategorized', '1', '0', '', '', '0', '', '', '0', '" . time() . "', 'zh-CN')");
                // 标签表
                Model::exec("DELETE FROM `tag`");
                Model::exec("ALTER TABLE `tag` AUTO_INCREMENT = 1");
                // 文章表
                Model::exec("DELETE FROM `article`");
                Model::exec("ALTER TABLE `article` AUTO_INCREMENT = 1");
                Model::exec("INSERT INTO `article` VALUES ('1', '2', '', '世界，你好！', 'hello-world', 'admin', '<p>\n	欢迎使用博客系统多国语言版。\n</p>\n<p>\n	这是您的第一篇文章。编辑或删除它，然后开始写作吧！\n</p>', '', '', '', '', '0', '0', '0', '0', '" . time() . "', '" . time() . "', 'zh-CN')");
                // 页面表
                Model::exec("DELETE FROM `page`");
                Model::exec("ALTER TABLE `page` AUTO_INCREMENT = 1");
                Model::exec("INSERT INTO `page` VALUES ('1', '0', '页面', 'page', '0', '', '', '', '0', '', '', '0', '0', '" . time() . "', 'zh-CN')");
                Model::exec("INSERT INTO `page` VALUES ('2', '1', '我的点赞', 'page-likes', '0', '', '', '', '0', '', '', '0', '0', '" . time() . "', 'zh-CN')");
                Model::exec("INSERT INTO `page` VALUES ('3', '1', '我的收藏', 'page-favorites', '0', '', '', '', '0', '', '', '0', '0', '" . time() . "', 'zh-CN')");
                // 媒体表
                Model::exec("DELETE FROM `media`");
                Model::exec("ALTER TABLE `media` AUTO_INCREMENT = 1");
                // 链接表
                Model::exec("DELETE FROM `link`");
                Model::exec("ALTER TABLE `link` AUTO_INCREMENT = 1");
                // 评论表
                Model::exec("DELETE FROM `comment`");
                Model::exec("ALTER TABLE `comment` AUTO_INCREMENT = 1");
                Model::exec("INSERT INTO `comment` VALUES ('1', '0', '1', '您好，这是一条评论。若需要审核、编辑或删除评论，请访问控制台的评论管理。', 'admin', '0', '0', '" . time() . "', 'zh-CN')");
                // 设置表
                Model::exec("DELETE FROM `setting`");
                Model::exec("ALTER TABLE `setting` AUTO_INCREMENT = 1");
                // 日志表
                Model::exec("DELETE FROM `log`");
                Model::exec("ALTER TABLE `log` AUTO_INCREMENT = 1");
                // 删除附件
                Util::delDir(IA_ROOT . '/static/upload/image/', true);
                Util::delDir(IA_ROOT . '/static/upload/flash/', true);
                Util::delDir(IA_ROOT . '/static/upload/media/', true);
                Util::delDir(IA_ROOT . '/static/upload/file/', true);
                Util::delDir(IA_ROOT . '/static/upload/articles/', true);
                // 删除模板静态缓存
                Util::delDir(IA_ROOT . '/mvc/tpl/', true);
                // 删除单点登录字符串
                Util::delDir(IA_ROOT . '/mvc/cache_str/', true);
                // 删除多国语言替换串
                if (is_file(IA_ROOT . '/mvc/locale/Lang.php')) {
                    $langArr = require IA_ROOT . '/mvc/locale/Lang.php';
                    foreach ($langArr as $v) {
                        if (isset($v['slug']) && $v['slug']) {
                            $file = IA_ROOT . '/mvc/locale/' . $v['slug'] . '.php';
                            is_file($file) && unlink($file);
                        }
                    }
                }
                Model::commit();
                $this->log(Util::tran('重置系统'), Util::tran('恢复出厂成功！'), 0);
                exit(json_encode(['code' => 0, 'msg' => Util::tran('恢复出厂成功！')]));
            } catch (\Exception $e) {
                Model::rollBack();
                $this->log(Util::tran('重置系统'), Util::tran('恢复出厂失败！'), 1);
                exit(json_encode(['code' => 1, 'msg' => Util::tran('恢复出厂失败！')]));
            }
        }
    }
}
