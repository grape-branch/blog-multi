<?php

namespace controller\admin;

defined('IA_ROOT') || exit();

use facade\View;
use facade\Model;
use facade\Util;

// 控制器
class Index extends Base
{
    // 后台首页
    public function index()
    {
        $menus = array_filter($this->auth, function ($item) {
            return in_array($item['id'], $this->user['role']['auth_ids']) && $item['status'] == 0 && $item['is_menu'] == 0;
        });
        // 无限层级递归
        $menus = Util::levels($menus);
        // 无限级菜单
        $menus = $this->menus($menus);
        $langs = array_map(function ($item) {
            $item['title'] = Util::tran($item['title']);
            return $item;
        }, Util::langs());
        View::assign([
            'menus' => $menus,
            'langs' => $langs,
            'username' => $this->user['username'],
            'avatar' => $this->user['avatar'] ? $this->user['avatar'] : 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD//gA7Q1JFQVRPUjogZ2QtanBlZyB2MS4wICh1c2luZyBJSkcgSlBFRyB2NjIpLCBxdWFsaXR5ID0gOTAK/9sAQwADAgIDAgIDAwMDBAMDBAUIBQUEBAUKBwcGCAwKDAwLCgsLDQ4SEA0OEQ4LCxAWEBETFBUVFQwPFxgWFBgSFBUU/9sAQwEDBAQFBAUJBQUJFA0LDRQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQU/8AAEQgAIAAgAwEiAAIRAQMRAf/EAB8AAAEFAQEBAQEBAAAAAAAAAAABAgMEBQYHCAkKC//EALUQAAIBAwMCBAMFBQQEAAABfQECAwAEEQUSITFBBhNRYQcicRQygZGhCCNCscEVUtHwJDNicoIJChYXGBkaJSYnKCkqNDU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6g4SFhoeIiYqSk5SVlpeYmZqio6Slpqeoqaqys7S1tre4ubrCw8TFxsfIycrS09TV1tfY2drh4uPk5ebn6Onq8fLz9PX29/j5+v/EAB8BAAMBAQEBAQEBAQEAAAAAAAABAgMEBQYHCAkKC//EALURAAIBAgQEAwQHBQQEAAECdwABAgMRBAUhMQYSQVEHYXETIjKBCBRCkaGxwQkjM1LwFWJy0QoWJDThJfEXGBkaJicoKSo1Njc4OTpDREVGR0hJSlNUVVZXWFlaY2RlZmdoaWpzdHV2d3h5eoKDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uLj5OXm5+jp6vLz9PX29/j5+v/aAAwDAQACEQMRAD8A+uK0tH8O6lr7sthaPcbfvMMBR9ScCs2voDwXaQWfhbTFgACvAsjEd2YZY/maAPEdY8OaloDKL+0eAN91shlP0IyKza+gfGVpBeeF9TS4AKLA8gJ7MoyD+Yr5+oAfFE88qRxoXkchVVRkknoBXufgPQ9R0HRlt7+4WQH5kgAz5Oeo3d/881wnwj0lL3XJ7yRQwtIxsB7M2QD+QavYKAOa8d6JqOvaM1vYXCx/xPCRgy45A3dv6+teGSxPBK8ciFJEJVlYYII6g19MV4/8XNJSy1yC8jUKLuM7wO7LgE/kVoA//9k=',
            'tran' => [
                'backend' => Util::tran('后台'),
                'front' => Util::tran('前台'),
                'refresh' => Util::tran('刷新'),
                'language' => Util::tran('语言'),
                'modify_profile' => Util::tran('修改资料'),
                'clear_cache' => Util::tran('清除缓存'),
                'logout' => Util::tran('登出'),
                'welcome' => Util::tran('欢迎使用后台管理系统'),
                'version' => Util::tran('版本'),
                'current_version' => Util::tran('当前版本'),
                'about' => Util::tran('关于'),
                'info' => Util::tran('信息'),
                'mvc_info' => Util::tran('迷你mvc小框架，一套用于开发各类管理系统的通用型后台管理系统模板，基于开源免费的 layui 的制作，内置各类实际业务场景下相对丰富的示例。'),
                'author_info' => Util::tran('作者：葡萄枝子'),
                'wechat' => Util::tran('微信'),
                'reset' => Util::tran('重置'),
                'reset_system_warning' => Util::tran('重置系统，将删除用户所有数据和附件，恢复到出厂设置。'),
                'reset_system' => Util::tran('重置系统'),
                'confirm_clear_cache' => Util::tran('确定清除缓存吗？'),
                'ok' => Util::tran('确定'),
                'cancel' => Util::tran('取消'),
                'confirm_logout' => Util::tran('确定要登出站点吗？'),
                'confirm_recovery' => Util::tran('此操作恢复到出厂设置？'),
            ],
            'version' => 'v1.0.0'
        ]);
        View::display('admin/index.html');
    }

    // 欢迎页面
    public function welcome()
    {
        $userCount = Model::fetchColumn("SELECT COUNT(*) FROM `user` ORDER BY `id`");
        $articleCount = Model::fetchColumn("SELECT COUNT(*) FROM `article` ORDER BY `id`");
        $commentCount = Model::fetchColumn("SELECT COUNT(*) FROM `comment` ORDER BY `id`");
        $check = Model::fetch("SELECT VERSION()");
        $data = Model::fetchAll("SELECT * FROM `comment` WHERE `pid` = 0 ORDER BY `sort` DESC, `id` DESC LIMIT 5");
        $authors = [];
        $aids = [];
        foreach ($data as $item) {
            $authors[] = $item['author'];
            $aids[] = $item['article_id'];
        }
        $authors = array_unique($authors);
        $authors = implode("','", $authors);
        $users = Model::fetchAll("SELECT * FROM `user` WHERE `username` IN ('$authors')");
        $aids = array_unique($aids);
        $aids = implode(',', $aids);
        if ($aids) {
            $articles = Model::fetchAll("SELECT * FROM `article` WHERE `status` = 0 AND `id` IN ($aids) ORDER BY `id`");
        } else {
            $articles = [];
        }
        $data = array_map(function ($item) use ($users, $articles) {
            // 评论图像
            $item['avatar'] = '';
            if ($user = array_filter($users, function ($elem) use ($item) {
                return $elem['username'] == $item['author'];
            })) {
                $user = array_values($user);
                $item['nickname'] = $user[0]['nickname'] ?: $user[0]['username'];
                $item['avatar'] = $user[0]['avatar'] ? $this->base_url . '/' . ltrim($user[0]['avatar'], '/') : '';
            }
            // 被评论的文章
            $item['article_title'] = $item['author'];
            $item['article_link'] = '#';
            if ($article = array_filter($articles, function ($elem) use ($item) {
                return $elem['id'] == $item['article_id'];
            })) {
                $article = array_values($article);
                $item['article_title'] = $article[0]['title'];
                $item['article_link'] = $this->base_url . "/{$this->replace_path}" . ($article[0]['slug'] ? $article[0]['slug'] : $article[0]['id']) . '.html';
            }
            return $item;
        }, $data);
        $commentList = $data;
        // 统计图表
        $week = [Util::tran("星期日"), Util::tran("星期一"), Util::tran("星期二"), Util::tran("星期三"), Util::tran("星期四"), Util::tran("星期五"), Util::tran("星期六")];
        $data = Model::fetchAll("SELECT DATE_FORMAT(FROM_UNIXTIME(`reg_time`), '%Y-%m-%d') AS date, COUNT(*) AS count FROM `user` WHERE `reg_time` >= UNIX_TIMESTAMP(DATE_SUB(CURDATE(), INTERVAL 7 DAY)) GROUP BY date ORDER BY `reg_time` ASC");
        $users = array_map(function ($item) {
            $item['w'] = date("w", strtotime($item['date']));
            return $item;
        }, $data);
        $data = Model::fetchAll("SELECT DATE_FORMAT(FROM_UNIXTIME(`create_at`), '%Y-%m-%d') AS date, COUNT(*) AS count FROM `article` WHERE `create_at` >= UNIX_TIMESTAMP(DATE_SUB(CURDATE(), INTERVAL 7 DAY)) GROUP BY date ORDER BY `create_at` ASC");
        $articles = array_map(function ($item) {
            $item['w'] = date("w", strtotime($item['date']));
            return $item;
        }, $data);
        $data = Model::fetchAll("SELECT DATE_FORMAT(FROM_UNIXTIME(`time`), '%Y-%m-%d') AS date, COUNT(*) AS count FROM `comment` WHERE `time` >= UNIX_TIMESTAMP(DATE_SUB(CURDATE(), INTERVAL 7 DAY)) GROUP BY date ORDER BY `time` ASC");
        $comments = array_map(function ($item) {
            $item['w'] = date("w", strtotime($item['date']));
            return $item;
        }, $data);
        $datas = array_reduce($users, function ($prev, $next) use ($week) {
            $prev[$next['w']] = ['user_count' => $next['count'], 'user_date' => $next['date'], 'user_week' => $week[$next['w']]];
            return $prev;
        }, []);
        $datas = array_reduce($articles, function ($prev, $next) use ($week) {
            $prev[$next['w']] = array_merge(isset($prev[$next['w']]) ? $prev[$next['w']] : [], ['article_count' => $next['count'], 'article_date' => $next['date'], 'article_week' => $week[$next['w']]]);
            return $prev;
        }, $datas);
        $datas = array_reduce($comments, function ($prev, $next) use ($week) {
            $prev[$next['w']] = array_merge(isset($prev[$next['w']]) ? $prev[$next['w']] : [], ['comment_count' => $next['count'], 'comment_date' => $next['date'], 'comment_week' => $week[$next['w']]]);
            return $prev;
        }, $datas);
        $charts = ['weeks' => [], 'users' => [], 'articles' => [], 'comments' => []];
        foreach ($datas as $key => $value) {
            $charts['weeks'][] = $week[$key];
            $charts['users'][] = isset($value['user_count']) ? $value['user_count'] : 0;
            $charts['articles'][] = isset($value['article_count']) ? $value['article_count'] : 0;
            $charts['comments'][] = isset($value['comment_count']) ? $value['comment_count'] : 0;
        }
        View::assign([
            'userCount' => $userCount,
            'articleCount' => $articleCount,
            'commentCount' => $commentCount,
            'databaseInfo' => array_shift($check),
            'serverCurrentTime' => date('Y-m-d H:i:s'),
            'commentList' => $commentList,
            'charts' => json_encode($charts),
            'tran' => [
                'userNum' => Util::tran('会员数'),
                'articleNum' => Util::tran('文章数'),
                'commentNum' => Util::tran('评论数'),
                'serverEnv' => Util::tran('服务器环境'),
                'version' => Util::tran('版本'),
                'db_info' => Util::tran('数据库信息'),
                'server_current_time' => Util::tran('服务器时间'),
                'new_comment' => Util::tran('最新评论'),
                'no_comments' => Util::tran('还没有评论'),
                'link_us' => Util::tran('联系我们'),
                'data_tongji' => Util::tran('数据统计'),
                'anonymous' => Util::tran('匿名'),
                'vip' => Util::tran('会员'),
                'article' => Util::tran('文章'),
                'comment' => Util::tran('评论'),
                'pending' => Util::tran('待审'),
                'no_datas' => Util::tran('还没有数据'),
                'welcome' => Util::tran('欢迎使用后台管理系统')
            ]
        ]);
        View::display('admin/index/welcome.html');
    }

    // 无限级菜单
    private function menus($dataArr)
    {
        $res = '';
        foreach ($dataArr as $k => $data) {
            $res .= $this->childMenus($data, $k);
        }
        return $res;
    }

    // 递归子数据
    private function childMenus($data, $k = 0, $indent = 0)
    {
        $res = '';
        if ($data['children']) {
            $res .= sprintf('<li class="layui-nav-item%s">', $k === 0 ? ' layui-nav-itemed' : '');
            $res .= sprintf('<a href="javascript:;" style="text-indent:%dem;" title="%s">%s%s</a>', max($indent, 0), strip_tags(Util::tran($data['title'])), $data['icon'] ? '<i class="' . $data['icon'] . '"></i> ' : '', Util::tran($data['title']));
            $res .= '<dl class="layui-nav-child">';
            foreach ($data['children'] as $k => $data) {
                if ($data['children']) {
                    $res .= $this->childMenus($data, $k, $indent + 1);
                } else {
                    $res .= sprintf('<dd><a href="javascript:;" style="text-indent:%dem;" data-lang="%s" data-module="%s" data-controller="%s" data-action="%s" onclick="fireMenu(this)" title="%s">%s%s</a></dd>', $indent + 1, Util::lang(), $this->module, $data['controller'], $data['action'], strip_tags(Util::tran($data['title'])), $data['icon'] ? '<i class="' . $data['icon'] . '"></i> ' : '', Util::tran($data['title']));
                }
            }
            $res .= '</dl>';
            $res .= "</li>\n";
        } else {
            $res .= sprintf('<li class="layui-nav-item%s"><a href="javascript:;" data-lang="%s" data-module="%s" data-controller="%s" data-action="%s" onclick="fireMenu(this)" title="%s">%s%s</a></li>', $k === 0 ? ' layui-this' : '', Util::lang(), $this->module, $data['controller'], $data['action'], strip_tags(Util::tran($data['title'])), $data['icon'] ? '<i class="' . $data['icon'] . '"></i> ' : '', Util::tran($data['title']));
        }
        return $res;
    }
}
