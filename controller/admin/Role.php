<?php

namespace controller\admin;

defined('IA_ROOT') || exit();

use facade\Model;
use facade\View;
use facade\Util;

// 角色管理
class Role extends Base
{
    // 列表
    public function index()
    {
        if (Util::isAjax()) {
            // 无限树
            $data = $this->role;
            $count = count($data);
            if ($count) {
                $res['code'] = 0;
            } else {
                $res['code'] = 1;
                $res['msg'] = Util::tran('暂无记录');
            }
            $res['count'] = $count;
            $res['data'] = array_map(function ($item) {
                $item['time'] = date('Y-m-d H:i', $item['time']);
                return $item;
            }, $data);
            exit(json_encode($res));
        }
        View::assign('tran', [
            'delall' => Util::tran('批量删除'),
            'add' => Util::tran('添加'),
            'refresh' => Util::tran('刷新'),
            'role_list' => Util::tran('角色列表'),
            'role_name' => Util::tran('角色名称'),
            'sort' => Util::tran('排序'),
            'status' => Util::tran('状态'),
            'time' => Util::tran('时间'),
            'option' => Util::tran('操作'),
            'edit' => Util::tran('编辑'),
            'delete' => Util::tran('删除'),
        ]);
        View::display('admin/role/index.html');
    }

    // 创建
    public function create()
    {
        // 无限层级
        $auth_menu = Util::levels(array_reduce($this->auth, function ($prev, $next) {
            $prev[] = ['id' => $next['id'], 'pid' => $next['pid'], 'title' => $next['title']];
            return $prev;
        }, []));
        $auth_menu = json_encode($auth_menu);
        View::assign('auth_menu', $auth_menu);
        View::assign('tran', [
            'role_name' => Util::tran('角色名称'),
            'input_role_name' => Util::tran('请输入角色名称'),
            'auth_assign' => Util::tran('权限分配'),
            'sort' => Util::tran('排序'),
            'input_sort' => Util::tran('请输入排序'),
            'status' => Util::tran('状态'),
            'save' => Util::tran('保存'),
            'reset' => Util::tran('重置'),
        ]);
        View::display('admin/role/create.html');
    }

    // 保存
    public function save()
    {
        $param = Util::param(['name', 'auth_ids', 'sort', 'status']);

        // 验证角色重复
        $check = Model::fetchColumn("SELECT `name` FROM `role` WHERE `name`=:name ORDER BY `id`", [':name' => $param['name']]);
        $check && Util::errMsg(Util::tran('角色已存在'));
        $param['auth_ids'] = isset($param['auth_ids']) ? array_map('intval', array_values((array)$param['auth_ids'])) : '';
        $auths = Model::fetchAll("SELECT `id`, `title` FROM `auth` ORDER BY `sort` DESC, `id` ASC");
        $auth_ids = array_map(function ($item) {
            return $item['id'];
        }, $auths);
        if (!empty($param['auth_ids'])) {
            $param['auth_ids'] != array_intersect($param['auth_ids'], $auth_ids) && Util::errMsg(Util::tran('权限数据错误'));
            $param['auth_ids'] = json_encode($param['auth_ids']);
        }

        // 写入数据库记录日志
        $param['time'] = time();
        $lastInsertId = Model::insert('role', $param);
        if ($lastInsertId) {
            $this->log(Util::tran('创建角色'), Util::tran('创建角色') . " {$param['name']} " . Util::tran('成功'), 0);
            Util::errMsg(['code' => 0, 'msg' => Util::tran('创建角色成功')]);
        } else {
            $this->log(Util::tran('创建角色'), Util::tran('创建角色') . " {$param['name']} " . Util::tran('失败'), 1);
            Util::errMsg(['code' => 1, 'msg' => Util::tran('创建角色失败')]);
        }
    }

    // 编辑
    public function edit($id = 0)
    {
        $role = array_filter($this->role, function ($item) use ($id) {
            return $id == $item['id'];
        });
        !$role && Util::errMsg('ID ' . Util::tran('错误'));
        $role = array_shift($role);
        View::assign('role', $role);
        // 无限层级
        $auth_menu = Util::levels(array_reduce($this->auth, function ($prev, $next) {
            $prev[] = ['id' => $next['id'], 'pid' => $next['pid'], 'title' => $next['title']];
            return $prev;
        }, []));
        $auth_menu = json_encode($auth_menu);
        View::assign('auth_menu', $auth_menu);
        View::assign('tran', [
            'role_name' => Util::tran('角色名称'),
            'input_role_name' => Util::tran('请输入角色名称'),
            'auth_assign' => Util::tran('权限分配'),
            'sort' => Util::tran('排序'),
            'input_sort' => Util::tran('请输入排序'),
            'status' => Util::tran('状态'),
            'save' => Util::tran('保存'),
            'reset' => Util::tran('重置'),
        ]);
        View::display('admin/role/edit.html');
    }

    // 更新
    public function update()
    {
        $param = Util::param(['id', 'name', 'auth_ids', 'sort', 'status']);

        // 验证角色重复
        $check = Model::fetchColumn("SELECT `name` FROM `role` WHERE `name`=:name ORDER BY `id`", [':name' => $param['name']]);
        $check && $check !== $param['name'] && Util::errMsg(Util::tran('角色已存在'));
        $param['auth_ids'] = isset($param['auth_ids']) ? array_map('intval', array_values((array)$param['auth_ids'])) : '';
        $auths = Model::fetchAll("SELECT `id`, `title` FROM `auth` ORDER BY `sort` DESC, `id` ASC");
        $auth_ids = array_map(function ($item) {
            return $item['id'];
        }, $auths);
        if (!empty($param['auth_ids'])) {
            $param['auth_ids'] != array_intersect($param['auth_ids'], $auth_ids) && Util::errMsg(Util::tran('权限数据错误'));
            $param['auth_ids'] = json_encode($param['auth_ids']);
        }
        $param['id'] == 1 && $param['status'] == 1 && Util::errMsg(Util::tran('管理员不可禁用'));

        // 更新数据库记录日志
        $rowCount = Model::update('role', $param, ['id' => $param['id']]);
        if ($rowCount) {
            $this->log(Util::tran('编辑角色'), Util::tran('编辑角色') . " {$param['name']} " . Util::tran('成功'), 0);
            Util::errMsg(['code' => 0, 'msg' => Util::tran('编辑角色成功')]);
        } else {
            $this->log(Util::tran('编辑角色'), Util::tran('没有改变') . " {$param['name']} " . Util::tran('信息'), 0);
            Util::errMsg(['code' => 1, 'msg' => Util::tran('没有改变信息')]);
        }
    }

    // 修改属性
    public function modify()
    {
        $param = Util::param(['id', 'field', 'value']);
        empty($param['id']) && Util::errMsg('ID ' . Util::tran('错误'));
        $role = array_filter($this->role, function ($item) use ($param) {
            return $param['id'] == $item['id'];
        });
        !$role && Util::errMsg('ID ' . Util::tran('非法'));
        $role = array_shift($role);
        (empty($param['field']) || !in_array($param['field'], ['sort', 'status'])) && Util::errMsg(Util::tran('非法字段'));
        $param['id'] == 1 && $param['field'] == 'status' && $param['value'] == 1 && Util::errMsg(Util::tran('管理员不可禁用'));

        // 更新数据库记录日志
        $rowCount = Model::update('role', [$param['field'] => $param['value']], ['id' => $param['id']]);
        if ($rowCount) {
            $this->log(Util::tran('修改属性'), Util::tran('修改属性') . " {$param['field']}: {$role[$param['field']]} -> {$param['value']} " . Util::tran('成功'), 0);
            Util::errMsg(['code' => 0, 'msg' => Util::tran('修改成功')]);
        } else {
            $this->log(Util::tran('修改属性'), Util::tran('修改属性') . " {$param['field']}: {$role[$param['field']]} -> {$param['value']} " . Util::tran('失败'), 1);
            Util::errMsg(['code' => 1, 'msg' => Util::tran('修改失败')]);
        }
    }

    // 删除
    public function delete()
    {
        $param = Util::param(['id']);
        (empty($param) || !isset($param['id'])) && Util::errMsg(Util::tran('请选择数据'));
        in_array('1', (array)$param['id']) && Util::errMsg(Util::tran('管理员不可删除'));
        in_array('2', (array)$param['id']) && Util::errMsg(Util::tran('游客不可删除'));
        $role = array_filter($this->role, function ($item) use ($param) {
            return in_array(strval($item['id']), (array)$param['id']);
        });
        $roleNames = array_map(function ($item) {
            return $item['name'];
        }, $role);

        // 删除数据记录日志
        $rowCount = Model::delete('role', ['id' => $param['id']]);
        if ($rowCount) {
            $this->log(Util::tran('删除角色'), Util::tran('删除角色') . " " . implode(' ', $roleNames) . " " . Util::tran('成功'), 0);
            Util::errMsg(['code' => 0, 'msg' => Util::tran('删除成功')]);
        } else {
            $this->log(Util::tran('删除角色'), Util::tran('删除角色') . " " . implode(' ', $roleNames) . " " . Util::tran('失败'), 1);
            Util::errMsg(['code' => 1, 'msg' => Util::tran('删除失败')]);
        }
    }
}
