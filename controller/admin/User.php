<?php

namespace controller\admin;

defined('IA_ROOT') || exit();

use facade\Model;
use facade\View;
use facade\Util;

// 用户管理
class User extends Base
{
    // 列表
    public function index()
    {
        if (Util::isAjax()) {
            $param = Util::param();
            $start = ($param['page'] - 1) * $param['limit'];
            $limit = $param['limit'];
            // $where = "`id` <= (SELECT `id` FROM `user` ORDER BY `id` DESC LIMIT $start, 1)";
            $where = "1 = 1";
            // 搜索
            if (!empty($param['search'])) {
                foreach ((array)$param['search'] as $k => $v) {
                    if (strlen($v)) {
                        if (in_array($k, ['status', 'role_id'])) {
                            $where .= " AND `$k` = '$v'";
                        } else {
                            $where .= " AND `$k` like '%{$v}%'";
                        }
                    }
                }
            }
            $count = Model::fetchColumn("SELECT COUNT(*) FROM `user` WHERE $where ORDER BY `id` DESC");
            $data = Model::fetchAll("SELECT * FROM `user` WHERE $where ORDER BY `id` DESC LIMIT $start, $limit");
            $role = Util::formatKey($this->role);
            if ($count) {
                $res['code'] = 0;
            } else {
                $res['code'] = 1;
                $res['msg'] = Util::tran('暂无记录');
            }
            $res['count'] = $count;
            $res['data'] = array_map(function ($item) use ($role) {
                $item['role_name'] = isset($role[$item['role_id']]) ? $role[$item['role_id']]['name'] : '';
                $item['avatar'] = '<img style="width:50px;height:100%" src="' . ($item['avatar'] ? $item['avatar'] : 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD//gA7Q1JFQVRPUjogZ2QtanBlZyB2MS4wICh1c2luZyBJSkcgSlBFRyB2NjIpLCBxdWFsaXR5ID0gOTAK/9sAQwADAgIDAgIDAwMDBAMDBAUIBQUEBAUKBwcGCAwKDAwLCgsLDQ4SEA0OEQ4LCxAWEBETFBUVFQwPFxgWFBgSFBUU/9sAQwEDBAQFBAUJBQUJFA0LDRQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQU/8AAEQgAIAAgAwEiAAIRAQMRAf/EAB8AAAEFAQEBAQEBAAAAAAAAAAABAgMEBQYHCAkKC//EALUQAAIBAwMCBAMFBQQEAAABfQECAwAEEQUSITFBBhNRYQcicRQygZGhCCNCscEVUtHwJDNicoIJChYXGBkaJSYnKCkqNDU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6g4SFhoeIiYqSk5SVlpeYmZqio6Slpqeoqaqys7S1tre4ubrCw8TFxsfIycrS09TV1tfY2drh4uPk5ebn6Onq8fLz9PX29/j5+v/EAB8BAAMBAQEBAQEBAQEAAAAAAAABAgMEBQYHCAkKC//EALURAAIBAgQEAwQHBQQEAAECdwABAgMRBAUhMQYSQVEHYXETIjKBCBRCkaGxwQkjM1LwFWJy0QoWJDThJfEXGBkaJicoKSo1Njc4OTpDREVGR0hJSlNUVVZXWFlaY2RlZmdoaWpzdHV2d3h5eoKDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uLj5OXm5+jp6vLz9PX29/j5+v/aAAwDAQACEQMRAD8A+uK0tH8O6lr7sthaPcbfvMMBR9ScCs2voDwXaQWfhbTFgACvAsjEd2YZY/maAPEdY8OaloDKL+0eAN91shlP0IyKza+gfGVpBeeF9TS4AKLA8gJ7MoyD+Yr5+oAfFE88qRxoXkchVVRkknoBXufgPQ9R0HRlt7+4WQH5kgAz5Oeo3d/881wnwj0lL3XJ7yRQwtIxsB7M2QD+QavYKAOa8d6JqOvaM1vYXCx/xPCRgy45A3dv6+teGSxPBK8ciFJEJVlYYII6g19MV4/8XNJSy1yC8jUKLuM7wO7LgE/kVoA//9k=') . '" alt="avatar">';
                $item['reg_time'] = date('Y-m-d H:i', $item['reg_time']);
                $item['first_login'] = date('Y-m-d H:i', $item['first_login']);
                $item['last_login'] = date('Y-m-d H:i', $item['last_login']);
                return $item;
            }, $data);
            exit(json_encode($res));
        }
        View::assign('role', $this->role);
        View::assign('tran', [
            'username' => Util::tran('用户名'),
            'input' => Util::tran('请输入'),
            'choose' => Util::tran('请选择'),
            'phone' => Util::tran('手机号'),
            'status' => Util::tran('状态'),
            'normal' => Util::tran('正常'),
            'disable' => Util::tran('禁用'),
            'role' => Util::tran('角色'),
            'delall' => Util::tran('批量删除'),
            'add' => Util::tran('添加'),
            'refresh' => Util::tran('刷新'),
            'user_list' => Util::tran('用户列表'),
            'nickname' => Util::tran('昵称'),
            'avatar' => Util::tran('头像'),
            'reg_time' => Util::tran('注册时间'),
            'first_login' => Util::tran('首次登陆'),
            'last_login' => Util::tran('最后登陆'),
            'option' => Util::tran('操作'),
            'edit' => Util::tran('编辑'),
            'delete' => Util::tran('删除'),
        ]);
        View::display('admin/user/index.html');
    }

    // 创建
    public function create()
    {
        $langs = array_map(function ($item) {
            $item['title'] = Util::tran($item['title']);
            return $item;
        }, $this->langs);
        View::assign('langs', $langs);
        View::assign('role', $this->role);
        View::assign('tran', [
            'username' => Util::tran('用户名'),
            'choose' => Util::tran('请选择'),
            'input_username' => Util::tran('请输入用户名'),
            'password' => Util::tran('密码'),
            'input_password' => Util::tran('请输入密码'),
            'nickname' => Util::tran('昵称'),
            'phone' => Util::tran('手机号'),
            'avatar' => Util::tran('头像'),
            'role' => Util::tran('角色'),
            'language_permission' => Util::tran('语言权限'),
            'status' => Util::tran('状态'),
            'save' => Util::tran('保存'),
            'reset' => Util::tran('重置'),
        ]);
        View::display('admin/user/create.html');
    }

    // 保存
    public function save()
    {
        $param = Util::param(['username', 'password', 'nickname', 'phone', 'avatar', 'role_id', 'lang_slugs', 'status']);
        if (empty($param['username']) || !preg_match('#^[a-z0-9_-]{3,12}$#', $param['username']))
            Util::errMsg(Util::tran('用户名只能英文字母数字下划线或中划线，位数') . ' 3~12');
        $check = Model::fetchColumn("SELECT `username` FROM `user` WHERE `username`=:username ORDER BY `id`", [':username' => $param['username']]);
        $check && Util::errMsg(Util::tran('用户名已存在'));
        if (empty($param['password']) || strlen($param['password']) < 5) {
            Util::errMsg(Util::tran('密码至少5位'));
        } else {
            $param['password'] = md5(base64_encode($param['password']));
        }
        if (!empty($param['nickname']) && !preg_match('#^[\x{4e00}-\x{9fa5}A-Za-z0-9_-]{2,12}$#u', $param['nickname']))
            Util::errMsg(Util::tran('昵称只能中英文字母数字下划线中划线，位数') . ' 2~12');
        if (!empty($param['nickname'])) {
            $check = Model::fetchColumn("SELECT `nickname` FROM `user` WHERE `nickname`=:nickname ORDER BY `id`", [':nickname' => $param['nickname']]);
            $check && Util::errMsg(Util::tran('昵称已存在'));
        }
        if (!empty($param['phone']) && !preg_match('#^1((34[0-8]\d{7})|((3[0-3|5-9])|(4[5-7|9])|(5[0-3|5-9])|(66)|(7[2-3|5-8])|(8[0-9])|(9[1|8|9]))\d{8})$#', $param['phone']))
            Util::errMsg(Util::tran('手机号码不正确'));
        if (!empty($param['phone'])) {
            $check = Model::fetchColumn("SELECT `phone` FROM `user` WHERE `phone`=:phone ORDER BY `id`", [':phone' => $param['phone']]);
            $check && Util::errMsg(Util::tran('手机号码已存在'));
        }
        if (!empty($param['avatar'])) {
            !preg_match('#/static/upload/.+?\.[png|jpg|jpeg|gif]#', $param['avatar']) && Util::errMsg(Util::tran('图片格式错误'));
            $param['avatar'] = str_replace($this->base_url, '', $param['avatar']);
        }

        $role_ids = array_map(function ($item) {
            return $item['id'];
        }, $this->role);
        $param['role_id'] = intval($param['role_id']);
        if (!empty($param['role_id']) && !in_array($param['role_id'], $role_ids))
            Util::errMsg(Util::tran('角色数据错误'));
        $param['lang_slugs'] = isset($param['lang_slugs']) ? array_values((array)$param['lang_slugs']) : '';
        $lang_slugs = array_map(function ($item) {
            return $item['slug'];
        }, $this->langs);
        if (!empty($param['lang_slugs'])) {
            $param['lang_slugs'] != array_intersect($param['lang_slugs'], $lang_slugs) && Util::errMsg(Util::tran('语言数据错误'));
            $param['lang_slugs'] = json_encode($param['lang_slugs']);
        }
        if (!isset($param['status']) || !in_array($param['status'], [0, 1]))
            Util::errMsg(Util::tran('状态数据错误'));

        // 写入数据库记录日志
        $param['reg_time'] = time();
        $lastInsertId = Model::insert('user', $param);
        if ($lastInsertId) {
            // 修改媒体状态
            if (preg_match('#/static/upload/.+?\.[png|jpg|jpeg|gif]#', $param['avatar'])) {
                if (is_file(IA_ROOT . str_replace($this->base_url, '', $param['avatar']))) {
                    Model::update('media', ['status' => 0], ['path' => str_replace($this->base_url, '', $param['avatar'])]);
                } else {
                    Model::update('media', ['status' => 2], ['path' => str_replace($this->base_url, '', $param['avatar'])]);
                }
            }
            $this->log(Util::tran('创建用户'), Util::tran('创建用户') . " {$param['username']} " . Util::tran('成功'), 0);
            Util::errMsg(['code' => 0, 'msg' => Util::tran('创建用户成功')]);
        } else {
            $this->log(Util::tran('创建用户'), Util::tran('创建用户') . " {$param['username']} " . Util::tran('失败'), 1);
            Util::errMsg(['code' => 1, 'msg' => Util::tran('创建用户失败')]);
        }
    }

    // 编辑
    public function edit($id = 0)
    {
        $user = Model::fetch("SELECT * FROM `user` WHERE `id`=:id", [':id' => $id]);
        !$user && Util::errMsg('ID ' . Util::tran('错误'));
        $langs = array_map(function ($item) {
            $item['title'] = Util::tran($item['title']);
            return $item;
        }, $this->langs);
        $lang_slugs = $user['lang_slugs'] ? json_decode($user['lang_slugs'], true) : [];
        $role = $this->role;
        View::assign(compact('user', 'role', 'langs', 'lang_slugs'));
        View::assign('tran', [
            'username' => Util::tran('用户名'),
            'choose' => Util::tran('请选择'),
            'input_username' => Util::tran('请输入用户名'),
            'not_modify' => Util::tran('不能修改'),
            'password' => Util::tran('密码'),
            'empty_no_modify' => Util::tran('留空不修改'),
            'nickname' => Util::tran('昵称'),
            'phone' => Util::tran('手机号'),
            'avatar' => Util::tran('头像'),
            'role' => Util::tran('角色'),
            'language_permission' => Util::tran('语言权限'),
            'status' => Util::tran('状态'),
            'save' => Util::tran('保存'),
            'reset' => Util::tran('重置'),
        ]);
        View::display('admin/user/edit.html');
    }

    // 更新
    public function update()
    {
        $param = Util::param(['id', 'password', 'nickname', 'phone', 'avatar', 'role_id', 'lang_slugs', 'status']);
        $user = Model::fetch("SELECT * FROM `user` WHERE `id`=:id", [':id' => $param['id']]);
        !$user && Util::errMsg('ID ' . Util::tran('错误'));
        if (!empty($param['password'])) {
            strlen($param['password']) < 5 && Util::errMsg(Util::tran('密码至少5位'));
            $param['password'] = md5(base64_encode($param['password']));
        } else {
            unset($param['password']);
        }
        if (!empty($param['nickname']) && !preg_match('#^[\x{4e00}-\x{9fa5}A-Za-z0-9_-]{2,12}$#u', $param['nickname']))
            Util::errMsg(Util::tran('昵称只能中英文字母数字下划线中划线，位数') . ' 2~12');
        if (!empty($param['nickname'])) {
            $check = Model::fetchColumn("SELECT `nickname` FROM `user` WHERE `nickname`=:nickname ORDER BY `id`", [':nickname' => $param['nickname']]);
            $check && $check !== $user['nickname'] && Util::errMsg(Util::tran('昵称已存在'));
        }
        if (!empty($param['phone']) && !preg_match('#^1((34[0-8]\d{7})|((3[0-3|5-9])|(4[5-7|9])|(5[0-3|5-9])|(66)|(7[2-3|5-8])|(8[0-9])|(9[1|8|9]))\d{8})$#', $param['phone']))
            Util::errMsg(Util::tran('手机号码不正确'));
        if (!empty($param['phone'])) {
            $check = Model::fetchColumn("SELECT `phone` FROM `user` WHERE `phone`=:phone ORDER BY `id`", [':phone' => $param['phone']]);
            $check && $check !== $user['phone'] && Util::errMsg(Util::tran('手机号码已存在'));
        }
        if (!empty($param['avatar'])) {
            !preg_match('#/static/upload/.+?\.[png|jpg|jpeg|gif]#', $param['avatar']) && Util::errMsg(Util::tran('图片格式错误'));
            $param['avatar'] = str_replace($this->base_url, '', $param['avatar']);
        }
        $role_ids = array_map(function ($item) {
            return $item['id'];
        }, $this->role);
        $param['role_id'] = intval($param['role_id']);
        if (!empty($param['role_id']) && !in_array($param['role_id'], $role_ids))
            Util::errMsg(Util::tran('角色数据错误'));
        $param['lang_slugs'] = isset($param['lang_slugs']) ? array_values((array)$param['lang_slugs']) : '';
        $lang_slugs = array_map(function ($item) {
            return $item['slug'];
        }, $this->langs);
        if (!empty($param['lang_slugs'])) {
            $param['lang_slugs'] != array_intersect($param['lang_slugs'], $lang_slugs) && Util::errMsg(Util::tran('语言数据错误'));
            $param['lang_slugs'] = json_encode($param['lang_slugs']);
        }
        if (!isset($param['status']) || !in_array($param['status'], [0, 1]))
            Util::errMsg(Util::tran('状态数据错误'));
        $param['id'] == 1 && $param['status'] == 1 && Util::errMsg(Util::tran('管理员不可禁用'));

        // 更新数据库记录日志
        $rowCount = Model::update('user', $param, ['id' => $param['id']]);
        // 修改媒体状态
        $paths = [0 => [], 1 => [], 2 => []];
        if (preg_match('#/static/upload/.+?\.[png|jpg|jpeg|gif]#', $user['avatar'])) {
            if (is_file(IA_ROOT . str_replace($this->base_url, '', $user['avatar']))) {
                $paths[1][] = str_replace($this->base_url, '', $user['avatar']);
            } else {
                $paths[2][] = str_replace($this->base_url, '', $user['avatar']);
            }
        }
        if (preg_match('#/static/upload/.+?\.[png|jpg|jpeg|gif]#', $param['avatar'])) {
            if (is_file(IA_ROOT . str_replace($this->base_url, '', $param['avatar']))) {
                $paths[0][] = str_replace($this->base_url, '', $param['avatar']);
            } else {
                $paths[2][] = str_replace($this->base_url, '', $param['avatar']);
            }
        }
        $paths[1] = array_filter($paths[1], function ($item) use ($paths) {
            return !in_array($item, $paths[0]);
        });
        $paths[1] && Model::update('media', ['status' => 1], ['path' => array_unique($paths[1])]);
        $paths[0] && Model::update('media', ['status' => 0], ['path' => array_unique($paths[0])]);
        $paths[2] && Model::update('media', ['status' => 2], ['path' => array_unique($paths[2])]);
        if ($rowCount) {
            $this->log(Util::tran('编辑用户'), Util::tran('编辑用户') . " {$user['username']} " . Util::tran('成功'), 0);
            Util::errMsg(['code' => 0, 'msg' => Util::tran('编辑用户成功')]);
        } else {
            $this->log(Util::tran('编辑用户'), Util::tran('没有改变') . " {$user['username']} " . Util::tran('信息'), 0);
            Util::errMsg(['code' => 1, 'msg' => Util::tran('没有改变信息')]);
        }
    }

    // 修改属性
    public function modify()
    {
        $param = Util::param(['id', 'field', 'value']);
        empty($param['id']) && Util::errMsg('ID ' . Util::tran('错误'));
        $user = Model::fetch("SELECT * FROM `user` WHERE `id`=:id", [':id' => $param['id']]);
        !$user && Util::errMsg('ID ' . Util::tran('非法'));
        (empty($param['field']) || !in_array($param['field'], ['nickname', 'phone', 'status'])) && Util::errMsg(Util::tran('非法字段'));

        if ($param['field'] == 'nickname' && !empty($param['value']) && !preg_match('#^[\x{4e00}-\x{9fa5}A-Za-z0-9_-]{2,12}$#u', $param['value']))
            Util::errMsg(Util::tran('昵称只能中英文字母数字下划线中划线，位数') . ' 2~12');
        if ($param['field'] == 'nickname' && !empty($param['value'])) {
            $check = Model::fetchColumn("SELECT `nickname` FROM `user` WHERE `nickname`=:nickname ORDER BY `id`", [':nickname' => $param['value']]);
            $check && $check !== $user['nickname'] && Util::errMsg(Util::tran('昵称已存在'));
        }
        if ($param['field'] == 'phone' && !preg_match('#^1((34[0-8]\d{7})|((3[0-3|5-9])|(4[5-7|9])|(5[0-3|5-9])|(66)|(7[2-3|5-8])|(8[0-9])|(9[1|8|9]))\d{8})$#', $param['value']))
            Util::errMsg(Util::tran('手机号码不正确'));
        if ($param['field'] == 'phone' && !empty($param['value'])) {
            $check = Model::fetchColumn("SELECT `phone` FROM `user` WHERE `phone`=:phone ORDER BY `id`", [':phone' => $param['value']]);
            $check && $check !== $user['phone'] && Util::errMsg(Util::tran('手机号码已存在'));
        }
        $param['id'] == 1 && $param['field'] == 'status' && $param['value'] == 1 && Util::errMsg(Util::tran('管理员不可禁用'));

        // 更新数据库记录日志
        $rowCount = Model::update('user', [$param['field'] => $param['value']], ['id' => $param['id']]);
        if ($rowCount) {
            $this->log(Util::tran('修改属性'), Util::tran('修改属性') . " {$param['field']}: {$user[$param['field']]} -> {$param['value']} " . Util::tran('成功'), 0);
            Util::errMsg(['code' => 0, 'msg' => Util::tran('修改成功')]);
        } else {
            $this->log(Util::tran('修改属性'), Util::tran('修改属性') . " {$param['field']}: {$user[$param['field']]} -> {$param['value']} " . Util::tran('失败'), 1);
            Util::errMsg(['code' => 1, 'msg' => Util::tran('修改失败')]);
        }
    }

    // 删除
    public function delete()
    {
        $param = Util::param(['id']);
        (empty($param) || !isset($param['id'])) && Util::errMsg(Util::tran('请选择数据'));
        in_array('1', (array)$param['id']) && Util::errMsg(Util::tran('管理员不可删除'));
        in_array('2', (array)$param['id']) && Util::errMsg(Util::tran('游客不可删除'));
        $idStr = implode(',', (array)$param['id']);
        $users = Model::fetchAll("SELECT `username`, `avatar` FROM `user` WHERE `id` IN ($idStr)");
        $paths = [0 => [], 1 => [], 2 => []];
        $userNames = [];
        foreach ($users as $user) {
            $userNames[] = $user['username'];
            if (preg_match('#/static/upload/.+?\.[png|jpg|jpeg|gif]#', $user['avatar'])) {
                if (is_file(IA_ROOT . str_replace($this->base_url, '', $user['avatar']))) {
                    $paths[1][] = str_replace($this->base_url, '', $user['avatar']);
                } else {
                    $paths[2][] = str_replace($this->base_url, '', $user['avatar']);
                }
            }
        }
        // 删除数据记录日志
        $rowCount = Model::delete('user', ['id' => $param['id']]);
        if ($rowCount) {
            // 修改媒体状态
            $paths[1] && Model::update('media', ['status' => 1], ['path' => array_unique($paths[1])]);
            $paths[2] && Model::update('media', ['status' => 2], ['path' => array_unique($paths[2])]);
            $this->log(Util::tran('删除用户'), Util::tran('删除用户') . " " . implode(' ', $userNames) . " " . Util::tran('成功'), 0);
            Util::errMsg(['code' => 0, 'msg' => Util::tran('删除成功')]);
        } else {
            $this->log(Util::tran('删除用户'), Util::tran('删除用户') . " " . implode(' ', $userNames) . " " . Util::tran('失败'), 1);
            Util::errMsg(['code' => 1, 'msg' => Util::tran('删除失败')]);
        }
    }
}
