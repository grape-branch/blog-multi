<?php

namespace controller\admin;

defined('IA_ROOT') || exit();

use facade\Model;
use facade\View;
use facade\Util;

// 页面管理
class Page extends Base
{
    // 列表
    public function index()
    {
        if (Util::isAjax()) {
            $param = Util::param();
            $start = ($param['page'] - 1) * $param['limit'];
            $limit = $param['limit'];
            $where = "";

            // 搜索
            if (!empty($param['search'])) {
                foreach ((array)$param['search'] as $k => $v) {
                    if (strlen($v)) {
                        if (in_array($k, ['is_full', 'is_nav', 'status'])) {
                            $where .= " AND `$k` = '$v'";
                        } else if (in_array($k, ['start', 'end'])) {
                            $timestamp = strtotime($v);
                            if ($k == 'start')
                                $where .= " AND `time` >= '$timestamp'";
                            else
                                $where .= " AND `time` <= '$timestamp'";
                        } else {
                            $where .= " AND `$k` like '%{$v}%'";
                        }
                    }
                }
            }

            // 数据库查询
            $count = Model::fetchColumn("SELECT COUNT(*) FROM `page` WHERE 1=1 $where AND `pid` = 0 AND `lang`='{$this->lang}' ORDER BY `id` ASC, `sort` DESC");
            $data = Model::fetchAll("SELECT * FROM `page` WHERE 1=1 $where AND `pid` = 0 AND `lang`='{$this->lang}' ORDER BY `id` ASC, `sort` DESC LIMIT $start, $limit");
            $ids = array_map(function ($item) {
                return $item['id'];
            }, $data);
            $idStr = implode(',', $ids);
            if ($idStr) {
                $childs = Model::fetchAll("SELECT * FROM `page` WHERE 1=1 $where AND `pid` IN ($idStr) AND `lang`='{$this->lang}' ORDER BY `id` ASC, `sort` DESC");
                $data = array_merge($data, $childs);
            }
            $data = $this->format_tree($data, 'title');

            // 分页输出
            if ($count) {
                $res['code'] = 0;
            } else {
                $res['code'] = 1;
                $res['msg'] = Util::tran('暂无记录');
            }
            $res['count'] = $count;
            $res['data'] = array_map(function ($item) {
                if (!$item['cover'] && preg_match_all('#<[^>]*=["\']([^"\']*/static/upload/.+?\.[^"\']*)["\'][^>]*>#i', $item['content'], $matches)) {
                    $item['cover'] = str_replace($this->base_url, '', $matches[1][0]);
                }
                $item['cover'] = $item['cover'] ? '<img src="' . $item['cover'] . '" style="width:50px;height:100%" alt="cover">' : '';
                $item['time'] = date('Y-m-d H:i', $item['time']);
                return $item;
            }, $data);
            exit(json_encode($res));
        }
        View::assign('tran', [
            'title' => Util::tran('标题'),
            'input' => Util::tran('请输入'),
            'choose' => Util::tran('请选择'),
            'slug' => Util::tran('别名'),
            'description' => Util::tran('描述'),
            'cover' => Util::tran('封面'),
            'full_screen' => Util::tran('宽屏'),
            'nav_menu' => Util::tran('导航'),
            'usual' => Util::tran('普通'),
            'status' => Util::tran('状态'),
            'normal' => Util::tran('正常'),
            'disable' => Util::tran('禁用'),
            'date' => Util::tran('日期'),
            'delall' => Util::tran('批量删除'),
            'add' => Util::tran('添加'),
            'refresh' => Util::tran('刷新'),
            'page_list' => Util::tran('页面列表'),
            'sort' => Util::tran('排序'),
            'time' => Util::tran('时间'),
            'option' => Util::tran('操作'),
            'read' => Util::tran('阅读'),
            'edit' => Util::tran('编辑'),
            'delete' => Util::tran('删除'),
        ]);
        View::display('admin/page/index.html');
    }

    // 创建
    public function create()
    {
        $pid = (int)Util::param('id');
        $rows = Model::fetchAll("SELECT * FROM `page` WHERE `lang`='{$this->lang}' ORDER BY `sort` DESC, `id` ASC");
        View::assign(['pid' => $pid, 'page' => $this->format_tree($rows, 'title')]);
        View::assign('tran', [
            'page_title' => Util::tran('页面标题'),
            'choose' => Util::tran('请选择'),
            'input_page_title' => Util::tran('请输入页面标题'),
            'parent_page' => Util::tran('父级页面'),
            'top_page' => Util::tran('顶级页面'),
            'slug' => Util::tran('别名'),
            'translate' => Util::tran('翻译'),
            'content' => Util::tran('内容'),
            'description' => Util::tran('描述'),
            'cover' => Util::tran('封面'),
            'input_content' => Util::tran('请输入内容'),
            'sort' => Util::tran('排序'),
            'suggest' => Util::tran('建议'),
            'keywords_suggest' => Util::tran('关键词建议'),
            'title' => Util::tran('标题'),
            'discription' => Util::tran('描述'),
            'full_screen' => Util::tran('宽屏'),
            'nav_menu' => Util::tran('导航'),
            'status' => Util::tran('状态'),
            'save' => Util::tran('保存'),
            'reset' => Util::tran('重置'),
        ]);
        View::display('admin/page/create.html');
    }

    // 保存
    public function save()
    {
        $param = Util::param(['title', 'pid', 'slug', 'content', 'description', 'cover', 'sort', 'seo_title', 'seo_description', 'is_full', 'is_nav', 'status']);
        empty($param['title']) && Util::errMsg(Util::tran('标题不能为空'));

        // 重复验证
        $check = Model::fetchColumn("SELECT `title` FROM `page` WHERE `title`=:title AND `lang`='{$this->lang}' ORDER BY `id`", [':title' => $param['title']]);
        $check && Util::errMsg(Util::tran('标题重名'));
        if (!empty($param['slug'])) {
            $param['slug'] = preg_replace('/[\W]/', ' ', $param['slug']);
            $param['slug'] = preg_replace('/\s+/', ' ', $param['slug']);
            $param['slug'] = strtolower($param['slug']);
            $param['slug'] = str_replace(' ', '-', $param['slug']);
            !preg_match('#^[a-z0-9_-]{2,100}$#i', $param['slug']) && Util::errMsg(Util::tran('别名只能英文字母数字下划线中划线，位数') . ' 2~100');
            $check = Model::fetchColumn("SELECT `slug` FROM `page` WHERE `slug`=:slug AND `lang`='{$this->lang}' ORDER BY `id`", [':slug' => $param['slug']]);
            $check && Util::errMsg(Util::tran('别名重复'));
        }

        if (!empty($param['cover'])) {
            !preg_match('#/static/upload/.+?\.[gif|jpg|jpeg|png|bmp|webp|psd|svg|tiff]#', $param['cover']) && Util::errMsg(Util::tran('图片格式错误'));
            $param['cover'] = str_replace($this->base_url, '', $param['cover']);
        }

        // 写入数据库记录日志
        $param['lang'] = $this->lang;
        $param['time'] = time();
        if (preg_match_all('#<[^>]*=["\']([^"\']*/static/upload/.+?\.[^"\']*)["\'][^>]*>#i', $param['content'], $matches)) {
            foreach ($matches[1] as $match) {
                $param['content'] = str_replace($match, str_replace($this->base_url, '', $match), $param['content']);
            }
        }
        if (preg_match_all('#<[^>]*=["\']([^"\']*/static/res/.+?\.[^"\']*)["\'][^>]*>#i', $param['content'], $matches)) {
            foreach ($matches[1] as $match) {
                $param['content'] = str_replace($match, str_replace($this->base_url, '', $match), $param['content']);
            }
        }
        !in_array($this->lang, $this->user['langs']) && Util::errMsg(Util::tran('没有语言权限，请联系管理员'));
        $lastInsertId = Model::insert('page', $param);
        if ($lastInsertId) {
            // 修改媒体状态
            $paths = [0 => [], 2 => []];
            if (preg_match('#/static/upload/.+?\.[gif|jpg|jpeg|png|bmp|webp|psd|svg|tiff]#', $param['cover'])) {
                if (is_file(IA_ROOT . str_replace($this->base_url, '', $param['cover'])))
                    $paths[0][] = str_replace($this->base_url, '', $param['cover']);
                else
                    $paths[2][] = str_replace($this->base_url, '', $param['cover']);
            }
            if (preg_match_all('#<[^>]*=["\']([^"\']*/static/upload/.+?\.[^"\']*)["\'][^>]*>#i', $param['content'], $matches)) {
                foreach ($matches[1] as $match) {
                    if (is_file(IA_ROOT . str_replace($this->base_url, '', $match)))
                        $paths[0][] = str_replace($this->base_url, '', $match);
                    else
                        $paths[2][] = str_replace($this->base_url, '', $match);
                }
            }
            $paths[0] && Model::update('media', ['status' => 0], ['path' => $paths[0]]);
            $paths[2] && Model::update('media', ['status' => 2], ['path' => $paths[2]]);
            $this->log(Util::tran('创建页面'), Util::tran('创建页面') . " {$param['title']} " . Util::tran('成功'), 0);
            Util::errMsg(['code' => 0, 'msg' => Util::tran('创建页面成功'), 'url' => Util::url("/{$this->lang}/admin/page/index.html")]);
        } else {
            $this->log(Util::tran('创建页面'), Util::tran('创建页面') . " {$param['title']} " . Util::tran('失败'), 1);
            Util::errMsg(['code' => 1, 'msg' => Util::tran('创建页面失败')]);
        }
    }

    // 编辑
    public function edit($id = 0)
    {
        $row = Model::fetch("SELECT * FROM `page` WHERE `id`=:id", [':id' => $id]);
        !$row && Util::errMsg('ID ' . Util::tran('错误'));
        $rows = Model::fetchAll("SELECT * FROM `page` WHERE `lang`='{$this->lang}' ORDER BY `sort` DESC, `id` ASC");
        View::assign(['row' => $row, 'page' => $this->format_tree($rows, 'title')]);
        View::assign('tran', [
            'page_title' => Util::tran('页面标题'),
            'choose' => Util::tran('请选择'),
            'input_page_title' => Util::tran('请输入页面标题'),
            'parent_page' => Util::tran('父级页面'),
            'top_page' => Util::tran('顶级页面'),
            'slug' => Util::tran('别名'),
            'translate' => Util::tran('翻译'),
            'content' => Util::tran('内容'),
            'description' => Util::tran('描述'),
            'cover' => Util::tran('封面'),
            'input_content' => Util::tran('请输入内容'),
            'sort' => Util::tran('排序'),
            'suggest' => Util::tran('建议'),
            'keywords_suggest' => Util::tran('关键词建议'),
            'title' => Util::tran('标题'),
            'discription' => Util::tran('描述'),
            'full_screen' => Util::tran('宽屏'),
            'nav_menu' => Util::tran('导航'),
            'status' => Util::tran('状态'),
            'save' => Util::tran('保存'),
            'reset' => Util::tran('重置'),
        ]);
        View::display('admin/page/edit.html');
    }

    // 更新
    public function update()
    {
        $param = Util::param(['id', 'title', 'pid', 'slug', 'content', 'description', 'cover', 'sort', 'seo_title', 'seo_description', 'is_full', 'is_nav', 'status']);
        empty($param['title']) && Util::errMsg(Util::tran('标题不能为空'));
        $row = Model::fetch("SELECT * FROM `page` WHERE `id`=:id", [':id' => $param['id']]);
        !$row && Util::errMsg('ID ' . Util::tran('错误'));

        // 重复验证
        $check = Model::fetchColumn("SELECT `title` FROM `page` WHERE `title`=:title AND `lang`='{$this->lang}' ORDER BY `id`", [':title' => $param['title']]);
        $check && $check !== $row['title'] && Util::errMsg(Util::tran('标题重名'));
        if (!empty($param['slug'])) {
            $param['slug'] = preg_replace('/[\W]/', ' ', $param['slug']);
            $param['slug'] = preg_replace('/\s+/', ' ', $param['slug']);
            $param['slug'] = strtolower($param['slug']);
            $param['slug'] = str_replace(' ', '-', $param['slug']);
            !preg_match('#^[a-z0-9_-]{2,100}$#i', $param['slug']) && Util::errMsg(Util::tran('别名只能英文字母数字下划线中划线，位数') . ' 2~100');
            $check = Model::fetchColumn("SELECT `slug` FROM `page` WHERE `slug`=:slug AND `lang`='{$this->lang}' ORDER BY `id`", [':slug' => $param['slug']]);
            $check && $check !== $row['slug'] && Util::errMsg(Util::tran('别名重复'));
        }

        if (!empty($param['cover'])) {
            !preg_match('#/static/upload/.+?\.[gif|jpg|jpeg|png|bmp|webp|psd|svg|tiff]#', $param['cover']) && Util::errMsg(Util::tran('图片格式错误'));
            $param['cover'] = str_replace($this->base_url, '', $param['cover']);
        }

        // 更新数据库记录日志
        if (preg_match_all('#<[^>]*=["\']([^"\']*/static/upload/.+?\.[^"\']*)["\'][^>]*>#i', $param['content'], $matches)) {
            foreach ($matches[1] as $match) {
                $param['content'] = str_replace($match, str_replace($this->base_url, '', $match), $param['content']);
            }
        }
        if (preg_match_all('#<[^>]*=["\']([^"\']*/static/res/.+?\.[^"\']*)["\'][^>]*>#i', $param['content'], $matches)) {
            foreach ($matches[1] as $match) {
                $param['content'] = str_replace($match, str_replace($this->base_url, '', $match), $param['content']);
            }
        }
        !in_array($this->lang, $this->user['langs']) && Util::errMsg(Util::tran('没有语言权限，请联系管理员'));
        $rowCount = Model::update('page', $param, ['id' => $param['id']]);

        // 修改媒体状态
        $paths = [0 => [], 1 => [], 2 => []];
        if (preg_match('#/static/upload/.+?\.[gif|jpg|jpeg|png|bmp|webp|psd|svg|tiff]#', $row['cover'])) {
            if (is_file(IA_ROOT . str_replace($this->base_url, '', $row['cover'])))
                $paths[1][] = str_replace($this->base_url, '', $row['cover']);
            else
                $paths[2][] = str_replace($this->base_url, '', $row['cover']);
        }
        if (preg_match('#/static/upload/.+?\.[gif|jpg|jpeg|png|bmp|webp|psd|svg|tiff]#', $param['cover'])) {
            if (is_file(IA_ROOT . str_replace($this->base_url, '', $param['cover'])))
                $paths[0][] = str_replace($this->base_url, '', $param['cover']);
            else
                $paths[2][] = str_replace($this->base_url, '', $param['cover']);
        }
        if (preg_match_all('#<[^>]*=["\']([^"\']*/static/upload/.+?\.[^"\']*)["\'][^>]*>#i', $row['content'], $matches)) {
            foreach ($matches[1] as $match) {
                if (is_file(IA_ROOT . str_replace($this->base_url, '', $match)))
                    $paths[1][] = str_replace($this->base_url, '', $match);
                else
                    $paths[2][] = str_replace($this->base_url, '', $match);
            }
        }
        if (preg_match_all('#<[^>]*=["\']([^"\']*/static/upload/.+?\.[^"\']*)["\'][^>]*>#i', $param['content'], $matches)) {
            foreach ($matches[1] as $match) {
                if (is_file(IA_ROOT . str_replace($this->base_url, '', $match)))
                    $paths[0][] = str_replace($this->base_url, '', $match);
                else
                    $paths[2][] = str_replace($this->base_url, '', $match);
            }
        }
        $paths[1] && Model::update('media', ['status' => 1], ['path' => array_unique($paths[1])]);
        $paths[0] && Model::update('media', ['status' => 0], ['path' => array_unique($paths[0])]);
        $paths[2] && Model::update('media', ['status' => 2], ['path' => array_unique($paths[2])]);
        if ($rowCount) {
            // 删除缓存
            $this->clearCache($param['id']);
            $this->log(Util::tran('编辑页面'), Util::tran('编辑页面') . " {$row['title']} -> {$param['title']} " . Util::tran('成功'), 0);
            Util::errMsg(['code' => 0, 'msg' => Util::tran('编辑页面成功'), 'url' => Util::url("/{$this->lang}/admin/page/index.html")]);
        } else {
            $this->log(Util::tran('编辑页面'), Util::tran('没有改变') . " {$row['title']} " . Util::tran('信息'), 0);
            Util::errMsg(['code' => 1, 'msg' => Util::tran('没有改变信息')]);
        }
    }

    // 修改属性
    public function modify()
    {
        $param = Util::param(['id', 'field', 'value']);
        empty($param['id']) && Util::errMsg('ID ' . Util::tran('错误'));
        $row = Model::fetch("SELECT * FROM `page` WHERE `id`=:id", [':id' => $param['id']]);
        !$row && Util::errMsg('ID ' . Util::tran('非法'));
        (empty($param['field']) || !in_array($param['field'], ['title', 'slug', 'description', 'sort', 'is_full', 'is_nav', 'status'])) && Util::errMsg(Util::tran('非法字段'));

        // 验证字段
        if ($param['field'] == 'title') {
            empty($param['value']) && Util::errMsg(Util::tran('标题不能为空'));
            $check = Model::fetchColumn("SELECT `title` FROM `page` WHERE `title`=:title AND `lang`='{$this->lang}' ORDER BY `id`", [':title' => $param['value']]);
            $check && $check !== $row['title'] && Util::errMsg(Util::tran('标题重名'));
        }
        if ($param['field'] == 'slug') {
            !preg_match('#^[a-z0-9_-]{2,100}$#i', $param['value']) && Util::errMsg(Util::tran('别名只能英文字母数字下划线中划线，位数') . ' 2~100');
            $check = Model::fetchColumn("SELECT `slug` FROM `page` WHERE `slug`=:slug AND `lang`='{$this->lang}' ORDER BY `id`", [':slug' => $param['value']]);
            $check && $check !== $row['slug'] && Util::errMsg(Util::tran('别名重复'));
        }
        if (in_array($param['field'], ['is_full', 'is_nav', 'status'])) {
            !is_numeric($param['value']) && Util::errMsg(Util::tran('填写整数'));
        }

        // 更新数据库记录日志
        !in_array($this->lang, $this->user['langs']) && Util::errMsg(Util::tran('没有语言权限，请联系管理员'));
        $rowCount = Model::update('page', [$param['field'] => $param['value']], ['id' => $param['id']]);
        if ($rowCount) {
            // 删除缓存
            $this->clearCache($param['id']);
            $this->log(Util::tran('修改属性'), Util::tran('修改属性') . " {$param['field']}: {$row[$param['field']]} -> {$param['value']} " . Util::tran('成功'), 0);
            Util::errMsg(['code' => 0, 'msg' => Util::tran('修改成功')]);
        } else {
            $this->log(Util::tran('修改属性'), Util::tran('修改属性') . " {$param['field']}: {$row[$param['field']]} -> {$param['value']} " . Util::tran('失败'), 1);
            Util::errMsg(['code' => 1, 'msg' => Util::tran('修改失败')]);
        }
    }

    // 软删除
    public function delete()
    {
        $param = Util::param(['id']);
        (empty($param) || !isset($param['id'])) && Util::errMsg(Util::tran('请选择数据'));

        // 如果有子页面
        $idStr = implode(',', (array)$param['id']);
        $check = Model::fetchColumn("SELECT `id` FROM `page` WHERE `pid` IN ($idStr) AND `lang`='{$this->lang}'");
        $check && Util::errMsg(Util::tran('请先删除子页面'));

        $idStr = implode(',', (array)$param['id']);
        $rows = Model::fetchAll("SELECT `title`, `content` FROM `page` WHERE `id` IN ($idStr) AND `lang`='{$this->lang}'");
        $names = [];
        $paths = [1 => [], 2 => []];
        foreach ($rows as $row) {
            $names[] = $row['title'];
            if (preg_match_all('#<[^>]*=["\']([^"\']*/static/upload/.+?\.[^"\']*)["\'][^>]*>#i', $row['content'], $matches)) {
                foreach ($matches[1] as $match) {
                    if (is_file(IA_ROOT . str_replace($this->base_url, '', $match)))
                        $paths[1][] = str_replace($this->base_url, '', $match);
                    else
                        $paths[2][] = str_replace($this->base_url, '', $match);
                }
            }
        }

        // 软删除数据记录日志
        !in_array($this->lang, $this->user['langs']) && Util::errMsg(Util::tran('没有语言权限，请联系管理员'));
        $rowCount = Model::delete('page', ['id' => $param['id']]);
        if ($rowCount) {
            // 删除缓存
            $this->clearCache($param['id']);
            // 修改媒体状态
            $paths[1] && Model::update('media', ['status' => 1], ['path' => array_unique($paths[1])]);
            $paths[2] && Model::update('media', ['status' => 2], ['path' => array_unique($paths[2])]);
            $this->log(Util::tran('删除页面'), Util::tran('删除页面') . " " . implode(' ', $names) . " " . Util::tran('成功'), 0);
            Util::errMsg(['code' => 0, 'msg' => Util::tran('删除成功')]);
        } else {
            $this->log(Util::tran('删除页面'), Util::tran('删除页面') . " " . implode(' ', $names) . " " . Util::tran('失败'), 1);
            Util::errMsg(['code' => 1, 'msg' => Util::tran('删除失败')]);
        }
    }

    // 删除缓存
    private function clearCache($ids = 0)
    {
        if ($ids) {
            $ids = (array)$ids;
            $ids = array_unique($ids);
            $idStr = implode(',', $ids);
            $rows = Model::fetchAll("SELECT `id`, `slug` FROM `page` WHERE `id` IN ($idStr)");
            $prefix = $this->base_url . ($this->lang === 'zh-CN' ? "/" : "/{$this->lang}/");
            foreach ($rows as $row) {
                $file_name = md5($prefix);
                $cache_file = IA_ROOT . '/mvc/tpl/' . substr($file_name, 0, 2) . '/' . $file_name . '.php';
                if (file_exists($cache_file))
                    unlink($cache_file);
                $file_name = md5($prefix . 'page/' . ($row['slug'] ? $row['slug'] : $row['id']) . '.html');
                $cache_file = IA_ROOT . '/mvc/tpl/' . substr($file_name, 0, 2) . '/' . $file_name . '.php';
                if (file_exists($cache_file))
                    unlink($cache_file);
                // 删除分页缓存
                $count = Model::fetchColumn("SELECT COUNT(*) FROM `page` WHERE `pid`=:pid AND `status`=0 ORDER BY `id` DESC", [':pid' => $row['id']]);
                for ($i = 2; $i <= $count; $i++) {
                    $file_name = md5($prefix . 'page/' . ($row['slug'] ? $row['slug'] : $row['id']) . '/page-' . $i . '.html');
                    $cache_file = IA_ROOT . '/mvc/tpl/' . substr($file_name, 0, 2) . '/' . $file_name . '.php';
                    file_exists($cache_file) && unlink($cache_file);
                }
            }
        }
    }
}
