<?php

namespace controller\admin;

defined('IA_ROOT') || exit();

use facade\Model;
use facade\View;
use facade\Util;

// 标签管理
class Tag extends Base
{
    // 列表
    public function index()
    {
        if (Util::isAjax()) {
            $param = Util::param();
            $start = ($param['page'] - 1) * $param['limit'];
            $limit = $param['limit'];
            $where = "";
            // 搜索
            if (!empty($param['search'])) {
                foreach ((array)$param['search'] as $k => $v) {
                    if (strlen($v)) {
                        if ($k == 'status')
                            $where .= " AND `$k` = '$v'";
                        else
                            $where .= " AND `$k` like '%{$v}%'";
                    }
                }
            }
            $count = Model::fetchColumn("SELECT COUNT(*) FROM `tag` WHERE 1=1 $where AND `lang`='{$this->lang}' ORDER BY `sort` DESC, `id` DESC");
            $data = Model::fetchAll("SELECT * FROM `tag` WHERE 1=1 $where AND `lang`='{$this->lang}' ORDER BY `sort` DESC, `id` DESC LIMIT $start, $limit");
            if ($count) {
                $res['code'] = 0;
            } else {
                $res['code'] = 1;
                $res['msg'] = Util::tran('暂无记录');
            }
            $res['count'] = $count;
            $res['data'] = array_map(function ($item) {
                $item['time'] = date('Y-m-d H:i', $item['time']);
                return $item;
            }, $data);
            exit(json_encode($res));
        }
        View::assign('tran', [
            'tag_name' => Util::tran('标签名称'),
            'input' => Util::tran('请输入'),
            'choose' => Util::tran('请选择'),
            'slug' => Util::tran('别名'),
            'description' => Util::tran('描述'),
            'status' => Util::tran('状态'),
            'normal' => Util::tran('正常'),
            'disable' => Util::tran('禁用'),
            'delall' => Util::tran('批量删除'),
            'add' => Util::tran('添加'),
            'refresh' => Util::tran('刷新'),
            'tag_list' => Util::tran('标签列表'),
            'tag' => Util::tran('标签'),
            'article_num' => Util::tran('文章数'),
            'sort' => Util::tran('排序'),
            'time' => Util::tran('时间'),
            'option' => Util::tran('操作'),
            'read' => Util::tran('阅读'),
            'edit' => Util::tran('编辑'),
            'delete' => Util::tran('删除'),
        ]);
        View::display('admin/tag/index.html');
    }

    // 创建
    public function create()
    {
        View::assign('tran', [
            'tag_name' => Util::tran('标签名称'),
            'input_tag_name' => Util::tran('请输入标签名称'),
            'slug' => Util::tran('别名'),
            'translate' => Util::tran('翻译'),
            'description' => Util::tran('描述'),
            'sort' => Util::tran('排序'),
            'suggest' => Util::tran('建议'),
            'keywords_suggest' => Util::tran('关键词建议'),
            'input_sort' => Util::tran('请输入排序'),
            'title' => Util::tran('标题'),
            'status' => Util::tran('状态'),
            'save' => Util::tran('保存'),
            'reset' => Util::tran('重置'),
        ]);
        View::display('admin/tag/create.html');
    }

    // 保存
    public function save()
    {
        $param = Util::param(['name', 'slug', 'description', 'sort', 'seo_title', 'seo_description', 'status']);
        empty($param['name']) && Util::errMsg(Util::tran('标签名称为空'));

        // 重复验证
        $check = Model::fetchColumn("SELECT `name` FROM `tag` WHERE `name`=:name AND `lang`='{$this->lang}' ORDER BY `id`", [':name' => $param['name']]);
        $check && Util::errMsg(Util::tran('标签重名'));
        if (!empty($param['slug'])) {
            $param['slug'] = preg_replace('/[\W]/', ' ', $param['slug']);
            $param['slug'] = preg_replace('/\s+/', ' ', $param['slug']);
            $param['slug'] = strtolower($param['slug']);
            $param['slug'] = str_replace(' ', '-', $param['slug']);
            !preg_match('#^[a-z0-9_-]{2,100}$#i', $param['slug']) && Util::errMsg(Util::tran('别名只能英文字母数字下划线中划线，位数') . ' 2~100');
            $check = Model::fetchColumn("SELECT `slug` FROM `tag` WHERE `slug`=:slug AND `lang`='{$this->lang}' ORDER BY `id`", [':slug' => $param['slug']]);
            $check && Util::errMsg(Util::tran('别名重复'));
        }

        // 写入数据库记录日志
        $param['time'] = time();
        $param['lang'] = $this->lang;
        !in_array($this->lang, $this->user['langs']) && Util::errMsg(Util::tran('没有语言权限，请联系管理员'));
        $lastInsertId = Model::insert('tag', $param);
        if ($lastInsertId) {
            $this->log(Util::tran('创建标签'), Util::tran('创建标签') . " {$param['name']} " . Util::tran('成功'), 0);
            Util::errMsg(['code' => 0, 'msg' => Util::tran('创建标签成功'), 'url' => Util::url("/{$this->lang}/admin/tag/index.html")]);
        } else {
            $this->log(Util::tran('创建标签'), Util::tran('创建标签') . " {$param['name']} " . Util::tran('失败'), 1);
            Util::errMsg(['code' => 1, 'msg' => Util::tran('创建标签失败')]);
        }
    }

    // 编辑
    public function edit($id = 0)
    {
        $row = Model::fetch("SELECT * FROM `tag` WHERE `id`=:id", [':id' => $id]);
        !$row && Util::errMsg('ID ' . Util::tran('错误'));
        View::assign('row', $row);
        View::assign('tran', [
            'tag_name' => Util::tran('标签名称'),
            'input_tag_name' => Util::tran('请输入标签名称'),
            'slug' => Util::tran('别名'),
            'translate' => Util::tran('翻译'),
            'description' => Util::tran('描述'),
            'sort' => Util::tran('排序'),
            'suggest' => Util::tran('建议'),
            'keywords_suggest' => Util::tran('关键词建议'),
            'input_sort' => Util::tran('请输入排序'),
            'title' => Util::tran('标题'),
            'status' => Util::tran('状态'),
            'save' => Util::tran('保存'),
            'reset' => Util::tran('重置'),
        ]);
        View::display('admin/tag/edit.html');
    }

    // 更新
    public function update()
    {
        $param = Util::param(['id', 'name', 'slug', 'description', 'sort', 'seo_title', 'seo_description', 'status']);
        $row = Model::fetch("SELECT * FROM `tag` WHERE `id`=:id", [':id' => $param['id']]);
        !$row && Util::errMsg('ID ' . Util::tran('错误'));
        empty($param['name']) && Util::errMsg(Util::tran('标签名称为空'));

        // 重复验证
        $check = Model::fetchColumn("SELECT `name` FROM `tag` WHERE `name`=:name AND `lang`='{$this->lang}' ORDER BY `id`", [':name' => $param['name']]);
        $check && $check !== $row['name'] && Util::errMsg(Util::tran('标签重名'));
        if (!empty($param['slug'])) {
            $param['slug'] = preg_replace('/[\W]/', ' ', $param['slug']);
            $param['slug'] = preg_replace('/\s+/', ' ', $param['slug']);
            $param['slug'] = strtolower($param['slug']);
            $param['slug'] = str_replace(' ', '-', $param['slug']);
            !preg_match('#^[a-z0-9_-]{2,100}$#i', $param['slug']) && Util::errMsg(Util::tran('别名只能英文字母数字下划线中划线，位数') . ' 2~100');
            $check = Model::fetchColumn("SELECT `slug` FROM `tag` WHERE `slug`=:slug AND `lang`='{$this->lang}' ORDER BY `id`", [':slug' => $param['slug']]);
            $check && $check !== $row['slug'] && Util::errMsg(Util::tran('别名重复'));
        }

        // 更新数据库记录日志
        !in_array($this->lang, $this->user['langs']) && Util::errMsg(Util::tran('没有语言权限，请联系管理员'));
        $rowCount = Model::update('tag', $param, ['id' => $param['id']]);
        if ($rowCount) {
            // 删除缓存
            $this->clearCache($param['id']);
            $this->log(Util::tran('编辑标签'), Util::tran('编辑标签') . " {$row['name']} -> {$param['name']} " . Util::tran('成功'), 0);
            Util::errMsg(['code' => 0, 'msg' => Util::tran('编辑标签成功')]);
        } else {
            $this->log(Util::tran('编辑标签'), Util::tran('没有改变') . " {$row['name']} " . Util::tran('信息'), 0);
            Util::errMsg(['code' => 1, 'msg' => Util::tran('没有改变信息')]);
        }
    }

    // 修改属性
    public function modify()
    {
        $param = Util::param(['id', 'field', 'value']);
        empty($param['id']) && Util::errMsg('ID ' . Util::tran('错误'));
        $row = Model::fetch("SELECT * FROM `tag` WHERE `id`=:id", [':id' => $param['id']]);
        !$row && Util::errMsg('ID ' . Util::tran('非法'));
        (empty($param['field']) || !in_array($param['field'], ['name', 'slug', 'description', 'sort', 'status'])) && Util::errMsg(Util::tran('非法字段'));

        // 验证字段
        if ($param['field'] == 'name') {
            empty($param['value']) && Util::errMsg(Util::tran('标签名不能为空'));
            $check = Model::fetchColumn("SELECT `name` FROM `tag` WHERE `name`=:name AND `lang`='{$this->lang}' ORDER BY `id`", [':name' => $param['value']]);
            $check && $check !== $row['name'] && Util::errMsg(Util::tran('标签重名'));
        }
        if ($param['field'] == 'slug') {
            $param['value'] = preg_replace('/[\W]/', ' ', $param['value']);
            $param['value'] = preg_replace('/\s+/', ' ', $param['value']);
            $param['value'] = strtolower($param['value']);
            $param['value'] = str_replace(' ', '-', $param['value']);
            !preg_match('#^[a-z0-9_-]{2,100}$#i', $param['value']) && Util::errMsg(Util::tran('别名只能英文字母数字下划线中划线，位数') . ' 2~100');
            $check = Model::fetchColumn("SELECT `slug` FROM `tag` WHERE `slug`=:slug AND `lang`='{$this->lang}' ORDER BY `id`", [':slug' => $param['value']]);
            $check && $check !== $row['slug'] && Util::errMsg(Util::tran('别名重复'));
        }
        $param['field'] == 'sort' && !is_numeric($param['value']) && Util::errMsg(Util::tran('排序只能是数字'));

        // 更新数据库记录日志
        !in_array($this->lang, $this->user['langs']) && Util::errMsg(Util::tran('没有语言权限，请联系管理员'));
        $rowCount = Model::update('tag', [$param['field'] => $param['value']], ['id' => $param['id']]);
        if ($rowCount) {
            // 删除缓存
            $this->clearCache($param['id']);
            $this->log(Util::tran('修改属性'), Util::tran('修改属性') . " {$param['field']}: {$row[$param['field']]} -> {$param['value']} " . Util::tran('成功'), 0);
            Util::errMsg(['code' => 0, 'msg' => Util::tran('修改成功')]);
        } else {
            $this->log(Util::tran('修改属性'), Util::tran('未修改属性') . " {$param['field']}: {$row[$param['field']]} -> {$param['value']} " . Util::tran('信息'), 0);
            Util::errMsg(['code' => 1, 'msg' => Util::tran('没有改变信息')]);
        }
    }

    // 删除
    public function delete()
    {
        $param = Util::param(['id']);
        (empty($param) || !isset($param['id'])) && Util::errMsg(Util::tran('请选择数据'));
        $idStr = implode(',', (array)$param['id']);
        $rows = Model::fetchAll("SELECT * FROM `tag` WHERE `id` IN ($idStr) AND `lang`='{$this->lang}'");
        $names = [];
        foreach ($rows as $row) {
            $row['num'] && Util::errMsg(Util::tran('标签有文章不能删除'));
            $names[] = $row['name'];
        }

        // 删除数据记录日志
        !in_array($this->lang, $this->user['langs']) && Util::errMsg(Util::tran('没有语言权限，请联系管理员'));
        $rowCount = Model::delete('tag', ['id' => $param['id']]);
        if ($rowCount) {
            // 删除缓存
            $this->clearCache($param['id']);
            $this->log(Util::tran('删除标签'), Util::tran('删除标签') . " " . implode(' ', $names) . " " . Util::tran('成功'), 0);
            Util::errMsg(['code' => 0, 'msg' => Util::tran('删除成功')]);
        } else {
            $this->log(Util::tran('删除标签'), Util::tran('删除标签') . " " . implode(' ', $names) . " " . Util::tran('失败'), 1);
            Util::errMsg(['code' => 1, 'msg' => Util::tran('删除失败')]);
        }
    }

    // 删除缓存
    private function clearCache($ids = 0)
    {
        if ($ids) {
            $ids = (array)$ids;
            $ids = array_unique($ids);
            $idStr = implode(',', $ids);
            $rows = Model::fetchAll("SELECT `id`, `slug` FROM `tag` WHERE `id` IN ($idStr)");
            $prefix = $this->base_url . ($this->lang === 'zh-CN' ? "/" : "/{$this->lang}/");
            foreach ($rows as $row) {
                $file_name = md5($prefix);
                $cache_file = IA_ROOT . '/mvc/tpl/' . substr($file_name, 0, 2) . '/' . $file_name . '.php';
                if (file_exists($cache_file))
                    unlink($cache_file);
                $file_name = md5($prefix . 'tag/' . ($row['slug'] ? $row['slug'] : $row['id']) . '.html');
                $cache_file = IA_ROOT . '/mvc/tpl/' . substr($file_name, 0, 2) . '/' . $file_name . '.php';
                if (file_exists($cache_file))
                    unlink($cache_file);
                // 删除分页缓存
                $count = Model::fetchColumn("SELECT COUNT(*) FROM `article` WHERE `tag_ids` like '%\"{$row['id']}\"%' AND `status`=0 ORDER BY `id` DESC");
                for ($i = 2; $i <= $count; $i++) {
                    $file_name = md5($prefix . 'tag/' . ($row['slug'] ? $row['slug'] : $row['id']) . '/page-' . $i . '.html');
                    $cache_file = IA_ROOT . '/mvc/tpl/' . substr($file_name, 0, 2) . '/' . $file_name . '.php';
                    file_exists($cache_file) && unlink($cache_file);
                }
            }
        }
    }
}
