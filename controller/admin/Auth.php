<?php

namespace controller\admin;

defined('IA_ROOT') || exit();

use facade\Model;
use facade\View;
use facade\Util;

// 权限菜单
class Auth extends Base
{
    // 列表
    public function index()
    {
        if (Util::isAjax()) {
            $auth = $this->auth;
            $auth = array_map(function ($item) {
                $item['time'] = date('Y-m-d H:i', $item['time']);
                return $item;
            }, $auth);
            $auth = Util::trees($auth);
            // 格式化菜单树
            $auth = array_map(function ($item) use ($auth) {
                $item['title'] = !array_filter($auth, function ($elem) use ($item) {
                    return $elem['pid'] === $item['id'];
                }) ? "┠ " . Util::tran($item['title']) : Util::tran($item['title']);
                return $item;
            }, $auth);
            exit(json_encode($auth));
        }
        View::assign('tran', [
            'delall' => Util::tran('批量删除'),
            'add' => Util::tran('添加'),
            'refresh' => Util::tran('刷新'),
            'expand_all' => Util::tran('全部展开'),
            'collapse_all' => Util::tran('全部折叠'),
            'menu_tree' => Util::tran('菜单树'),
            'controll' => Util::tran('控制器'),
            'action' => Util::tran('方法'),
            'sort' => Util::tran('排序'),
            'icon' => Util::tran('图标'),
            'menu' => Util::tran('菜单'),
            'status' => Util::tran('状态'),
            'time' => Util::tran('时间'),
            'option' => Util::tran('操作'),
            'add_sub' => Util::tran('添加下级'),
            'edit' => Util::tran('编辑'),
            'delete' => Util::tran('删除'),
            'choice_data' => Util::tran('请选择数据'),
            'edit_menu' => Util::tran('编辑菜单'),
            'delete_row' => Util::tran('真的删除行么？'),
            'info' => Util::tran('信息'),
        ]);
        View::display('admin/auth/index.html');
    }

    // 创建
    public function create()
    {
        // select无限下拉树
        $trees = Util::trees($this->auth);
        $trees = array_map(function ($item) {
            $item['title'] = $item['level'] ? str_repeat("&nbsp;&nbsp;&nbsp;", $item['level']) . "┠ " . Util::tran($item['title']) : Util::tran($item['title']);
            return $item;
        }, $trees);

        $pid = (int)Util::param('id');
        View::assign(compact('trees', 'pid'));
        View::assign('tran', [
            'menu_name' => Util::tran('菜单名称'),
            'choose' => Util::tran('请选择'),
            'input_menu_name' => Util::tran('请输入菜单名称'),
            'icon' => Util::tran('图标'),
            'parent_menu' => Util::tran('父级菜单'),
            'top_menu' => Util::tran('顶级菜单'),
            'controller' => Util::tran('控制器'),
            'action' => Util::tran('方法'),
            'sort' => Util::tran('排序'),
            'menu' => Util::tran('菜单'),
            'status' => Util::tran('状态'),
            'save' => Util::tran('保存'),
            'reset' => Util::tran('重置'),
        ]);
        View::display('admin/auth/create.html');
    }

    // 保存
    public function save()
    {
        $param = Util::param(['pid', 'title', 'icon', 'controller', 'action', 'sort', 'is_menu', 'status']);

        // 菜单名称重复验证
        $check = array_filter($this->auth, function ($item) use ($param) {
            return $param['title'] === $item['title'] && $param['pid'] == $item['pid'];
        });
        $check && Util::errMsg(Util::tran('菜单名称重复'));

        // 控制器方法重复验证
        if (!empty($param['controller']) && !empty($param['action'])) {
            $check = array_filter($this->auth, function ($item) use ($param) {
                return $param['controller'] === $item['controller'] && $param['action'] === $item['action'] && $param['pid'] != $item['id'];
            });
            $check && Util::errMsg(Util::tran('控制器与方法已存在'));
        }

        // 写入数据库记录日志
        $param['time'] = time();
        $lastInsertId = Model::insert('auth', $param);
        if ($lastInsertId) {
            $this->log(Util::tran('创建菜单'), Util::tran('创建菜单') . " {$param['title']} " . Util::tran('成功'), 0);
            Util::errMsg(['code' => 0, 'msg' => Util::tran('创建菜单成功')]);
        } else {
            $this->log(Util::tran('创建菜单'), Util::tran('创建菜单') . " {$param['title']} " . Util::tran('失败'), 1);
            Util::errMsg(['code' => 1, 'msg' => Util::tran('创建菜单失败')]);
        }
    }

    // 编辑
    public function edit($id = 0)
    {
        $auth = array_filter($this->auth, function ($item) use ($id) {
            return $id == $item['id'];
        });
        !$auth && Util::errMsg('ID ' . Util::tran('错误'));
        $auth = array_shift($auth);

        // select无限下拉树
        $trees = Util::trees($this->auth);
        $trees = array_map(function ($item) {
            $item['title'] = $item['level'] ? str_repeat("&nbsp;&nbsp;&nbsp;", $item['level']) . "┠ " . $item['title'] : $item['title'];
            return $item;
        }, $trees);

        View::assign(compact('auth', 'trees'));
        View::assign('tran', [
            'menu_name' => Util::tran('菜单名称'),
            'choose' => Util::tran('请选择'),
            'input_menu_name' => Util::tran('请输入菜单名称'),
            'icon' => Util::tran('图标'),
            'parent_menu' => Util::tran('父级菜单'),
            'top_menu' => Util::tran('顶级菜单'),
            'controller' => Util::tran('控制器'),
            'action' => Util::tran('方法'),
            'sort' => Util::tran('排序'),
            'menu' => Util::tran('菜单'),
            'status' => Util::tran('状态'),
            'save' => Util::tran('保存'),
            'reset' => Util::tran('重置'),
        ]);
        View::display('admin/auth/edit.html');
    }

    // 更新
    public function update()
    {
        $param = Util::param(['id', 'pid', 'title', 'icon', 'controller', 'action', 'sort', 'is_menu', 'status']);

        // ID合法性验证
        $auth = array_filter($this->auth, function ($item) use ($param) {
            return $param['id'] == $item['id'];
        });
        !$auth && Util::errMsg('ID ' . Util::tran('错误'));
        $auth = array_shift($auth);

        // 菜单名称重复验证
        $check = array_filter($this->auth, function ($item) use ($param) {
            return $param['title'] === $item['title'] && $param['pid'] == $item['pid'] && $param['id'] != $item['id'];
        });
        $check && ($check_ids = array_map(function ($item) {
            return $item['id'];
        }, $check)) && in_array($param['id'], $check_ids) && Util::errMsg(Util::tran('菜单名称重复'));

        // 控制器方法重复验证
        if (!empty($param['controller']) && !empty($param['action'])) {
            $check = array_filter($this->auth, function ($item) use ($param) {
                return $param['controller'] === $item['controller'] && $param['action'] === $item['action'] && $param['pid'] != $item['id'] && $param['id'] != $item['id'];
            });
            $check && Util::errMsg(Util::tran('控制器与方法已存在'));
        }

        // 更新数据库记录日志
        $rowCount = Model::update('auth', $param, ['id' => $param['id']]);
        if ($rowCount) {
            $this->log(Util::tran('编辑菜单'), Util::tran('编辑菜单') . " {$param['title']} " . Util::tran('成功'), 0);
            Util::errMsg(['code' => 0, 'msg' => Util::tran('编辑菜单成功')]);
        } else {
            $this->log(Util::tran('编辑菜单'), Util::tran('没有改变') . " {$param['title']} " . Util::tran('信息'), 0);
            Util::errMsg(['code' => 1, 'msg' => Util::tran('没有改变信息')]);
        }
    }

    // 修改属性
    public function modify()
    {
        $param = Util::param(['id', 'is_menu', 'status']);
        empty($param['id']) && Util::errMsg('ID ' . Util::tran('错误'));

        // ID合法性验证
        $auth = array_filter($this->auth, function ($item) use ($param) {
            return $param['id'] == $item['id'];
        });
        !$auth && Util::errMsg('ID ' . Util::tran('错误'));
        $auth = array_shift($auth);

        // 更新数据库记录日志
        $rowCount = Model::update('auth', $param, ['id' => $param['id']]);
        $modify_field = isset($param['is_menu']) ? 'is_menu' : 'status';
        $old_value = isset($param['is_menu']) ? $auth['is_menu'] : $auth['status'];
        $new_value = isset($param['is_menu']) ? $param['is_menu'] : $param['status'];
        if ($rowCount) {
            $this->log(Util::tran('修改属性'), Util::tran('修改属性') . " {$modify_field}: {$old_value} -> {$new_value} " . Util::tran('成功'), 0);
            Util::errMsg(['code' => 0, 'msg' => Util::tran('修改成功')]);
        } else {
            $this->log(Util::tran('修改属性'), Util::tran('修改属性') . " {$modify_field}: {$old_value} -> {$new_value} " . Util::tran('失败'), 1);
            Util::errMsg(['code' => 1, 'msg' => Util::tran('修改失败')]);
        }
    }

    // 删除
    public function delete()
    {
        $param = Util::param(['id']);
        (empty($param) || !isset($param['id'])) && Util::errMsg(Util::tran('请选择数据'));

        // 如果有子菜单
        $check = array_filter($this->auth, function ($item) use ($param) {
            return in_array(strval($item['pid']), (array)$param['id']);
        });
        $check && Util::errMsg(Util::tran('请先删除子菜单'));

        $auth = array_filter($this->auth, function ($item) use ($param) {
            return in_array(strval($item['id']), (array)$param['id']);
        });
        $authNames = array_map(function ($item) {
            $parent['title'] = '';
            if ($item['pid'] !== 0) {
                $parent = array_filter($this->auth, function ($elem) use ($item) {
                    return $elem['id'] === $item['pid'];
                });
                $parent = array_shift($parent);
            }
            return $parent['title'] . ' -> ' . $item['title'];
        }, $auth);

        // 删除数据记录日志
        $rowCount = Model::delete('auth', ['id' => $param['id']]);
        if ($rowCount) {
            $this->log(Util::tran('删除菜单'), Util::tran('删除菜单') . " " . implode(' ', $authNames) . " " . Util::tran('成功'), 0);
            Util::errMsg(['code' => 0, 'msg' => Util::tran('删除成功')]);
        } else {
            $this->log(Util::tran('删除菜单'), Util::tran('删除菜单') . " " . implode(' ', $authNames) . " " . Util::tran('失败'), 1);
            Util::errMsg(['code' => 1, 'msg' => Util::tran('删除失败')]);
        }
    }
}
