<?php

namespace controller\admin;

defined('IA_ROOT') || exit();

use facade\Model;
use facade\View;
use facade\Util;

// 链接管理
class Link extends Base
{
    // 列表
    public function index()
    {
        if (Util::isAjax()) {
            $param = Util::param();
            $start = ($param['page'] - 1) * $param['limit'];
            $limit = $param['limit'];
            $where = "";
            // 搜索
            if (!empty($param['search'])) {
                foreach ((array)$param['search'] as $k => $v) {
                    if (strlen($v)) {
                        if ($k == 'status') {
                            $where .= " AND `$k` = '$v'";
                        } else if (in_array($k, ['start', 'end'])) {
                            $timestamp = strtotime($v);
                            if ($k == 'start')
                                $where .= " AND `create_at` >= '$timestamp'";
                            else
                                $where .= " AND `create_at` <= '$timestamp'";
                        } else {
                            $where .= " AND `$k` like '%{$v}%'";
                        }
                    }
                }
            }
            $count = Model::fetchColumn("SELECT COUNT(*) FROM `link` WHERE 1=1 $where ORDER BY `id` DESC, `sort` DESC");
            $data = Model::fetchAll("SELECT * FROM `link` WHERE 1=1 $where ORDER BY `id` DESC, `sort` DESC LIMIT $start, $limit");
            if ($count) {
                $res['code'] = 0;
            } else {
                $res['code'] = 1;
                $res['msg'] = Util::tran('暂无记录');
            }
            $res['count'] = $count;
            $res['data'] = array_map(function ($item) {
                $item['url'] = '<a href="' . $item['url'] . '" title="' . $item['name'] . '" target="_blank">' . $item['url'] . '</a>';
                $item['logo'] = $item['logo'] ? '<img src="' . $item['logo'] . '" style="width:50px;height:100%" />' : '';
                $item['time'] = date('Y-m-d H:i', $item['time']);
                return $item;
            }, $data);
            exit(json_encode($res));
        }
        View::assign('tran', [
            'webname' => Util::tran('网站名称'),
            'input' => Util::tran('请输入'),
            'choose' => Util::tran('请选择'),
            'weburl' => Util::tran('网站地址'),
            'status' => Util::tran('状态'),
            'normal' => Util::tran('正常'),
            'disable' => Util::tran('禁用'),
            'date' => Util::tran('日期'),
            'delall' => Util::tran('批量删除'),
            'add' => Util::tran('添加'),
            'refresh' => Util::tran('刷新'),
            'link_list' => Util::tran('链接列表'),
            'sort' => Util::tran('排序'),
            'time' => Util::tran('时间'),
            'option' => Util::tran('操作'),
            'edit' => Util::tran('编辑'),
            'delete' => Util::tran('删除'),
        ]);
        View::display('admin/link/index.html');
    }

    // 创建
    public function create()
    {
        View::assign('tran', [
            'webname' => Util::tran('网站名称'),
            'input_webname' => Util::tran('请输入网站名称'),
            'weburl' => Util::tran('网站地址'),
            'input_weburl' => Util::tran('请输入网站地址'),
            'sort' => Util::tran('排序'),
            'input_sort' => Util::tran('请输入排序'),
            'status' => Util::tran('状态'),
            'save' => Util::tran('保存'),
            'reset' => Util::tran('重置'),
        ]);
        View::display('admin/link/create.html');
    }

    // 保存
    public function save()
    {
        $param = Util::param(['name', 'url', 'logo', 'sort', 'status']);
        (empty($param['name']) || empty($param['url'])) && Util::errMsg(Util::tran('网站名称或地址为空'));

        // 重复验证
        $check1 = Model::fetchColumn("SELECT `name` FROM `link` WHERE `name`=:name ORDER BY `id`", [':name' => $param['name']]);
        $check2 = Model::fetchColumn("SELECT `url` FROM `link` WHERE `url`=:url ORDER BY `id`", [':url' => $param['url']]);
        ($check1 || $check2) && Util::errMsg(Util::tran('网站名称或地址重名'));

        false === filter_var($param['url'], FILTER_VALIDATE_URL) && Util::errMsg(Util::tran('网址不合法'));

        if (!empty($param['logo'])) {
            !preg_match('#/static/upload/.+?\.[gif|jpg|jpeg|png|bmp|webp|psd|svg|tiff]#', $param['logo']) && Util::errMsg(Util::tran('图片格式错误'));
            $param['logo'] = str_replace($this->base_url, '', $param['logo']);
        }

        // 写入数据库记录日志
        $param['time'] = time();
        $lastInsertId = Model::insert('link', $param);
        if ($lastInsertId) {
            // 修改媒体状态
            $paths = [0 => [], 2 => []];
            if (preg_match('#/static/upload/.+?\.[gif|jpg|jpeg|png|bmp|webp|psd|svg|tiff]#', $param['url'])) {
                if (is_file(IA_ROOT . str_replace($this->base_url, '', $param['url'])))
                    $paths[0][] = str_replace($this->base_url, '', $param['url']);
                else
                    $paths[2][] = str_replace($this->base_url, '', $param['url']);
            }
            $paths[0] && Model::update('media', ['status' => 0], ['path' => $paths[0]]);
            $paths[2] && Model::update('media', ['status' => 2], ['path' => $paths[2]]);
            $this->log(Util::tran('创建链接'), Util::tran('创建链接') . " {$param['name']} " . Util::tran('成功'), 0);
            Util::errMsg(['code' => 0, 'msg' => Util::tran('创建链接成功'), 'url' => Util::url("/{$this->lang}/admin/link/index.html")]);
        } else {
            $this->log(Util::tran('创建链接'), Util::tran('创建链接') . " {$param['name']} " . Util::tran('失败'), 1);
            Util::errMsg(['code' => 1, 'msg' => Util::tran('创建链接失败')]);
        }
    }

    // 编辑
    public function edit($id = 0)
    {
        $row = Model::fetch("SELECT * FROM `link` WHERE `id`=:id", [':id' => $id]);
        !$row && Util::errMsg('ID ' . Util::tran('错误'));
        View::assign('row', $row);
        View::assign('tran', [
            'webname' => Util::tran('网站名称'),
            'input_webname' => Util::tran('请输入网站名称'),
            'weburl' => Util::tran('网站地址'),
            'input_weburl' => Util::tran('请输入网站地址'),
            'sort' => Util::tran('排序'),
            'input_sort' => Util::tran('请输入排序'),
            'status' => Util::tran('状态'),
            'save' => Util::tran('保存'),
            'reset' => Util::tran('重置'),
        ]);
        View::display('admin/link/edit.html');
    }

    // 更新
    public function update()
    {
        $param = Util::param(['id', 'name', 'url', 'logo', 'sort', 'status']);
        $row = Model::fetch("SELECT * FROM `link` WHERE `id`=:id", [':id' => $param['id']]);
        !$row && Util::errMsg('ID ' . Util::tran('错误'));
        (empty($param['name']) || empty($param['url'])) && Util::errMsg(Util::tran('网站名称或地址为空'));

        // 重复验证
        $check1 = Model::fetchColumn("SELECT `name` FROM `link` WHERE `name`=:name ORDER BY `id`", [':name' => $param['name']]);
        $check2 = Model::fetchColumn("SELECT `url` FROM `link` WHERE `url`=:url ORDER BY `id`", [':url' => $param['url']]);
        (($check1 && $check1 !== $row['name']) || ($check2 && $check2 !== $row['url'])) && Util::errMsg(Util::tran('网站名称或地址重名'));

        false === filter_var($param['url'], FILTER_VALIDATE_URL) && Util::errMsg(Util::tran('网址不合法'));

        if (!empty($param['logo'])) {
            !preg_match('#/static/upload/.+?\.[gif|jpg|jpeg|png|bmp|webp|psd|svg|tiff]#', $param['logo']) && Util::errMsg(Util::tran('图片格式错误'));
            $param['logo'] = str_replace($this->base_url, '', $param['logo']);
        }

        // 更新数据库记录日志
        $rowCount = Model::update('link', $param, ['id' => $param['id']]);

        // 修改媒体状态
        $paths = [0 => [], 1 => [], 2 => []];
        if (preg_match('#/static/upload/.+?\.[gif|jpg|jpeg|png|bmp|webp|psd|svg|tiff]#', $row['logo'])) {
            if (is_file(IA_ROOT . str_replace($this->base_url, '', $row['logo'])))
                $paths[1][] = str_replace($this->base_url, '', $row['logo']);
            else
                $paths[2][] = str_replace($this->base_url, '', $row['logo']);
        }
        if (preg_match('#/static/upload/.+?\.[gif|jpg|jpeg|png|bmp|webp|psd|svg|tiff]#', $param['logo'])) {
            if (is_file(IA_ROOT . str_replace($this->base_url, '', $param['logo'])))
                $paths[0][] = str_replace($this->base_url, '', $param['logo']);
            else
                $paths[2][] = str_replace($this->base_url, '', $param['logo']);
        }
        $paths[1] = array_filter($paths[1], function ($item) use ($paths) {
            return !in_array($item, $paths[0]);
        });
        $paths[1] && Model::update('media', ['status' => 1], ['path' => array_unique($paths[1])]);
        $paths[0] && Model::update('media', ['status' => 0], ['path' => array_unique($paths[0])]);
        $paths[2] && Model::update('media', ['status' => 2], ['path' => array_unique($paths[2])]);
        if ($rowCount) {
            $this->log(Util::tran('编辑链接'), Util::tran('编辑链接') . " {$row['name']} -> {$param['name']} " . Util::tran('成功'), 0);
            Util::errMsg(['code' => 0, 'msg' => Util::tran('编辑链接成功')]);
        } else {
            $this->log(Util::tran('编辑链接'), Util::tran('没有改变') . " {$row['name']} " . Util::tran('信息'), 0);
            Util::errMsg(['code' => 1, 'msg' => Util::tran('没有改变信息')]);
        }
    }

    // 修改属性
    public function modify()
    {
        $param = Util::param(['id', 'field', 'value']);
        empty($param['id']) && Util::errMsg('ID ' . Util::tran('错误'));
        $row = Model::fetch("SELECT * FROM `link` WHERE `id`=:id", [':id' => $param['id']]);
        !$row && Util::errMsg('ID ' . Util::tran('非法'));
        (empty($param['field']) || !in_array($param['field'], ['name', 'url', 'sort', 'status'])) && Util::errMsg(Util::tran('非法字段'));

        // 验证字段
        if ($param['field'] == 'name') {
            empty($param['value']) && Util::errMsg(Util::tran('链接名不能为空'));
            $check = Model::fetchColumn("SELECT `name` FROM `link` WHERE `name`=:name ORDER BY `id`", [':name' => $param['value']]);
            $check && $check !== $row['name'] && Util::errMsg(Util::tran('网站名称重名'));
        }
        if ($param['field'] == 'url') {
            false === filter_var($param['value'], FILTER_VALIDATE_URL) && Util::errMsg(Util::tran('网址不合法'));
            $check = Model::fetchColumn("SELECT `url` FROM `link` WHERE `url`=:url ORDER BY `id`", [':url' => $param['value']]);
            $check && $check !== $row['url'] && Util::errMsg(Util::tran('网站地址重复'));
        }

        // 更新数据库记录日志
        $rowCount = Model::update('link', [$param['field'] => $param['value']], ['id' => $param['id']]);
        if ($rowCount) {
            $this->log(Util::tran('修改属性'), Util::tran('修改属性') . " {$param['field']}: {$row[$param['field']]} -> {$param['value']} " . Util::tran('成功'), 0);
            Util::errMsg(['code' => 0, 'msg' => Util::tran('修改成功')]);
        } else {
            $this->log(Util::tran('修改属性'), Util::tran('修改属性') . " {$param['field']}: {$row[$param['field']]} -> {$param['value']} " . Util::tran('失败'), 1);
            Util::errMsg(['code' => 1, 'msg' => Util::tran('修改失败')]);
        }
    }

    // 删除
    public function delete()
    {
        $param = Util::param(['id']);
        (empty($param) || !isset($param['id'])) && Util::errMsg(Util::tran('请选择数据'));
        $idStr = implode(',', (array)$param['id']);
        $rows = Model::fetchAll("SELECT `name`, `logo` FROM `link` WHERE `id` IN ($idStr)");
        $names = [];
        $paths = [1 => [], 2 => []];
        foreach ($rows as $row) {
            $names[] = $row['name'];
            if (preg_match('#/static/upload/.+?\.[gif|jpg|jpeg|png|bmp|webp|psd|svg|tiff]#', $row['logo'])) {
                if (is_file(IA_ROOT . str_replace($this->base_url, '', $row['logo'])))
                    $paths[1][] = str_replace($this->base_url, '', $row['logo']);
                else
                    $paths[2][] = str_replace($this->base_url, '', $row['logo']);
            }
        }

        // 删除数据记录日志
        $rowCount = Model::delete('link', ['id' => $param['id']]);
        if ($rowCount) {
            // 修改媒体状态
            $paths[1] && Model::update('media', ['status' => 1], ['path' => array_unique($paths[1])]);
            $paths[2] && Model::update('media', ['status' => 2], ['path' => array_unique($paths[2])]);
            $this->log(Util::tran('删除链接'), Util::tran('删除链接') . " " . implode(' ', $names) . " " . Util::tran('成功'), 0);
            Util::errMsg(['code' => 0, 'msg' => Util::tran('删除成功')]);
        } else {
            $this->log(Util::tran('删除链接'), Util::tran('删除链接') . " " . implode(' ', $names) . " " . Util::tran('失败'), 1);
            Util::errMsg(['code' => 1, 'msg' => Util::tran('删除失败')]);
        }
    }
}
