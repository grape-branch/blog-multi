<?php

namespace controller\index;

defined('IA_ROOT') || exit();

use mvc\Controller;
use facade\Model;
use facade\View;
use facade\Util;

// 控制器
class Index extends Controller
{
    // 语言
    private $lang;
    private $langs;
    private $voice;
    private $replace_lang;
    private $replace_path;

    // 系统属性
    private $setting; //系统设置
    private $action; //获取方法
    private $param; //请求参数
    private $site_url; //站点地址
    private $login; //登录用户

    // 页面判断
    private $is_index; //是否首页
    private $is_category; //是否分类页
    private $is_tag; //是否标签页
    private $is_search; //是否搜索页
    private $is_detail; //是否详情页
    private $is_page; //是否页面
    private $is_404; //是否404页面

    // 分类数据
    private $categories = []; //全部分类
    private $categories_tree = []; //分类树
    private $category = []; //当前分类
    private $category_id = 0; //当前分类id

    // 标签数据
    private $tags = []; //全部标签
    private $tag = []; //当前标签
    private $tag_id = 0; //当前标签id

    // 热门文章
    private $hots = []; //热门文章

    // 文章|页面数据
    private $pages = []; //全部页面
    private $pages_tree = []; //页面树
    private $page = []; //当前页面
    private $page_id = 0; //当前页面id
    private $article; //当前文章
    private $article_id = 0; //当前文章id
    private $article_views = 0; //当前文章点击数
    private $article_likes = 0; //当前文章点赞数
    private $download = ''; //文章下载

    // 公共数据
    private $q = ''; //搜索词
    private $p = 1; //分页
    private $page_suffix = ''; //分页后缀
    private $start = 0; //第n页开始数量
    private $limit = 10; //分页数量

    // 布局数据
    private $header = []; //头部
    private $main = []; //主体
    private $aside = []; //侧栏
    private $footer = []; //底部

    // 压缩css|js
    private $css = []; //css
    private $css_url = '';
    private $js = []; //js
    private $js_url = '';

    // 初始化
    protected function initialize()
    {
        // 语言
        !session_id() && session_start();
        $this->lang = Util::lang();
        $this->langs = array_map(function ($item) {
            $item['title'] = Util::tran($item['title']);
            return $item;
        }, Util::langs());
        $this->voice = Util::voice();
        $this->replace_lang = $this->lang == 'zh-TW' ? 'zh-CN' : $this->lang;
        $this->replace_path = $this->lang == 'zh-CN' ? '' : $this->lang . '/';

        // 系统属性
        $this->site_url = rtrim(Util::url('/'), '/'); //站点地址
        $this->setting = $this->setting(); //系统设置
        $this->action = Util::action(); //获取方法
        $this->param = Util::param(); //请求参数
        $this->login = $this->checkLogin(); //登录用户

        // 页面判断
        $this->is_index = 'index' == $this->action; //是否首页
        $this->is_category = 'category' == $this->action; //是否分类页
        $this->is_tag = 'tag' == $this->action; //是否标签页
        $this->is_search = 'search' == $this->action; //是否搜索页
        $this->is_detail = 'detail' == $this->action; //是否详情页
        $this->is_page = 'page' == $this->action; //是否页面
        $this->is_404 = !$this->is_index //是否404页面
            && !$this->is_category
            && !$this->is_tag
            && !$this->is_search
            && !$this->is_detail
            && !$this->is_page;

        // 压缩css|js
        $this->css = [
            isset($this->setting['cdn_speed']) && $this->setting['cdn_speed'] ? 'https://fastly.jsdelivr.net/npm/bootstrap@3.3.6/dist/css/bootstrap.min.css' : 'res/bootstrap/css/bootstrap.min.css',
            isset($this->setting['cdn_speed']) && $this->setting['cdn_speed'] ? 'https://fastly.jsdelivr.net/npm/kindeditor@4.1.10/plugins/code/prettify.css' : 'res/kindeditor/plugins/code/prettify.css',
            isset($this->setting['cdn_speed']) && $this->setting['cdn_speed'] ? 'https://fastly.jsdelivr.net/npm/social-share.js@1.0.16/dist/css/share.min.css' : 'css/share.min.css',
            'css/custom.css'
        ];
        $this->css_url = '';
        $this->js = [
            isset($this->setting['cdn_speed']) && $this->setting['cdn_speed'] ? 'https://fastly.jsdelivr.net/npm/jquery@1.12.3/dist/jquery.min.js' : 'js/jquery-1.12.3.min.js',
            isset($this->setting['cdn_speed']) && $this->setting['cdn_speed'] ? 'https://fastly.jsdelivr.net/npm/bootstrap@3.3.6/dist/js/bootstrap.min.js' : 'res/bootstrap/js/bootstrap.min.js',
            isset($this->setting['cdn_speed']) && $this->setting['cdn_speed'] ? 'https://fastly.jsdelivr.net/npm/kindeditor@4.1.10/plugins/code/prettify.js' : 'res/kindeditor/plugins/code/prettify.js',
            isset($this->setting['cdn_speed']) && $this->setting['cdn_speed'] ? 'https://fastly.jsdelivr.net/npm/social-share.js@1.0.16/dist/js/social-share.min.js' : 'js/social-share.min.js',
            'js/custom.js'
        ];
        $this->js_url = '';
        $this->compress();

        // 本地化公共语言串
        View::assign('lang', $this->lang);
        View::assign('langs', $this->langs);
        View::assign('voice', $this->voice);
        View::assign('localize', [
            'language' => Util::tran('语言'),
            'choose_language' => Util::tran('选择语言'),
            'mine' => Util::tran('我的'),
            'articles' => Util::tran('篇文章'),
            'no_article' => Util::tran('还没有文章'),
            'no_page' => Util::tran('还没有页面'),
            'no_category' => Util::tran('还没有分类'),
            'category_empty' => Util::tran('分类为空'),
            'no_tag' => Util::tran('还没有标签'),
            'no_hot' => Util::tran('还没有热门'),
            'no_update' => Util::tran('还没有更新'),
            'no_comment' => Util::tran('还没有评论'),
            'sure' => Util::tran('确定'),
            'info' => Util::tran('信息'),
            'anonymous' => Util::tran('匿名'),
            'say' => Util::tran('说道'),
            'delete' => Util::tran('删除'),
            'friend_link' => Util::tran('友情链接'),
            'login' => Util::tran('登录'),
            'switch_login' => Util::tran('切换登录'),
            'register' => Util::tran('注册'),
            'logout' => Util::tran('登出'),
            'switch_register' => Util::tran('切换注册'),
            'repeat_password' => Util::tran('重复密码'),
            'username' => Util::tran('用户名'),
            'input_username' => Util::tran('请输入用户名'),
            'password' => Util::tran('密码'),
            'input_password' => Util::tran('请输入密码'),
            'input_repeat_password' => Util::tran('请重输入密码'),
            'nickname' => Util::tran('昵称'),
            'phone' => Util::tran('手机'),
            'optional' => Util::tran('可选'),
            'remember_me' => Util::tran('记住我'),
            'home' => Util::tran('首页'),
            'console' => Util::tran('控制台'),
            'search' => Util::tran('搜索'),
            'logining' => Util::tran('登录中'),
            'registering' => Util::tran('注册中'),
            'redirecting' => Util::tran('跳转中'),
            'merging' => Util::tran('正在合成，请稍后'),
            'merge_success' => Util::tran('合成成功！再次点击按钮下载。'),
            'favorite_article' => Util::tran('收藏文章'),
            'favorite_success' => Util::tran('收藏成功'),
            'favorite_cancel' => Util::tran('取消收藏'),
            'submiting' => Util::tran('提交中'),
            'say' => Util::tran('说道'),
            'reply' => Util::tran('回复'),
            'reply_comment' => Util::tran('回复评论'),
            'cancel_reply' => Util::tran('取消回复'),
            'send_comment' => Util::tran('发表评论'),
            'no_comment' => Util::tran('还没有评论'),
            'pending' => Util::tran('审核中'),
            'waiting' => Util::tran('请稍后'),
            'audio_play' => Util::tran('语音播放'),
            'back_top' => Util::tran('回到顶部'),
            'voice_read_error' => Util::tran('朗读错误，请联网后再试'),
            'voice_not_support' => Util::tran('语音不支持，需科学上网使用谷歌接口。'),
        ]);
    }

    // 私有方法
    private function properties($page = 0)
    {
        // 分类数据
        $data = Model::fetchAll("SELECT * FROM `category` WHERE `status` = 0 AND `lang` = '{$this->replace_lang}' ORDER BY `id` ASC, `sort` DESC");
        $this->categories = array_map(function ($item) {
            $item['link'] = "{$this->site_url}/{$this->replace_path}category/" . ($item['slug'] ? $item['slug'] : $item['id']) . ".html";
            $item['cover'] = $item['cover'] ? "{$this->site_url}{$item['cover']}" : '';
            return $item;
        }, $data); //全部分类
        $this->categories_tree = Util::levels($this->categories); //分类树

        // 标签数据
        $data = Model::fetchAll("SELECT * FROM `tag` WHERE `status` = 0 AND `lang` = '{$this->replace_lang}' ORDER BY `sort` DESC, `id` DESC");
        $this->tags = array_map(function ($item) {
            $item['link'] = "{$this->site_url}/{$this->replace_path}tag/" . ($item['slug'] ? $item['slug'] : $item['id']) . ".html";
            return $item;
        }, $data); //全部标签

        // 热门文章
        $data = Model::fetchAll("SELECT * FROM `article` WHERE `status` = 0 AND `lang` = '{$this->replace_lang}' AND `likes` > 0 ORDER BY `likes` DESC LIMIT {$this->setting['hot_num']}");
        $this->hots = array_map(function ($item) {
            $item['link'] = "{$this->site_url}/{$this->replace_path}" . ($item['slug'] ? $item['slug'] : $item['id']) . ".html";
            return $item;
        }, $data); //全部热门文章

        // 页面数据
        $data = Model::fetchAll("SELECT * FROM `page` WHERE `status` = 0 AND `lang` = '{$this->replace_lang}' ORDER BY `id` ASC, `sort` DESC");
        $this->pages = array_map(function ($item) {
            $item['link'] = "{$this->site_url}/{$this->replace_path}page/" . ($item['slug'] ? $item['slug'] : $item['id']) . ".html";
            $item['name'] = $item['title'];
            return $item;
        }, $data); //全部页面
        $this->pages = array_filter($this->pages, function ($item) {
            return $item['is_nav'] == '0';
        });
        $this->pages_tree = Util::levels($this->pages); //页面树

        // 公共数据
        $this->q = isset($this->param['q']) ? stripslashes($this->param['q']) : ''; //搜索词
        $this->p = $page ? max((int)$page, 1) : 1; //分页
        $this->page_suffix = $this->p > 1 ? Util::tran('页码') . "{$this->p}" : '';
        $this->start = ($this->p - 1) * $this->limit; //第n页开始数

        // 布局数据
        $this->header = [
            'title' => [$this->setting['webName']], //SEO标题
            'description' => [], //SEO描述
            'og_tags' => [
                'og:type' => 'website',
                'og:title' => $this->setting['webName'],
                'og:description' => '',
                'og:url' => $this->site_url . '/' . $this->replace_path,
                'og:locale' => preg_replace('/-/', '_', $this->lang),
                'og:site_name' => $this->setting['webName'],
                'og:image' => $this->setting['webLogo'],
            ], // OG标签
            'nav' => $this->categories_tree, //导航
            'page_nav' => $this->pages_tree, //页面导航
            'crumbs' => [['name' => Util::tran('首页'), 'link' => $this->site_url . '/' . $this->replace_path]], //面包屑导航
        ]; //头部
        $this->main = []; //主体
        $this->aside = [
            'categories' => ['list' => $this->categories, 'title' => Util::tran('分类目录')],
            'tags' => ['list' => $this->tags, 'title' => Util::tran('标签')],
            'pages' => ['list' => $this->pages, 'title' => Util::tran('页面')],
            'hot' => ['list' => $this->hots, 'title' => Util::tran('热门文章')],
            'category_id' => $this->category_id,
            'tag_id' => $this->tag_id,
            'article_id' => $this->article_id,
            'page_id' => $this->page_id,
        ]; //侧栏
        $this->footer = []; //底部
    }

    // 系统设置
    private function setting($field = '')
    {
        static $setting;
        $default = [
            'webLogo' => '/static/images/brand.png',
            'webName' => 'Brand',
            'seo_title' => '',
            'seo_description' => '',
            'upload_type' => 'gif|jpg|jpeg|png|mp3|mp4|zip',
            'upload_size' => '2048',
            'robot_index' => '0',
            'alone_login' => '0',
            'copyright' => '&copy; 2021-' . date('Y') . ' Company, Inc.',
            'icp' => 'ICP No.001',
            'slide_image_1' => '',
            'slide_url_1' => '',
            'slide_image_2' => '',
            'slide_url_2' => '',
            'slide_image_3' => '',
            'slide_url_3' => '',
            'slide_image_4' => '',
            'slide_url_4' => '',
            'slide_image_5' => '',
            'slide_url_5' => '',
            'hot_num' => '5',
            'comm_num' => '5',
            'related_num' => '3',
            'page_num' => '24',
            'link_num' => 8,
            'voice_read' => '1',
            'can_comment' => '1',
            'pending_comment' => '1',
            'cdn_speed' => '0'
        ];
        if ($field == 'default')
            return $default;
        if ($setting === null) {
            $setting = Model::fetch('SELECT * FROM `setting` WHERE id=:id', [':id' => 1]);
            if ($setting) {
                $setting = json_decode($setting['value'], true);
                $setting = array_intersect_key($setting, $default);
                $setting = array_merge($default, $setting);
            } else {
                $setting = $default;
            }
            if (isset($setting['webLogo']) && $setting['webLogo'] && preg_match('#/static/#', $setting['webLogo'])) {
                $setting['webLogo'] = $this->site_url . str_replace($this->site_url, '', $setting['webLogo']);
            }
            $setting['webName'] = Util::tran($setting['webName']);
            $setting['seo_title'] = Util::tran($setting['seo_title']);
            $setting['seo_description'] = Util::tran($setting['seo_description']);
            $setting['copyright'] = Util::tran($setting['copyright']);
            $setting['icp'] = Util::tran($setting['icp']);
        }
        if ($field) {
            return $setting[$field];
        }
        return $setting;
    }

    // 分页数据
    private function pager($count, $p = 0, $base = '', $query = [], $is_home = false, $anchor = '')
    {
        $total = ceil($count / $this->limit);
        $list = [];
        $prev = max($p - 1, 1);
        $next = min($p + 1, max($total, 1));
        if ($p <= 2) {
            $begin = 1;
            $end = min($total, 5);
        } else {
            $end = min($p + 2, $total);
            $begin = max($end - 4, 1);
        }
        $list[] = ['title' => Util::tran("上一页"), 'p' => $prev, 'link' => $this->site_url . "/{$base}" . ($prev == 1 ? "" : (($base ? "/" : "") . "page-{$prev}"))];
        for ($i = $begin; $i <= $end; $i++) {
            $list[] = ['title' => Util::tran("页码") . "{$i}", 'p' => $i, 'link' => $this->site_url . "/{$base}" . ($i == 1 ? "" : (($base ? "/" : "") . "page-{$i}"))];
        }
        $list[] = ['title' => Util::tran("下一页"), 'p' => $next, 'link' => $this->site_url . "/{$base}" . ($next == 1 ? "" : (($base ? "/" : "") . "page-{$next}"))];
        $list = array_map(function ($item) use ($query, $is_home, $anchor) {
            $item['link'] .= ($item['p'] == 1 && $is_home ? "" : ".html") . ($query ? "?" . implode("&", $query) : "") . $anchor;
            return $item;
        }, $list);
        return $list;
    }

    // 模板赋值
    private function vars()
    {
        $vars = get_class_vars(__CLASS__);
        array_walk($vars, function (&$value, $key) {
            $value = $this->$key;
        });
        return $vars;
    }

    // css|js压缩
    private function compress()
    {
        if (!file_exists(IA_ROOT . '/static/upload/style.css') || filemtime(IA_ROOT . '/static/upload/style.css') < filemtime(IA_ROOT . '/static/css/custom.css')) {
            $res = [];
            foreach ($this->css as $src) {
                if (is_file(IA_ROOT . '/static/' . $src)) {
                    $content = file_get_contents(IA_ROOT . '/static/' . $src);
                    if (preg_match('#^res/bootstrap#', $src)) {
                        $content = str_replace('../fonts', '../res/bootstrap/fonts', $content);
                    }
                    $res[] = trim($content);
                }
            }
            $res && file_put_contents(IA_ROOT . '/static/upload/style.css', $this->compressCss(join('', $res))) && Util::delDir(IA_ROOT . '/mvc/tpl/', true);
        }
        if (!file_exists(IA_ROOT . '/static/upload/script.js') || filemtime(IA_ROOT . '/static/upload/script.js') < filemtime(IA_ROOT . '/static/js/custom.js')) {
            $res = [];
            foreach ($this->js as $src) {
                if (is_file(IA_ROOT . '/static/' . $src)) {
                    $content = file_get_contents(IA_ROOT . '/static/' . $src);
                    $res[] = trim(trim($content, ';'));
                }
            }
            $res && file_put_contents(IA_ROOT . '/static/upload/script.js', $this->compressJs(join(';', $res))) && Util::delDir(IA_ROOT . '/mvc/tpl/', true);
        }
        if (is_file(IA_ROOT . '/static/upload/style.css')) {
            $this->css_url = $this->site_url . '/static/upload/style.css?t=' . filemtime(IA_ROOT . '/static/upload/style.css');
        }
        if (is_file(IA_ROOT . '/static/upload/script.js')) {
            $this->js_url = $this->site_url . '/static/upload/script.js?t=' . filemtime(IA_ROOT . '/static/upload/script.js');
        }
    }

    // 压缩css
    private function compressCss($css)
    {
        require_once IA_ROOT . '/util/CSSmin.php';
        return trim((new \CSSmin())->run($css));
    }

    // 压缩js
    private function compressJS($js)
    {
        require_once IA_ROOT . '/util/JSMin.php';
        return trim(\JSMin::minify($js));
    }

    // 文章数据
    private function data($data)
    {
        $data = array_map(function ($item) {
            $item['link'] = $this->site_url . "/{$this->replace_path}" . ($this->is_page ? 'page/' : '') . ($item['slug'] ? $item['slug'] : $item['id']) . '.html';
            // 如果没有描述，尝试截取内容中的描述
            if (isset($item['description']) && !$item['description']) {
                $item['description'] = mb_strimwidth(trim(preg_replace('/\s+/', ' ', str_replace('"', '', strip_tags($item['content'])))), 0, 140, '...', 'UTF-8');
            }
            // SEO简单优化图片和链接
            if (isset($item['content']) && ($n = preg_match_all('#<[^>]*[href|src]=["\']([^"\']*/.+?[^"\']*)["\'][^>]*>#i', $item['content'], $matches))) {
                $pImg = 0;
                $pA = 0;
                for ($i = 0; $i < $n; $i++) {
                    // 替换相对路径为绝对路径
                    $url = $this->site_url . '/' . ltrim(str_replace($this->site_url, '/', $matches[1][$i]), '/');
                    if (!preg_match('#^http[s]*://#', $matches[1][$i]) && false === strpos($matches[1][$i], $this->site_url)) {
                        $replace = str_replace($matches[1][$i], $url, $matches[0][$i]);
                        $item['content'] = str_replace($matches[0][$i], $replace, $item['content']);
                        $matches[0][$i] = $replace;
                    }
                    // 优化图片alt
                    if (preg_match('#(<img)([^>]*>)#', $matches[0][$i], $match)) {
                        // 有alt情况
                        $pImg++;
                        $attr = $item['title'] . ($pImg > 1 ? "-" . Util::tran('图片') . "{$pImg}" : "");
                        if (preg_match('#<img[^>]*alt=["\'][^"\']*["\'][^>]*>#', $matches[0][$i])) {
                            // 如果alt为空
                            if (preg_match('#(<img[^>]*alt=["\'])\s*(["\'][^>]*>)#', $matches[0][$i], $match2)) {
                                $replace = $match2[1] . $attr . $match2[2];
                                $item['content'] = str_replace($matches[0][$i], $replace, $item['content']);
                                $matches[0][$i] = $replace;
                            }
                        } else {
                            // 没有alt情况
                            $replace = $match[1] . ' alt="' . $attr . '"' . $match[2];
                            $item['content'] = str_replace($matches[0][$i], $replace, $item['content']);
                            $matches[0][$i] = $replace;
                        }
                    }
                    // 优化图片title
                    if (preg_match('#(<img)([^>]*>)#', $matches[0][$i], $match)) {
                        // 有title情况
                        if (preg_match('#<img[^>]*title=["\'][^"\']*["\'][^>]*>#', $matches[0][$i])) {
                            // 如果title为空
                            if (preg_match('#(<img[^>]*title=["\'])\s*(["\'][^>]*>)#', $matches[0][$i], $match2)) {
                                $replace = $match2[1] . $attr . $match2[2];
                                $item['content'] = str_replace($matches[0][$i], $replace, $item['content']);
                                $matches[0][$i] = $replace;
                            }
                        } else {
                            // 没有title情况
                            $replace = $match[1] . ' title="' . $attr . '"' . $match[2];
                            $item['content'] = str_replace($matches[0][$i], $replace, $item['content']);
                            $matches[0][$i] = $replace;
                        }
                    }
                    // 图片限定有效宽度
                    if (preg_match('#(<img)([^>]*>)#', $matches[0][$i], $match) && !preg_match('#<img[^>]*src=["\'][^"\']*/static/res/.+?\.[^"\']*["\'][^>]*>#', $matches[0][$i])) {
                        // 有class情况
                        if (preg_match('#(<img[^>]*class=["\'])([^"\']*)(["\'][^>]*>)#', $matches[0][$i], $match2)) {
                            // 如果没有thumbnail类
                            if (false === strpos($match2[2], 'thumbnail')) {
                                $replace = $match2[1] . ($match2[2] ? $match2[2] . ' thumbnail' : 'thumbnail') . $match2[3];
                                $item['content'] = str_replace($matches[0][$i], $replace, $item['content']);
                                $matches[0][$i] = $replace;
                            }
                        } else {
                            // 没有class情况
                            $replace = $match[1] . ' class="thumbnail"' . $match[2];
                            $item['content'] = str_replace($matches[0][$i], $replace, $item['content']);
                            $matches[0][$i] = $replace;
                        }
                    }
                    // 剔除属性设置
                    if (preg_match('#(<img)([^>]*>)#', $matches[0][$i], $match)) {
                        // 剔除宽度
                        if (preg_match('#(<img[^>]*)(width=["\'][^"\']*["\'])([^>]*>)#', $matches[0][$i], $match2)) {
                            $replace = $match2[1] . $match2[3];
                            $item['content'] = str_replace($matches[0][$i], $replace, $item['content']);
                            $matches[0][$i] = $replace;
                        }
                        // 剔除高度
                        if (preg_match('#(<img[^>]*)(height=["\'][^"\']*["\'])([^>]*>)#', $matches[0][$i], $match2)) {
                            $replace = $match2[1] . $match2[3];
                            $item['content'] = str_replace($matches[0][$i], $replace, $item['content']);
                            $matches[0][$i] = $replace;
                        }
                        // 剔除align
                        if (preg_match('#(<img[^>]*)(align=["\'][^"\']*["\'])([^>]*>)#', $matches[0][$i], $match2)) {
                            $replace = $match2[1] . $match2[3];
                            $item['content'] = str_replace($matches[0][$i], $replace, $item['content']);
                            $matches[0][$i] = $replace;
                        }
                    }
                    // 添加最大宽度
                    if (preg_match('#(<img)([^>]*>)#', $matches[0][$i], $match)) {
                        // 有style情况
                        if (preg_match('#(<img[^>]*style=["\'])([^"\']*)(["\'][^>]*>)#', $matches[0][$i], $match2)) {
                            // 如果没有设置max-width
                            if (false === strpos($match2[2], 'max-width:')) {
                                $replace = $match2[1] . ($match2[2] ? (substr($match2[2], -1) == ';' ? $match2[2] . 'max-width:100%' : $match2[2] . ';max-width:100%') : 'max-width:100%') . $match2[3];
                                $item['content'] = str_replace($matches[0][$i], $replace, $item['content']);
                                $matches[0][$i] = $replace;
                            }
                        } else {
                            // 没有style情况
                            $replace = $match[1] . ' style="max-width:100%"' . $match[2];
                            $item['content'] = str_replace($matches[0][$i], $replace, $item['content']);
                            $matches[0][$i] = $replace;
                        }
                    }
                    // 优化连接title
                    if (preg_match('#(<a)([^>]*>)#', $matches[0][$i], $match)) {
                        // 有title情况
                        $pA++;
                        $attr = $item['title'] . ($pA > 1 ? "-" . Util::tran('个数') . "{$pA}" : "");
                        if (preg_match('#<a[^>]*title=["\'][^"\']*["\'][^>]*>#', $matches[0][$i])) {
                            // 如果title为空
                            if (preg_match('#(<a[^>]*title=["\'])\s*(["\'][^>]*>)#', $matches[0][$i], $match2)) {
                                $replace = $match2[1] . $attr . $match2[2];
                                $item['content'] = str_replace($matches[0][$i], $replace, $item['content']);
                                $matches[0][$i] = $replace;
                            }
                        } else {
                            // 没有title情况
                            $replace = $match[1] . ' title="' . $attr . '"' . $match[2];
                            $item['content'] = str_replace($matches[0][$i], $replace, $item['content']);
                            $matches[0][$i] = $replace;
                        }
                    }
                    // 如果没有封面，尝试获取内容中图片
                    if (isset($item['cover']) && !$item['cover'] && preg_match('#<img[^>]*src=["\']([^"\']*/static/upload.+?\.[^"\']*)["\'][^>]*>#i', $matches[0][$i], $match)) {
                        $item['cover'] = $match[1];
                    }
                    // 图片懒加载
                    if (isset($this->setting['cdn_speed']) && $this->setting['cdn_speed'] == '1' && preg_match('#<img([^>]*)src=(["\'])([^"\']*/static/upload.+?\.[^"\']*)(["\'])([^>]*)>#i', $matches[0][$i], $match)) {
                        $item['content'] = str_replace($matches[0][$i], '<img' . $match[1] . 'data-src=' . $match[2] . $match[3] . $match[4] . ' src=' . $match[2] . 'data:image/gif;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAADUlEQVQImWNgYGBgAAAABQABh6FO1AAAAABJRU5ErkJggg==' . $match[4] . $match[5] . '>', $item['content']);
                    }
                }
            }
            return $item;
        }, $data);
        return $data;
    }

    // 访问类中不存在的方法时抛出404
    public function __call($name, $arguments)
    {
        View::display('404.html');
    }

    // 首页
    public function index($page = 0)
    {
        $this->properties($page);

        // 头部
        if ($this->setting['seo_title']) array_unshift($this->header['title'], $this->setting['seo_title']);
        if ($this->setting['seo_description']) array_unshift($this->header['description'], $this->setting['seo_description']);
        $this->header['og_tags']['og:type'] = 'website';
        $this->header['og_tags']['og:title'] = implode('-', $this->header['title']);
        $this->header['og_tags']['og:description'] = implode('-', $this->header['description']);
        array_push($this->header['crumbs'], ['name' => $this->setting['webName']]);

        // 主体
        $count = Model::fetchColumn("SELECT COUNT(*) FROM `article` WHERE `status` = 0 AND `lang` = '{$this->replace_lang}' ORDER BY `id` DESC");
        $data = Model::fetchAll("SELECT * FROM `article` WHERE `status` = 0 AND `lang` = '{$this->replace_lang}' ORDER BY `id` DESC LIMIT $this->start, $this->limit");
        $this->main = array_merge(
            $this->main,
            ['title' => Util::tran('最新文章') . ($this->page_suffix ? "({$this->page_suffix})" : '')],
            ['list' => $this->data($data)],
            ['count' => $count],
            ['pager' => $this->pager($count, $this->p, '', [], true)]
        );

        // 侧栏
        $data = Model::fetchAll("SELECT * FROM `comment` WHERE `pid` = 0 AND `status` = 0 AND `lang` = '{$this->replace_lang}' ORDER BY `sort` DESC, `id` DESC LIMIT {$this->setting['comm_num']}");
        $authors = [];
        $aids = [];
        foreach ($data as $item) {
            $authors[] = $item['author'];
            $aids[] = $item['article_id'];
        }
        $authors = array_unique($authors);
        $authors = implode("','", $authors);
        $users = Model::fetchAll("SELECT * FROM `user` WHERE `username` IN ('$authors')");
        $aids = array_unique($aids);
        $aids = implode(',', $aids);
        if ($aids) {
            $articles = Model::fetchAll("SELECT * FROM `article` WHERE `status` = 0 AND `id` IN ($aids) ORDER BY `id`");
        } else {
            $articles = [];
        }
        $data = array_map(function ($item) use ($users, $articles) {
            // 评论图像
            $item['avatar'] = '';
            if ($user = array_filter($users, function ($elem) use ($item) {
                return $elem['username'] == $item['author'];
            })) {
                $user = array_values($user);
                $item['nickname'] = $user[0]['nickname'] ?: $user[0]['username'];
                $item['avatar'] = $user[0]['avatar'] ? $this->site_url . '/' . ltrim($user[0]['avatar'], '/') : '';
            }
            // 被评论的文章
            $item['article_title'] = $item['author'];
            $item['article_link'] = '#';
            if ($article = array_filter($articles, function ($elem) use ($item) {
                return $elem['id'] == $item['article_id'];
            })) {
                $article = array_values($article);
                $item['article_title'] = $article[0]['title'];
                $item['article_link'] = $this->site_url . "/{$this->replace_path}" . ($article[0]['slug'] ? $article[0]['slug'] : $article[0]['id']) . '.html';
            }
            return $item;
        }, $data);
        $this->aside['comment'] = ['list' => $data, 'title' => Util::tran('最新评论')];

        // 底部
        $data = Model::fetchAll("SELECT * FROM `link` WHERE `status` = 0 ORDER BY `id` ASC, `sort` DESC LIMIT {$this->setting['link_num']}");
        $this->footer = array_merge($this->footer, ['link' => $data]);
        View::assign($this->vars());
        View::display('index/index.html');
    }

    // 分类页
    public function category($id_or_slug, $page = 0)
    {
        $this->properties($page);
        $rows = array_filter($this->categories, function ($item) use ($id_or_slug) {
            return is_numeric($id_or_slug) ? $item['id'] == $id_or_slug : $item['slug'] == $id_or_slug;
        });
        $rows = array_values($rows);
        $cid = $rows ? $rows[0]['id'] : 0;
        $this->category = $rows ? $rows[0] : [];
        $this->category_id = $rows ? $rows[0]['id'] : 0;

        // 相册分页
        if ($this->category && $this->category['is_gallery']) {
            $this->limit = $this->setting['page_num'];
            $this->start = ($this->p - 1) * $this->limit; //第n页开始数
        }

        // 头部
        $this->page_suffix && array_unshift($this->header['title'], $this->page_suffix);
        array_unshift($this->header['title'], $rows ? ($rows[0]['seo_title'] ? $rows[0]['seo_title'] : $rows[0]['name']) : Util::tran('未分类'));
        array_unshift($this->header['description'], $rows ? ($rows[0]['seo_description'] ? strip_tags($rows[0]['seo_description']) : strip_tags($rows[0]['description'])) : '');
        $this->page_suffix && array_unshift($this->header['description'], $this->page_suffix);
        $this->header['og_tags']['og:type'] = 'website';
        $this->header['og_tags']['og:title'] = implode('-', $this->header['title']);
        $this->header['og_tags']['og:description'] = implode('-', $this->header['description']);
        $this->header['og_tags']['og:url'] = $this->site_url . "/{$this->replace_path}category/" . ($this->p == 1 ? "{$id_or_slug}.html" : "{$id_or_slug}/page-{$this->p}.html");
        if (is_file(str_replace($this->site_url, IA_ROOT, isset($this->category['cover']) ? $this->category['cover'] : ''))) {
            list($width, $height) = getimagesize(str_replace($this->site_url, IA_ROOT, $this->category['cover']));
            $this->header['og_tags']['og:image'] = $this->category['cover'];
            $this->header['og_tags']['og:image:width'] = $width;
            $this->header['og_tags']['og:image:height'] = $height;
            $this->header['og_tags']['og:image:alt'] = strip_tags($this->category['name']);
        }
        $parents = Util::parents($this->categories, $cid);
        $parents = array_reverse($parents);
        $ids = [];
        foreach ($parents as $item) {
            $ids[] = $item['id'];
            if (count($parents) == count($ids))
                continue;
            array_push($this->header['crumbs'], ['link' => $item['link'], 'name' => $item['name']]);
        }
        array_push($this->header['crumbs'], ['name' => $rows ? $rows[0]['name'] : Util::tran('未分类')]);

        // 主体
        $childs = Util::trees($this->categories, $cid);
        $cids = array_map(function ($item) {
            return $item['id'];
        }, $childs);
        array_unshift($cids, $cid);
        $cidStr = implode(',', $cids);
        $count = Model::fetchColumn("SELECT COUNT(*) FROM `article` WHERE `status` = 0 AND `category_id` IN ($cidStr) ORDER BY `id` DESC");
        $data = Model::fetchAll("SELECT * FROM `article` WHERE `status` = 0 AND `category_id` IN ($cidStr) ORDER BY `id` DESC LIMIT $this->start, $this->limit");
        $this->main = array_merge(
            $this->main,
            ['title' => ($rows ? $rows[0]['name'] : Util::tran('未分类')) . ($this->page_suffix ? "({$this->page_suffix})" : '')],
            ['list' => $this->data($data)],
            ['count' => $count],
            ['pager' => $this->pager($count, $this->p, "category/{$id_or_slug}")]
        );

        // 侧栏
        $data = Model::fetchAll("SELECT `id`, `title`, `slug`, `likes`, `update_at` FROM `article` WHERE `status` = 0 AND `category_id` IN ($cidStr) ORDER BY `id` DESC");
        $this->aside['self'] = ['list' => $this->data($data), 'title' => $rows ? $rows[0]['name'] : Util::tran('未分类')];

        View::assign($this->vars());
        View::assign('tran', [
            'uncategoried' => Util::tran('未分类'),
            'articles' => Util::tran('篇文章'),
            'no_category_description' => Util::tran('还没有添加分类描述信息'),
        ]);
        if ($this->category && $this->category['is_gallery']) {
            // 相册
            View::display('index/gallery.html');
        } elseif ($this->category) {
            // 列表
            View::display('index/category.html');
        } else {
            // 404
            $this->is_404 = true;
            View::display('404.html');
        }
    }

    // 标签页
    public function tag($id_or_slug, $page = 0)
    {
        $this->properties($page);
        $rows = array_filter($this->tags, function ($item) use ($id_or_slug) {
            return is_numeric($id_or_slug) ? $item['id'] == $id_or_slug : $item['slug'] == $id_or_slug;
        });
        $rows = array_values($rows);
        $tid = $rows ? $rows[0]['id'] : '';
        $this->tag = $rows ? $rows[0] : [];
        $this->tag_id = $rows ? $rows[0]['id'] : 0;

        // 头部
        $this->page_suffix && array_unshift($this->header['title'], $this->page_suffix);
        array_unshift($this->header['title'], $rows ? ($rows[0]['seo_title'] ? $rows[0]['seo_title'] : $rows[0]['name']) : Util::tran('空标签'));
        array_unshift($this->header['description'], $rows ? ($rows[0]['seo_description'] ? strip_tags($rows[0]['seo_description']) : strip_tags($rows[0]['description'])) : '');
        $this->page_suffix && array_unshift($this->header['description'], $this->page_suffix);
        $this->header['og_tags']['og:type'] = 'website';
        $this->header['og_tags']['og:title'] = implode('-', $this->header['title']);
        $this->header['og_tags']['og:description'] = implode('-', $this->header['description']);
        $this->header['og_tags']['og:url'] = $this->site_url . ($this->p == 1 ? "/{$this->replace_path}tag/{$id_or_slug}.html" : "/{$this->replace_path}tag/{$id_or_slug}.html?p={$this->p}");
        array_push($this->header['crumbs'], ['name' => $rows ? $rows[0]['name'] : Util::tran('空标签')]);

        // 主体
        $count = Model::fetchColumn("SELECT COUNT(*) FROM `article` WHERE `status` = 0 AND `tag_ids` LIKE '%\"{$tid}\"%' ORDER BY `id` DESC");
        $data = Model::fetchAll("SELECT * FROM `article` WHERE `status` = 0 AND `tag_ids` LIKE '%\"{$tid}\"%' ORDER BY `id` DESC LIMIT $this->start, $this->limit");
        $this->main = array_merge(
            $this->main,
            ['title' => ($rows ? $rows[0]['name'] : Util::tran('空标签')) . ($this->page_suffix ? "({$this->page_suffix})" : '')],
            ['list' => $this->data($data)],
            ['count' => $count],
            ['pager' => $this->pager($count, $this->p, "tag/{$id_or_slug}")]
        );

        View::assign($this->vars());
        if ($this->tag) {
            View::display('index/tag.html');
        } else {
            // 404
            $this->is_404 = true;
            View::display('404.html');
        }
    }

    // 搜索页
    public function search($page = 0)
    {
        $this->properties($page);

        // 头部
        $this->page_suffix && array_unshift($this->header['title'], $this->page_suffix);
        array_unshift($this->header['title'], Util::tran('搜索结果'));
        array_unshift($this->header['description'], Util::tran('搜索结果') . $this->q);
        $this->page_suffix && array_unshift($this->header['description'], $this->page_suffix);
        $this->header['og_tags']['og:type'] = 'website';
        $this->header['og_tags']['og:title'] = implode('-', $this->header['title']);
        $this->header['og_tags']['og:description'] = implode('-', $this->header['description']);
        $this->header['og_tags']['og:url'] = $this->site_url . ($this->p == 1 ? "/{$this->replace_path}search.html?q={$this->q}" : "/{$this->replace_path}search/page-{$this->p}.html?q={$this->q}");
        array_push($this->header['crumbs'], ['name' => Util::tran('搜索结果')]);

        // 主体
        $count = Model::fetchColumn("SELECT COUNT(*) FROM `article` WHERE `status` = 0 AND `lang` = '{$this->replace_lang}' AND `title` LIKE '%{$this->q}%' ORDER BY `id` DESC");
        $data = Model::fetchAll("SELECT * FROM `article` WHERE `status` = 0 AND `lang` = '{$this->replace_lang}' AND `title` LIKE '%{$this->q}%' ORDER BY `id` DESC LIMIT $this->start, $this->limit");
        $this->main = array_merge(
            $this->main,
            ['title' => Util::tran('搜索结果') . ($this->page_suffix ? "({$this->page_suffix})" : '')],
            ['list' => $this->data($data)],
            ['count' => $count],
            ['pager' => $this->pager($count, $this->p, 'search', ['q=' . $this->q])]
        );

        View::assign($this->vars());
        View::display('index/search.html');
    }

    // 内容页
    public function detail($id_or_slug, $page = 0)
    {
        $this->properties($page);
        $where = is_numeric($id_or_slug) ? " AND `id` = $id_or_slug" : " AND `slug` = '$id_or_slug'";
        $row = Model::fetch("SELECT * FROM `article` WHERE `status` = 0 AND `lang` = '{$this->replace_lang}' $where ORDER BY `id` LIMIT 1");
        $this->article = $row ? $this->data([$row])[0] : [];
        $this->article && ($this->article['link'] = "{$this->site_url}/{$this->replace_path}{$id_or_slug}.html");
        $this->article_id = $row ? $row['id'] : 0;
        $this->article_views = $row ? $row['views'] : 0;
        $this->article_likes = $row ? $row['likes'] : 0;
        $file = '/static/upload/articles/ID' . str_pad($this->article_id, 7, '0', STR_PAD_LEFT) . '-' . str_replace('/', '-', $this->replace_path) . $id_or_slug . '.html';
        $this->download = is_file(IA_ROOT . $file) ? $this->site_url . $file : '';

        $rows = array_filter($this->categories, function ($item) use ($row) {
            return $item['id'] == ($row ? $row['category_id'] : 0);
        });
        $rows = array_values($rows);
        $cid = $rows ? $rows[0]['id'] : 0;
        $this->category = $rows ? $rows[0] : [];
        $this->category_id = $rows ? $rows[0]['id'] : 0;

        // 头部
        $this->page_suffix && array_unshift($this->header['title'], $this->page_suffix);
        array_unshift($this->header['title'], $row ? ($row['seo_title'] ? $row['seo_title'] : $row['title']) : Util::tran('无标题'));
        array_unshift($this->header['description'], $row ? ($row['seo_description'] ? strip_tags($row['seo_description']) : ($row['description'] ? strip_tags($row['description']) : mb_strimwidth(preg_replace('/\s+/sim', ' ', str_replace('"', '', strip_tags($row['content']))), 0, 140, '...', 'UTF-8'))) : '');
        $this->page_suffix && array_unshift($this->header['description'], $this->page_suffix);
        $this->header['og_tags']['og:type'] = 'article';
        $this->header['og_tags']['og:title'] = implode('-', $this->header['title']);
        $this->header['og_tags']['og:description'] = implode('-', $this->header['description']);
        $this->header['og_tags']['og:url'] = $this->site_url . ($this->p == 1 ? "/{$this->replace_path}{$id_or_slug}.html" : "/{$this->replace_path}{$id_or_slug}.html?p={$this->p}");
        if ($row && is_file(str_replace($this->site_url, IA_ROOT, $this->article['cover']))) {
            list($width, $height) = getimagesize(str_replace($this->site_url, IA_ROOT, $this->article['cover']));
            $this->header['og_tags']['og:image'] = $this->article['cover'];
            $this->header['og_tags']['og:image:width'] = $width;
            $this->header['og_tags']['og:image:height'] = $height;
            $this->header['og_tags']['og:image:alt'] = isset($this->article['title']) ? strip_tags($this->article['title']) : Util::tran('无标题');
        }
        $parents = Util::parents($this->categories, $cid);
        $parents = array_reverse($parents);
        $ids = [];
        $this->header['og_tags']['article:section'] = [];
        foreach ($parents as $item) {
            $ids[] = $item['id'];
            array_push($this->header['crumbs'], ['link' => $item['link'], 'name' => $item['name']]);
            $this->header['og_tags']['article:section'][] = $item['name'];
        }
        array_push($this->header['crumbs'], ['name' => $row ? $row['title'] : Util::tran('无标题')]);

        // 主体
        $this->main = array_merge(
            $this->main,
            ['article' => $this->article]
        );
        // 上一篇|下一篇
        $prev = Model::fetch("SELECT * FROM `article` WHERE `status` = 0 AND `category_id` = :cid AND `id` < :id ORDER BY `id` DESC LIMIT 1", [':cid' => $cid, ':id' => $this->article_id]);
        $prev && ($prev['link'] = $this->site_url . "/{$this->replace_path}" . ($prev['slug'] ? $prev['slug'] : $prev['id']) . '.html');
        $next = Model::fetch("SELECT * FROM `article` WHERE `status` = 0 AND `category_id` = :cid AND `id` > :id ORDER BY `id` ASC LIMIT 1", [':cid' => $cid, ':id' => $this->article_id]);
        $next && ($next['link'] = $this->site_url . "/{$this->replace_path}" . ($next['slug'] ? $next['slug'] : $next['id']) . '.html');
        $this->main = array_merge(
            $this->main,
            ['prev' => $prev],
            ['next' => $next]
        );
        // 标签
        $tags = array_filter($this->tags, function ($item) use ($row) {
            return in_array($item['id'], $row ? (array)json_decode($row['tag_ids'], true) : []);
        });
        $this->header['og_tags']['article:tag'] = array_map(function ($item) {
            return $item['name'];
        }, $tags);
        // rich attachment
        if ($row && preg_match_all('#<(video|audio|source|embed)[^>]*src=["\'](' . preg_quote($this->site_url, '#') . '[^"\']*\.[^"\']*)["\'][^>]*>#sim', $this->article['content'], $matches)) {
            $reverse = array_reverse($matches[2]);
            foreach ($reverse as $url) {
                $ext = pathinfo($url, PATHINFO_EXTENSION);
                $type = Util::mimeType($ext);
                $this->header['og_tags']['og:rich_attachment'] = 'true';
                if (preg_match('/^video/', $type)) {
                    $this->header['og_tags']['og:video:url'] = $url;
                    $this->header['og_tags']['og:video:type'] = $type;
                } else if (preg_match('/^audio/', $type)) {
                    $this->header['og_tags']['og:audio:url'] = $url;
                    $this->header['og_tags']['og:audio:type'] = $type;
                }
            }
        }
        // twitter
        $this->header['og_tags']['twitter:card'] = 'summary';
        if (isset($this->header['og_tags']['og:image:width']) && $this->header['og_tags']['og:image:width'] > 520) {
            $this->header['og_tags']['twitter:card'] = 'summary_large_image';
        }
        foreach (['title', 'description', 'image', 'url'] as $key) {
            if (isset($this->header['og_tags']['og:' . $key])) {
                $this->header['og_tags']['twitter:' . $key] = $this->header['og_tags']['og:' . $key];
            }
        }
        $this->header['og_tags']['article:published_time'] = date('c', $row ? $this->article['create_at'] : '0');
        $this->header['og_tags']['article:modified_time'] = date('c', $row ? $this->article['update_at'] : '0');
        $this->header['og_tags']['article:author'] = $row ? $this->article['author'] : 'anonymous';
        $this->header['og_tags']['article:published_first'] = sprintf('%s,%s', $row ? $this->article['title'] : Util::tran('无标题'), $this->header['og_tags']['og:url']);
        $this->header['og_tags']['og:release_date'] = date('c', $row ? $this->article['create_at'] : '0');
        $this->header['og_tags']['og:updated_time'] = date('c', $row ? $this->article['update_at'] : '0');
        $this->header['og_tags']['profile:username'] = $row ? $this->article['author'] : 'anonymous';
        $this->main = array_merge(
            $this->main,
            ['tags' => array_values($tags)]
        );
        // 相关文章
        $tag_ids = isset($this->article['tag_ids']) && $this->article['tag_ids'] ? json_decode($this->article['tag_ids'], true) : [];
        $tag_ids = array_map('intval', $tag_ids);
        $tag_ids = array_filter($tag_ids);
        // 按标签获取
        $related = [];
        for ($i = 0; $i < count($tag_ids); $i++) {
            $related_num = $this->setting['related_num'] + 1;
            $related_articles = Model::fetchAll("SELECT * FROM `article` WHERE `status` = 0 AND `tag_ids` LIKE '%\"{$tag_ids[$i]}\"%' ORDER BY `id` DESC LIMIT $related_num");
            // 合并去重
            $related = array_merge($related, $related_articles);
            $related = array_unique($related, SORT_REGULAR);
            // 排除当前文章
            $related = array_filter($related, function ($item) {
                return $item['id'] !== $this->article_id;
            });
            // 获取设置的数量
            $related = array_slice($related, 0, $related_num - 1);
            if (count($related) >= $related_num - 1)
                break;
        }
        $this->main = array_merge($this->main, ['related' => $this->data($related)]);
        // 评论列表
        $count = Model::fetchColumn("SELECT COUNT(*) FROM `comment` WHERE `status` = 0 AND `article_id` = $this->article_id AND `pid` = 0 ORDER BY `sort` DESC, `id` DESC");
        $data = Model::fetchAll("SELECT * FROM `comment` WHERE `status` = 0 AND `article_id` = $this->article_id AND `pid` = 0 ORDER BY `sort` DESC, `id` DESC LIMIT $this->start, $this->limit");
        $replay_ids = array_map(function ($item) {
            return $item['id'];
        }, $data);
        $replay_ids = array_unique($replay_ids);
        $replay_ids = implode(',', $replay_ids);
        if ($replay_ids)
            $replies = Model::fetchAll("SELECT * FROM `comment` WHERE `status` = 0 AND `article_id` = $this->article_id AND `pid` IN ($replay_ids) ORDER BY `sort` DESC, `id` DESC");
        else
            $replies = [];
        // 获取用户图像
        $authors = array_map(function ($item) {
            return $item['author'];
        }, array_merge($data, $replies));
        $authors = array_unique($authors);
        $authors = implode("','", $authors);
        if ($authors)
            $users = Model::fetchAll("SELECT * FROM `user` WHERE `status` = 0 AND `username` IN ('$authors') ORDER BY `id`");
        else
            $users = [];
        $names = [];
        foreach ($users as $user) {
            $names[$user['username']] = [];
            $names[$user['username']]['nickname'] = $user['nickname'] ?: $user['username'];
            $names[$user['username']]['avatar'] = $user['avatar'] ? $this->site_url . '/' . ltrim($user['avatar'], '/') : '';
        }
        // 组合数据
        $data = array_map(function ($item) use ($names) {
            $item['avatar'] = isset($names[$item['author']]) ? $names[$item['author']]['avatar'] : '';
            $item['nickname'] = isset($names[$item['author']]) ? $names[$item['author']]['nickname'] : '';
            if (preg_match_all('#<[^>]*=["\']([^"\']*/static/.+?\.[^"\']*)["\'][^>]*>#i', $item['comment'], $matches)) {
                foreach ($matches[1] as $match) {
                    $item['comment'] = str_replace($match, $this->site_url . str_replace($this->site_url, '', $match), $item['comment']);
                }
            }
            return $item;
        }, $data);
        $replies = array_map(function ($item) use ($names) {
            $item['avatar'] = isset($names[$item['author']]) ? $names[$item['author']]['avatar'] : '';
            $item['nickname'] = isset($names[$item['author']]) ? $names[$item['author']]['nickname'] : '';
            if (preg_match_all('#<[^>]*=["\']([^"\']*/static/.+?\.[^"\']*)["\'][^>]*>#i', $item['comment'], $matches)) {
                foreach ($matches[1] as $match) {
                    $item['comment'] = str_replace($match, $this->site_url . str_replace($this->site_url, '', $match), $item['comment']);
                }
            }
            return $item;
        }, $replies);
        $data = array_map(function ($item) use ($replies) {
            $item['replies'] = array_values(array_filter($replies, function ($elem) use ($item) {
                return $elem['pid'] == $item['id'];
            }));
            return $item;
        }, $data);
        $this->main = array_merge(
            $this->main,
            ['comments' => $data],
            ['count' => $count],
            ['pager' => $this->pager($count, $this->p, $id_or_slug, [], false, '#comments')]
        );

        // 侧栏
        $idStr = $ids ? implode(',', $ids) : $cid;
        $data = Model::fetchAll("SELECT `id`, `title`, `slug`, `likes`, `update_at` FROM `article` WHERE `status` = 0 AND `category_id` IN ($idStr) ORDER BY `id` DESC");
        $this->aside['self'] = ['list' => $this->data($data), 'title' => $rows ? $rows[0]['name'] : Util::tran('未分类')];

        View::assign($this->vars());
        View::assign('tran', [
            'untitled' => Util::tran('无标题'),
            'uncategoried' => Util::tran('未分类'),
            'custom' => Util::tran('客户端'),
            'no_content' => Util::tran('还没有内容信息'),
            'download_content' => Util::tran('下载内容'),
            'favorite_article' => Util::tran('收藏文章'),
            'copyr_info' => Util::tran('除非特别说明，本博客均为原创，如需转载，请以链接形式标明来源出处。'),
            'copyr_address' => Util::tran('本文博客网址'),
            'no_address' => Util::tran('还没有网址'),
            'article_useful' => Util::tran('这篇文章很有用'),
            'people_likes' => Util::tran('人觉得这篇文章很赞'),
            'nothing' => Util::tran('没有了'),
            'related_articles' => Util::tran('相关文章'),
            'post_comment' => Util::tran('发表评论'),
            'comment_content' => Util::tran('评论内容'),
            'please' => Util::tran('请'),
            'login' => Util::tran('登录'),
            'after_comment' => Util::tran('后评论'),
            'say' => Util::tran('说道'),
            'delete' => Util::tran('删除'),
            'reply' => Util::tran('回复'),
            'no_comment' => Util::tran('还没有评论'),
        ]);
        if ($this->article) {
            View::display('index/detail.html');
        } else {
            // 404
            $this->is_404 = true;
            View::display('404.html');
        }
    }

    // 独立页面
    public function page($id_or_slug, $page = 0)
    {
        $this->properties($page);
        $rows = array_filter($this->pages, function ($item) use ($id_or_slug) {
            return is_numeric($id_or_slug) ? $item['id'] == $id_or_slug && $item['status'] == 0 : $item['slug'] == $id_or_slug && $item['status'] == 0;
        });
        $rows = array_values($rows);
        $row = $rows ? $this->data([$rows[0]])[0] : [];
        $this->page = $row ? $row : [];
        $this->page_id = $row ? $row['id'] : 0;

        // 头部
        $this->page_suffix && array_unshift($this->header['title'], $this->page_suffix);
        array_unshift($this->header['title'], $row ? ($row['seo_title'] ? $row['seo_title'] : $row['title']) : Util::tran('无标题'));
        array_unshift($this->header['description'], $row ? (strip_tags($row['seo_description']) ? strip_tags($row['seo_description']) : (strip_tags($row['description']) ? strip_tags($row['description']) : mb_strimwidth(preg_replace('/\s+/sim', ' ', str_replace('"', '', strip_tags($row['content']))), 0, 140, '...', 'UTF-8'))) : '');
        $this->page_suffix && array_unshift($this->header['description'], $this->page_suffix);
        $this->header['og_tags']['og:type'] = 'article';
        $this->header['og_tags']['og:title'] = implode('-', $this->header['title']);
        $this->header['og_tags']['og:description'] = implode('-', $this->header['description']);
        $this->header['og_tags']['og:url'] = $this->site_url . ($this->p == 1 ? "/{$this->replace_path}page/{$id_or_slug}.html" : "/{$this->replace_path}page/{$id_or_slug}.html?p={$this->p}");
        // rich attachment
        if ($row && preg_match_all('#<(video|audio|source|embed|img)[^>]*src=["\'](' . preg_quote($this->site_url, '#') . '[^"\']*\.[^"\']*)["\'][^>]*>#sim', $this->page['content'], $matches)) {
            $reverse = array_reverse($matches[2]);
            foreach ($reverse as $url) {
                $ext = pathinfo($url, PATHINFO_EXTENSION);
                $type = Util::mimeType($ext);
                if (preg_match('/^image/', $type)) {
                    list($width, $height) = getimagesize(str_replace($this->site_url, IA_ROOT, $url));
                    $this->header['og_tags']['og:image'] = $url;
                    $this->header['og_tags']['og:image:width'] = $width;
                    $this->header['og_tags']['og:image:height'] = $height;
                    $this->header['og_tags']['og:image:alt'] = strip_tags($this->page['title']);
                } else if (preg_match('/^video/', $type)) {
                    $this->header['og_tags']['og:rich_attachment'] = 'true';
                    $this->header['og_tags']['og:video:url'] = $url;
                    $this->header['og_tags']['og:video:type'] = $type;
                } else if (preg_match('/^audio/', $type)) {
                    $this->header['og_tags']['og:rich_attachment'] = 'true';
                    $this->header['og_tags']['og:audio:url'] = $url;
                    $this->header['og_tags']['og:audio:type'] = $type;
                }
            }
        }
        // twitter
        $this->header['og_tags']['twitter:card'] = 'summary';
        if (isset($this->header['og_tags']['og:image:width']) && $this->header['og_tags']['og:image:width'] > 520) {
            $this->header['og_tags']['twitter:card'] = 'summary_large_image';
        }
        foreach (['title', 'description', 'image', 'url'] as $key) {
            if (isset($this->header['og_tags']['og:' . $key])) {
                $this->header['og_tags']['twitter:' . $key] = $this->header['og_tags']['og:' . $key];
            }
        }
        $this->header['og_tags']['article:published_time'] = date('c', $row ? $this->page['time'] : '0');
        $this->header['og_tags']['article:published_first'] = sprintf('%s,%s', $row ? $this->page['title'] : Util::tran('无标题'), $this->header['og_tags']['og:url']);
        $this->header['og_tags']['og:release_date'] = date('c', $row ? $this->page['time'] : '0');
        $parents = Util::parents($this->pages, $this->page_id);
        $parents = array_reverse($parents);
        $ids = [];
        foreach ($parents as $item) {
            $ids[] = $item['id'];
            if (count($parents) == count($ids))
                continue;
            array_push($this->header['crumbs'], ['link' => $item['link'], 'name' => $item['title']]);
        }
        array_push($this->header['crumbs'], ['name' => $rows ? $rows[0]['name'] : Util::tran('无标题')]);

        // 主体
        $children = array_filter($this->pages, function ($item) {
            return $item['pid'] == $this->page_id;
        });
        $children = array_values($children);
        $this->main = array_merge(
            $this->main,
            ['count' => count($children)],
            ['list' => $this->data(array_slice($children, $this->start, $this->limit))],
            ['pager' => $this->pager(count($children), $this->p, "page/{$id_or_slug}")]
        );

        View::assign($this->vars());
        View::assign('tran', [
            'unnamed' => Util::tran('未命名'),
            'no_page' => Util::tran('还没有页面'),
            'articles' => Util::tran('篇文章'),
            'no_page_description' => Util::tran('还没有页面描述信息'),
        ]);
        if ($this->page && $this->page['slug'] && is_file(IA_ROOT . '/view/index/' . $this->page['slug'] . '.html')) {
            View::display('index/' . $this->page['slug'] . '.html');
        } elseif ($this->page && $this->page['is_full']) {
            View::display('index/page-full.html');
        } elseif ($this->page) {
            View::display('index/page.html');
        } else {
            // 404
            $this->is_404 = true;
            View::display('404.html');
        }
    }

    // Ajax处理
    public function ajax($action)
    {
        if (Util::isAjax()) {
            switch ($action) {
                    // ajax点击数|点赞数
                case 'views':
                case 'likes':
                    $param = Util::param(['id', $action]);
                    if (isset($param[$action])) {
                        $$action = (int)$param[$action];
                        if (isset($param['id']) && $param['id']) {
                            Model::update('article', "$action = $action + 1", ['id' => $param['id']]);
                            $$action += 1;
                        }
                        echo json_encode(['code' => 0, 'msg' => $$action]);
                    }
                    break;

                    // 登录检查
                case 'islogin':
                    $this->login && Util::errMsg(Util::tran('你已经登录'));
                    exit(Util::errMsg(['code' => 0, 'msg' => Util::tran('请先登录')]));
                    break;

                    // 登入
                case 'login':
                    $this->login && Util::errMsg(Util::tran('你已经登录'));
                    $ip = Util::getIp();
                    $param = Util::param(['username', 'password', 'remember']);
                    if (empty($param['username']) || !preg_match('#^[a-z0-9_-]{3,12}$#', $param['username']))
                        Util::errMsg(Util::tran('用户名只能英文字母数字下划线或中划线，位数') . ' 3~12');
                    if (empty($param['password']) || strlen($param['password']) < 5)
                        Util::errMsg(Util::tran('密码至少位数') . ' 5');
                    if (!in_array($param['remember'], [0, 1]))
                        Util::errMsg(Util::tran('包含非法参数'));

                    // 数据库查询
                    $user = Model::fetch("SELECT * FROM `user` WHERE `username`=:username AND `password`=:password ORDER BY `id` ASC LIMIT 1", [
                        'username' => $param['username'],
                        'password' => md5(base64_encode($param['password']))
                    ]);
                    if (!$user) {
                        Util::config('log') && Model::insert('log', ['author' => $param['username'], 'request_method' => 'ajax', 'url' => htmlentities($_SERVER['REQUEST_URI']), 'title' => Util::tran('前端登录'), 'message' => Util::tran('用户名或密码错误'), 'ip' => $ip, 'status' => 1, 'time' => time()]);
                        Util::errMsg(Util::tran('用户名或密码错误'));
                    }
                    if ('1' == $user['status']) {
                        Util::config('log') && Model::insert('log', ['author' => $param['username'], 'request_method' => 'ajax', 'url' => htmlentities($_SERVER['REQUEST_URI']), 'title' => Util::tran('前端登录'), 'message' => Util::tran('账号被禁用'), 'ip' => $ip, 'status' => 1, 'time' => time()]);
                        Util::errMsg(Util::tran('你的账号已被禁用，请联系管理员'));
                    }

                    // 登录成功
                    $_SESSION['uid'] = $user['id'];

                    // 记住登录
                    $token = md5($user['username'] . md5(base64_encode($user['password'])));
                    if ('1' == $param['remember']) {
                        setcookie('token', $token, time() + 3600 * 24 * 30, '/');
                        Model::update('user', ['remember_token' => $token], ['id' => $user['id']]);
                    } else {
                        Model::update('user', ['remember_token' => ''], ['id' => $user['id']]);
                    }

                    // 单点登录
                    $setting = Model::fetch('SELECT * FROM `setting` WHERE id=:id ORDER BY `id` ASC LIMIT 1', [':id' => 1]);
                    if ($setting && ($check = json_decode($setting['value'], true)) && isset($check['alone_login']) && $check['alone_login']) {
                        $randstr = md5($token . rand(100000, 999999));
                        Util::setCache('rand' . $user['id'], $randstr);
                        setcookie('randstr', $randstr, time() + 3600 * 24 * 30, '/');
                    }

                    // 更新时间
                    !$user['first_login'] && Model::update('user', ['first_login' => time()], ['id' => $user['id']]);
                    Model::update('user', ['last_login' => time()], ['id' => $user['id']]);

                    // 记录日志
                    Util::config('log') && Model::insert('log', ['author' => $user['username'], 'request_method' => 'ajax', 'url' => htmlentities($_SERVER['REQUEST_URI']), 'title' => Util::tran('前端登录'), 'message' => Util::tran('登录成功'), 'ip' => $ip, 'status' => 0, 'time' => time()]);

                    // 清除来源页缓存
                    if (isset($_SERVER["HTTP_REFERER"])) {
                        $file_name = md5($_SERVER["HTTP_REFERER"]);
                        if ($pos = strpos($_SERVER["HTTP_REFERER"], 'redirect_to=')) {
                            $file_name = md5(substr($_SERVER["HTTP_REFERER"], $pos + 12));
                        }
                        $cache_file = IA_ROOT . '/mvc/tpl/' . substr($file_name, 0, 2) . '/' . $file_name . '.php';
                        is_file($cache_file) && unlink($cache_file);
                    }

                    // 清除来源页缓存
                    $this->clearOrgin();

                    // 登录跳转
                    exit(Util::errMsg(['code' => 0, 'token' => $token, 'msg' => Util::tran('登录成功')]));
                    break;

                    // 注册
                case 'register':
                    $this->login && Util::errMsg(Util::tran('你已经注册'));
                    $param = Util::param(['username', 'password', 'repeat_password', 'nickname', 'phone', 'remember']);
                    if (empty($param['username']) || !preg_match('#^[a-z0-9_-]{3,12}$#', $param['username']))
                        Util::errMsg(Util::tran('用户名只能英文字母数字下划线或中划线，位数') . ' 3~12');
                    if (empty($param['password']) || strlen($param['password']) < 5)
                        Util::errMsg(Util::tran('密码至少位数') . ' 5');
                    if ($param['password'] !== $param['repeat_password'])
                        Util::errMsg(Util::tran('两次密码输入不一致'));
                    if (!empty($param['nickname']) && !preg_match('#^[\x{4e00}-\x{9fa5}A-Za-z0-9_-]{2,12}$#u', $param['nickname']))
                        Util::errMsg(Util::tran('昵称只能中英文字母数字下划线中划线，位数') . ' 2~12');
                    if (!empty($param['phone']) && !preg_match('#^1((34[0-8]\d{7})|((3[0-3|5-9])|(4[5-7|9])|(5[0-3|5-9])|(66)|(7[2-3|5-8])|(8[0-9])|(9[1|8|9]))\d{8})$#', $param['phone']))
                        Util::errMsg(Util::tran('手机号码不正确'));
                    if (!in_array($param['remember'], [0, 1]))
                        Util::errMsg(Util::tran('包含非法参数'));

                    $remember_me = $param['remember'];
                    $param['password'] = md5(base64_encode($param['password']));
                    unset($param['repeat_password']);
                    unset($param['remember']);

                    // 验证用户名或手机号重复
                    $check = Model::fetchColumn("SELECT `username` FROM `user` WHERE `username`=:username ORDER BY `id`", [':username' => $param['username']]);
                    $check && Util::errMsg(Util::tran('用户名已存在'));
                    if (!empty($param['phone'])) {
                        $check = Model::fetchColumn("SELECT `phone` FROM `user` WHERE `phone`=:phone ORDER BY `id`", [':phone' => $param['phone']]);
                        $check && Util::errMsg(Util::tran('手机号码已存在'));
                    }

                    // 写入数据库记录日志
                    $param['role_id'] = 2;
                    $param['reg_time'] = time();
                    $lastInsertId = Model::insert('user', $param);
                    if ($lastInsertId) {
                        // 注册后登录
                        $_SESSION['uid'] = $lastInsertId;

                        // 记住登录
                        $token = md5($param['username'] . md5(base64_encode($param['password'])));
                        if ('1' == $remember_me) {
                            setcookie('token', $token, time() + 3600 * 24 * 30, '/');
                            Model::update('user', ['remember_token' => $token], ['id' => $lastInsertId]);
                        } else {
                            Model::update('user', ['remember_token' => ''], ['id' => $lastInsertId]);
                        }

                        // 单点登录
                        $setting = Model::fetch('SELECT * FROM `setting` WHERE id=:id ORDER BY `id` ASC LIMIT 1', [':id' => 1]);
                        if ($setting && ($check = json_decode($setting['value'], true)) && isset($check['alone_login']) && $check['alone_login']) {
                            $randstr = md5($token . rand(100000, 999999));
                            Util::setCache('rand' . $lastInsertId, $randstr);
                            setcookie('randstr', $randstr, time() + 3600 * 24 * 30, '/');
                        }

                        // 更新时间
                        $user = Model::fetch('SELECT * FROM `user` WHERE `id`=:id', [':id' => $lastInsertId]);
                        !$user['first_login'] && Model::update('user', ['first_login' => time()], ['id' => $user['id']]);
                        Model::update('user', ['last_login' => time()], ['id' => $user['id']]);

                        // 清除来源页缓存
                        if (isset($_SERVER["HTTP_REFERER"])) {
                            $file_name = md5($_SERVER["HTTP_REFERER"]);
                            if ($pos = strpos($_SERVER["HTTP_REFERER"], 'redirect_to=')) {
                                $file_name = md5(substr($_SERVER["HTTP_REFERER"], $pos + 12));
                            }
                            $cache_file = IA_ROOT . '/mvc/tpl/' . substr($file_name, 0, 2) . '/' . $file_name . '.php';
                            is_file($cache_file) && unlink($cache_file);
                        }

                        // 修改媒体状态
                        if (isset($param['avatar']) && preg_match('#/static/upload/.+?\.[png|jpg|jpeg|gif]#', $param['avatar'])) {
                            if (is_file(IA_ROOT . str_replace($this->site_url, '', $param['avatar']))) {
                                Model::update('media', ['status' => 0], ['path' => str_replace($this->site_url, '', $param['avatar'])]);
                            } else {
                                Model::update('media', ['status' => 2], ['path' => str_replace($this->site_url, '', $param['avatar'])]);
                            }
                        }

                        // 清除来源页缓存
                        $this->clearOrgin();
                        Util::config('log') && Model::insert('log', ['author' => $param['username'], 'request_method' => Util::requestMethod(), 'url' => htmlentities($_SERVER['REQUEST_URI']), 'title' => Util::tran('前端注册'), 'message' => Util::tran('前端注册') . " {$param['username']} " . Util::tran('成功'), 'ip' => Util::getIp(), 'status' => 0, 'time' => time()]);
                        exit(Util::errMsg(['code' => 0, 'token' => $token, 'msg' => Util::tran('注册成功')]));
                    } else {
                        Util::config('log') && Model::insert('log', ['author' => $param['username'], 'request_method' => Util::requestMethod(), 'url' => htmlentities($_SERVER['REQUEST_URI']), 'title' => Util::tran('前端注册'), 'message' => Util::tran('前端注册') . " {$param['username']} " . Util::tran('失败'), 'ip' => Util::getIp(), 'status' => 1, 'time' => time()]);
                        Util::errMsg(Util::tran('注册失败'));
                    }
                    break;

                    // 登出
                case 'logout':
                    !$this->login && Util::errMsg(Util::tran('你已登出'));

                    // 清除token
                    Model::update('user', ['remember_token' => ''], ['id' => $this->login['id']]);

                    // 记录日志
                    $username = Model::fetchColumn("SELECT `username` FROM `user` WHERE `id`=:id ORDER BY `id` ASC LIMIT 1", ['id' => $this->login['id']]);
                    Util::config('log') && Model::insert('log', ['author' => $username, 'request_method' => Util::requestMethod(), 'url' => htmlentities($_SERVER['REQUEST_URI']), 'title' => Util::tran('前端登出'), 'message' => Util::tran('登出成功'), 'ip' => Util::getIp(), 'status' => 1, 'time' => time()]);

                    // 清除session和cookie
                    unset($_SESSION['uid']);
                    setcookie('token', '', time() - 3600, '/');
                    setcookie('randstr', '', time() - 3600, '/');
                    Util::delCache('rand' . $this->login['id']);

                    // 清除来源页缓存
                    $this->clearOrgin();
                    exit(Util::errMsg(['code' => 0, 'token' => '', 'msg' => Util::tran('登出成功')]));
                    break;

                    // ajax获取点赞收藏的文章
                case 'page_lists':
                    $param = Util::param(['ids', 'p', 'slug']);
                    $p = max((empty($param['p']) ? 0 : (int)$param['p']), 1);
                    $start = ($p - 1) * $this->limit;
                    $ids = empty($param['ids']) ? [] : (array)$param['ids'];
                    $idStr = isset($param['ids']) && $param['ids'] ? implode(',', $ids) : '0';
                    $count = Model::fetchColumn("SELECT COUNT(*) FROM `article` WHERE `lang` = '{$this->replace_lang}' AND `status` = 0 AND `id` IN ($idStr)");
                    $data = Model::fetchAll("SELECT * FROM `article` WHERE `lang` = '{$this->replace_lang}' AND `status` = 0 AND `id` IN ($idStr) LIMIT $start, $this->limit");
                    $arrs = [];
                    foreach ($ids as $id) {
                        $item = array_filter($data, function ($item) use ($id) {
                            return $item['id'] == $id;
                        });
                        if ($item) {
                            $arrs[] = array_shift($item);
                        }
                    }
                    $noArr = array_filter($data, function ($item) use ($ids) {
                        return !in_array($item['id'], $ids);
                    });
                    foreach ($noArr as $item) {
                        $arrs[] = $item;
                    }
                    $data = $arrs;
                    $data = $this->data($data);
                    $data = array_map(function ($item) {
                        $item = '<div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <div class="row">
                                        ' . ($item['cover'] ? '<div class="col-sm-3 col-md-3">
                                        <a class="thumbnail" href="' . $item['link'] . '" title="' . strip_tags($item['title']) . '">
                                            <img style="width:100%;max-height:100px;background-color:#f1f1f1;" src="data:image/gif;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAADUlEQVQImWNgYGBgAAAABQABh6FO1AAAAABJRU5ErkJggg==" data-src="' . $item['cover'] . '" alt="' . strip_tags($item['title']) . '" title="' . strip_tags($item['title']) . '" />
                                        </a>
                                    </div>' : '') . '
                                        <div class="' . ($item['cover'] ? 'col-sm-9 col-md-9' : 'col-sm-12 col-md-12') . '">
                                            <h2 style="margin-top: 0;">
                                                <a href="' . $item['link'] . '" title="' . strip_tags($item['title']) . '">' . $item['title'] . '</a>
                                                ' . ($item['update_at'] > time() - 24 * 3600 * 3 ? '<span class="label label-success">New</span>' : '') . '
                                                ' . (($item['likes'] && $item['update_at'] > time() - 24 * 3600 * 3) || $item['likes'] > 99 ? '<span class="label label-danger">Hot</span>' : '') . '
                                            </h2>
                                            <p>' . ($item['description'] ? strip_tags($item['description']) : mb_strimwidth(preg_replace('/\s+/sim', ' ', strip_tags($item['content'])), 0, 140, '...', 'UTF-8')) . '</p>
                                            <p>
                                                <a href="' . $item['link'] . '" title="Read more" rel="nofollow"
                                                    class="btn btn-primary pull-right" role="button">
                                                    <span class="glyphicon glyphicon-link" aria-hidden="true"></span>
                                                    Read more
                                                </a>
                                                <span class="glyphicon glyphicon-time text-muted" aria-hidden="true"></span>
                                                <span class="text-muted">' . date('m/d/y', $item['create_at']) . '</span> &nbsp;
                                                <span class="glyphicon glyphicon-eye-open text-muted" aria-hidden="true"></span>
                                                <span class="text-muted">' . $item['views'] . '</span>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>';
                        return $item;
                    }, $data);
                    if (!$data) {
                        $data = ['<div class="row">
                        <div class="col-md-12">
                            <p class="text-center text-muted">' . Util::tran('还没有文章') . '</p>
                        </div>
                    </div>'];
                    }
                    $base = empty($param['slug']) ? 'page/page-favorites' : "page/{$param['slug']}";
                    $pager = $this->pager($count, $p, $base);
                    $num = count($pager);
                    $pageStr = '<div class="row">
                        <div class="col-md-12 text-center">
                            <nav aria-label="Page navigation">
                                <ul class="pagination pagination-lg">';
                    for ($i = 0; $i < $num; $i++) {
                        if ($i == 0) {
                            $pageStr .= '<li' . ($p == $pager[$i]['p'] ? ' class="disabled"' : '') . '>
                                <a href="' . ($p == $pager[$i]['p'] ? ' javascript:;' : $pager[$i]['link']) . '" aria-label="Previous" title="' . $pager[$i]['title'] . '">
                                    <span aria-hidden="true">&laquo;</span>
                                </a>
                            </li>';
                        }
                        if ($i > 0 && $i < $num - 1) {
                            $pageStr .= '<li' . ($p == $pager[$i]['p'] ? ' class="active"' : '') . '>
                                <a href="' . ($p == $pager[$i]['p'] ? ' javascript:;' : $pager[$i]['link']) . '" title="' . $pager[$i]['title'] . '">' . $pager[$i]['p'] . '</a>
                            </li>';
                        }
                        if ($i == $num - 1) {
                            $pageStr .= '<li' . ($p == $pager[$i]['p'] ? ' class="disabled"' : '') . '>
                                <a href="' . ($p == $pager[$i]['p'] ? ' javascript:;' : $pager[$i]['link']) . '" aria-label="Next" title="' . $pager[$i]['title'] . '">
                                    <span aria-hidden="true">&raquo;</span>
                                </a>
                            </li>';
                        }
                    }
                    $pageStr .= '</ul>
                            </nav>
                        </div>
                    </div>';
                    if (!$count) $pageStr = '';
                    echo json_encode([
                        'code' => 0,
                        'data' => json_encode($data),
                        'count' => $count,
                        'pager' => $pageStr,
                    ]);
                    break;

                    // ajax评论
                case 'comment':
                    $param = Util::param(['post_id', 'pid', 'comment', 'anti_spam']);
                    empty($param['post_id']) && Util::errMsg('ID' . Util::tran('非法'));
                    $this->setting['can_comment'] && !$this->login && Util::errMsg(Util::tran('请先登录'));
                    $comment_text = trim(htmlentities(preg_replace('/\s+/sim', ' ', strip_tags(empty($param['comment']) ? '' : $param['comment']))));
                    (empty($comment_text) || strlen($comment_text) < 2) && Util::errMsg(Util::tran('请写点评论'));
                    $can_passed = false;
                    if ($this->login && isset($param['pid']) && $param['pid'] == '0' && in_array(81, $this->login['auth_ids'])) {
                        $can_passed = true;
                    }
                    if ($this->login && isset($param['pid']) && $param['pid'] != '0' && in_array(83, $this->login['auth_ids'])) {
                        $can_passed = true;
                    }
                    $pid = empty($param['pid']) ? 0 : (int)$param['pid'];
                    $check = Model::fetchColumn("SELECT `comment` FROM `comment` WHERE `comment` = :comment AND `pid` = :pid ORDER BY `id`", [':comment' => $can_passed ? $param['comment'] : $comment_text, ':pid' => $pid]);
                    $check && Util::errMsg(empty($param['pid']) ? Util::tran('评论内容重复') : Util::tran('回复内容重复'));
                    if ($pid > 0) {
                        $check = Model::fetchColumn("SELECT `comment` FROM `comment` WHERE `comment` = :comment AND `id` = :pid ORDER BY `id`", [':comment' => $can_passed ? $param['comment'] : $comment_text, ':pid' => $pid]);
                        $check && Util::errMsg(Util::tran('回复与评论内容重复'));
                    }
                    $data = [];
                    $data['pid'] = isset($param['pid']) ? (int)$param['pid'] : 0;
                    $data['article_id'] = isset($param['post_id']) ? $param['post_id'] : 0;
                    $data['comment'] = $can_passed ? $param['comment'] : $comment_text;
                    $data['author'] = $this->login ? $this->login['username'] : 'anonymous';
                    $data['status'] = $can_passed ? 0 : (int)$this->setting['pending_comment'];
                    // 机器人评论
                    if (empty($param['anti_spam']) || $param['anti_spam'] !== md5($param['post_id'])) {
                        $data['status'] = 3;
                    }
                    $data['time'] = time();
                    $data['lang'] = $this->replace_lang;
                    $lastInsertId = Model::insert('comment', $data);
                    if ($lastInsertId) {
                        // 查找图像
                        $user = Model::fetch("SELECT * FROM `user` WHERE `status` = 0 AND `username` = :author ORDER BY `id` LIMIT 1", [':author' => $data['author']]);
                        // 清除文章缓存
                        $this->clearCache($param['post_id']);
                        Util::errMsg(['code' => 0, 'msg' => Util::tran('评论发表成功') . ($data['status'] == 0 ? '' : Util::tran('，等待审核') . '...'), 'data' => [
                            'id' => $lastInsertId,
                            'author' => $data['author'],
                            'nickname' => isset($user['nickname']) && $user['nickname'] ? strip_tags($user['nickname']) : $data['author'],
                            'comment' => $data['comment'],
                            'status' => $data['status'],
                            'avatar' => isset($user['avatar']) && $user['avatar'] ? $this->site_url . str_replace($this->site_url, '', $user['avatar']) : ''
                        ]]);
                    } else {
                        Util::errMsg(Util::tran('发表评论失败'));
                    }
                    break;

                    // 删除评论
                case 'delete_comment':
                    $param = Util::param(['comment_id']);
                    $row = Model::fetch("SELECT `id`, `pid`, `article_id` FROM `comment` WHERE `id` = :id AND `status` = 0 ORDER BY `id` LIMIT 1", [':id' => empty($param['comment_id']) ? 0 : (int)$param['comment_id']]);
                    !$row && Util::errMsg('ID ' . Util::tran('错误'));
                    $rows = Model::fetchAll("SELECT `id` FROM `comment` WHERE `pid` = :pid AND `status` = 0 ORDER BY `id`", [':pid' => $row['id']]);
                    $ids = array_map(function ($item) {
                        return $item['id'];
                    }, $rows);
                    array_push($ids, $row['id']);
                    // 检查用户的删除权限
                    if (!$this->login || ($this->login && !in_array(88, $this->login['auth_ids']))) {
                        Util::errMsg(Util::tran('没有权限，请联系管理员'));
                    }
                    $paths = [1 => [], 2 => []];
                    $comNames = [];
                    $idStr = implode(',', $ids);
                    $rows = Model::fetchAll("SELECT `article_id`, `comment` FROM `comment` WHERE `id` IN ($idStr)");
                    foreach ($rows as $row) {
                        $comNames[] = mb_strimwidth(preg_replace('/\s+/sim', ' ', strip_tags($row['comment'])), 0, 60, '...', 'UTF-8');
                        if (preg_match_all('#<[^>]*=["\']([^"\']*/static/upload/.+?\.[^"\']*)["\'][^>]*>#i', $row['comment'], $matches)) {
                            foreach ($matches[1] as $match) {
                                if (is_file(IA_ROOT . str_replace($this->site_url, '', $match)))
                                    $paths[1][] = str_replace($this->site_url, '', $match);
                                else
                                    $paths[2][] = str_replace($this->site_url, '', $match);
                            }
                        }
                    }
                    $rowCount = Model::delete('comment', ['id' => array_unique($ids)]);
                    if ($rowCount) {
                        // 修改媒体状态
                        $paths[1] && Model::update('media', ['status' => 1], ['path' => array_unique($paths[1])]);
                        $paths[2] && Model::update('media', ['status' => 2], ['path' => array_unique($paths[2])]);
                        // 清除文章缓存
                        $this->clearCache($row['article_id']);
                        Util::config('log') && Model::insert('log', ['author' => $this->login['username'], 'request_method' => Util::requestMethod(), 'url' => htmlentities($_SERVER['REQUEST_URI']), 'title' => Util::tran('前端删除评论'), 'message' => Util::tran('前端删除评论') . " " . implode(' ', $comNames) . " " . Util::tran('成功'), 'ip' => Util::getIp(), 'status' => 0, 'time' => time()]);
                        Util::errMsg(['code' => 0, 'msg' => Util::tran('删除成功')]);
                    } else {
                        Util::config('log') && Model::insert('log', ['author' => $this->login['username'], 'request_method' => Util::requestMethod(), 'url' => htmlentities($_SERVER['REQUEST_URI']), 'title' => Util::tran('前端删除评论'), 'message' => Util::tran('前端删除评论') . " " . implode(' ', $comNames) . " " . Util::tran('失败'), 'ip' => Util::getIp(), 'status' => 1, 'time' => time()]);
                        Util::errMsg(['code' => 1, 'msg' => Util::tran('删除失败')]);
                    }
                    break;

                    // 下载内容
                case 'download':
                    $param = Util::param(['post_id']);
                    $row = Model::fetch("SELECT * FROM `article` WHERE `status` = 0  AND `id` = :id ORDER BY `id` LIMIT 1", [':id' => empty($param['post_id']) ? 0 : (int)$param['post_id']]);
                    empty($row) && Util::errMsg(Util::tran('非法文章ID'));
                    $row = $this->data([$row])[0];
                    $id_or_slug = isset($row['slug']) && $row['slug'] ? $row['slug'] : $param['post_id'];
                    $link = "{$this->site_url}/{$this->replace_path}{$id_or_slug}.html";
                    $title = $row['title'];
                    $description = $row['description'];
                    $entry_content = $row['content'];
                    // 繁体中文，字典替换
                    if ($this->lang === 'zh-TW') {
                        require IA_ROOT . '/util/zh-tranTW.php';
                        $title = zh_tranTW($title);
                        $description = zh_tranTW($description);
                        $entry_content = zh_tranTW($entry_content);
                    }
                    // $tts_content = $row['content'];
                    // $ignore = ['style', 'script', 'iframe', 'video', 'audio', 'source', 'pre', 'code', 'img', 'table', 'form', 'fieldset', 'legend', 'label', 'input', 'textarea', 'select', 'button'];
                    // foreach ($ignore as $v) {
                    //     $tts_content = preg_replace('/<' . $v . '[^>]*>.*?<\/' . $v . '>/sim', '', $tts_content);
                    // }
                    // $tts_content = preg_replace('/<br[^>]*>/sim', '。', $tts_content);
                    // $tts_content = preg_replace('/<[^>]+>(.+?)<\/[^>]+>/sim', '$1。', $tts_content);
                    // $tts_content = preg_replace('/<[^>]+>/sim', '', $tts_content);
                    // $tts_content = preg_replace('/\s+/sim', ' ', $tts_content);
                    // $tts_content = trim(preg_replace('/\。+/sim', '', strip_tags($tts_content)));
                    // 内容媒体编码
                    $n = preg_match_all('/<(img|video|audio|source|embed)([^>]*)src=["\'](.*?)["\']([^>]*)>/sim', $entry_content, $matches);
                    for ($i = 0; $i < $n; $i++) {
                        list($src) = explode('?', $matches[3][$i]);
                        if (false !== strpos($src, $this->site_url)) {
                            !is_file(str_replace($this->site_url, IA_ROOT, $src)) && Util::errMsg(Util::tran('本地媒体已失效或不存在'));
                            $media = $this->base64Code($src);
                            if (!$media) Util::errMsg(Util::tran('媒体合成') . ' ' . ($i + 1) . '/' . $n . ' ' . Util::tran('失败！请稍后再试。'));
                            $entry_content = str_replace($matches[0][$i], '<' . $matches[1][$i] . $matches[2][$i] . 'src="' . $media . '"' . $matches[4][$i] . '>', $entry_content);
                        }
                    }
                    // 合成语音编码
                    // $ttsUrl = 'https://dict.youdao.com/dictvoice?le=zh&audio=';
                    // $ttsUrl = 'https://translate.googleapis.com/translate_tts?ie=UTF-8&tl=zh-CN&tk=663060.841892&client=webapp&q=';
                    // 不支持日语，土耳其语，英语，韩语
                    // $ttsUrl = 'https://tts.baidu.com/text2audio?cuid=baike&lan=zh&ctp=1&pdt=301&vol=9&rate=32&per=0&tex=';
                    // 不支持德语，日语，阿拉伯语，土耳其语，韩语，法语
                    // $ttsUrl = 'https://fanyi.baidu.com/gettts?lan=zh&spd=5&source=web&text=';
                    // 不支持日语，俄语，阿拉伯语，泰语，韩语
                    // $ttsUrl = 'https://fanyi.sogou.com/reventondc/synthesis?speed=1&lang=zh-CHS&from=translateweb&speaker=6&text=';
                    // 保持停顿等距分割
                    // $ttsArr = $this->ttsStrSplit($tts_content, preg_match('/\/\/[\w]+\.google/', $ttsUrl) ? 200 : 500);
                    // $len = count($ttsArr);
                    // for ($i = 0; $i < $len; $i++) {
                    //     parse_str(substr(strstr($ttsUrl . urlencode($ttsArr[$i]), '?'), 1), $data);
                    //     $tts = $this->base64Code(strstr($ttsUrl, '?', true), $data, 'POST');
                    //     if (!$tts) Util::errMsg(Util::tran('语音合成') . ' ' . ($i + 1) . '/' . $len . ' ' . Util::tran('失败！请稍后再试。'));
                    //     $ttsArr[$i] = $tts;
                    // }
                    // $tts_content = json_encode($ttsArr);
                    !is_dir(IA_ROOT . '/static/upload/articles/') && mkdir(IA_ROOT . '/static/upload/articles/', 0700, true);
                    $file = '/static/upload/articles/ID' . str_pad($param['post_id'], 7, '0', STR_PAD_LEFT) . '-' . str_replace('/', '-', $this->replace_path) . $id_or_slug . '.html';
                    $handle = fopen(IA_ROOT . $file, "w");
                    fwrite($handle, '<!DOCTYPE html>
<html lang="' . $this->lang . '">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>' . strip_tags($title) . '</title>
    <link rel="shortcut icon" href="data:image/svg+xml;utf8,%3Csvg xmlns=\'http://www.w3.org/2000/svg\' fill=\'%23673ab7\' viewBox=\'0 0 16 16\'%3E%3Cpath d=\'M6.375 7.125V4.658h1.78c.973 0 1.542.457 1.542 1.237 0 .802-.604 1.23-1.764 1.23zm0 3.762h1.898c1.184 0 1.81-.48 1.81-1.377 0-.885-.65-1.348-1.886-1.348H6.375z\'/%3E%3Cpath d=\'M4.002 0a4 4 0 0 0-4 4v8a4 4 0 0 0 4 4h8a4 4 0 0 0 4-4V4a4 4 0 0 0-4-4zm1.06 12V3.545h3.399c1.587 0 2.543.809 2.543 2.11 0 .884-.65 1.675-1.483 1.816v.1c1.143.117 1.904.931 1.904 2.033 0 1.488-1.084 2.396-2.888 2.396z\'/%3E%3C/svg%3E">
    <link rel="icon" href="data:image/svg+xml;utf8,%3Csvg xmlns=\'http://www.w3.org/2000/svg\' fill=\'%23673ab7\' viewBox=\'0 0 16 16\'%3E%3Cpath d=\'M6.375 7.125V4.658h1.78c.973 0 1.542.457 1.542 1.237 0 .802-.604 1.23-1.764 1.23zm0 3.762h1.898c1.184 0 1.81-.48 1.81-1.377 0-.885-.65-1.348-1.886-1.348H6.375z\'/%3E%3Cpath d=\'M4.002 0a4 4 0 0 0-4 4v8a4 4 0 0 0 4 4h8a4 4 0 0 0 4-4V4a4 4 0 0 0-4-4zm1.06 12V3.545h3.399c1.587 0 2.543.809 2.543 2.11 0 .884-.65 1.675-1.483 1.816v.1c1.143.117 1.904.931 1.904 2.033 0 1.488-1.084 2.396-2.888 2.396z\'/%3E%3C/svg%3E">
    ' . ($this->lang === 'mni-Mtei' ? '<link href="https://fonts.googleapis.com/css?family=Noto Sans Meetei Mayek" rel="stylesheet">' : '') . '
    <style type="text/css">
        <!--
        *{margin:0;padding:0;box-sizing:border-box}
        html{text-align:center}
        body{text-align:left;color:#333;font-family:"Helvetica Neue",Helvetica,Arial,sans-serif' . ($this->lang === 'mni-Mtei' ? ',"Noto Sans Meetei Mayek"' : '') . '}
        body>svg{position:absolute;width:0;height:0;overflow:hidden}
        .container{max-width:980px;margin:20px auto;padding:0 20px}
        .container>*{margin:15px 0}
        .entry-title{text-align:center;margin:15px 0}
        .entry-toolbar{margin:15px 0}
        .entry-toolbar .icon{display:inline-block;width:1em;height:1em;stroke-width:0;stroke:currentColor;fill:currentColor;color:#333}
        .entry-toolbar button{width:40px;height:40px}
        .entry-toolbar button:hover{cursor:pointer}
        .entry-toolbar button.not-allowed:hover{cursor:not-allowed}
        .entry-toolbar .left button svg,.entry-toolbar .right button{font-size:22px}
        .entry-toolbar .left,.entry-toolbar .left button{float:left;margin-right:5px}
        .entry-toolbar .right,.entry-toolbar .right button{float:right;margin-left:5px}
        .entry-toolbar button.invisible{display:none}
        .entry-toolbar button.visible{display:block}
        .entry-toolbar .clear{clear:both}
        .icon-loading{display:inline-block;width:1em;height:1em;border:3px solid #e5e5e5;border-top:3px solid #333;border-radius:50%;-o-animation:turn-around 1.5s linear infinite;-webkit-animation:turn-around 1.5s linear infinite;-moz-animation:turn-around 1.5s linear infinite;-ms-animation:turn-around 1.5s linear infinite;animation:turn-around 1.5s linear infinite}
        @keyframes turn-around{from{transform:rotate(0deg)} to{transform:rotate(360deg)}}
        .entry-content >*{margin:15px 0}
        .entry-content .lead{font-size:22px}
        .entry-content .thumbnail{display:block;max-width:100%;width:auto;clear:both;margin:15px 0;border:1px solid #ddd;border-radius:4px;padding:4px}
        .entry-content ul, .entry-content ol{list-style-position:inside}
        .entry-content>.tts{margin:0;padding:0;border:0;border-radius:0;background-color:#fde6e0}
        .entry-content>div.tts, .entry-content>p.tts{margin:15px 0;padding:10px;border:1px solid #dcd6d6;border-radius:3px;background-color:#f9f2f4}
        .entry-content pre.prettyprint{display:block;line-height:1.42857143;color:#333;word-break:break-all;word-wrap:break-word;background-color:#f5f5f5;border:0;border-radius:4px;border-left:3px solid #ccc;padding:.5em;font-size:110%;font-family:Consolas,Monaco,"Bitstream Vera Sans Mono","Courier New",Courier,monospace;margin:1em 0;white-space:pre;overflow-x:auto}
        .entry-footer{margin:15px 0 25px;text-align:left;font-size:14px;color:#999}
        .entry-footer a,.entry-footer a:hover,.entry-footer a:visited{color:#999}
        -->
    </style>
</head>
<body>
    <svg aria-hidden="true" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
        <defs>
            <symbol id="icon-headphones" viewBox="0 0 32 32">
                <path d="M9 18h-2v14h2c0.55 0 1-0.45 1-1v-12c0-0.55-0.45-1-1-1z"></path>
                <path d="M23 18c-0.55 0-1 0.45-1 1v12c0 0.55 0.45 1 1 1h2v-14h-2z"></path>
                <path d="M32 16c0-8.837-7.163-16-16-16s-16 7.163-16 16c0 1.919 0.338 3.759 0.958 5.464-0.609 1.038-0.958 2.246-0.958 3.536 0 3.526 2.608 6.443 6 6.929v-13.857c-0.997 0.143-1.927 0.495-2.742 1.012-0.168-0.835-0.258-1.699-0.258-2.584 0-7.18 5.82-13 13-13s13 5.82 13 13c0 0.885-0.088 1.749-0.257 2.584-0.816-0.517-1.745-0.87-2.743-1.013v13.858c3.392-0.485 6-3.402 6-6.929 0-1.29-0.349-2.498-0.958-3.536 0.62-1.705 0.958-3.545 0.958-5.465z"></path>
            </symbol>
            <symbol id="icon-play" viewBox="0 0 32 32">
                <path d="M16 0c-8.837 0-16 7.163-16 16s7.163 16 16 16 16-7.163 16-16-7.163-16-16-16zM16 29c-7.18 0-13-5.82-13-13s5.82-13 13-13 13 5.82 13 13-5.82 13-13 13zM12 9l12 7-12 7z"></path>
            </symbol>
            <symbol id="icon-pause" viewBox="0 0 32 32">
                <path d="M16 0c-8.837 0-16 7.163-16 16s7.163 16 16 16 16-7.163 16-16-7.163-16-16-16zM16 29c-7.18 0-13-5.82-13-13s5.82-13 13-13 13 5.82 13 13-5.82 13-13 13zM10 10h4v12h-4zM18 10h4v12h-4z"></path>
            </symbol>
            <symbol id="icon-stop" viewBox="0 0 32 32">
                <path d="M16 0c-8.837 0-16 7.163-16 16s7.163 16 16 16 16-7.163 16-16-7.163-16-16-16zM16 29c-7.18 0-13-5.82-13-13s5.82-13 13-13 13 5.82 13 13-5.82 13-13 13zM10 10h12v12h-12z"></path>
            </symbol>
            <symbol id="icon-top" viewBox="0 0 32 32">
                <path d="M0 16c0 8.837 7.163 16 16 16s16-7.163 16-16-7.163-16-16-16-16 7.163-16 16zM29 16c0 7.18-5.82 13-13 13s-13-5.82-13-13 5.82-13 13-13 13 5.82 13 13z"></path>
                <path d="M22.086 20.914l2.829-2.829-8.914-8.914-8.914 8.914 2.828 2.828 6.086-6.086z"></path>
            </symbol>
            <symbol id="icon-mobile" viewBox="0 0 32 32">
                <path d="M23 0h-14c-1.65 0-3 1.35-3 3v26c0 1.65 1.35 3 3 3h14c1.65 0 3-1.35 3-3v-26c0-1.65-1.35-3-3-3zM12 1.5h8v1h-8v-1zM16 30c-1.105 0-2-0.895-2-2s0.895-2 2-2 2 0.895 2 2-0.895 2-2 2zM24 24h-16v-20h16v20z"></path>
            </symbol>
        </defs>
    </svg>
    <div class="container" id="container">
        <h1 class="entry-title">' . $title . '</h1>
        <div class="entry-toolbar">
            <div class="left">
                ' . (isset($this->setting['voice_read']) && $this->setting['voice_read'] ? '<button type="button" id="btn-headphones" class="not-allowed" disabled="disabled"><svg class="icon icon-headphones"><use xlink:href="#icon-headphones"></use></svg></button>
                <button type="button" id="btn-play" class="visible' . ($this->voice ? '' : ' not-allowed') . '"' . ($this->voice ? '' : ' disabled="disabled"') . '><svg class="icon icon-play"><use xlink:href="#icon-play"></use></svg></button>
                <button type="button" id="btn-pause" class="invisible"><svg class="icon icon-pause"><use xlink:href="#icon-pause"></use></svg></button>
                <button type="button" id="btn-stop" class="invisible"><svg class="icon icon-stop"><use xlink:href="#icon-stop"></use></svg></button>' : '<button type="button" id="btn-mobile"><a href="' . $this->site_url . '/app.html#' . str_replace($this->site_url, '', $link) . '" title="H5 ' . Util::tran('客户端') . '"><svg class="icon icon-top"><use xlink:href="#icon-mobile"></use></svg></a></button>') . '
                <button type="button" id="btn-top" class="invisible"><svg class="icon icon-top"><use xlink:href="#icon-top"></use></svg></button>
            </div>
            <div class="right">
                <button type="button" id="btn-smaller" data-size="smaller">A <sup>-</sup></button>
                <button type="button" id="btn-bigger">A <sup>+</sup></button>
            </div>
            <div class="clear"></div>
        </div>
        <div id="loading"></div>
        <div class="entry-content" id="entry-content" style="font-size:16px">
            ' . ($description ? '<p class="lead">' . strip_tags($description) . '</p>' : '') . '
            ' . stripslashes($entry_content) . '
        </div>
        <div class="entry-footer">' . sprintf('Time: %1$s<br />URL: <a href="%2$s" title="%3$s">%2$s</a>', date('Y-m-d H:i:s', time()), $link, strip_tags($title)) . '</div>
    </div>
    <script type="text/javascript">
        /* <![CDATA[ */
        var render = {
            stopped: true,
            audioArray: [],
            audio: new Audio(),
            textArray: [],
            textFontSize: 16,
            timer: null,
            nodes: [],
            speechInstance: null,
            offLine: false,
            api: "",
            playErrors: 0,
            playErrorTimer: null,
            init: function () {
                var nodes = document.getElementById("entry-content").childNodes;
                var ignore = ["STYLE", "SCRIPT", "IFRAME", "VIDEO", "AUDIO", "SOURCE", "PRE", "CODE", "IMG", "BR", "HR", "TABLE", "FORM", "FIELDSET", "LEGEND", "LABEL", "INPUT", "TEXTAREA", "SELECT", "BUTTON"];
                var meta = document.createElement("meta");
                meta.setAttribute("name", "referrer");
                meta.setAttribute("content", "same-origin");
                document.head.appendChild(meta).parentNode.removeChild(meta);
                for (var i = 0; i < nodes.length; i++) {
                    var node = nodes.item(i);
                    if (ignore.indexOf(node.tagName) === -1 && node.textContent.trim()) {
                        render.nodes.push({ node: node, index: i });
                    }
                }
                if ("speechSynthesis" in window) {
                    var i = 0;
                    var timer = setInterval(function () {
                        i++;
                        var voices = window.speechSynthesis.getVoices();
                        if (voices.length || i > 10) {
                            clearInterval(timer);
                            timer = null;
                            for (var j = 0; j < voices.length; j++) {
                                if (voices[j].lang.indexOf("' . $this->lang . '") !== -1) {
                                    render.offLine = voices[j].lang;
                                    break;
                                }
                            }
                        }
                    }, 100);
                }
                document.getElementById("btn-play") && document.getElementById("btn-play").addEventListener("click", render.play);
                document.getElementById("btn-pause") && document.getElementById("btn-pause").addEventListener("click", render.pause);
                document.getElementById("btn-stop") && document.getElementById("btn-stop").addEventListener("click", render.stop);
                document.getElementById("btn-top").addEventListener("click", render.top);
                document.getElementById("btn-bigger").addEventListener("click", render.bigger);
                document.getElementById("btn-smaller").addEventListener("click", render.smaller);
                window.addEventListener("scroll", render.scrollToTop);
            },
            bigger: function () {
                if (render.textFontSize <= 24) {
                    render.textFontSize += 2;
                }
                document.getElementById("entry-content").style.cssText = "font-size:" + render.textFontSize + "px";
            },
            smaller: function () {
                if (render.textFontSize >= 14) {
                    render.textFontSize -= 2;
                }
                document.getElementById("entry-content").style.cssText = "font-size:" + render.textFontSize + "px";
            },
            getTk: function (a) {
                var tkk = "406398.2087938574", e = tkk.split("."), h = Number(e[0]) || 0, g = [], d = 0, i, j;
                for (var f = 0; f < a.length; f++) {
                    var c = a.charCodeAt(f);
                    128 > c ? g[d++] = c : (2048 > c ? g[d++] = c >> 6 | 192 : (55296 == (c & 64512) && f + 1 < a.length && 56320 == (a.charCodeAt(f + 1) & 64512) ? (c = 65536 + ((c & 1023) << 10) + (a.charCodeAt(++f) & 1023), g[d++] = c >> 18 | 240, g[d++] = c >> 12 & 63 | 128) : g[d++] = c >> 12 | 224, g[d++] = c >> 6 & 63 | 128), g[d++] = c & 63 | 128)
                }
                a = h;
                for (var d = 0; d < g.length; d++) {
                    a += g[d];
                    i = "+-a^+6";
                    for (var k = 0; k < i.length - 2; k += 3) {
                        j = i.charAt(k + 2);
                        j = "a" <= j ? j.charCodeAt(0) - 87 : Number(j);
                        j = "+" == i.charAt(k + 1) ? a >>> j : a << j;
                        a = "+" == i.charAt(k) ? a + j & 4294967295 : a ^ j;
                    }
                }
                i = "+-3^+b+-f";
                for (var k = 0; k < i.length - 2; k += 3) {
                    j = i.charAt(k + 2);
                    j = "a" <= j ? j.charCodeAt(0) - 87 : Number(j);
                    j = "+" == i.charAt(k + 1) ? a >>> j : a << j;
                    a = "+" == i.charAt(k) ? a + j & 4294967295 : a ^ j;
                }
                a ^= Number(e[1]) || 0;
                0 > a && (a = (a & 2147483647) + 2147483648);
                a %= 1E6;
                return a.toString() + "." + (a ^ h);
            },
            playEndedHandler: function () {
                render.playErrors = 0;
                if (render.textArray.length > 0) {
                    if (!render.offLine) {
                        render.audio.src = render.audioArray.pop();
                        render.audio.play();
                    }
                    var reader = render.textArray.shift();
                    if (render.offLine) {
                        render.speechInstance.text = reader.text;
                        window.speechSynthesis.speak(render.speechInstance);
                    }
                    var elem = null;
                    for (var i = 0; i < render.nodes.length; i++) {
                        if (reader.index == render.nodes[i].index) {
                            if (render.nodes[i].node.nodeType === 3) {
                                var wrapper = document.createElement("span");
                                render.nodes[i].node.parentNode.insertBefore(wrapper, render.nodes[i].node);
                                render.nodes[i].node.parentNode.removeChild(render.nodes[i].node);
                                wrapper.appendChild(render.nodes[i].node);
                                render.nodes[i].node = wrapper;
                            }
                            elem = render.nodes[i].node;
                            if (render.nodes[i].node.className) {
                                render.nodes[i].node.className += render.nodes[i].node.className.match(/^tts$|^tts\s+|\s+tts/) ? "" : " tts";
                            } else {
                                render.nodes[i].node.className = "tts";
                            }
                        } else {
                            if (render.nodes[i].node.nodeType === 1 && render.nodes[i].node.className) {
                                render.nodes[i].node.className = render.nodes[i].node.className.replace(/^tts$|^tts\s+|\s+tts/g, "");
                            }
                        }
                    }
                    var clientHeight = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;
                    var scrollTop = document.documentElement.scrollTop || document.body.scrollTop;
                    var pos = 0;
                    var topH = elem.offsetHeight;
                    while (elem) {
                        pos += elem.offsetTop;
                        elem = elem.offsetParent;
                    }
                    if (pos > clientHeight + scrollTop - topH) {
                        var distance = 0;
                        var timer = setInterval(function () {
                            distance += Math.ceil((pos - distance) / 10);
                            window.scrollTo(0, Math.min(distance, pos));
                            if (distance >= pos - 10) {
                                window.scrollTo(0, pos - 10);
                                clearInterval(timer);
                                timer = null;
                            }
                        }, 10);
                    }
                } else {
                    render.stop();
                    setTimeout(function () {
                        if (render.nodes[render.nodes.length - 1].node.nodeType === 3) {
                            var wrapper = document.createElement("span");
                            render.nodes[render.nodes.length - 1].node.parentNode.insertBefore(wrapper, render.nodes[render.nodes.length - 1].node);
                            render.nodes[render.nodes.length - 1].node.parentNode.removeChild(render.nodes[render.nodes.length - 1].node);
                            wrapper.appendChild(render.nodes[render.nodes.length - 1].node);
                            render.nodes[render.nodes.length - 1].node = wrapper;
                        }
                        elem = render.nodes[render.nodes.length - 1].node;
                        var distance = 0;
                        while (elem) {
                            distance += elem.offsetTop;
                            elem = elem.offsetParent;
                        }
                        var timer = setInterval(function () {
                            distance += Math.floor((0 - distance) / 10);
                            window.scrollTo(0, Math.max(distance, 0));
                            if (distance <= 0) {
                                window.scrollTo(0, 0);
                                clearInterval(timer);
                                timer = null;
                            }
                        }, 10);
                    }, 1000);
                }
            },
            playErrorHandler: function () {
                render.playErrorTimer && clearTimeout(render.playErrorTimer);
                render.playErrorTimer = setTimeout(function () {
                    if (render.playErrors % 10 < 9) {
                        render.playErrors++;
                        render.audio.src = render.audio.src.split("&t=")[0] + "&t=" + new Date().getTime();
                        render.audio.play();
                    } else {
                        clearTimeout(render.playErrorTimer);
                        render.playErrorTimer = null;
                        render.playErrors = 0;
                        document.getElementById("loading").innerHTML = "<p>Error! Audio error code: " + render.audio.error.code + "</p>";
                        setTimeout(function() {
                            render.stop();
                        }, 5000);
                    }
                }, 3000 + Math.ceil(Math.random() * 2000));
            },
            play: function () {
                document.getElementById("btn-play").setAttribute("class", "invisible");
                document.getElementById("btn-pause").setAttribute("class", "visible");
                document.getElementById("btn-stop").setAttribute("class", "visible");
                if (render.stopped) {
                    if (render.nodes.length < 1) {
                        return false;
                    }
                    document.getElementById("loading").innerHTML = "<p><span class=\"icon-loading\"></span> ' . Util::tran('请稍后') . ' ...</p>";
                    var icon = new Image();
                    icon.src = "https://translate.google.com/favicon.ico?t=" + new Date().getTime();
                    var timer = setTimeout(function () {
                        icon.onerror = icon.onload = null;
                        youdao_dictvoice();
                    }, 3000);
                    icon.onerror = function () {
                        clearTimeout(timer);
                        timer = null;
                        youdao_dictvoice();
                    }
                    icon.onload = function () {
                        clearTimeout(timer);
                        timer = null;
                        google_translate_tts();
                    }
                    function google_translate_tts() {
                        render.offLine = false;
                        render.api = "https://translate.google.com/translate_tts?client=webapp&ie=UTF-8&tl=' . ($this->voice ? $this->voice : $this->lang) . '&q=";
                        doPlay();
                    }
                    function youdao_dictvoice() {
                        var icon = new Image();
                        icon.src = "https://dict.youdao.com/favicon.ico?t=" + new Date().getTime();
                        var timer = setTimeout(function () {
                            icon.onerror = icon.onload = null;
                            if (render.offLine) {
                                doPlay();
                            } else {
                                document.getElementById("loading").innerHTML = "<p>' . Util::tran('朗读错误，请联网后再试') . ' ...</p>";
                                setTimeout(function() {
                                    render.stop();
                                }, 5000);
                            }
                        }, 3000);
                        icon.onerror = function () {
                            clearTimeout(timer);
                            timer = null;
                            if (render.offLine) {
                                doPlay();
                            } else {
                                document.getElementById("loading").innerHTML = "<p>' . Util::tran('朗读错误，请联网后再试') . ' ...</p>";
                                setTimeout(function() {
                                    render.stop();
                                }, 5000);
                            }
                        }
                        icon.onload = function () {
                            clearTimeout(timer);
                            timer = null;
                            if (["ar", "de", "ru", "fr", "ko", "nl", "pt", "ja", "th", "es", "en", "it", "vi", "id"].indexOf("' . $this->lang . '") !== -1 || "' . $this->lang . '".match(/^zh-/)) {
                                render.offLine = false;
                                render.api = "https://dict.youdao.com/dictvoice?le=' . (preg_match('/^zh-/', $this->lang) ? 'zh-CHS' : $this->lang) . '&product=aibox&audio=";
                                doPlay();
                            } else {
                                document.getElementById("loading").innerHTML = "<p>' . Util::tran('有道接口不支持，需科学上网使用谷歌接口。') . '</p>";
                                document.getElementById("btn-play").setAttribute("disabled", "disabled");
                            }
                        }
                    }
                    function doPlay() {
                        var num = 200;
                        for (var i = 0; i < render.nodes.length; i++) {
                            var arrs = render.nodes[i].node.textContent.trim().split(/(\r\n|\r|\n)/g);
                            for (var j = 0; j < arrs.length; j++) {
                                var aAudioText = arrs[j].trim();
                                if (aAudioText && !render.offLine) {
                                    while (aAudioText.length >= num) {
                                        var arr = [];
                                        for (var k in aAudioText) {
                                            arr.push(aAudioText[k]);
                                            if (arr.join("").length >= num) {
                                                arr.pop();
                                                break;
                                            }
                                        }
                                        var pos = arr.join("").length;
                                        render.textArray.push({ text: aAudioText.slice(0, pos), index: render.nodes[i].index });
                                        aAudioText = aAudioText.slice(pos);
                                    }
                                }
                                aAudioText && render.textArray.push({ text: aAudioText, index: render.nodes[i].index });
                            }
                        }
                        if (render.textArray.length < 1) {
                            return false;
                        }
                        if (!render.offLine) {
                            for (var i = 0; i < render.textArray.length; i++) {
                                var address = render.api + encodeURIComponent(render.textArray[i]["text"]);
                                if (render.api.match(/\/\/[\w]+\.google/)) {
                                    address += "&tk=" + render.getTk(render.textArray[i]["text"]);
                                }
                                render.audioArray.unshift(address);
                            }
                            render.audio.preload = true;
                            render.audio.controls = true;
                            render.audio.src = render.audioArray.pop();
                            render.audio.addEventListener("ended", render.playEndedHandler);
                            render.audio.addEventListener("error", render.playErrorHandler);
                            render.audio.loop = false;
                            render.audio.play();
                        }
                        render.stopped = false;
                        var reader = render.textArray.shift();
                        if (render.offLine) {
                            render.speechInstance = new SpeechSynthesisUtterance();
                            window.speechSynthesis.cancel();
                            render.speechInstance.lang = render.offLine;
                            render.speechInstance.text = reader.text;
                            window.speechSynthesis.speak(render.speechInstance);
                            render.speechInstance.addEventListener("end", render.playEndedHandler);
                        }
                        for (var i = 0; i < render.nodes.length; i++) {
                            if (reader.index == render.nodes[i].index) {
                                if (render.nodes[i].node.nodeType === 3) {
                                    var wrapper = document.createElement("span");
                                    render.nodes[i].node.parentNode.insertBefore(wrapper, render.nodes[i].node);
                                    render.nodes[i].node.parentNode.removeChild(render.nodes[i].node);
                                    wrapper.appendChild(render.nodes[i].node);
                                    render.nodes[i].node = wrapper;
                                }
                                if (render.nodes[i].node.className) {
                                    render.nodes[i].node.className += render.nodes[i].node.className.match(/^tts$|^tts\s+|\s+tts/) ? "" : " tts";
                                } else {
                                    render.nodes[i].node.className = "tts";
                                }
                            } else {
                                if (render.nodes[i].node.nodeType === 1 && render.nodes[i].node.className) {
                                    render.nodes[i].node.className = render.nodes[i].node.className.replace(/^tts$|^tts\s+|\s+tts/g, "");
                                }
                            }
                        }
                        document.getElementById("loading").innerHTML = "";
                    }
                } else {
                    if (!render.offLine) {
                        render.audio.play();
                    } else {
                        window.speechSynthesis.resume();
                    }
                }
            },
            pause: function () {
                document.getElementById("btn-play").setAttribute("class", "visible");
                document.getElementById("btn-pause").setAttribute("class", "invisible");
                document.getElementById("btn-stop").setAttribute("class", "visible");
                document.getElementById("loading").innerHTML = "";
                if (!render.offLine) {
                    render.audio.pause();
                } else {
                    if (window.speechSynthesis.speaking) {
                        window.speechSynthesis.pause();
                    }
                }
            },
            stop: function () {
                document.getElementById("btn-play").setAttribute("class", "visible");
                document.getElementById("btn-pause").setAttribute("class", "invisible");
                document.getElementById("btn-stop").setAttribute("class", "invisible");
                document.getElementById("loading").innerHTML = "";
                if (!render.offLine) {
                    render.audio.pause();
                    render.audio.removeEventListener("ended", render.playEndedHandler);
                    render.audio.removeEventListener("error", render.playErrorHandler);
                    render.audioArray = [];
                } else {
                    window.speechSynthesis.cancel();
                    render.speechInstance.removeEventListener("end", render.playEndedHandler)
                }
                render.stopped = true;
                render.textArray = [];
                for (var i = 0; i < render.nodes.length; i++) {
                    if (render.nodes[i].node.nodeType === 1 && render.nodes[i].node.className) {
                        render.nodes[i].node.className = render.nodes[i].node.className.replace(/^tts$|^tts\s+|\s+tts/g, "");
                    }
                }
            },
            top: function () {
                var distance = document.documentElement.scrollTop || document.body.scrollTop;
                var timer = setInterval(function () {
                    distance += Math.floor((0 - distance) / 10);
                    window.scrollTo(0, Math.max(distance, 0));
                    if (distance <= 0) {
                        window.scrollTo(0, 0);
                        clearInterval(timer);
                        timer = null;
                    }
                }, 10);
            },
            scrollToTop: function () {
                if (render.timer) return false;
                render.timer = setTimeout(function () {
                    var scrollTop = document.documentElement.scrollTop || document.body.scrollTop;
                    var clientWidth = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
                    var offsetWidth = document.getElementById("container").offsetWidth;
                    var cssText = "";
                    if (scrollTop > 150) {
                        cssText = "position:fixed;right:calc(" + (clientWidth > 980 ? (clientWidth - offsetWidth) / 2 : 50) + "px - 40px);opacity:.7;";
                        document.getElementById("btn-play").style.cssText = cssText + "bottom:75px;";
                        document.getElementById("btn-pause").style.cssText = cssText + "bottom:75px;";
                        document.getElementById("btn-top").className = "visible";
                        document.getElementById("btn-top").style.cssText = cssText + "bottom:30px;";
                    } else {
                        cssText = "position:static;";
                        document.getElementById("btn-play").style.cssText = cssText;
                        document.getElementById("btn-pause").style.cssText = cssText;
                        document.getElementById("btn-top").className = "invisible";
                        document.getElementById("btn-top").style.cssText = cssText;
                    }
                    clearTimeout(render.timer);
                    render.timer = null;
                }, 100);
            }
        }
        render.init();
        /* ]]> */
    </script>
</body>
</html>');
                    fclose($handle);
                    !is_file(IA_ROOT . $file) && Util::errMsg(Util::tran('合成失败，请稍后再试') . '...');
                    // 清除文章缓存
                    $this->clearCache($param['post_id']);
                    Util::errMsg(['code' => 0, 'msg' => join('-', preg_split('/\s+/', $title)), 'link' => $this->site_url . $file]);
                    break;
                    // 非法请求
                default:
                    Util::errMsg(Util::tran('非法的ajax请求'));
            }
        } else {
            Util::errMsg(Util::tran('不合理的请求方法'));
        }
    }

    // base64媒体编码
    private function base64Code($url, $data = [], $type = 'GET')
    {
        $base64_code = '';
        $newFile = str_replace($this->site_url, IA_ROOT, $url);
        if (file_exists($newFile)) {
            // 本地文件
            $mime_type = '';
            if (function_exists('finfo_file')) {
                $finfo = finfo_open(FILEINFO_MIME_TYPE);
                $mime_type = finfo_file($finfo, $newFile);
                finfo_close($finfo);
            } elseif (function_exists('mime_content_type')) {
                $mime_type = mime_content_type($newFile);
            } else {
                $mime_type = Util::mimeType($newFile);
            }
            $base64_code = 'data:' . $mime_type . ';base64,' . base64_encode(file_get_contents($newFile));
        } else {
            // 网络文件
            $remote = Util::curl($url, $data, $type, [], true);
            if ($remote['result'] && $remote['mime_type']) {
                $base64_code = 'data:' . $remote['mime_type'] . ';base64,' . base64_encode($remote['result']);
            }
        }
        return $base64_code;
    }

    // tts语音字符保持停顿等距分割
    private function ttsStrSplit($string, $num = 1)
    {
        $aAudioText = '';
        $textArray = [];
        $textArr = preg_split('/;/', $string);
        for ($i = 0; $i < count($textArr); $i++) {
            if (mb_strlen(trim($textArr[$i]), 'utf8')) {
                if (mb_strlen($aAudioText, 'utf8') + mb_strlen($textArr[$i], 'utf8') < $num) {
                    $aAudioText .= $textArr[$i] . ';';
                } else {
                    while (mb_strlen($aAudioText, 'utf8') > $num) {
                        array_push($textArray, mb_substr($aAudioText, 0, $num, 'utf8'));
                        $aAudioText = mb_substr($aAudioText, $num, mb_strlen($aAudioText, 'utf8') - $num, 'utf8');
                    }
                    mb_strlen($aAudioText, 'utf8') && array_push($textArray, $aAudioText);
                    $aAudioText = $textArr[$i] . ';';
                    while (mb_strlen($aAudioText, 'utf8') > $num) {
                        array_push($textArray, mb_substr($aAudioText, 0, $num, 'utf8'));
                        $aAudioText = mb_substr($aAudioText, $num, mb_strlen($aAudioText, 'utf8') - $num, 'utf8');
                    }
                }
            }
        }
        mb_strlen($aAudioText, 'utf8') && array_push($textArray, $aAudioText);
        return $textArray;
    }

    // 清除文章缓存
    private function clearCache($article_id)
    {
        $slug = Model::fetchColumn("SELECT `slug` FROM `article` WHERE `id` = $article_id ORDER BY `id` LIMIT 1");
        $prefix = "http" . ((isset($_SERVER["HTTPS"]) && in_array(strtolower($_SERVER["HTTPS"]), ["on", "1"])) || (isset($_SERVER["SERVER_PORT"]) && "443" == $_SERVER["SERVER_PORT"]) ? "s" : "") . "://{$_SERVER["SERVER_NAME"]}" . (in_array($_SERVER["SERVER_PORT"], ["80", "443"]) ? "" : ":{$_SERVER['SERVER_PORT']}") . ($this->lang === 'zh-CN' ? "/" : "/{$this->lang}/");
        $file_name = md5($prefix);
        $cache_file = IA_ROOT . '/mvc/tpl/' . substr($file_name, 0, 2) . '/' . $file_name . '.php';
        if (file_exists($cache_file))
            unlink($cache_file);
        $file_name = md5($prefix . ($slug ? $slug : $article_id) . '.html');
        $cache_file = IA_ROOT . '/mvc/tpl/' . substr($file_name, 0, 2) . '/' . $file_name . '.php';
        if (file_exists($cache_file))
            unlink($cache_file);
        // 评论分页缓存
        $count = Model::fetchColumn("SELECT COUNT(*) FROM `comment` WHERE `pid`=0 AND `article_id`=:article_id AND `status`=0 ORDER BY `id` DESC", [':article_id' => $article_id]);
        for ($i = 2; $i <= ceil(intval($count) / $this->limit); $i++) {
            $file_name = md5($prefix . ($slug ? $slug : $article_id) . '/page-' . $i . '.html');
            $cache_file = IA_ROOT . '/mvc/tpl/' . substr($file_name, 0, 2) . '/' . $file_name . '.php';
            if (file_exists($cache_file))
                unlink($cache_file);
        }
    }

    // 获取接口
    public function fetch()
    {
        $param = Util::param();
        $url = $param['url'];
        $method = isset($param['method']) && $param['method'] ? strtoupper($param['method']) : 'GET';
        if (isset($param['url'])) unset($param['url']);
        if (isset($param['method'])) unset($param['method']);
        echo Util::curl($url, $param, $method);
    }

    // 网站地图
    public function sitemap()
    {
        $posts = Model::fetchAll("SELECT * FROM `article` WHERE `status` = 0 AND `lang` = '{$this->replace_lang}' ORDER BY `id` DESC LIMIT 1000");
        array_multisort(array_map(function ($item) {
            return $item['update_at'];
        }, $posts), SORT_DESC, $posts);
        $ltime = date('Y-m-d\TH:i:s+00:00', isset($posts[0]) ? $posts[0]['update_at'] : 0);
        echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
        echo "<urlset xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\" xmlns:mobile=\"http://www.baidu.com/schemas/sitemap-mobile/1/\">\n";
        echo "<url>\n";
        echo "\t<loc>{$this->site_url}/{$this->replace_path}</loc>\n";
        echo "\t<mobile:mobile type=\"pc,mobile\" />\n";
        echo "\t<lastmod>$ltime</lastmod>\n";
        echo "\t<changefreq>daily</changefreq>\n";
        echo "\t<priority>1.0</priority>\n";
        echo "</url>\n";
        // 文章页
        foreach ($posts as $item) {
            echo "<url>\n";
            echo "\t<loc>{$this->site_url}/{$this->replace_path}" . ($item['slug'] ? $item['slug'] : $item['id']) . ".html</loc>\n";
            echo "\t<lastmod>" . date('Y-m-d\TH:i:s+00:00', $item['update_at']) . "</lastmod>\n";
            echo "\t<changefreq>monthly</changefreq>\n";
            echo "\t<priority>0.6</priority>\n";
            echo "</url>\n";
        }
        // 独立页面
        $pages = Model::fetchAll("SELECT * FROM `page` WHERE `status` = 0 AND `lang` = '{$this->replace_lang}' ORDER BY `id` DESC");
        array_multisort(array_map(function ($item) {
            return $item['time'];
        }, $pages), SORT_DESC, $pages);
        foreach ($pages as $item) {
            echo "<url>\n";
            echo "\t<loc>{$this->site_url}/{$this->replace_path}page/" . ($item['slug'] ? $item['slug'] : $item['id']) . ".html</loc>\n";
            echo "\t<lastmod>" . date('Y-m-d\TH:i:s+00:00', $item['time']) . "</lastmod>\n";
            echo "\t<changefreq>weekly</changefreq>\n";
            echo "\t<priority>0.6</priority>\n";
            echo "</url>\n";
        }
        // 分类页
        $categories = Model::fetchAll("SELECT * FROM `category` WHERE `status` = 0 AND `lang` = '{$this->replace_lang}' ORDER BY `id` DESC");
        foreach ($categories as $item) {
            echo "<url>\n";
            echo "\t<loc>{$this->site_url}/{$this->replace_path}category/" . ($item['slug'] ? $item['slug'] : $item['id']) . ".html</loc>\n";
            echo "\t<lastmod>" . date('Y-m-d\TH:i:s+00:00', $item['time']) . "</lastmod>\n";
            echo "\t<changefreq>weekly</changefreq>\n";
            echo "\t<priority>0.8</priority>\n";
            echo "</url>\n";
        }
        echo "</urlset>\n";
        exit;
    }

    // 清除来源页缓存
    private function clearOrgin()
    {
        if (isset($_SERVER["HTTP_REFERER"])) {
            $file_name = md5($_SERVER["HTTP_REFERER"]);
            if ($pos = strpos($_SERVER["HTTP_REFERER"], 'redirect_to=')) {
                $file_name = md5(substr($_SERVER["HTTP_REFERER"], $pos + 12));
            }
            $cache_file = IA_ROOT . '/mvc/tpl/' . substr($file_name, 0, 2) . '/' . $file_name . '.php';
            is_file($cache_file) && unlink($cache_file);
        }
    }

    // 检查登录
    private function checkLogin()
    {
        $row = false;
        if (!isset($_SESSION['uid']) || empty($_SESSION['uid'])) {
            if (!isset($_COOKIE['token']) || empty($_COOKIE['token'])) return false;
            $row = Model::fetch("SELECT * FROM `user` WHERE `remember_token`=:token AND `status`=0 ORDER BY `id` ASC LIMIT 1", [':token' => $_COOKIE['token']]);
            if (!$row) return false;
            $_SESSION['uid'] = $row['id'];
        }
        !$row && $row = Model::fetch("SELECT * FROM `user` WHERE `id`=:id AND `status`=0 ORDER BY `id` ASC LIMIT 1", [':id' => $_SESSION['uid']]);
        if (!$row) return false;
        $auth_ids = Model::fetchColumn("SELECT `auth_ids` FROM `role` WHERE `id`=:role_id AND `status`=0 ORDER BY `id` ASC LIMIT 1", [':role_id' => $row['role_id']]);
        $row['auth_ids'] = json_decode($auth_ids ?: '[]', true);

        // 检查单点登录
        $setting = Model::fetch('SELECT * FROM `setting` WHERE id=:id ORDER BY `id` ASC LIMIT 1', [':id' => 1]);
        if ($setting && ($check = json_decode($setting['value'], true)) && isset($check['alone_login']) && $check['alone_login']) {
            $randstr = isset($_COOKIE['randstr']) ? $_COOKIE('randstr') : false;
            $cache_randstr = Util::getCache('rand' . $row['id']);
            if ($randstr !== $cache_randstr) {
                return false;
            }
        }

        return $row;
    }
}
