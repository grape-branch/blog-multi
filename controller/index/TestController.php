<?php

namespace Controller\index;

defined('IA_ROOT') || exit();

use mvc\Controller;
// use mvc\Facade;
// use model\TestModel;
use facade\TestModel;
use facade\Util;

// 控制器
class TestController extends Controller
{
    protected function initialize()
    {
        // 依赖的对象添加到容器中
        // Facade::bind('testmodel', function () {
        //     return new TestModel();
        // });
    }

    public function index()
    {
        // 从容器中取出方法
        // $data = Facade::make('testmodel')->getData();

        // 门面类静态化调用
        $data = TestModel::getData();

        $res = '';
        foreach ($data as $_data) {
            $res .= print_r($_data, true) . '<br>';
        }

        // controller/action/[params], querystring
        // $url = url('/test/index.html', $_GET)
        // $url = url('/test/index/', '#/test.html');

        $args = func_get_args();
        $res .= sprintf('路由地址: %s<br>', Util::url('/test/index/' . implode('/', $args), $_GET));

        $res .= sprintf('控制器: %s<br>', __CLASS__);
        $res .= sprintf('方法: %s<br>', __FUNCTION__);
        for ($i = 0; $i < count($args); $i++) {
            $res .= sprintf('第 %d 个参数: %s<br>', $i + 1, $args[$i]);
        }
        if (!empty($_SERVER['QUERY_STRING'])) {
            $res .= sprintf('查询字符串: %s<br>', $_SERVER['QUERY_STRING']);
        }

        echo $res;

        /**
         * http://localhost/test.html
         * 
         * Array ( [id] => 1 [name] => test1 )
         * Array ( [id] => 2 [name] => test2 )
         * 路由地址: http://localhost/test/index/
         * 控制器: Controller\TestController
         * 方法: index
         * 
         * http://localhost/test/index/arg1/arg2/demo.html?a=1&b=2
         * 
         * Array ( [id] => 1 [name] => test1 )
         * Array ( [id] => 2 [name] => test2 )
         * 路由地址: http://localhost/test/index/arg1/arg2/demo.html?a=1&b=2
         * 控制器: Controller\TestController
         * 方法: index
         * 第 1 个参数: arg1
         * 第 2 个参数: arg2
         * 第 3 个参数: demo.html
         * 查询字符串: a=1&b=2
         */
    }
}
