<?php

namespace facade;

defined('IA_ROOT') || exit();

use mvc\Facade;

// 门面静态化
class TestModel extends Facade
{
    public static function __callStatic($name, $arguments)
    {
        static::bind(__CLASS__, new \model\TestModel());
        return call_user_func_array([static::make(__CLASS__), $name], $arguments);
    }
}
