<?php
// 类自动加载器
spl_autoload_register(function ($class) {
    $path = str_replace('\\', DIRECTORY_SEPARATOR, $class) . '.php';
    if (!is_file(IA_ROOT . '/' . $path)) {
        throw new Exception($class);
    }
    require_once IA_ROOT . '/' . $path;
});
