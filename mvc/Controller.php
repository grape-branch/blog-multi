<?php

namespace Mvc;

// 控制器类
class Controller
{
    public function __construct()
    {
        $this->initialize();
    }

    // 初始化
    protected function initialize()
    {
    }

    // 设置类中不存在的属性时调用
    public function __set($name, $value)
    {
    }

    // 访问类中不存在的属性时调用
    public function __get($name)
    {
    }

    // 访问类中不存在的方法时调用
    public function __call($name, $arguments)
    {
    }

    // 访问类中不存在的静态方法时调用
    public static function __callStatic($name, $arguments)
    {
    }

    // 对象当字符串使用时调用
    public function __toString()
    {
    }

    // 对象当函数使用时调用
    public function __invoke()
    {
    }

    // 对象序列化时调用
    public function __sleep()
    {
    }

    // 对象反序列化时调用
    public function __wakeup()
    {
    }

    // 访问不可访问的属性时调用
    public function __isset($name)
    {
    }

    // 删除不可访问的属性时调用
    public function __unset($name)
    {
    }
}
