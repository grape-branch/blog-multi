<?php
/**
 * @author admin
 * @date 2024-06-06 04:59:01
 * @desc 吉尔吉斯语语言包
 */
return [
    "迷你mvc小框架，一套用于开发各类管理系统的通用型后台管理系统模板，基于开源免费的 layui 的制作，内置各类实际业务场景下相对丰富的示例。" => "Mini MVC - бул кичинекей алкак, ар кандай башкаруу системаларын иштеп чыгуу үчүн универсалдуу башкаруу тутумунун үлгүлөрүнүн жыйындысы, ал ачык булакка жана эркин layuiге негизделген жана ар кандай практикалык бизнес сценарийлеринде салыштырмалуу бай үлгүлөргө ээ.",
    "除非特别说明，本博客均为原创，如需转载，请以链接形式标明来源出处。" => "Эгерде башкасы айтылбаса, бул блог оригиналдуу болуп саналат.",
    "重置系统，将删除用户所有数据和附件，恢复到出厂设置。" => "Системаны баштапкы абалга келтирүү бардык колдонуучу дайындарын жана тиркемелерди жок кылат жана аны заводдук жөндөөлөргө кайтарат.",
    "用户名只能英文字母数字下划线或中划线，位数" => "Колдонуучунун аты англисче тамгаларды, сандарды, астын сызыктарды же астын сызыктарды жана цифралардын санын гана камтышы мүмкүн",
    "昵称只能中英文字母数字下划线中划线，位数" => "Каймана ат кытай жана англис тамгаларын, сандарды, астын сызыктарды, астын сызыктарды жана цифраларды гана камтышы мүмкүн.",
    "有道接口不支持，需科学上网使用谷歌接口。" => "Youdao интерфейси колдоого алынбайт. Интернетке илимий жактан кирүү үчүн Google интерфейсин колдонушуңуз керек.",
    "别名只能英文字母数字下划线中划线，位数" => "Лакап аттар англис тамгаларын, сандарды, астын сызыктарды, астын сызыктарды жана цифраларды гана камтышы мүмкүн.",
    "计算孤立文件需要较长时间，确定继续吗？" => "Жетим файлдарды эсептөө көп убакытты талап кылат. Чын эле уланткыңыз келеби?",
    "语音不支持，需科学上网使用谷歌接口。" => "Үн колдоого алынбайт, андыктан интернетке илимий жактан кирүү үчүн Google интерфейсин колдонушуңуз керек.",
    "您的账号已在别处登录，请重新登录！" => "Каттоо эсебиңиз башка жерден кирген, кайра кириңиз!",
    "你的账号已被禁用，请联系管理员" => "Каттоо эсебиңиз өчүрүлгөн, администратор менен байланышыңыз",
    "文件有在使用中，请重新计算状态" => "Файл колдонулууда, статусун кайра эсептеңиз",
    "目标语言权限不够，请联系管理员" => "Максаттуу тилге уруксаттар жетишсиз, администратор менен байланышыңыз",
    "真的标记垃圾评论选中的行吗？" => "Сиз чын эле тандалган саптарды спам деп белгилейсизби?",
    "源语言别名为空，请联系源作者" => "Булак тилинин лакап аты бош, булактын автору менен байланышыңыз",
    "点击上传，或将文件拖拽到此处" => "Жүктөө үчүн чыкылдатыңыз же файлды бул жерге сүйрөңүз",
    "合成成功！再次点击按钮下载。" => "Синтез ийгиликтүү! Жүктөп алуу үчүн баскычты кайра басыңыз.",
    "没有语言权限，请联系管理员" => "Тилге уруксат жок, администраторго кайрылыңыз",
    "源语言别名为空，不能国际化" => "Булак тилинин лакап аты бош жана аны эл аралык кылуу мүмкүн эмес.",
    "源语言与目标语言都不能为空" => "Булак тил да, максаттуу тил да бош болушу мүмкүн эмес.",
    "源语言与目标语言数量不一致" => "Булак жана максаттуу тилдердин саны дал келбейт",
    "删除文件失败，文件有在使用" => "Файл жок кылынбай калды, файл колдонулууда",
    "角色不存在，请联系管理员" => "Рол жок, администратор менен байланышыңыз",
    "角色被禁用，请联系管理员" => "Рол өчүрүлгөн, администратор менен байланышыңыз",
    "功能不存在，请联系管理员" => "Функция жок, администраторго кайрылыңыз",
    "功能被禁用，请联系管理员" => "Функция өчүрүлгөн, администраторго кайрылыңыз",
    "真的彻底删除选中的行吗？" => "Ал чындап эле тандалган сапты толугу менен жок кылабы?",
    "真的审核通过选中的行吗？" => "Тандалган саптар чындап эле бекитилгенби?",
    "目标语言与源语言不能相同" => "Максаттуу тил булак тил менен бирдей болушу мүмкүн эмес",
    "提取语言包与源语言不一致" => "Чыгарылган тил пакети баштапкы тилге дал келбейт",
    "文件有在使用，删除失败！" => "Файл колдонулууда жана жок кылынган жок!",
    "没有权限，请联系管理员" => "Уруксат жок, администраторго кайрылыңыз",
    "真的待审核选中的行吗？" => "Бул чындап эле каралып, тандалып алынышы керек болгон сызыкпы?",
    "目标语言包有空行，行数" => "Максаттуу тил пакетинде бош саптар, саптардын саны бар",
    "此操作恢复到出厂设置？" => "Бул операция заводдук жөндөөлөргө кайтарылабы?",
    "朗读错误，请联网后再试" => "Окуу катасы, Интернетке туташыңыз жана кайра аракет кылыңыз",
    "还没有添加分类描述信息" => "Категориянын сүрөттөмө маалыматы азырынча кошула элек",
    "本地媒体已失效或不存在" => "Жергиликтүү маалымат каражаттары жараксыз же жок",
    "库尔德语（库尔曼吉语）" => "күрт (курманжи)",
    "作者名未注册或被禁用" => "Автордун аты катталган эмес же өчүрүлгөн",
    "真的删除选中的行吗？" => "Тандалган сапты чын эле жок кыласызбы?",
    "真的还原选中的行吗？" => "Чын эле тандалган саптарды калыбына келтиресизби?",
    "真的删除行或子行么？" => "Сиз чындап эле саптарды же ички саптарды жок кылгыңыз келеби?",
    "目标语言包未发现空行" => "Кошумча тил топтомунда бош сап табылган жок",
    "该语言不支持语音朗读" => "Бул тил үн менен окууну колдоого албайт",
    "语言包数据未发生改变" => "Тил топтомунун дайындары өзгөргөн жок",
    "欢迎使用后台管理系统" => "Backend башкаруу системасына кош келиңиз",
    "文件有在使用或已失效" => "Файл колдонулууда же мөөнөтү бүттү",
    "合成失败，请稍后再试" => "Синтез ишке ашкан жок, бир аздан кийин кайталап көрүңүз",
    "&copy; 2021-2023 Company, Inc." => "© 2021-2023 Company, Inc.",
    "梅泰语（曼尼普尔语）" => "Мейтейче (Манипури)",
    "两次密码输入不一致" => "Эки сырсөз киргизүү дал келбейт",
    "该用户密码不可修改" => "Бул колдонуучунун сырсөзүн өзгөртүү мүмкүн эмес",
    "添加文章时创建标签" => "Макалаларды кошууда тегдерди түзүңүз",
    "删除文章时删除评论" => "Макаланы өчүрүүдө комментарийлерди өчүрүңүз",
    "控制器与方法已存在" => "Контроллер жана ыкма мурунтан эле бар",
    "标题或名称不能为空" => "Аталышы же аты бош болбошу керек",
    "请选择一种语言朗读" => "Үн чыгарып окуу үчүн тил тандаңыз",
    "分类有文章不能删除" => "Категорияда жок кылынбай турган макалалар бар",
    "审核为垃圾评论成功" => "Ийгиликтүү спам катары каралып чыкты",
    "审核为垃圾评论失败" => "Спам жорумду карап чыгуу ишке ашкан жок",
    "确定要登出站点吗？" => "Чын эле сайттан чыккыңыз келеби?",
    "网站名称或地址为空" => "Вебсайттын аты же дареги бош",
    "网站名称或地址重名" => "Кайталанма веб-сайттын аталышы же дареги",
    "允许上传的文件类型" => "Файл түрлөрүн жүктөөгө уруксат берилген",
    "标签有文章不能删除" => "Тегдери бар макалаларды жок кылуу мүмкүн эмес",
    "人觉得这篇文章很赞" => "Адамдар бул макаланы сонун деп ойлошот",
    "还没有页面描述信息" => "Азырынча беттин сүрөттөө маалыматы жок",
    "回复与评论内容重复" => "Жооптун жана комментарийдин кайталанган мазмуну",
    "失败！请稍后再试。" => "ийгиликсиз! Сураныч, кийинчерээк кайра аракет кылыңыз.",
    "主耶稣基督里的教会" => "Мырзабыз Иса Машаяктын чиркөөсү",
    "库尔德语（索拉尼）" => "күрт (сорани)",
    "布尔语(南非荷兰语)" => "логикалык (африкалыктар)",
    "用户名或密码错误" => "туура эмес колдонуучу аты же сырсөз",
    "源语言必须是中文" => "Булак тили кытай тили болушу керек",
    "目标语言别名为空" => "Максаттуу тилдин лакап аты бош",
    "国际化文章时标签" => "Эл аралык макала тэгдери",
    "确定清除缓存吗？" => "Кэшти чын эле тазалагыңыз келеби?",
    "超过的单文件大小" => "Жалгыз файлдын өлчөмү ашып кетти",
    "超过前端表单限制" => "Фронт формасынын чегинен ашып кетти",
    "目标没有写入权限" => "Максаттын жазууга уруксаты жок",
    "不允许的上传类型" => "Жүктөө түрлөрүнө уруксат берилбейт",
    "文件大小不能超过" => "Файлдын өлчөмү ашпоого тийиш",
    "保存基础设置成功" => "Негизги орнотуулар ийгиликтүү сакталды",
    "首次基础设置成功" => "Биринчи негизги орнотуу ийгиликтүү",
    "正在合成，请稍后" => "Синтезделүүдө, күтө туруңуз",
    "不合理的请求方法" => "Негизсиз өтүнүч ыкмасы",
    "Session无效或过期" => "Сеанс жараксыз же мөөнөтү бүттү",
    "手机号码不正确" => "телефон номери туура эмес",
    "手机号码已存在" => "Мобилдик номер мурунтан эле бар",
    "标题或内容为空" => "Аталышы же мазмуну бош",
    "创建文章时标签" => "Макала түзүүдө тегдер",
    "编辑文章时标签" => "Макаланы түзөтүүдө тегдер",
    "真的删除行么？" => "Чын эле сызыкты жок кылгыңыз келеби?",
    "请输入菜单名称" => "Сураныч, менюнун атын киргизиңиз",
    "请先删除子菜单" => "Сураныч, адегенде субменюну жок кылыңыз",
    "未登录访问后台" => "Каттоо эсебине кирбестен эле артка кириңиз",
    "Cookie无效或过期" => "Cookie жараксыз же мөөнөтү бүткөн",
    "真的还原行么？" => "Аны калыбына келтирүүгө чындап эле мүмкүнбү?",
    "编辑器内容为空" => "Редактордун мазмуну бош",
    "未选择整行文本" => "Тексттин бүт саптары тандалган жок",
    "划词选择行错误" => "Сөз тандоо катасы",
    "数据源发生改变" => "Маалымат булагы өзгөрөт",
    "填充成功，行号" => "Ийгиликтүү толтурулду, сап номери",
    "请输入分类名称" => "Категориянын атын киргизиңиз",
    "分类名不能为空" => "Категориянын аталышы бош болбошу керек",
    "排序只能是数字" => "Сорттоо сандык гана болушу мүмкүн",
    "请先删除子分类" => "Адегенде субкатегорияларды жок кылыңыз",
    "请输入评论作者" => "Сураныч, комментарий жазыңыз автор",
    "请选择目标语言" => "Сураныч, максаттуу тилди тандаңыз",
    "语言包生成成功" => "Тил топтому ийгиликтүү түзүлдү",
    "语言包生成失败" => "Тил пакети түзүлбөй калды",
    "国际化分类成功" => "Эл аралык классификация ийгиликтүү",
    "国际化分类失败" => "Эл аралык классификация ишке ашкан жок",
    "请输入标签名称" => "Энбелги атын киргизиңиз",
    "国际化标签成功" => "Эл аралык энбелги ийгилик",
    "国际化标签失败" => "Эл аралык теги ишке ашкан жок",
    "国际化文章成功" => "Эл аралык макала ийгилик",
    "国际化文章失败" => "Эл аралык макала ишке ашкан жок",
    "请输入页面标题" => "Барактын аталышын киргизиңиз",
    "国际化页面成功" => "Интернационалдаштыруу барагы ийгиликтүү болду",
    "国际化页面失败" => "Интернационалдаштыруу барагы ишке ашкан жок",
    "作者：葡萄枝子" => "Author: Жүзүм бутагы",
    "请输入网站名称" => "Вебсайттын атын киргизиңиз",
    "请输入网站地址" => "Вебсайттын дарегин киргизиңиз",
    "链接名不能为空" => "Шилтеменин аталышы бош болбошу керек",
    "上传文件不完整" => "Жүктөлгөн файл толук эмес",
    "没有文件被上传" => "Эч файл жүктөлгөн жок",
    "找不到临时目录" => "Temp каталог табылган жок",
    "未知的文件类型" => "Белгисиз файл түрү",
    "文件名不能为空" => "Файлдын аталышы бош болбошу керек",
    "个文件有在使用" => "файлдар колдонулууда",
    "请先删除子页面" => "Сураныч, адегенде суббаракты жок кылыңыз",
    "请输入角色名称" => "Сураныч, ролдун атын киргизиңиз",
    "管理员不可禁用" => "Администраторлор өчүрө албайт",
    "管理员不可删除" => "Администраторлор жок кыла албайт",
    "请输入限制大小" => "Сураныч, чектөө өлчөмүн киргизиңиз",
    "请输入版权信息" => "Сураныч, автордук укук маалыматын киргизиңиз",
    "恢复出厂成功！" => "Демейки баштапкы абалга кайтарылды!",
    "恢复出厂失败！" => "Демейкиге кайтаруу ишке ашкан жок!",
    "标签名不能为空" => "Тегдин аталышы бош болбошу керек",
    "还没有内容信息" => "Азырынча мазмун тууралуу маалымат жок",
    "这篇文章很有用" => "Бул макала абдан пайдалуу",
    "保存分类国际化" => "Категорияны интернационалдаштырууну сактаңыз",
    "分类国际化帮助" => "Классификацияны интернационалдаштыруу жардам",
    "保存标签国际化" => "Энбелгилерди эл аралык кылууну сактаңыз",
    "标签国际化帮助" => "Тег эл аралык жардам",
    "保存文章国际化" => "Макаланы интернационалдаштырууну сактаңыз",
    "文章国际化帮助" => "Макала эл аралык жардам",
    "保存页面国际化" => "Барактарды эл аралык сактоо",
    "页面国际化帮助" => "Барактын эл аралык жардам",
    "海地克里奥尔语" => "Гаити креол",
    "非法的ajax请求" => "Мыйзамсыз ajax өтүнүчү",
    "密码至少位数" => "Сырсөз кеминде сандардан турушу керек",
    "验证码不正确" => "Текшерүү коду туура эмес",
    "包含非法参数" => "Мыйзамсыз параметрлерди камтыйт",
    "请输入用户名" => "колдонуучунун атын киргизиңиз",
    "用户名已存在" => "Колдонуучунун аты мурунтан эле бар",
    "请重输入密码" => "Сырсөзүңүздү кайра киргизиңиз",
    "图片格式错误" => "Сүрөт форматында ката",
    "修改资料成功" => "Дайындар ийгиликтүү өзгөртүлдү",
    "没有改变信息" => "Маалымат өзгөргөн жок",
    "请输入浏览量" => "Көрүүлөрдүн санын киргизиңиз",
    "请输入点赞数" => "Лайктардын санын киргизиңиз",
    "请选择子分类" => "Сураныч, субкатегория тандаңыз",
    "创建文章成功" => "Макала ийгиликтүү түзүлдү",
    "创建文章失败" => "Макала түзүлбөй калды",
    "编辑文章成功" => "Макаланы ийгиликтүү түзөтүңүз",
    "标题不能为空" => "Аталышы бош болбошу керек",
    "菜单名称重复" => "Менюнун аталышын кайталоо",
    "创建菜单成功" => "Меню ийгиликтүү түзүлдү",
    "创建菜单失败" => "Меню түзүлбөй калды",
    "编辑菜单成功" => "Менюну оңдоо ийгиликтүү болду",
    "请选择行数据" => "Сураныч, катар дайындарын тандаңыз",
    "计算孤立文件" => "Жетим файлдарды сана",
    "划词选择错误" => "Сөздөрдү туура эмес тандоо",
    "请提取语言包" => "Сураныч, тил пакетин чыгарып алыңыз",
    "没有语音文字" => "Үн тексти жок",
    "语音朗读完成" => "Үн менен окуу аяктады",
    "分类名称为空" => "Категориянын аталышы бош",
    "创建分类成功" => "Классификация ийгиликтүү түзүлдү",
    "创建分类失败" => "Категория түзүлбөй калды",
    "编辑分类成功" => "Категорияны ийгиликтүү түзөтүү",
    "回复评论为空" => "Комментарийге жооп бош",
    "回复评论成功" => "Комментарийге ийгиликтүү жооп бериңиз",
    "回复评论失败" => "Жорумга жооп берүү ишке ашкан жок",
    "评论内容为空" => "Комментарий мазмуну бош",
    "编辑评论成功" => "Пикирди ийгиликтүү түзөтүү",
    "待审评论成功" => "Комментарий кароо ийгиликтүү аяктады",
    "待审评论失败" => "Каралып жаткан жорум ишке ашкан жок",
    "审核通过评论" => "Бекитилген комментарийлер",
    "审核评论成功" => "Карап чыгуу ийгиликтүү болду",
    "审核评论失败" => "Комментарий текшерүү ишке ашкан жок",
    "删除评论失败" => "Жорум жок кылынбай калды",
    "请选择源语言" => "Сураныч, булак тилин тандаңыз",
    "别名不可更改" => "Лакап аттарды өзгөртүү мүмкүн эмес",
    "标签名称为空" => "Тегдин аты бош",
    "创建链接成功" => "Шилтеме ийгиликтүү түзүлдү",
    "创建链接失败" => "Шилтеме түзүлбөй калды",
    "编辑链接成功" => "Шилтемени ийгиликтүү түзөтүңүз",
    "网站名称重名" => "Кайталанма вебсайт аталыштары",
    "网站地址重复" => "Кайталанма вебсайт дареги",
    "图片压缩失败" => "Сүрөт кысуу ишке ашкан жок",
    "移动文件失败" => "Файл жылдырылган жок",
    "上传文件成功" => "файл ийгиликтүү жүктөлдү",
    "上传文件失败" => "Файл жүктөлбөй калды",
    "共找到文件：" => "Жалпы табылган файлдар:",
    "创建页面成功" => "Барак ийгиликтүү түзүлдү",
    "创建页面失败" => "Барак түзүү ишке ашкан жок",
    "编辑页面成功" => "Баракты ийгиликтүү түзөтүңүз",
    "权限数据错误" => "Уруксат берилиштер катасы",
    "创建角色成功" => "Рол ийгиликтүү түзүлдү",
    "创建角色失败" => "Рол түзүлбөй калды",
    "编辑角色成功" => "Ролду ийгиликтүү түзөтүңүз",
    "游客不可删除" => "Коноктор жок кыла албайт",
    "创建标签成功" => "Тег ийгиликтүү түзүлдү",
    "创建标签失败" => "Энбелги түзүлбөй калды",
    "编辑标签成功" => "Тег ийгиликтүү түзөт",
    "角色数据错误" => "Белги дайындарынын катасы",
    "语言数据错误" => "Тил маалыматтарынын катасы",
    "状态数据错误" => "Статус маалыматынын катасы",
    "创建用户成功" => "Колдонуучу ийгиликтүү түзүлдү",
    "创建用户失败" => "Колдонуучу түзүлбөй калды",
    "编辑用户成功" => "Колдонуучуну ийгиликтүү түзөтүү",
    "本文博客网址" => "Бул макаланын блогунун URL дареги",
    "评论内容重复" => "Кайталанган комментарий мазмуну",
    "回复内容重复" => "Кайталанма жооп мазмуну",
    "评论发表成功" => "Комментарий ийгиликтүү жарыяланды",
    "发表评论失败" => "Комментарий калтырылган жок",
    "前端删除评论" => "Беттеги комментарийлерди жок кылуу",
    "中文（简体）" => "Кытай (жөнөкөйлөштүрүлгөн)",
    "加泰罗尼亚语" => "Каталанча",
    "苏格兰盖尔语" => "Шотландиялык галец",
    "中文（繁体）" => "салттуу кытай)",
    "马拉雅拉姆语" => "Малаяламча",
    "斯洛文尼亚语" => "Словенияча",
    "阿尔巴尼亚语" => "Албанча",
    "密码至少5位" => "Сырсөз кеминде 5 белгиден турушу керек",
    "你已经登录" => "Сиз мурунтан эле киргенсиз",
    "账号被禁用" => "Каттоо эсеби өчүрүлгөн",
    "请输入密码" => "Сырсөздү киргизиңиз",
    "留空不修改" => "Бош калтырып, өзгөртүүгө болбойт",
    "请输入标题" => "Сураныч, аталыш киргизиңиз",
    "请输入内容" => "Сураныч, мазмунду киргизиңиз",
    "多标签半角" => "Көп белгилүү жарым туурасы",
    "关键词建议" => "Ачкычтуу сүйлөм боюнча сунуштар",
    "请输入作者" => "Авторду киргизиңиз",
    "请选择数据" => "Сураныч, дайындарды тандаңыз",
    "软删除文章" => "макаланы жумшак өчүрүү",
    "软删除成功" => "Жумшак өчүрүү ийгиликтүү болду",
    "软删除失败" => "Жумшак өчүрүү ишке ашкан жок",
    "角色不存在" => "ролу жок",
    "角色被禁用" => "Рол өчүрүлгөн",
    "功能不存在" => "Функция жок",
    "功能被禁用" => "Функция өчүрүлгөн",
    "国际化帮助" => "Интернационалдык жардам",
    "未选择文本" => "Текст тандалган жок",
    "请选择语言" => "Сураныч, тил тандаңыз",
    "请输入排序" => "Сураныч, сорттоо киргизиңиз",
    "未修改属性" => "касиеттери өзгөртүлгөн эмес",
    "机器人评论" => "Робот сын-пикирлер",
    "生成语言包" => "Тил пакетин түзүү",
    "国际化分类" => "Эл аралык классификация",
    "国际化标签" => "эл аралык теги",
    "国际化文章" => "Эл аралык макалалар",
    "国际化页面" => "Эл аралык баракча",
    "服务器环境" => "Сервер чөйрөсү",
    "数据库信息" => "Маалыматтар базасы маалыматы",
    "服务器时间" => "сервер убактысы",
    "还没有评论" => "Азырынча комментарийлер жок",
    "还没有数据" => "Азырынча маалымат жок",
    "网址不合法" => "URL мыйзамсыз",
    "选择多文件" => "Бир нече файлды тандаңыз",
    "个未使用，" => "пайдаланылбаган,",
    "文件不存在" => "файл жок",
    "重命名失败" => "Атын өзгөртүү ишке ашкан жок",
    "重命名成功" => "Атын өзгөртүү ийгиликтүү",
    "角色已存在" => "Рол мурунтан эле бар",
    "蜘蛛不索引" => "жөргөмүш индекстөө эмес",
    "请输入数量" => "Сураныч, санын киргизиңиз",
    "昵称已存在" => "Лакап ат мурунтан эле бар",
    "页面没找到" => "Барак табылган жок",
    "还没有文章" => "Азырынча макалалар жок",
    "还没有页面" => "Азырынча бет жок",
    "还没有分类" => "Азырынча классификация жок",
    "还没有标签" => "Азырынча тэгдер жок",
    "还没有热门" => "Азырынча популярдуу эмес",
    "还没有更新" => "Азырынча жаңыртыла элек",
    "还没有网址" => "Азырынча URL жок",
    "请写点评论" => "Комментарий жазыңыз",
    "，等待审核" => ",Модерацияланган",
    "你已经注册" => "сиз буга чейин катталгансыз",
    "提取语言包" => "Тил пакетин чыгаруу",
    "分类国际化" => "Классификация Интернационализация",
    "标签国际化" => "Белги интернационализациясы",
    "文章国际化" => "Макаланы интернационалдаштыруу",
    "页面国际化" => "Барактын интернационализациясы",
    "格鲁吉亚语" => "Грузинче",
    "博杰普尔语" => "Bhojpuri",
    "白俄罗斯语" => "Беларусча",
    "斯瓦希里语" => "Свахиличе",
    "古吉拉特语" => "Гужаратиче",
    "斯洛伐克语" => "Словакча",
    "阿塞拜疆语" => "азербайжан",
    "印尼巽他语" => "Индонезиялык сундан",
    "加利西亚语" => "Галицияча",
    "拉脱维亚语" => "Латвияча",
    "罗马尼亚语" => "Румынча",
    "亚美尼亚语" => "Армянча",
    "保加利亚语" => "Болгарча",
    "爱沙尼亚语" => "Эстончо",
    "阿姆哈拉语" => "Амхарча",
    "吉尔吉斯语" => "Кыргызча",
    "克罗地亚语" => "Хорватча",
    "波斯尼亚语" => "Бошнакча",
    "蒂格尼亚语" => "Тигнан",
    "克里奥尔语" => "Креол",
    "马尔加什语" => "Малагаси",
    "南非科萨语" => "Түштүк Африкалык Кхоса",
    "南非祖鲁语" => "Зулу, Түштүк Африка",
    "塞尔维亚语" => "Сербче",
    "乌兹别克语" => "Өзбекче",
    "伊洛卡诺语" => "Ilocano",
    "印尼爪哇语" => "Javanese Indonesian",
    "回复ID错误" => "Жооп ID катасы",
    "非法文章ID" => "Мыйзамсыз макала ID",
    "管理后台" => "Башкаруу фон",
    "管理登录" => "Админ кирүү",
    "登录成功" => "ийгиликтүү кирүү",
    "切换注册" => "Каттоону которуштуруу",
    "你已登出" => "Сиз чыгып кеттиңиз",
    "登出成功" => "Чыгуу ийгиликтүү",
    "用户注册" => "Колдонуучуну каттоо",
    "注册成功" => "каттоо ийгиликтүү",
    "注册失败" => "каттоо ишке ашкан жок",
    "重复密码" => "Сырсөздү кайталаңыз",
    "切换登录" => "Кирүү которуштуруу",
    "请先登录" => "биринчи кириңиз",
    "修改资料" => "Маалыматты өзгөртүү",
    "没有改变" => "өзгөртүү жок",
    "不可修改" => "Өзгөрүлгүс",
    "上传失败" => "жүктөө ишке ашкан жок",
    "清除成功" => "Ийгиликтүү тазалоо",
    "暂无记录" => "Жазуулар жок",
    "批量删除" => "пакетти жок кылуу",
    "文章列表" => "Макала тизмеси",
    "发布时间" => "бошотуу убактысы",
    "修改时间" => "Убакытты өзгөрт",
    "文章标题" => "Макаланын аталышы",
    "标题重名" => "Кайталанма аталыш",
    "别名重复" => "Кайталануучу лакап ат",
    "创建文章" => "Макала түзүү",
    "编辑文章" => "Макаланы түзөтүү",
    "非法字段" => "Мыйзамсыз талаа",
    "填写整数" => "Бүтүн санды толтуруңуз",
    "修改属性" => "касиеттерин өзгөртүү",
    "修改成功" => "Ийгиликтүү өзгөртүлдү",
    "修改失败" => "түзөтүү мүмкүн эмес",
    "批量还原" => "Пакеттик калыбына келтирүү",
    "还原文章" => "Макаланы калыбына келтирүү",
    "还原成功" => "Ийгиликтүү калыбына келтирилди",
    "还原失败" => "Калыбына келтирүү ишке ашкан жок",
    "删除文章" => "Макаланы жок кылуу",
    "删除成功" => "ийгиликтүү жок кылынды",
    "删除失败" => "жок кыла алган жок",
    "全部展开" => "Баарын кеңейтүү",
    "全部折叠" => "Баарын жыйноо",
    "添加下级" => "Багынуучу кошуу",
    "编辑菜单" => "Менюну түзөтүү",
    "菜单名称" => "Меню аты",
    "父级菜单" => "Аталык меню",
    "顶级菜单" => "жогорку меню",
    "创建菜单" => "Меню түзүү",
    "删除菜单" => "менюну жок кылуу",
    "非法访问" => "Уруксатсыз кирүү",
    "拒绝访问" => "кирүү четке кагылды",
    "没有权限" => "уруксат четке кагылды",
    "彻底删除" => "Толугу менен алып салуу",
    "批量待审" => "Пакет каралып жатат",
    "批量审核" => "Пакет карап чыгуу",
    "垃圾评论" => "Спам комментарийлер",
    "分类名称" => "Категориянын аталышы",
    "分类列表" => "Категориялар тизмеси",
    "父级分类" => "Ата-эне категориясы",
    "顶级分类" => "Жогорку классификация",
    "分类重名" => "Кайталанма аттары бар категориялар",
    "创建分类" => "категорияларды түзүү",
    "编辑分类" => "Категорияны түзөтүү",
    "删除分类" => "Категорияны жок кылуу",
    "评论作者" => "Сын автору",
    "评论内容" => "комментарийлер",
    "评论列表" => "комментарий тизмеси",
    "评论文章" => "Макалаларды карап чыгуу",
    "回复内容" => "Мазмунга жооп берүү",
    "回复评论" => "Комментарийге жооп бериңиз",
    "编辑评论" => "Редакциялык комментарийлер",
    "待审评论" => "Күтүүдөгү комментарийлер",
    "待审成功" => "Сыноо ийгиликтүү аяктады",
    "待审失败" => "Соттук териштирүүдө ийгиликсиздик",
    "审核成功" => "Карап чыгуу ийгиликтүү болду",
    "审核失败" => "Аудиттин катасы",
    "删除评论" => "Пикирди жок кылуу",
    "谷歌翻译" => "Google которуу",
    "检测语言" => "Тилди аныктоо",
    "别名错误" => "лакап ат катасы",
    "语言错误" => "Тил катасы",
    "标签名称" => "Тег аты",
    "标签列表" => "тег тизмеси",
    "标签重名" => "Кайталанма аттары бар тегдер",
    "页面列表" => "Барак тизмеси",
    "页面标题" => "беттин аталышы",
    "父级页面" => "аталык бет",
    "顶级页面" => "жогорку бет",
    "清除缓存" => "Кэшти тазалоо",
    "当前版本" => "учурдагы версия",
    "重置系统" => "Системаны кайра орнотуу",
    "最新评论" => "акыркы комментарий",
    "联系我们" => "Биз менен байланыш",
    "数据统计" => "Статистика",
    "网站名称" => "Вебсайттын аты",
    "网站地址" => "веб-сайттын дареги",
    "链接列表" => "Шилтемеленген тизме",
    "创建链接" => "Шилтеме түзүү",
    "编辑链接" => "Шилтемени түзөтүү",
    "删除链接" => "Шилтемени алып салуу",
    "日志标题" => "Журналдын аталышы",
    "消息内容" => "Кабардын мазмуну",
    "请求方法" => "Сурам ыкмасы",
    "请求路径" => "Сурам жолу",
    "日志列表" => "Журнал тизмеси",
    "上传成功" => "Жүктөлүп берилди",
    "媒体列表" => "медиа тизмеси",
    "开始上传" => "Жүктөп баштаңыз",
    "等待上传" => "Жүктөп берүү күтүлүүдө",
    "重新上传" => "кайра жүктөө",
    "上传完成" => "жүктөө аяктады",
    "系统保留" => "Система сакталган",
    "发现重复" => "Дубликат табылды",
    "上传文件" => "файлдарды жүктөө",
    "个在用，" => "Бири колдонууда,",
    "文件重名" => "Кайталанма файл аталыштары",
    "删除文件" => "Файлдарды өчүрүү",
    "创建页面" => "Барак түзүү",
    "编辑页面" => "Баракты түзөтүү",
    "删除页面" => "баракты жок кылуу",
    "角色列表" => "ролдор тизмеси",
    "角色名称" => "Ролдун аты",
    "权限分配" => "Уруксат тапшыруу",
    "创建角色" => "Рол түзүү",
    "编辑角色" => "Ролду түзөтүү",
    "删除角色" => "Ролду жок кылуу",
    "基础设置" => "Негизги орнотуулар",
    "高级设置" => "өркүндөтүлгөн орнотуулар",
    "其他设置" => "башка орнотуулар",
    "上传类型" => "Жүктөө түрү",
    "限制大小" => "чектөө өлчөмү",
    "默认最大" => "Демейки максимум",
    "单点登录" => "кирүү",
    "版权信息" => "Автордук укук жөнүндө маалымат",
    "链接地址" => "шилтеме дареги",
    "热门文章" => "популярдуу макалалар",
    "首页评论" => "Башкы беттин комментарийлери",
    "内容相关" => "Мазмунга байланыштуу",
    "图片分页" => "Сүрөт пейджинги",
    "友情链接" => "Шилтемелер",
    "语音朗读" => "Үн окуу",
    "评论登录" => "Комментарий кирүү",
    "评论待审" => "Комментарий күтүлүүдө",
    "保存成功" => "Ийгиликтүү сакталды",
    "创建标签" => "Тегдерди түзүү",
    "编辑标签" => "Тегди түзөтүү",
    "删除标签" => "Тегди жок кылуу",
    "用户列表" => "колдонуучу тизмеси",
    "注册时间" => "Каттоо убактысы",
    "首次登陆" => "Биринчи жолу кирүү",
    "最后登陆" => "акыркы Кирүү",
    "语言权限" => "Тил уруксаттары",
    "创建用户" => "Колдонуучу түзүү",
    "不能修改" => "Өзгөртүү мүмкүн эмес",
    "编辑用户" => "Колдонуучуну түзөтүү",
    "删除用户" => "колдонуучуларды жок кылуу",
    "最新文章" => "акыркы макалалар",
    "搜索结果" => "издөө натыйжалары",
    "选择语言" => "Тилди тандаңыз",
    "分类为空" => "Категория бош",
    "收藏文章" => "Макалаларды чогултуу",
    "收藏成功" => "Жыйнак ийгиликтүү",
    "取消收藏" => "Сүйүктүүлөрдү жокко чыгаруу",
    "取消回复" => "Жоопту жокко чыгаруу",
    "发表评论" => "Комментарий",
    "语音播放" => "Үн ойнотуу",
    "回到顶部" => "чокусуна кайтуу",
    "分类目录" => "Категориялар",
    "下载内容" => "Мазмунду жүктөп алыңыз",
    "相关文章" => "байланыштуу макалалар",
    "媒体合成" => "медиа синтези",
    "语音合成" => "сүйлөө синтези",
    "前端登录" => "Front-end логин",
    "前端注册" => "Фронттук каттоо",
    "前端登出" => "Frontend чыгуу",
    "后台首页" => "Фондук үй",
    "欢迎页面" => "саламдашуу барагы",
    "权限管理" => "бийликти башкаруу",
    "用户管理" => "Колдонуучуну башкаруу",
    "角色管理" => "ролду башкаруу",
    "权限菜单" => "Уруксаттар менюсу",
    "分类管理" => "Классификацияны башкаруу",
    "标签管理" => "тег башкаруу",
    "文章管理" => "Макаланы башкаруу",
    "页面管理" => "Баракты башкаруу",
    "媒体管理" => "медиа башкаруу",
    "上传接口" => "Жүктөп берүү интерфейси",
    "链接管理" => "Шилтеме башкаруу",
    "评论管理" => "Комментарий башкаруу",
    "审核通过" => "сынактан өттү",
    "机器评论" => "машина карап чыгуу",
    "系统管理" => "Системаны башкаруу",
    "系统设置" => "Система орнотуулары",
    "保存设置" => "Жөндөөлөрдү сактоо",
    "恢复出厂" => "Заводду калыбына келтирүү",
    "操作日志" => "Операция журналы",
    "删除日志" => "Журналды жок кылуу",
    "科西嘉语" => "Корсикче",
    "瓜拉尼语" => "Guaraní",
    "卢旺达语" => "Kinyarwanda",
    "约鲁巴语" => "Йорубача",
    "尼泊尔语" => "Непаличе",
    "夏威夷语" => "Гавайча",
    "意第绪语" => "Идишче",
    "爱尔兰语" => "Ирландча",
    "希伯来语" => "Еврейче",
    "卡纳达语" => "Каннадача",
    "匈牙利语" => "Венгерче",
    "泰米尔语" => "Тамилче",
    "阿拉伯语" => "Арабча",
    "孟加拉语" => "Бенгалча",
    "萨摩亚语" => "самоа",
    "班巴拉语" => "Бамбара",
    "立陶宛语" => "Литвача",
    "马耳他语" => "Малтизче",
    "土库曼语" => "Түркмөнчө",
    "阿萨姆语" => "Ассам",
    "僧伽罗语" => "сингал",
    "乌克兰语" => "Украинче",
    "威尔士语" => "Валлийче",
    "菲律宾语" => "Филиппин",
    "艾马拉语" => "Аймара",
    "泰卢固语" => "Телугуча",
    "多格来语" => "dogglai",
    "迈蒂利语" => "Maithili",
    "普什图语" => "Пуштунча",
    "迪维希语" => "Dhivehi",
    "卢森堡语" => "Люксембургча",
    "土耳其语" => "Түркчө",
    "马其顿语" => "Македонияча",
    "卢干达语" => "Луганда",
    "马拉地语" => "Маратиче",
    "乌尔都语" => "Урдуча",
    "葡萄牙语" => "Португалча",
    "奥罗莫语" => "Oromo",
    "西班牙语" => "Испанча",
    "弗里西语" => "фриз",
    "索马里语" => "Сомаличе",
    "齐切瓦语" => "Чичева",
    "旁遮普语" => "Пунжабиче",
    "巴斯克语" => "Баскча",
    "意大利语" => "Итальянча",
    "塔吉克语" => "тажик",
    "克丘亚语" => "Кечуа",
    "奥利亚语" => "Орияча",
    "哈萨克语" => "Казакча",
    "林格拉语" => "Lingala",
    "塞佩蒂语" => "Sepeti",
    "塞索托语" => "Сесото",
    "维吾尔语" => "уйгур",
    "ICP No.001" => "ICP №001",
    "用户名" => "колдонуучунун аты",
    "验证码" => "Текшерүү коду",
    "记住我" => "мени эстеп жүр",
    "手机号" => "Тел номери",
    "请输入" => "кириңиз",
    "请选择" => "сураныч тандаңыз",
    "浏览量" => "Көрүүлөр",
    "回收站" => "таштанды челек",
    "菜单树" => "Меню дарагы",
    "控制器" => "контролер",
    "第一页" => "Биринчи бет",
    "最后页" => "акыркы бет",
    "筛选列" => "Мамычаларды чыпкалоо",
    "删除行" => "Сапты жок кылуу",
    "还原行" => "Сапты жокко чыгаруу",
    "重命名" => "Атын өзгөртүү",
    "国际化" => "глобализация",
    "文章数" => "Макалалардын саны",
    "机器人" => "робот",
    "星期日" => "Жекшемби",
    "星期一" => "Дүйшөмбү",
    "星期二" => "Шейшемби",
    "星期三" => "Шаршемби",
    "星期四" => "Бейшемби",
    "星期五" => "Жума",
    "星期六" => "Ишемби",
    "会员数" => "Мүчөлөрдүн саны",
    "评论数" => "Комментарийлердин саны",
    "文件名" => "Шилтемелер аты",
    "音视频" => "Аудио жана видео",
    "扩展名" => "кеңейтүү аты",
    "未使用" => "Колдонулбаган",
    "个无效" => "жараксыз",
    "共删除" => "Бардыгы жок кылынды",
    "个文件" => "файлдар",
    "备案号" => "иштин номери",
    "轮播图" => "карусель",
    "条显示" => "тилке дисплей",
    "上一页" => "Мурунку бет",
    "下一页" => "Кийинки бет",
    "未分类" => "категориясыз",
    "空标签" => "бош белги",
    "无标题" => "Аталышы жок",
    "控制台" => "консол",
    "登录中" => "кирүү",
    "注册中" => "Каттоо",
    "跳转中" => "Багыттоо",
    "提交中" => "тапшыруу",
    "审核中" => "каралып жатат",
    "请稍后" => "күтө тур",
    "篇文章" => "Макала",
    "客户端" => "кардар",
    "没有了" => "эч ким калган жок",
    "后评论" => "комментарий калтыруу",
    "未命名" => "аты аталбаган",
    "软删除" => "жумшак өчүрүү",
    "语言包" => "Тил пакети",
    "豪萨语" => "Хаусача",
    "挪威语" => "Норвежче",
    "贡根语" => "гонган тили",
    "拉丁语" => "Латынча",
    "捷克语" => "Чехче",
    "波斯语" => "Парсча",
    "印地语" => "Хиндиче",
    "冰岛语" => "Исландияча",
    "契维语" => "Twi",
    "高棉语" => "Камбоджа",
    "丹麦语" => "Датча",
    "修纳语" => "Shona",
    "越南语" => "Вьетнамча",
    "宿务语" => "Себуано",
    "波兰语" => "Полякча",
    "鞑靼语" => "татар",
    "老挝语" => "Лаочо",
    "瑞典语" => "Шведче",
    "缅甸语" => "Бурмача",
    "信德语" => "Синдиче",
    "马来语" => "Малайча",
    "伊博语" => "Игбо",
    "希腊语" => "Грекче",
    "芬兰语" => "Финче",
    "埃维语" => "Эве",
    "毛利语" => "Маориче",
    "荷兰语" => "Нидерландча",
    "蒙古语" => "Монголчо",
    "米佐语" => "Mizo",
    "世界语" => "Эсперанто",
    "印尼语" => "Индонезияча",
    "宗加语" => "Дзонга",
    "密码" => "купуя сөз",
    "登录" => "Кириңиз",
    "重置" => "баштапкы абалга келтирүү",
    "信息" => "маалымат",
    "成功" => "ийгилик",
    "失败" => "ийгиликсиз",
    "昵称" => "Ник аты",
    "选填" => "Кошумча",
    "注册" => "катталуу",
    "错误" => "ката",
    "头像" => "аватар",
    "上传" => "жүктөө",
    "保存" => "сактоо",
    "标题" => "аталышы",
    "别名" => "Алиас",
    "描述" => "сүрөттөп",
    "封面" => "жабуу",
    "分类" => "Классификация",
    "状态" => "мамлекет",
    "正常" => "нормалдуу",
    "禁用" => "Өчүрүү",
    "作者" => "автор",
    "日期" => "дата",
    "添加" => "кошуу",
    "刷新" => "жаңылоо",
    "标签" => "Белги",
    "浏览" => "Серептөө",
    "点赞" => "сыяктуу",
    "评论" => "Комментарий",
    "操作" => "иштетүү",
    "阅读" => "окуу",
    "编辑" => "түзөтүү",
    "删除" => "жок кылуу",
    "翻译" => "которуу",
    "内容" => "мазмун",
    "分割" => "сегментациялоо",
    "建议" => "сунуш",
    "非法" => "мыйзамсыз",
    "还原" => "кыскартуу",
    "方法" => "ыкмасы",
    "排序" => "сорттоо",
    "图标" => "сөлөкөтү",
    "菜单" => "меню",
    "时间" => "убакыт",
    "匿名" => "анонимдүү",
    "确定" => "Албетте",
    "取消" => "Жокко чыгаруу",
    "跳转" => "Секирүү",
    "页码" => "беттин номери",
    "共计" => "жалпы",
    "每页" => "бетине",
    "导出" => "Экспорт",
    "打印" => "Басып чыгаруу",
    "回复" => "жооп бер",
    "路径" => "жол",
    "预览" => "Алдын ала көрүү",
    "名称" => "аты",
    "行数" => "Катарлар",
    "相册" => "фотоальбом",
    "文章" => "макала",
    "待审" => "Күтүүдө",
    "通过" => "өтүү",
    "垃圾" => "Таштанды",
    "提取" => "үзүндү",
    "生成" => "түзүү",
    "定位" => "позиция",
    "填充" => "толтуруу",
    "帮助" => "жардам",
    "返回" => "кайтуу",
    "语言" => "тил",
    "宽屏" => "кең экран",
    "普通" => "кадимки",
    "后台" => "Backstage",
    "前台" => "алдыңкы стол",
    "登出" => "Чыгуу",
    "版本" => "Версия",
    "关于" => "жөнүндө",
    "微信" => "WeChat",
    "会员" => "мүчөсү",
    "地址" => "дареги",
    "类型" => "түрү",
    "图片" => "сүрөт",
    "文件" => "документ",
    "失效" => "Жараксыз",
    "大小" => "өлчөмү",
    "下载" => "жүктөө",
    "导航" => "навигация",
    "站标" => "Сайттын логотиби",
    "单位" => "бирдиги",
    "加速" => "тездетүү",
    "角色" => "Рол",
    "首页" => "алдыңкы бет",
    "个数" => "саны",
    "我的" => "меники",
    "说道" => "деди",
    "手机" => "уюлдук телефон",
    "可选" => "Кошумча",
    "搜索" => "издөө",
    "页面" => "бет",
    "查看" => "Текшерүү",
    "创建" => "түзүү",
    "更新" => "жаңылоо",
    "英语" => "Англисче",
    "法语" => "Франсузча",
    "俄语" => "Орусча",
    "梵语" => "Санскритче",
    "日语" => "Жапончо",
    "泰语" => "Тайча",
    "苗语" => "Хмонгчо",
    "德语" => "Немисче",
    "韩语" => "Корейче",
    "请" => "өтүнөмүн",
];
