<?php
/**
 * @author admin
 * @date 2024-06-06 05:04:09
 * @desc 维吾尔语语言包
 */
return [
    "迷你mvc小框架，一套用于开发各类管理系统的通用型后台管理系统模板，基于开源免费的 layui 的制作，内置各类实际业务场景下相对丰富的示例。" => "Mini MVC بىر خىل كىچىك رامكا ، ھەر خىل باشقۇرۇش سىستېمىلىرىنى تەرەققىي قىلدۇرۇشنىڭ بىر يۈرۈش ئۇنىۋېرسال ئارقا باشقۇرۇش سىستېمىسى قېلىپى.",
    "除非特别说明，本博客均为原创，如需转载，请以链接形式标明来源出处。" => "باشقىچە بايان قىلىنمىغان بولسا ، بۇ بىلوگ ئەسلى. ئەگەر قايتا بېسىشقا توغرا كەلسە ، ئۇلىنىش شەكلىدە مەنبەنى كۆرسىتىڭ.",
    "重置系统，将删除用户所有数据和附件，恢复到出厂设置。" => "سىستېمىنى ئەسلىگە كەلتۈرگەندە بارلىق ئىشلەتكۈچى سانلىق مەلۇماتلىرى ۋە قوشۇمچە ھۆججەتلەرنى ئۆچۈرۈپ ، زاۋۇت تەڭشىكىگە قايتۇرىدۇ.",
    "用户名只能英文字母数字下划线或中划线，位数" => "ئىشلەتكۈچى ئىسمى پەقەت ئىنگلىزچە ھەرپ ، سان ، ئاستى سىزىق ياكى ئاستى سىزىق ۋە رەقەم سانىنى ئۆز ئىچىگە ئالىدۇ",
    "昵称只能中英文字母数字下划线中划线，位数" => "بۇ لەقەمدە پەقەت خەنزۇچە ۋە ئىنگلىزچە ھەرپلەر ، سانلار ، ئاستى سىزىقلار ، ئاستى سىزىقلار ۋە رەقەملەرلا بار.",
    "有道接口不支持，需科学上网使用谷歌接口。" => "Youdao كۆرۈنمە يۈزى قوللىمايدۇ.",
    "别名只能英文字母数字下划线中划线，位数" => "تەخەللۇس پەقەت ئىنگلىزچە ھەرپ ، سان ، ئاستى سىزىق ، ئاستى سىزىق ۋە رەقەمنى ئۆز ئىچىگە ئالىدۇ.",
    "计算孤立文件需要较长时间，确定继续吗？" => "يېتىم ھۆججەتلەرنى ھېسابلاشقا ئۇزۇن ۋاقىت كېتىدۇ. داۋاملاشتۇرۇشنى خالامسىز؟",
    "语音不支持，需科学上网使用谷歌接口。" => "ئاۋاز قوللىمايدۇ ، شۇڭا گۇگۇل كۆرۈنمە يۈزىدىن پايدىلىنىپ ئىنتېرنېتنى ئىلمىي زىيارەت قىلىشىڭىز كېرەك.",
    "您的账号已在别处登录，请重新登录！" => "ھېساباتىڭىز باشقا يەرگە كىردى ، قايتا كىرىڭ!",
    "你的账号已被禁用，请联系管理员" => "ھېساباتىڭىز چەكلەنگەن ، باشقۇرغۇچى بىلەن ئالاقىلىشىڭ",
    "文件有在使用中，请重新计算状态" => "ھۆججەت ئىشلىتىلىۋاتىدۇ ، ھالىتىنى قايتا ھېسابلاڭ",
    "目标语言权限不够，请联系管理员" => "نىشان تىل ئىجازىتى يېتەرلىك ئەمەس ، باشقۇرغۇچى بىلەن ئالاقىلىشىڭ",
    "真的标记垃圾评论选中的行吗？" => "سىز تاللانغان قۇرلارنى ئەخلەت خەت دەپ بەلگە قويامسىز؟",
    "源语言别名为空，请联系源作者" => "ئەسلى تىلنىڭ ئىسمى قۇرۇق ، مەنبە ئاپتور بىلەن ئالاقىلىشىڭ",
    "点击上传，或将文件拖拽到此处" => "يۈكلەش ئۈچۈن چېكىڭ ياكى ھۆججەتنى بۇ يەرگە سۆرۈڭ",
    "合成成功！再次点击按钮下载。" => "بىرىكمە مۇۋەپپەقىيەتلىك بولدى! چۈشۈرۈش ئۈچۈن يەنە بىر كۇنۇپكىنى بېسىڭ.",
    "没有语言权限，请联系管理员" => "تىل ئىجازىتى يوق ، باشقۇرغۇچى بىلەن ئالاقىلىشىڭ",
    "源语言别名为空，不能国际化" => "ئەسلى تىلنىڭ ئىسمى قۇرۇق ، خەلقئارالاشتۇرۇلمايدۇ.",
    "源语言与目标语言都不能为空" => "مەنبە تىلىمۇ ، نىشان تىلىمۇ قۇرۇق بولالمايدۇ.",
    "源语言与目标语言数量不一致" => "مەنبە ۋە نىشان تىللارنىڭ سانى بىردەك ئەمەس",
    "删除文件失败，文件有在使用" => "ھۆججەت ئۆچۈرۈلمىدى ، ھۆججەت ئىشلىتىلىۋاتىدۇ",
    "角色不存在，请联系管理员" => "رولى مەۋجۇت ئەمەس ، باشقۇرغۇچى بىلەن ئالاقىلىشىڭ",
    "角色被禁用，请联系管理员" => "رولى چەكلەنگەن ، باشقۇرغۇچى بىلەن ئالاقىلىشىڭ",
    "功能不存在，请联系管理员" => "ئىقتىدار مەۋجۇت ئەمەس ، باشقۇرغۇچى بىلەن ئالاقىلىشىڭ",
    "功能被禁用，请联系管理员" => "ئىقتىدارى چەكلەنگەن ، باشقۇرغۇچى بىلەن ئالاقىلىشىڭ",
    "真的彻底删除选中的行吗？" => "تاللانغان قۇرنى راستىنلا ئۆچۈرەمدۇ؟",
    "真的审核通过选中的行吗？" => "تاللانغان قۇرلار راستىنلا تەستىقلىنامدۇ؟",
    "目标语言与源语言不能相同" => "نىشان تىلى ئەسلى تىل بىلەن ئوخشاش بولمايدۇ",
    "提取语言包与源语言不一致" => "ئېلىنغان تىل بولىقى مەنبە تىلى بىلەن بىردەك ئەمەس",
    "文件有在使用，删除失败！" => "ھۆججەت ئىشلىتىلىۋاتىدۇ ، ئۆچۈرۈش مەغلۇب بولدى!",
    "没有权限，请联系管理员" => "ئىجازەت يوق ، باشقۇرغۇچى بىلەن ئالاقىلىشىڭ",
    "真的待审核选中的行吗？" => "بۇ ھەقىقەتەن قايتا قاراپ چىقىشقا تېگىشلىك بىر قۇرمۇ؟",
    "目标语言包有空行，行数" => "نىشان تىل بولىقى قۇرۇق قۇر ، قۇر سانى بار",
    "此操作恢复到出厂设置？" => "بۇ مەشغۇلات زاۋۇت تەڭشىكىنى ئەسلىگە كەلتۈرىدۇ؟",
    "朗读错误，请联网后再试" => "ئوقۇش خاتالىقى ، تورغا ئۇلاپ قايتا سىناڭ",
    "还没有添加分类描述信息" => "ھېچقانداق سەھىپە چۈشەندۈرۈش ئۇچۇرى تېخى قوشۇلمىدى",
    "本地媒体已失效或不存在" => "يەرلىك تاراتقۇلار ئىناۋەتسىز ياكى مەۋجۇت ئەمەس",
    "库尔德语（库尔曼吉语）" => "كۇرد (كۇرمانجى)",
    "作者名未注册或被禁用" => "ئاپتورنىڭ ئىسمى تىزىملىتىلمىغان ياكى چەكلەنگەن",
    "真的删除选中的行吗？" => "تاللانغان قۇرنى راستىنلا ئۆچۈرەمسىز؟",
    "真的还原选中的行吗？" => "سىز تاللانغان قۇرلارنى ئەسلىگە كەلتۈرەمسىز؟",
    "真的删除行或子行么？" => "قۇر ياكى تارماق ئورۇننى ئۆچۈرمەكچىمۇ؟",
    "目标语言包未发现空行" => "نىشان تىل بولىقىدا بوش قۇر تېپىلمىدى",
    "该语言不支持语音朗读" => "بۇ تىل ئاۋازلىق ئوقۇشنى قوللىمايدۇ",
    "语言包数据未发生改变" => "تىل بولىقى سانلىق مەلۇماتلىرى ئۆزگەرمىدى",
    "欢迎使用后台管理系统" => "ئارقا باشقۇرۇش سىستېمىسىغا كەلگەنلىكىڭىزنى قارشى ئالىمىز",
    "文件有在使用或已失效" => "بۇ ھۆججەت ئىشلىتىلىۋاتىدۇ ياكى ۋاقتى توشىدۇ",
    "合成失败，请稍后再试" => "بىرىكمە مەغلۇب بولدى ، كېيىن قايتا سىناڭ",
    "&copy; 2021-2023 Company, Inc." => "© 2021-2023 Company, Inc.",
    "梅泰语（曼尼普尔语）" => "Meitei (Manipuri)",
    "两次密码输入不一致" => "ئىككى پارول كىرگۈزۈش بىردەك ئەمەس",
    "该用户密码不可修改" => "بۇ ئىشلەتكۈچىنىڭ مەخپىي نومۇرىنى ئۆزگەرتىشكە بولمايدۇ",
    "添加文章时创建标签" => "ماقالە قوشقاندا بەلگە قۇر",
    "删除文章时删除评论" => "ماقالىنى ئۆچۈرگەندە باھالارنى ئۆچۈرۈڭ",
    "控制器与方法已存在" => "كونتروللىغۇچ ۋە ئۇسۇل ئاللىبۇرۇن مەۋجۇت",
    "标题或名称不能为空" => "ئىسمى ياكى ئىسمى قۇرۇق بولمايدۇ",
    "请选择一种语言朗读" => "يۇقىرى ئاۋازدا ئوقۇيدىغان تىلنى تاللاڭ",
    "分类有文章不能删除" => "بۇ تۈردە ئۆچۈرگىلى بولمايدىغان ماقالىلەر بار",
    "审核为垃圾评论成功" => "ئەخلەت خەت سۈپىتىدە مۇۋەپپەقىيەتلىك تەكشۈرۈلدى",
    "审核为垃圾评论失败" => "ئەخلەت خەتكە باھا بېرىش مەغلۇب بولدى",
    "确定要登出站点吗？" => "تور بەتتىن چېكىنىشنى خالامسىز؟",
    "网站名称或地址为空" => "تور بېكەتنىڭ ئىسمى ياكى ئادرېسى قۇرۇق",
    "网站名称或地址重名" => "تور بېكەتنىڭ ئىسمى ياكى ئادرېسىنى كۆپەيتىڭ",
    "允许上传的文件类型" => "يۈكلەشكە رۇخسەت قىلىنغان ھۆججەت تىپلىرى",
    "标签有文章不能删除" => "خەتكۈچ بار ماقالىلەرنى ئۆچۈرگىلى بولمايدۇ",
    "人觉得这篇文章很赞" => "كىشىلەر بۇ ماقالىنى ناھايىتى ياخشى دەپ قارايدۇ",
    "还没有页面描述信息" => "ھازىرچە بەت چۈشەندۈرۈش ئۇچۇرى يوق",
    "回复与评论内容重复" => "جاۋاب ۋە باھانىڭ مەزمۇنىنى كۆپەيتىڭ",
    "失败！请稍后再试。" => "مەغلۇب! كېيىن قايتا سىناڭ.",
    "主耶稣基督里的教会" => "خوجايىن ئەيسا مەسىھتىكى چېركاۋ",
    "库尔德语（索拉尼）" => "كۇرد (سورانى)",
    "布尔语(南非荷兰语)" => "Boolean (Afrikaans)",
    "用户名或密码错误" => "ئىشلەتكۈچى ئىسمى ياكى پارولى خاتا",
    "源语言必须是中文" => "مەنبە تىلى چوقۇم خەنزۇچە بولۇشى كېرەك",
    "目标语言别名为空" => "نىشانلىق تىلنىڭ ئىسمى قۇرۇق",
    "国际化文章时标签" => "خەلقئارالاشقان ماقالە خەتكۈچلىرى",
    "确定清除缓存吗？" => "غەملەكنى تازىلاشنى خالامسىز؟",
    "超过的单文件大小" => "يەككە ھۆججەتنىڭ ئۆلچىمى ئېشىپ كەتتى",
    "超过前端表单限制" => "ئالدى شەكىل چەكلىمىسىدىن ئېشىپ كەتتى",
    "目标没有写入权限" => "نىشاننىڭ يېزىش ئىجازىتى يوق",
    "不允许的上传类型" => "يوللاشقا رۇخسەت قىلىنمايدۇ",
    "文件大小不能超过" => "ھۆججەت چوڭلۇقىدىن ئېشىپ كەتسە بولمايدۇ",
    "保存基础设置成功" => "ئاساسىي تەڭشەكلەر مۇۋەپپەقىيەتلىك ساقلاندى",
    "首次基础设置成功" => "بىرىنچى ئاساسىي تەڭشەش مۇۋەپپەقىيەتلىك بولدى",
    "正在合成，请稍后" => "بىرىكتۈرۈش ، ساقلاپ تۇرۇڭ",
    "不合理的请求方法" => "ئورۇنسىز تەلەپ قىلىش ئۇسۇلى",
    "Session无效或过期" => "يىغىن ئىناۋەتسىز ياكى ۋاقتى توشىدۇ",
    "手机号码不正确" => "تېلېفون نومۇرى خاتا",
    "手机号码已存在" => "كۆچمە تېلېفون ئاللىبۇرۇن مەۋجۇت",
    "标题或内容为空" => "تېما ياكى مەزمۇن قۇرۇق",
    "创建文章时标签" => "ماقالە قۇرغاندا خەتكۈچ",
    "编辑文章时标签" => "ماقالە تەھرىرلىگەندە خەتكۈچ",
    "真的删除行么？" => "بۇ قۇرنى ئۆچۈرمەكچىمۇ؟",
    "请输入菜单名称" => "تىزىملىك ​​نامىنى كىرگۈزۈڭ",
    "请先删除子菜单" => "ئالدى بىلەن تارماق تىزىملىكنى ئۆچۈرۈڭ",
    "未登录访问后台" => "تىزىمغا كىرمەيلا ئارقا سۇپىغا كىرىڭ",
    "Cookie无效或过期" => "Cookie ئىناۋەتسىز ياكى ۋاقتى ئۆتكەن",
    "真的还原行么？" => "ئۇنى ئەسلىگە كەلتۈرۈش ھەقىقەتەن مۇمكىنمۇ؟",
    "编辑器内容为空" => "تەھرىرلىگۈچ مەزمۇنى قۇرۇق",
    "未选择整行文本" => "پۈتۈن تېكىست تاللانمىدى",
    "划词选择行错误" => "سۆز تاللاش لىنىيىسىدىكى خاتالىق",
    "数据源发生改变" => "سانلىق مەلۇمات مەنبەسى ئۆزگىرىدۇ",
    "填充成功，行号" => "مۇۋەپپەقىيەتلىك تولدۇرۇلدى ، قۇر نومۇرى",
    "请输入分类名称" => "سەھىپە نامىنى كىرگۈزۈڭ",
    "分类名不能为空" => "سەھىپە ئىسمى قۇرۇق بولمايدۇ",
    "排序只能是数字" => "تەرتىپلەش پەقەت سان جەھەتتىن بولىدۇ",
    "请先删除子分类" => "ئالدى بىلەن تارماق تۈرلەرنى ئۆچۈرۈڭ",
    "请输入评论作者" => "باھا ئاپتورىنى كىرگۈزۈڭ",
    "请选择目标语言" => "نىشان تىلنى تاللاڭ",
    "语言包生成成功" => "تىل بولىقى مۇۋەپپەقىيەتلىك ھاسىل قىلىندى",
    "语言包生成失败" => "تىل بولىقى ھاسىل قىلىش مەغلۇب بولدى",
    "国际化分类成功" => "خەلقئارالىق تۈرگە ئايرىش مۇۋەپپەقىيەتلىك بولدى",
    "国际化分类失败" => "خەلقئارا تۈرگە ئايرىش مەغلۇب بولدى",
    "请输入标签名称" => "بەلگە نامىنى كىرگۈزۈڭ",
    "国际化标签成功" => "خەلقئارالىق بەلگە مۇۋەپپەقىيىتى",
    "国际化标签失败" => "خەلقئارالىشىش بەلگىسى مەغلۇپ بولدى",
    "国际化文章成功" => "خەلقئارالىق ماقالە مۇۋەپپەقىيىتى",
    "国际化文章失败" => "خەلقئارالىق ماقالە مەغلۇپ بولدى",
    "请输入页面标题" => "بەت ماۋزۇسىنى كىرگۈزۈڭ",
    "国际化页面成功" => "خەلقئارالىشىش بېتى مۇۋەپپەقىيەتلىك بولدى",
    "国际化页面失败" => "خەلقئارالىشىش بېتى مەغلۇپ بولدى",
    "作者：葡萄枝子" => "ئاپتور: ئۈزۈم شاخچىسى",
    "请输入网站名称" => "تور بېكەت نامىنى كىرگۈزۈڭ",
    "请输入网站地址" => "تور بېكەت ئادرېسىنى كىرگۈزۈڭ",
    "链接名不能为空" => "ئۇلىنىش ئىسمى قۇرۇق بولمايدۇ",
    "上传文件不完整" => "يۈكلەنگەن ھۆججەت تولۇق ئەمەس",
    "没有文件被上传" => "ھېچقانداق ھۆججەت يۈكلەنمىدى",
    "找不到临时目录" => "Temp مۇندەرىجىسى تېپىلمىدى",
    "未知的文件类型" => "نامەلۇم ھۆججەت تىپى",
    "文件名不能为空" => "ھۆججەت ئىسمى قۇرۇق بولمايدۇ",
    "个文件有在使用" => "ھۆججەتلەر ئىشلىتىلىۋاتىدۇ",
    "请先删除子页面" => "ئاۋال تارماق بەتنى ئۆچۈرۈڭ",
    "请输入角色名称" => "رول نامىنى كىرگۈزۈڭ",
    "管理员不可禁用" => "باشقۇرغۇچىلار چەكلىيەلمەيدۇ",
    "管理员不可删除" => "باشقۇرغۇچى ئۆچۈرەلمەيدۇ",
    "请输入限制大小" => "چەك چوڭلۇقىنى كىرگۈزۈڭ",
    "请输入版权信息" => "نەشر ھوقۇقى ئۇچۇرىنى كىرگۈزۈڭ",
    "恢复出厂成功！" => "زاۋۇت ئەسلىگە كەلتۈرۈش مۇۋەپپەقىيەتلىك بولدى!",
    "恢复出厂失败！" => "زاۋۇتنى ئەسلىگە كەلتۈرۈش مەغلۇپ بولدى!",
    "标签名不能为空" => "بەلگە ئىسمى قۇرۇق بولمايدۇ",
    "还没有内容信息" => "تېخى مەزمۇن ئۇچۇرى يوق",
    "这篇文章很有用" => "بۇ ماقالە ناھايىتى پايدىلىق",
    "保存分类国际化" => "كاتېگورىيەنى خەلقئارالاشتۇرۇڭ",
    "分类国际化帮助" => "تۈرگە ئايرىش خەلقئارالىشىش ياردىمى",
    "保存标签国际化" => "بەلگىنى خەلقئارالاشتۇرۇش",
    "标签国际化帮助" => "خەتكۈچ خەلقئارالاشتۇرۇش ياردىمى",
    "保存文章国际化" => "ماقالىنى خەلقئارالاشتۇرۇش",
    "文章国际化帮助" => "ماقالە خەلقئارالىشىش ياردىمى",
    "保存页面国际化" => "بەتنى خەلقئارالاشتۇرۇڭ",
    "页面国际化帮助" => "بەت خەلقئارالاشتۇرۇش ياردىمى",
    "海地克里奥尔语" => "ھايتى كرېئول",
    "非法的ajax请求" => "قانۇنسىز ajax تەلىپى",
    "密码至少位数" => "پارولدا كەم دېگەندە رەقەم بولۇشى كېرەك",
    "验证码不正确" => "دەلىللەش كودى خاتا",
    "包含非法参数" => "قانۇنسىز پارامېتىرلارنى ئۆز ئىچىگە ئالىدۇ",
    "请输入用户名" => "ئىشلەتكۈچى نامىنى كىرگۈزۈڭ",
    "用户名已存在" => "ئىشلەتكۈچى ئىسمى مەۋجۇت",
    "请重输入密码" => "پارولىڭىزنى قايتا كىرگۈزۈڭ",
    "图片格式错误" => "رەسىم فورماتى خاتا",
    "修改资料成功" => "سانلىق مەلۇمات مۇۋەپپەقىيەتلىك ئۆزگەرتىلدى",
    "没有改变信息" => "ھېچقانداق ئۇچۇر ئۆزگەرمىدى",
    "请输入浏览量" => "كۆرۈنۈش سانىنى كىرگۈزۈڭ",
    "请输入点赞数" => "ياقتۇرىدىغانلارنىڭ سانىنى كىرگۈزۈڭ",
    "请选择子分类" => "تارماق تۈرنى تاللاڭ",
    "创建文章成功" => "ماقالە مۇۋەپپەقىيەتلىك قۇرۇلدى",
    "创建文章失败" => "ماقالە قۇرالمىدى",
    "编辑文章成功" => "ماقالىنى مۇۋەپپەقىيەتلىك تەھرىرلەڭ",
    "标题不能为空" => "ماۋزۇ قۇرۇق بولالمايدۇ",
    "菜单名称重复" => "تىزىملىك ​​نامىنى كۆپەيتىش",
    "创建菜单成功" => "تىزىملىك ​​مۇۋەپپەقىيەتلىك قۇرۇلدى",
    "创建菜单失败" => "تىزىملىك ​​قۇرالمىدى",
    "编辑菜单成功" => "تىزىملىكنى مۇۋەپپەقىيەتلىك تەھرىرلەڭ",
    "请选择行数据" => "قۇر سانلىق مەلۇماتلىرىنى تاللاڭ",
    "计算孤立文件" => "يېتىم ھۆججەتلەرنى ساناپ بېقىڭ",
    "划词选择错误" => "سۆزنى خاتا تاللاش",
    "请提取语言包" => "تىل بولىقىنى چىقىرىڭ",
    "没有语音文字" => "ئاۋازلىق تېكىست يوق",
    "语音朗读完成" => "ئاۋازلىق ئوقۇش تاماملاندى",
    "分类名称为空" => "تۈر ئىسمى قۇرۇق",
    "创建分类成功" => "تۈرگە ئايرىش مۇۋەپپەقىيەتلىك قۇرۇلدى",
    "创建分类失败" => "سەھىپە قۇرالمىدى",
    "编辑分类成功" => "كاتېگورىيەنى مۇۋەپپەقىيەتلىك تەھرىرلەڭ",
    "回复评论为空" => "ئىنكاسقا جاۋاب قۇرۇق",
    "回复评论成功" => "ئىنكاسقا مۇۋەپپەقىيەتلىك جاۋاب قايتۇرۇڭ",
    "回复评论失败" => "ئىنكاس قايتۇرۇش مەغلۇب بولدى",
    "评论内容为空" => "باھا مەزمۇنى قۇرۇق",
    "编辑评论成功" => "ئىنكاسنى مۇۋەپپەقىيەتلىك تەھرىرلەڭ",
    "待审评论成功" => "باھا مۇۋەپپەقىيەتلىك تەكشۈرۈلۈۋاتىدۇ",
    "待审评论失败" => "باھا كۈتۈلمىگەن باھا مەغلۇب بولدى",
    "审核通过评论" => "تەستىقلانغان باھا",
    "审核评论成功" => "تەكشۈرۈش مۇۋەپپەقىيەتلىك بولدى",
    "审核评论失败" => "باھا تەكشۈرۈش مەغلۇب بولدى",
    "删除评论失败" => "باھانى ئۆچۈرەلمىدى",
    "请选择源语言" => "مەنبە تىلىنى تاللاڭ",
    "别名不可更改" => "ئۆزگەرتىشنى ئۆزگەرتكىلى بولمايدۇ",
    "标签名称为空" => "خەتكۈچ ئىسمى قۇرۇق",
    "创建链接成功" => "ئۇلىنىش مۇۋەپپەقىيەتلىك قۇرۇلدى",
    "创建链接失败" => "ئۇلىنىش قۇرالمىدى",
    "编辑链接成功" => "ئۇلىنىشنى مۇۋەپپەقىيەتلىك تەھرىرلەڭ",
    "网站名称重名" => "تور بېكەت نامىنى كۆپەيتىش",
    "网站地址重复" => "تور بېكەت ئادرېسىنى كۆپەيتىش",
    "图片压缩失败" => "رەسىم پىرىسلاش مەغلۇپ بولدى",
    "移动文件失败" => "ھۆججەت يۆتكەش مەغلۇب بولدى",
    "上传文件成功" => "ھۆججەت مۇۋەپپەقىيەتلىك يۈكلەندى",
    "上传文件失败" => "ھۆججەت يوللاش مەغلۇب بولدى",
    "共找到文件：" => "تېپىلغان ئومۇمىي ھۆججەتلەر:",
    "创建页面成功" => "بەت مۇۋەپپەقىيەتلىك قۇرۇلدى",
    "创建页面失败" => "بەت قۇرۇش مەغلۇب بولدى",
    "编辑页面成功" => "بەتنى مۇۋەپپەقىيەتلىك تەھرىرلەڭ",
    "权限数据错误" => "ئىجازەت سانلىق مەلۇمات خاتالىقى",
    "创建角色成功" => "رولى مۇۋەپپەقىيەتلىك يارىتىلدى",
    "创建角色失败" => "رول ئالالمىدى",
    "编辑角色成功" => "رولنى مۇۋەپپەقىيەتلىك تەھرىرلەڭ",
    "游客不可删除" => "زىيارەتچىلەر ئۆچۈرەلمەيدۇ",
    "创建标签成功" => "خەتكۈچ مۇۋەپپەقىيەتلىك قۇرۇلدى",
    "创建标签失败" => "بەلگە قۇرالمىدى",
    "编辑标签成功" => "خەتكۈچنى مۇۋەپپەقىيەتلىك تەھرىرلەڭ",
    "角色数据错误" => "ھەرپ سانلىق مەلۇمات خاتالىقى",
    "语言数据错误" => "تىل سانلىق مەلۇمات خاتالىقى",
    "状态数据错误" => "ھالەت سانلىق مەلۇمات خاتالىقى",
    "创建用户成功" => "ئىشلەتكۈچى مۇۋەپپەقىيەتلىك قۇرۇلدى",
    "创建用户失败" => "ئىشلەتكۈچى قۇرالمىدى",
    "编辑用户成功" => "ئىشلەتكۈچىنى مۇۋەپپەقىيەتلىك تەھرىرلەڭ",
    "本文博客网址" => "بۇ ماقالىنىڭ بىلوگ ئادرېسى",
    "评论内容重复" => "باھا مەزمۇنىنى كۆپەيتىش",
    "回复内容重复" => "جاۋاب مەزمۇنىنى كۆپەيتىش",
    "评论发表成功" => "ئىنكاس مۇۋەپپەقىيەتلىك يوللاندى",
    "发表评论失败" => "باھا يازمىدى",
    "前端删除评论" => "ئالدى تەرىپىدىكى ئىنكاسلارنى ئۆچۈرۈش",
    "中文（简体）" => "خەنزۇچە (ئاددىيلاشتۇرۇلغان)",
    "加泰罗尼亚语" => "Catalan",
    "苏格兰盖尔语" => "شوتلاندىيە گال تىلى",
    "中文（繁体）" => "ئەنئەنىۋى خەنزۇچە)",
    "马拉雅拉姆语" => "Malayalam",
    "斯洛文尼亚语" => "سىلوۋېنىيە",
    "阿尔巴尼亚语" => "albanian",
    "密码至少5位" => "پارول كەم دېگەندە 5 ھەرپ بولۇشى كېرەك",
    "你已经登录" => "سىز ئاللىبۇرۇن كىرگەن",
    "账号被禁用" => "ھېسابات چەكلەنگەن",
    "请输入密码" => "پارول كىرگۈزۈڭ",
    "留空不修改" => "بوش قويۇڭ ، ئۆزگەرتمەڭ",
    "请输入标题" => "ماۋزۇ كىرگۈزۈڭ",
    "请输入内容" => "مەزمۇن كىرگۈزۈڭ",
    "多标签半角" => "كۆپ بەلگە يېرىم كەڭلىك",
    "关键词建议" => "ئاچقۇچلۇق سۆز تەكلىپلىرى",
    "请输入作者" => "ئاپتورنى كىرگۈزۈڭ",
    "请选择数据" => "سانلىق مەلۇماتنى تاللاڭ",
    "软删除文章" => "يۇمشاق ئۆچۈرۈش ماقالىسى",
    "软删除成功" => "يۇمشاق دېتال مۇۋەپپەقىيەتلىك بولدى",
    "软删除失败" => "يۇمشاق ئۆچۈرۈش مەغلۇب بولدى",
    "角色不存在" => "رولى مەۋجۇت ئەمەس",
    "角色被禁用" => "رولى چەكلەنگەن",
    "功能不存在" => "ئىقتىدار مەۋجۇت ئەمەس",
    "功能被禁用" => "ئىقتىدارى چەكلەنگەن",
    "国际化帮助" => "خەلقئارالىشىش ياردىمى",
    "未选择文本" => "ھېچقانداق تېكىست تاللانمىدى",
    "请选择语言" => "تىل تاللاڭ",
    "请输入排序" => "رەتلەشنى كىرگۈزۈڭ",
    "未修改属性" => "خاسلىقى ئۆزگەرتىلمىگەن",
    "机器人评论" => "ماشىنا ئادەم باھاسى",
    "生成语言包" => "تىل بولىقى ھاسىل قىلىڭ",
    "国际化分类" => "خەلقئارالىق تۈرگە ئايرىش",
    "国际化标签" => "خەلقئارالىشىش بەلگىسى",
    "国际化文章" => "خەلقئارالىق ماقالىلەر",
    "国际化页面" => "خەلقئارالىق بەت",
    "服务器环境" => "مۇلازىمېتىر مۇھىتى",
    "数据库信息" => "ساندان ئۇچۇرى",
    "服务器时间" => "مۇلازىمېتىر ۋاقتى",
    "还没有评论" => "ھازىرچە ھېچقانداق باھا يوق",
    "还没有数据" => "ھازىرچە ھېچقانداق سانلىق مەلۇمات يوق",
    "网址不合法" => "URL قانۇنسىز",
    "选择多文件" => "كۆپ ھۆججەتلەرنى تاللاڭ",
    "个未使用，" => "ئىشلىتىلمىگەن ،",
    "文件不存在" => "ھۆججەت مەۋجۇت ئەمەس",
    "重命名失败" => "ئىسىم ئۆزگەرتىش مەغلۇب بولدى",
    "重命名成功" => "مۇۋەپپەقىيەتلىك ئۆزگەرتىڭ",
    "角色已存在" => "رولى ئاللىبۇرۇن مەۋجۇت",
    "蜘蛛不索引" => "ئۆمۈچۈك كۆرسەتمەيدۇ",
    "请输入数量" => "مىقدارنى كىرگۈزۈڭ",
    "昵称已存在" => "لەقەم ئاللىبۇرۇن مەۋجۇت",
    "页面没找到" => "بەت تېپىلمىدى",
    "还没有文章" => "ھازىرچە ھېچقانداق ماقالە يوق",
    "还没有页面" => "تېخى بەت يوق",
    "还没有分类" => "تېخى تۈرگە ئايرىلمىدى",
    "还没有标签" => "خەتكۈچ يوق",
    "还没有热门" => "تېخى مودا ئەمەس",
    "还没有更新" => "تېخى يېڭىلانمىدى",
    "还没有网址" => "تېخى URL يوق",
    "请写点评论" => "باھا يېزىڭ",
    "，等待审核" => ", Moderated",
    "你已经注册" => "تىزىملاتتىڭىز",
    "提取语言包" => "تىل بولىقىنى چىقىرىڭ",
    "分类国际化" => "تۈرگە ئايرىش خەلقئارالىشىش",
    "标签国际化" => "بەلگە خەلقئارالىشىش",
    "文章国际化" => "ماقالە خەلقئارالىشىش",
    "页面国际化" => "بەت خەلقئارالىشىش",
    "格鲁吉亚语" => "گرۇزىيە",
    "博杰普尔语" => "Bhojpuri",
    "白俄罗斯语" => "بېلورۇسىيە",
    "斯瓦希里语" => "Swahili",
    "古吉拉特语" => "gujarati",
    "斯洛伐克语" => "سلوۋاكىيە",
    "阿塞拜疆语" => "ئەزەربەيجان",
    "印尼巽他语" => "ھىندونېزىيە سۇندان",
    "加利西亚语" => "Galician",
    "拉脱维亚语" => "Latvian",
    "罗马尼亚语" => "Romanian",
    "亚美尼亚语" => "ئەرمىنىيە",
    "保加利亚语" => "بۇلغارىيە",
    "爱沙尼亚语" => "ئېستونىيە",
    "阿姆哈拉语" => "Amharic",
    "吉尔吉斯语" => "قىرغىزىستان",
    "克罗地亚语" => "كىرودىيە",
    "波斯尼亚语" => "Bosnian",
    "蒂格尼亚语" => "Tignan",
    "克里奥尔语" => "Creole",
    "马尔加什语" => "Malagasy",
    "南非科萨语" => "جەنۇبىي ئافرىقا جوسا",
    "南非祖鲁语" => "زۇلۇ ، جەنۇبى ئافرىقا",
    "塞尔维亚语" => "سېربىيە",
    "乌兹别克语" => "ئۆزبېك",
    "伊洛卡诺语" => "Ilocano",
    "印尼爪哇语" => "Javanese Indonesian",
    "回复ID错误" => "كىملىك ​​خاتالىقىغا جاۋاب قايتۇرۇڭ",
    "非法文章ID" => "قانۇنسىز ماقالە كىملىكى",
    "管理后台" => "باشقۇرۇش ئارقا كۆرۈنۈشى",
    "管理登录" => "باشقۇرغۇچى كىرىش",
    "登录成功" => "كىرىش مۇۋەپپەقىيەتلىك",
    "切换注册" => "تىزىملىتىش",
    "你已登出" => "سىز چىقىپ كەتتىڭىز",
    "登出成功" => "كىرىش مۇۋەپپەقىيەتلىك بولدى",
    "用户注册" => "ئىشلەتكۈچى تىزىملاش",
    "注册成功" => "تىزىملىتىش مۇۋەپپەقىيىتى",
    "注册失败" => "تىزىملىتىش مەغلۇب بولدى",
    "重复密码" => "پارولنى تەكرارلاڭ",
    "切换登录" => "كىرىشنى ئالماشتۇرۇش",
    "请先登录" => "ئالدى بىلەن كىرىڭ",
    "修改资料" => "ئۇچۇرنى ئۆزگەرتىڭ",
    "没有改变" => "ئۆزگەرتىش يوق",
    "不可修改" => "ئۆزگەرتكىلى بولمايدۇ",
    "上传失败" => "يوللاش مەغلۇپ بولدى",
    "清除成功" => "مۇۋەپپەقىيەتلىك تازىلاش",
    "暂无记录" => "خاتىرە يوق",
    "批量删除" => "تۈركۈمدىكى ئۆچۈرۈش",
    "文章列表" => "ماقالە تىزىملىكى",
    "发布时间" => "قويۇپ بېرىش ۋاقتى",
    "修改时间" => "ۋاقىتنى ئۆزگەرتىڭ",
    "文章标题" => "ماقالىنىڭ ئىسمى",
    "标题重名" => "كۆپەيتىلگەن ماۋزۇ",
    "别名重复" => "كۆپەيتىلگەن ئىسىم",
    "创建文章" => "ماقالە قۇر",
    "编辑文章" => "ماقالىنى تەھرىرلەش",
    "非法字段" => "قانۇنسىز ساھە",
    "填写整数" => "پۈتۈن ساننى تولدۇرۇڭ",
    "修改属性" => "خاسلىقنى ئۆزگەرتىڭ",
    "修改成功" => "مۇۋەپپەقىيەتلىك ئۆزگەرتىلدى",
    "修改失败" => "تەھرىرلىيەلمىدى",
    "批量还原" => "تۈركۈملەپ ئەسلىگە كەلتۈرۈش",
    "还原文章" => "ماقالىنى ئەسلىگە كەلتۈرۈش",
    "还原成功" => "مۇۋەپپەقىيەتلىك ئەسلىگە كەلتۈرۈڭ",
    "还原失败" => "ئەسلىگە كەلتۈرۈش مەغلۇپ بولدى",
    "删除文章" => "ماقالىنى ئۆچۈرۈڭ",
    "删除成功" => "مۇۋەپپەقىيەتلىك ئۆچۈرۈلدى",
    "删除失败" => "ئۆچۈرەلمىدى",
    "全部展开" => "ھەممىنى كېڭەيتىڭ",
    "全部折叠" => "ھەممىنى يىمىرىڭ",
    "添加下级" => "قول ئاستىدىكىلەرنى قوشۇڭ",
    "编辑菜单" => "تىزىملىكنى تەھرىرلەش",
    "菜单名称" => "تىزىملىك ​​ئىسمى",
    "父级菜单" => "ئاتا-ئانىلار تىزىملىكى",
    "顶级菜单" => "ئۈستى تىزىملىك",
    "创建菜单" => "تىزىملىك ​​قۇرۇش",
    "删除菜单" => "تىزىملىكنى ئۆچۈرۈڭ",
    "非法访问" => "ئىجازەتسىز زىيارەت",
    "拒绝访问" => "زىيارەت رەت قىلىندى",
    "没有权限" => "ئىجازەت رەت قىلىندى",
    "彻底删除" => "پۈتۈنلەي ئۆچۈرۈڭ",
    "批量待审" => "بىر تەرەپ قىلىنىش ئالدىدا",
    "批量审核" => "Batch review",
    "垃圾评论" => "ئەخلەت خەتلەر",
    "分类名称" => "تۈر ئىسمى",
    "分类列表" => "سەھىپە تىزىملىكى",
    "父级分类" => "ئاتا-ئانىلار تۈرى",
    "顶级分类" => "ئەڭ يۇقىرى تۈرگە ئايرىش",
    "分类重名" => "كۆپەيتىلگەن ئىسمى بار تۈرلەر",
    "创建分类" => "سەھىپىلەرنى قۇر",
    "编辑分类" => "سەھىپىنى تەھرىرلەش",
    "删除分类" => "كاتېگورىيەنى ئۆچۈرۈڭ",
    "评论作者" => "ئاپتورنى كۆزدىن كەچۈرۈڭ",
    "评论内容" => "باھا",
    "评论列表" => "باھا تىزىملىكى",
    "评论文章" => "ماقالىلەرنى كۆزدىن كەچۈرۈڭ",
    "回复内容" => "مەزمۇنغا جاۋاب قايتۇرۇڭ",
    "回复评论" => "ئىنكاسقا جاۋاب قايتۇرۇڭ",
    "编辑评论" => "تەھرىرلىك باھا",
    "待审评论" => "ساقلىنىۋاتقان باھا",
    "待审成功" => "سىناق مۇۋەپپەقىيەتلىك بولۇۋاتىدۇ",
    "待审失败" => "سوتلاش مەغلۇب بولدى",
    "审核成功" => "مۇۋەپپەقىيەتلىك تەكشۈرۈڭ",
    "审核失败" => "ئىقتىسادىي تەپتىش مەغلۇب بولدى",
    "删除评论" => "باھانى ئۆچۈرۈڭ",
    "谷歌翻译" => "Google Translate",
    "检测语言" => "تىلنى ئېنىقلاش",
    "别名错误" => "Alias ​​خاتالىق",
    "语言错误" => "تىل خاتالىقى",
    "标签名称" => "خەتكۈچ ئىسمى",
    "标签列表" => "خەتكۈچ تىزىملىكى",
    "标签重名" => "تەكرارلانغان ئىسىملار بىلەن خەتكۈچلەر",
    "页面列表" => "بەت تىزىملىكى",
    "页面标题" => "بەت ماۋزۇسى",
    "父级页面" => "ئانا بەت",
    "顶级页面" => "ئۈستى بەت",
    "清除缓存" => "cache",
    "当前版本" => "نۆۋەتتىكى نەشرى",
    "重置系统" => "سىستېمىنى ئەسلىگە كەلتۈرۈش",
    "最新评论" => "ئەڭ يېڭى باھا",
    "联系我们" => "بىز بىلەن ئالاقىلىشىڭ",
    "数据统计" => "ستاتىستىكا",
    "网站名称" => "تور بېكەت ئىسمى",
    "网站地址" => "تور بېكەت ئادرېسى",
    "链接列表" => "ئۇلانغان تىزىملىك",
    "创建链接" => "ئۇلىنىش قۇر",
    "编辑链接" => "ئۇلىنىشنى تەھرىرلەڭ",
    "删除链接" => "ئۇلىنىشنى ئۆچۈرۈڭ",
    "日志标题" => "خاتىرە ئىسمى",
    "消息内容" => "ئۇچۇر مەزمۇنى",
    "请求方法" => "تەلەپ قىلىش ئۇسۇلى",
    "请求路径" => "يول تەلەپ قىلىش",
    "日志列表" => "خاتىرە تىزىملىكى",
    "上传成功" => "يۈكلەش مۇۋەپپەقىيەتلىك",
    "媒体列表" => "media list",
    "开始上传" => "يوللاشنى باشلاڭ",
    "等待上传" => "يوللاشنى ساقلاۋاتىدۇ",
    "重新上传" => "قايتا يۈكلەش",
    "上传完成" => "يوللاش تاماملاندى",
    "系统保留" => "سىستېما ساقلانغان",
    "发现重复" => "كۆپەيتىلگەن نۇسخىسى تېپىلدى",
    "上传文件" => "ھۆججەت يوللاش",
    "个在用，" => "بىرى ئىشلىتىلىۋاتىدۇ ،",
    "文件重名" => "ھۆججەت نامىنى كۆپەيتىش",
    "删除文件" => "ھۆججەتلەرنى ئۆچۈرۈڭ",
    "创建页面" => "بەت قۇرۇش",
    "编辑页面" => "بەتنى تەھرىرلەش",
    "删除页面" => "بەتنى ئۆچۈرۈڭ",
    "角色列表" => "رول تىزىملىكى",
    "角色名称" => "رول ئىسمى",
    "权限分配" => "ئىجازەتنامە",
    "创建角色" => "رول يارىتىش",
    "编辑角色" => "رولنى تەھرىرلەش",
    "删除角色" => "رولنى ئۆچۈرۈڭ",
    "基础设置" => "Basic Settings",
    "高级设置" => "ئىلغار تەڭشەكلەر",
    "其他设置" => "باشقا تەڭشەكلەر",
    "上传类型" => "يۈكلەش تىپى",
    "限制大小" => "چەك چوڭلۇقى",
    "默认最大" => "كۆڭۈلدىكى ئەڭ چوڭ",
    "单点登录" => "تىزىملىتىڭ",
    "版权信息" => "نەشر ھوقۇقى ئۇچۇرى",
    "链接地址" => "ئۇلىنىش ئادرېسى",
    "热门文章" => "ئاممىباب ماقالىلەر",
    "首页评论" => "باش بەتتىكى باھا",
    "内容相关" => "مەزمۇنغا مۇناسىۋەتلىك",
    "图片分页" => "رەسىملىك ​​بەت",
    "友情链接" => "ئۇلىنىشلار",
    "语音朗读" => "ئاۋازلىق ئوقۇش",
    "评论登录" => "باھا كىرىش",
    "评论待审" => "باھا ساقلىنىۋاتىدۇ",
    "保存成功" => "مۇۋەپپەقىيەتلىك ساقلاندى",
    "创建标签" => "خەتكۈچ قۇر",
    "编辑标签" => "خەتكۈچنى تەھرىرلەش",
    "删除标签" => "خەتكۈچنى ئۆچۈرۈڭ",
    "用户列表" => "ئىشلەتكۈچى تىزىملىكى",
    "注册时间" => "تىزىملىتىش ۋاقتى",
    "首次登陆" => "تۇنجى قېتىم كىرىش",
    "最后登陆" => "ئاخىرقى كىرىش",
    "语言权限" => "تىل ئىجازىتى",
    "创建用户" => "ئىشلەتكۈچى قۇرۇش",
    "不能修改" => "ئۆزگەرتىشكە بولمايدۇ",
    "编辑用户" => "ئىشلەتكۈچىنى تەھرىرلەش",
    "删除用户" => "ئىشلەتكۈچىنى ئۆچۈرۈڭ",
    "最新文章" => "ئەڭ يېڭى ماقالىلەر",
    "搜索结果" => "ئىزدەش نەتىجىسى",
    "选择语言" => "تىل تاللاڭ",
    "分类为空" => "سەھىپە قۇرۇق",
    "收藏文章" => "ماقالە توپلاڭ",
    "收藏成功" => "توپلاش مۇۋەپپەقىيەتلىك",
    "取消收藏" => "ياقتۇرىدىغانلارنى ئەمەلدىن قالدۇرۇڭ",
    "取消回复" => "جاۋابنى ئەمەلدىن قالدۇرۇڭ",
    "发表评论" => "باھا",
    "语音播放" => "ئاۋاز قويۇش",
    "回到顶部" => "قايتىش",
    "分类目录" => "سەھىپىلەر",
    "下载内容" => "مەزمۇننى چۈشۈرۈش",
    "相关文章" => "مۇناسىۋەتلىك ماقالىلەر",
    "媒体合成" => "media synthesis",
    "语音合成" => "سۆز بىرىكمىسى",
    "前端登录" => "ئالدى كىرىش كىرىش",
    "前端注册" => "ئالدى-كەينى تىزىملاش",
    "前端登出" => "ئالدى كىرىش ئېغىزى",
    "后台首页" => "تەگلىك ئۆي",
    "欢迎页面" => "قارشى ئالىمىز",
    "权限管理" => "ھوقۇق باشقۇرۇش",
    "用户管理" => "ئىشلەتكۈچى باشقۇرۇش",
    "角色管理" => "رول باشقۇرۇش",
    "权限菜单" => "ئىجازەت تىزىملىكى",
    "分类管理" => "تۈرگە ئايرىش باشقۇرۇش",
    "标签管理" => "خەتكۈچ باشقۇرۇش",
    "文章管理" => "ماقالە باشقۇرۇش",
    "页面管理" => "بەت باشقۇرۇش",
    "媒体管理" => "media management",
    "上传接口" => "كۆرۈنمە يۈزى يۈكلەش",
    "链接管理" => "ئۇلىنىش باشقۇرۇش",
    "评论管理" => "باھا باشقۇرۇش",
    "审核通过" => "ئىمتىھاندىن ئۆتتى",
    "机器评论" => "ماشىنا تەكشۈرۈشى",
    "系统管理" => "سىستېما باشقۇرۇش",
    "系统设置" => "سىستېما تەڭشىكى",
    "保存设置" => "تەڭشەكلەرنى ساقلاڭ",
    "恢复出厂" => "زاۋۇتنى ئەسلىگە كەلتۈرۈش",
    "操作日志" => "مەشغۇلات خاتىرىسى",
    "删除日志" => "خاتىرىنى ئۆچۈرۈڭ",
    "科西嘉语" => "كورسىكان",
    "瓜拉尼语" => "Guaraní",
    "卢旺达语" => "Kinyarwanda",
    "约鲁巴语" => "yoruba",
    "尼泊尔语" => "نېپال",
    "夏威夷语" => "Hawaiian",
    "意第绪语" => "Yiddish",
    "爱尔兰语" => "irish",
    "希伯来语" => "hebrew",
    "卡纳达语" => "kannada",
    "匈牙利语" => "ئاچارچىلىق",
    "泰米尔语" => "tamil",
    "阿拉伯语" => "ئەرەبچە",
    "孟加拉语" => "Bengali",
    "萨摩亚语" => "Samoan",
    "班巴拉语" => "Bambara",
    "立陶宛语" => "لىتۋانىيە",
    "马耳他语" => "Maltese",
    "土库曼语" => "تۈركمەنلەر",
    "阿萨姆语" => "Assamese",
    "僧伽罗语" => "sinhala",
    "乌克兰语" => "ئۇكرائىنا",
    "威尔士语" => "Welsh",
    "菲律宾语" => "فىلىپپىنلىق",
    "艾马拉语" => "Aymara",
    "泰卢固语" => "telugu",
    "多格来语" => "dogglai",
    "迈蒂利语" => "Maithili",
    "普什图语" => "پۇشتۇ",
    "迪维希语" => "Dhivehi",
    "卢森堡语" => "لىيۇكسېمبۇرگ",
    "土耳其语" => "turkish",
    "马其顿语" => "ماكېدونىيە",
    "卢干达语" => "Luganda",
    "马拉地语" => "marathi",
    "乌尔都语" => "ئوردۇچە",
    "葡萄牙语" => "پورتۇگال تىلى",
    "奥罗莫语" => "Oromo",
    "西班牙语" => "ئىسپانىيە",
    "弗里西语" => "Frisian",
    "索马里语" => "سومالى",
    "齐切瓦语" => "چىچېۋا",
    "旁遮普语" => "punjabi",
    "巴斯克语" => "باسك",
    "意大利语" => "Italian",
    "塔吉克语" => "تاجىك",
    "克丘亚语" => "Quechua",
    "奥利亚语" => "Oriya",
    "哈萨克语" => "قازاق",
    "林格拉语" => "Lingala",
    "塞佩蒂语" => "Sepeti",
    "塞索托语" => "Sesotho",
    "维吾尔语" => "ئۇيغۇر",
    "ICP No.001" => "ICP No.001",
    "用户名" => "ئىشلەتكۈچى ئىسمى",
    "验证码" => "دەلىللەش كودى",
    "记住我" => "مېنى ئەسلەڭ",
    "手机号" => "تېلېفون نومۇرى",
    "请输入" => "كىرگۈزۈڭ",
    "请选择" => "تاللاڭ",
    "浏览量" => "كۆرۈنۈشلەر",
    "回收站" => "يىغىۋېلىش ساندۇقى",
    "菜单树" => "تىزىملىك ​​دەرىخى",
    "控制器" => "كونتروللىغۇچ",
    "第一页" => "بىرىنچى بەت",
    "最后页" => "ئاخىرقى بەت",
    "筛选列" => "ستوننى سۈزۈڭ",
    "删除行" => "قۇرنى ئۆچۈرۈڭ",
    "还原行" => "قۇرنى ئەمەلدىن قالدۇرۇش",
    "重命名" => "ئىسىم ئۆزگەرتىش",
    "国际化" => "يەر شارىلىشىش",
    "文章数" => "ماقالىلەرنىڭ سانى",
    "机器人" => "ماشىنا ئادەم",
    "星期日" => "يەكشەنبە",
    "星期一" => "دۈشەنبە",
    "星期二" => "سەيشەنبە",
    "星期三" => "چارشەنبە",
    "星期四" => "پەيشەنبە",
    "星期五" => "جۈمە",
    "星期六" => "شەنبە",
    "会员数" => "ئەزا سانى",
    "评论数" => "ئىنكاس سانى",
    "文件名" => "ھۆججەت ئىسمى",
    "音视频" => "Audio and video",
    "扩展名" => "كېڭەيتىلمە ئىسمى",
    "未使用" => "ئىشلىتىلمىگەن",
    "个无效" => "ئىناۋەتسىز",
    "共删除" => "ئومۇمىي ئۆچۈرۈلدى",
    "个文件" => "ھۆججەتلەر",
    "备案号" => "دېلو نومۇرى",
    "轮播图" => "carousel",
    "条显示" => "بالداق كۆرسىتىش",
    "上一页" => "ئالدىنقى بەت",
    "下一页" => "كېيىنكى بەت",
    "未分类" => "تۈرگە ئايرىلمىغان",
    "空标签" => "قۇرۇق بەلگە",
    "无标题" => "نامسىز",
    "控制台" => "console",
    "登录中" => "كىرىش",
    "注册中" => "تىزىملىتىش",
    "跳转中" => "قايتا نىشانلاش",
    "提交中" => "يوللاش",
    "审核中" => "تەكشۈرۈلۈۋاتىدۇ",
    "请稍后" => "ساقلاپ تۇرۇڭ",
    "篇文章" => "Article",
    "客户端" => "خېرىدار",
    "没有了" => "ھېچكىم قالمىدى",
    "后评论" => "ئىنكاس يوللاڭ",
    "未命名" => "نامسىز",
    "软删除" => "يۇمشاق ئۆچۈرۈش",
    "语言包" => "تىل بولىقى",
    "豪萨语" => "Hausa",
    "挪威语" => "norwegian",
    "贡根语" => "gonggan language",
    "拉丁语" => "latin",
    "捷克语" => "چېخ",
    "波斯语" => "پارسچە",
    "印地语" => "Hindi",
    "冰岛语" => "ئىسلاندىيە",
    "契维语" => "Twi",
    "高棉语" => "كامبودژا",
    "丹麦语" => "danish",
    "修纳语" => "Shona",
    "越南语" => "ۋېيتنام",
    "宿务语" => "Cebuano",
    "波兰语" => "پولشا",
    "鞑靼语" => "تاتار",
    "老挝语" => "لائوس",
    "瑞典语" => "شىۋىتسىيە",
    "缅甸语" => "بېرما",
    "信德语" => "سىندى",
    "马来语" => "مالايسىيا",
    "伊博语" => "Igbo",
    "希腊语" => "گرېتسىيە",
    "芬兰语" => "فىنلاندىيە",
    "埃维语" => "Ewe",
    "毛利语" => "Maori",
    "荷兰语" => "گوللاندىيە",
    "蒙古语" => "موڭغۇل",
    "米佐语" => "Mizo",
    "世界语" => "Esperanto",
    "印尼语" => "ھىندونېزىيە",
    "宗加语" => "Dzonga",
    "密码" => "پارول",
    "登录" => "كىرىڭ",
    "重置" => "reset",
    "信息" => "ئۇچۇر",
    "成功" => "مۇۋەپپەقىيەت",
    "失败" => "مەغلۇب",
    "昵称" => "Nick name",
    "选填" => "ئىختىيارىي",
    "注册" => "تىزىملىتىڭ",
    "错误" => "خاتالىق",
    "头像" => "avatar",
    "上传" => "يوللاش",
    "保存" => "تېجەڭ",
    "标题" => "ماۋزۇ",
    "别名" => "Alias",
    "描述" => "تەسۋىرلەڭ",
    "封面" => "cover",
    "分类" => "تۈرگە ئايرىش",
    "状态" => "ئىشتات",
    "正常" => "نورمال",
    "禁用" => "چەكلەش",
    "作者" => "ئاپتور",
    "日期" => "چېسلا",
    "添加" => "قوشۇش",
    "刷新" => "يېڭىلاش",
    "标签" => "بەلگە",
    "浏览" => "توركۆرگۈ",
    "点赞" => "like",
    "评论" => "باھا",
    "操作" => "مەشغۇلات قىلىڭ",
    "阅读" => "ئوقۇش",
    "编辑" => "تەھرىر",
    "删除" => "ئۆچۈرۈش",
    "翻译" => "تەرجىمە قىلىڭ",
    "内容" => "مەزمۇن",
    "分割" => "بۆلەك",
    "建议" => "تەكلىپ",
    "非法" => "قانۇنسىز",
    "还原" => "كېمەيتىش",
    "方法" => "method",
    "排序" => "sort",
    "图标" => "سىنبەلگە",
    "菜单" => "تىزىملىك",
    "时间" => "ۋاقىت",
    "匿名" => "نامسىز",
    "确定" => "ئەلۋەتتە",
    "取消" => "بىكار قىلىش",
    "跳转" => "سەكرەش",
    "页码" => "بەت نومۇرى",
    "共计" => "ئومۇمىي",
    "每页" => "ھەر بىر بەت",
    "导出" => "ئېكسپورت",
    "打印" => "بېسىش",
    "回复" => "جاۋاب",
    "路径" => "يول",
    "预览" => "ئالدىن كۆرۈش",
    "名称" => "name",
    "行数" => "قۇر",
    "相册" => "سۈرەت پىلاستىنكىسى",
    "文章" => "ماقالە",
    "待审" => "ساقلىنىۋاتىدۇ",
    "通过" => "pass",
    "垃圾" => "ئەخلەت",
    "提取" => "extract",
    "生成" => "ھاسىل قىلىش",
    "定位" => "ئورنى",
    "填充" => "تولدۇرۇش",
    "帮助" => "ياردەم",
    "返回" => "قايتىش",
    "语言" => "تىل",
    "宽屏" => "كەڭ ئېكران",
    "普通" => "ئادەتتىكى",
    "后台" => "ئارقا سەھنى",
    "前台" => "ئالدى ئۈستەل",
    "登出" => "چېكىنىش",
    "版本" => "نەشرى",
    "关于" => "ھەققىدە",
    "微信" => "ئۈندىدار",
    "会员" => "ئەزا",
    "地址" => "ئادرېس",
    "类型" => "تىپى",
    "图片" => "رەسىم",
    "文件" => "ھۆججەت",
    "失效" => "ئىناۋەتسىز",
    "大小" => "size",
    "下载" => "چۈشۈرۈش",
    "导航" => "يول باشلاش",
    "站标" => "بېكەت بەلگىسى",
    "单位" => "unit",
    "加速" => "تېزلىتىڭ",
    "角色" => "رولى",
    "首页" => "ئالدى بەت",
    "个数" => "سان",
    "我的" => "مېنىڭ",
    "说道" => "دېدى",
    "手机" => "يان تېلېفون",
    "可选" => "ئىختىيارىي",
    "搜索" => "ئىزدەش",
    "页面" => "page",
    "查看" => "تەكشۈرۈڭ",
    "创建" => "قۇرۇش",
    "更新" => "يېڭىلاش",
    "英语" => "ئىنگىلىزچە",
    "法语" => "فىرانسۇزچە",
    "俄语" => "رۇسچە",
    "梵语" => "سانسكرىت تىلى",
    "日语" => "ياپون",
    "泰语" => "تايلاند",
    "苗语" => "Hmong",
    "德语" => "German",
    "韩语" => "كورېيە",
    "请" => "كەچۈرۈڭ",
];
