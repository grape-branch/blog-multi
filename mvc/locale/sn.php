<?php
/**
 * @author admin
 * @date 2024-06-05 12:47:02
 * @desc 修纳语语言包
 */
return [
    "迷你mvc小框架，一套用于开发各类管理系统的通用型后台管理系统模板，基于开源免费的 layui 的制作，内置各类实际业务场景下相对丰富的示例。" => "Mini MVC idiki dhizaini, seti yepasirese backend manejimendi masisitimu ekugadzira akasiyana manejimendi masisitimu Iyo yakavakirwa pane yakavhurika sosi uye yemahara layui uye ine yakapfuma seti yemienzaniso mune akasiyana chaiwo mabhizinesi mamiriro.",
    "除非特别说明，本博客均为原创，如需转载，请以链接形式标明来源出处。" => "Kunze kwekunge zvataurwa neimwe nzira, iyi blog ndeyekutanga.",
    "重置系统，将删除用户所有数据和附件，恢复到出厂设置。" => "Kugadzirisa zvakare sisitimu kunodzima data rese remushandisi uye zvakanamirwa uye kudzoreredza kune marongero efekitori.",
    "用户名只能英文字母数字下划线或中划线，位数" => "Zita rekushandisa rinongokwanisa kuva nemavara echiRungu, manhamba, pasi pasi kana pasi pasi, uye nhamba yemanhamba",
    "昵称只能中英文字母数字下划线中划线，位数" => "Zita remadunhurirwa rinokwanisa chete kuva nemavara echiChinese neChirungu, nhamba, pasi pasi, pasi pasi, uye manhamba.",
    "有道接口不支持，需科学上网使用谷歌接口。" => "Youdao interface haina kutsigirwa Unoda kushandisa Google interface kuti uwane iyo Internet nesainzi.",
    "别名只能英文字母数字下划线中划线，位数" => "Mazita echirungu anogona chete kuva nemavara echiRungu, manhamba, underscores, underscores, and digits.",
    "计算孤立文件需要较长时间，确定继续吗？" => "Zvinotora nguva yakareba kuverenga mafaira enherera Une chokwadi chekuti unoda kuenderera mberi here?",
    "语音不支持，需科学上网使用谷歌接口。" => "Izwi haritsigirwe, saka unofanirwa kushandisa Google interface kuti uwane iyo Internet nesainzi.",
    "您的账号已在别处登录，请重新登录！" => "Akaunti yako yaiswa kune imwe nzvimbo, ndapota pinda zvakare!",
    "你的账号已被禁用，请联系管理员" => "Akaunti yako yakavharwa, ndapota taura nemutongi",
    "文件有在使用中，请重新计算状态" => "Iyo faira iri kushandiswa, ndokumbira uverenge mamiriro acho",
    "目标语言权限不够，请联系管理员" => "Mvumo dzisina kukwana dzemutauro, tapota taura nemutarisiri",
    "真的标记垃圾评论选中的行吗？" => "Unonyatsomaka mutsara wakasarudzwa se spam?",
    "源语言别名为空，请联系源作者" => "Mutauro wemabviro hauna chinhu, tapota taura nemunyori",
    "点击上传，或将文件拖拽到此处" => "Dzvanya kuti uise, kana kudhonza faira pano",
    "合成成功！再次点击按钮下载。" => "Synthesis yakabudirira! Baya bhatani zvakare kuti udhaunirodhe.",
    "没有语言权限，请联系管理员" => "Hapana mvumo yemutauro, tapota taura nemutarisiri",
    "源语言别名为空，不能国际化" => "Mutauro wemabviro hauna chinhu uye haugone kuitwa pasi rose.",
    "源语言与目标语言都不能为空" => "Mutauro wacho kana kuti mutauro waunotumirwa haugone kuva usina chinhu.",
    "源语言与目标语言数量不一致" => "Huwandu hwemabviro nemitauro yaunonangwa hauenderane",
    "删除文件失败，文件有在使用" => "Yatadza kudzima faira, faira riri kushandiswa",
    "角色不存在，请联系管理员" => "Basa racho harisipo, tapota taura nemutongi",
    "角色被禁用，请联系管理员" => "Basa rakadzimwa, ndapota taura nemutungamiriri",
    "功能不存在，请联系管理员" => "Basa harisipo, ndapota taura nemutungamiriri",
    "功能被禁用，请联系管理员" => "Basa rakadzimwa, ndapota taura nemutarisiri",
    "真的彻底删除选中的行吗？" => "Inonyatso kudzima mutsara wakasarudzwa zvachose?",
    "真的审核通过选中的行吗？" => "Mitsara yakasarudzwa inotenderwa here?",
    "目标语言与源语言不能相同" => "Mutauro waunonangidzirwa haugoni kufanana nemutauro wemabviro",
    "提取语言包与源语言不一致" => "Mutauro wabviswa hauenderane nemutauro wemabviro",
    "文件有在使用，删除失败！" => "Iyo faira iri kushandiswa uye kudzima kwakundikana!",
    "没有权限，请联系管理员" => "Hapana mvumo, ndapota taura nemutungamiriri",
    "真的待审核选中的行吗？" => "Ichokwadi here mutsara unoda kuongororwa uye kusarudzwa?",
    "目标语言包有空行，行数" => "Pasuru yemutauro yaunonangwa ine mitsetse isina chinhu, nhamba yemitsara",
    "此操作恢复到出厂设置？" => "Kuvhiya uku kunodzosera kumasetin\'i efekitari?",
    "朗读错误，请联网后再试" => "Kutadza kuverenga, tapota batanidza kuIndaneti woedza zvakare",
    "还没有添加分类描述信息" => "Hapasati pave neruzivo rwetsanangudzo yechikamu",
    "本地媒体已失效或不存在" => "Nhepfenyuro yemuno haina kushanda kana kuti haipo",
    "库尔德语（库尔曼吉语）" => "Kurdish (Kurmanji)",
    "作者名未注册或被禁用" => "Zita remunyori harina kunyoreswa kana kuremara",
    "真的删除选中的行吗？" => "Unodzima here mutsara wakasarudzwa?",
    "真的还原选中的行吗？" => "Unonyatso dzorera here mitsara yakasarudzwa?",
    "真的删除行或子行么？" => "Iwe unoda chaizvo kudzima mitsara kana mitsara yepasi?",
    "目标语言包未发现空行" => "Mutsara usina chinhu hauna kuwanikwa mumutauro waunonongedza",
    "该语言不支持语音朗读" => "Mutauro uyu hautsigire kuverenga inzwi",
    "语言包数据未发生改变" => "Mutauro wepakiti data haina kuchinja",
    "欢迎使用后台管理系统" => "Kugamuchirwa kune backend management system",
    "文件有在使用或已失效" => "Iyo faira iri kushandiswa kana kupera nguva",
    "合成失败，请稍后再试" => "Synthesis yatadza, ndapota edza zvakare gare gare",
    "&copy; 2021-2023 Company, Inc." => "© 2021-2023 Company, Inc.",
    "梅泰语（曼尼普尔语）" => "Meitei (Manipuri)",
    "两次密码输入不一致" => "Iwo maviri ekuisa password haaenderane",
    "该用户密码不可修改" => "Iyi password yemushandisi haigone kugadziridzwa",
    "添加文章时创建标签" => "Gadzira ma tag paunenge uchiwedzera zvinyorwa",
    "删除文章时删除评论" => "Delete comments pakudzima chinyorwa",
    "控制器与方法已存在" => "Mudzori uye nzira yatovepo",
    "标题或名称不能为空" => "Musoro kana zita haringashaikwe",
    "请选择一种语言朗读" => "Ndapota sarudza mutauro wekuverenga zvinonzwika",
    "分类有文章不能删除" => "Pane zvinyorwa zviri muchikwata zvisingagoni kudzimwa",
    "审核为垃圾评论成功" => "Yakaongororwa se spam yakabudirira",
    "审核为垃圾评论失败" => "Ongororo yakundikana pamhinduro ye spam",
    "确定要登出站点吗？" => "Une chokwadi chekuti unoda kubuda kunze kwesaiti?",
    "网站名称或地址为空" => "Zita rewebhusaiti kana kero haina chinhu",
    "网站名称或地址重名" => "Dudzira zita rewebhusaiti kana kero",
    "允许上传的文件类型" => "Mhando dzemafaira anotenderwa kurodha",
    "标签有文章不能删除" => "Zvinyorwa zvine ma tag hazvigone kudzimwa",
    "人觉得这篇文章很赞" => "Vanhu vanofunga kuti chinyorwa ichi chikuru",
    "还没有页面描述信息" => "Hapana ruzivo rwetsanangudzo yepeji parizvino",
    "回复与评论内容重复" => "Duplicate zvemukati zvemhinduro nekutaura",
    "失败！请稍后再试。" => "kukundikana! Ndapota edza zvakare gare gare.",
    "主耶稣基督里的教会" => "kereke munashe Jesu Kristu",
    "库尔德语（索拉尼）" => "Kurdish (Sorani)",
    "布尔语(南非荷兰语)" => "Boolean (Afrikaans)",
    "用户名或密码错误" => "zita risiri iro remushandisi kana password",
    "源语言必须是中文" => "Mutauro wekutanga unofanirwa kunge uri weChinese",
    "目标语言别名为空" => "Zita rezita remutauro waunovavarira harina chinhu",
    "国际化文章时标签" => "Internationalized article tags",
    "确定清除缓存吗？" => "Une chokwadi chekuti unoda kubvisa cache?",
    "超过的单文件大小" => "Saizi yefaira rimwechete yadarika",
    "超过前端表单限制" => "Frontend form limit yadarika",
    "目标没有写入权限" => "Chinangwa hachina mvumo yekunyora",
    "不允许的上传类型" => "Marudzi ekurodha asingatenderwe",
    "文件大小不能超过" => "Saizi yefaira haigone kudarika",
    "保存基础设置成功" => "Masettings ekutanga akachengetedzwa zvinobudirira",
    "首次基础设置成功" => "Kutanga kwekutanga kwakabudirira",
    "正在合成，请稍后" => "Synthesizing, ndapota mira",
    "不合理的请求方法" => "Nzira yekukumbira isina musoro",
    "Session无效或过期" => "Session haina kushanda kana kupera nguva",
    "手机号码不正确" => "nhamba yefoni haina kururama",
    "手机号码已存在" => "Nharembozha yatovepo",
    "标题或内容为空" => "Musoro kana zvirimo hazvina chinhu",
    "创建文章时标签" => "Tagi pakugadzira chinyorwa",
    "编辑文章时标签" => "Matagi paunenge uchigadzirisa chinyorwa",
    "真的删除行么？" => "Unoda kudzima mutsara here?",
    "请输入菜单名称" => "Ndapota isa zita remenyu",
    "请先删除子菜单" => "Ndapota bvisa submenu kutanga",
    "未登录访问后台" => "Svika kubackend pasina kupinda mukati",
    "Cookie无效或过期" => "Cookie haisiyo kana kuti yapera basa",
    "真的还原行么？" => "Zvinonyatsoita here kuidzorera?",
    "编辑器内容为空" => "Zvemupepeti hazvina chinhu",
    "未选择整行文本" => "Mutsetse wese wemavara hauna kusarudzwa",
    "划词选择行错误" => "Kukanganisa kwemutsara wekusarudza izwi",
    "数据源发生改变" => "Data source shanduko",
    "填充成功，行号" => "Yakazadzwa zvinobudirira, nhamba yemutsara",
    "请输入分类名称" => "Isa zita rechikamu",
    "分类名不能为空" => "Zita rechikwata haringashaikwe",
    "排序只能是数字" => "Kuronga kunogona kuva nhamba chete",
    "请先删除子分类" => "Ndapota bvisa zvikamu zviduku kutanga",
    "请输入评论作者" => "Ndokumbira uise munyori wemhinduro",
    "请选择目标语言" => "Ndapota sarudza mutauro waunonongedza",
    "语言包生成成功" => "Mutauro wepakiti wakagadzirwa zvinobudirira",
    "语言包生成失败" => "Kugadzira mapepa emutauro zvakundikana",
    "国际化分类成功" => "Kurongwa kwepasi rose kwakabudirira",
    "国际化分类失败" => "Kusarudzwa kwepasi rose kwakundikana",
    "请输入标签名称" => "Ndapota isa zita rezita",
    "国际化标签成功" => "International label kubudirira",
    "国际化标签失败" => "Tagi yekudyidzana nedzimwe nyika yakundikana",
    "国际化文章成功" => "International article kubudirira",
    "国际化文章失败" => "Chinyorwa chepasi rose chakakundikana",
    "请输入页面标题" => "Ndapota isa musoro wepeji",
    "国际化页面成功" => "Peji rekudyidzana nedzimwe nyika rakabudirira",
    "国际化页面失败" => "Peji rekuita pasi rose rakundikana",
    "作者：葡萄枝子" => "Munyori: Bazi remuzambiringa",
    "请输入网站名称" => "Ndapota isa zita rewebhusaiti",
    "请输入网站地址" => "Ndokumbira uise kero yewebhusaiti",
    "链接名不能为空" => "Zita rekubatanidza harigone kushaikwa",
    "上传文件不完整" => "Iro faira raiswa harina kukwana",
    "没有文件被上传" => "Hapana mafaira akaiswa",
    "找不到临时目录" => "Temp directory haina kuwanikwa",
    "未知的文件类型" => "Mhando yefaira isingazivikanwe",
    "文件名不能为空" => "Zita refaira harigone kushaikwa",
    "个文件有在使用" => "mafaira ari kushandiswa",
    "请先删除子页面" => "Ndapota bvisa subpage kutanga",
    "请输入角色名称" => "Ndapota isa zita rebasa",
    "管理员不可禁用" => "Vatungamiri havakwanisi kudzima",
    "管理员不可删除" => "Vatungamiri havakwanisi kudzima",
    "请输入限制大小" => "Ndokumbira uise muganhu",
    "请输入版权信息" => "Ndapota isa ruzivo rwekodzero",
    "恢复出厂成功！" => "Kugadzirisa patsva kwabudirira!",
    "恢复出厂失败！" => "Kugadzirisazve fekitari kwatadza!",
    "标签名不能为空" => "Tag name haigone kushaikwa",
    "还没有内容信息" => "Hapana ruzivo rwemukati parizvino",
    "这篇文章很有用" => "Ichi chinyorwa chinobatsira zvikuru",
    "保存分类国际化" => "Chengetedza chikamu chekudyidzana nedzimwe nyika",
    "分类国际化帮助" => "Classification internationalization rubatsiro",
    "保存标签国际化" => "Chengetedza label internationalization",
    "标签国际化帮助" => "Tag internationalization rubatsiro",
    "保存文章国际化" => "Sevha nyaya internationalization",
    "文章国际化帮助" => "Article internationalization rubatsiro",
    "保存页面国际化" => "Sevha mapeji internationalization",
    "页面国际化帮助" => "Peji internationalization rubatsiro",
    "海地克里奥尔语" => "Kiriyoro yeHaiti",
    "非法的ajax请求" => "Chikumbiro chisiri pamutemo cheAjax",
    "密码至少位数" => "Password inofanira kunge iine manhamba",
    "验证码不正确" => "Kodhi yekusimbisa isiriyo",
    "包含非法参数" => "Rine zvisiri pamutemo parameters",
    "请输入用户名" => "ndapota isa zita remushandisi",
    "用户名已存在" => "Username ratovepo",
    "请重输入密码" => "Ndokumbira uisezve password yako",
    "图片格式错误" => "Kukanganisa kwechimiro chemufananidzo",
    "修改资料成功" => "Data yakagadziridzwa",
    "没有改变信息" => "Hapana ruzivo rwakachinja",
    "请输入浏览量" => "Ndapota isa nhamba yemaonero",
    "请输入点赞数" => "Isa nhamba yema likes",
    "请选择子分类" => "Ndapota sarudza chikamu chiduku",
    "创建文章成功" => "Chinyorwa chakagadzirwa zvinobudirira",
    "创建文章失败" => "Tatadza kugadzira chinyorwa",
    "编辑文章成功" => "Gadzirisa chinyorwa",
    "标题不能为空" => "Musoro haugone kushaikwa",
    "菜单名称重复" => "Duplicate menyu zita",
    "创建菜单成功" => "Menyu yagadzirwa zvinobudirira",
    "创建菜单失败" => "Tatadza kugadzira menyu",
    "编辑菜单成功" => "Kugadzirisa menyu kwabudirira",
    "请选择行数据" => "Ndapota sarudza mutsara data",
    "计算孤立文件" => "Verenga mafaira enherera",
    "划词选择错误" => "Sarudzo yakaipa yemazwi",
    "请提取语言包" => "Ndapota bvisa paki yemutauro",
    "没有语音文字" => "Hapana mavara ezwi",
    "语音朗读完成" => "Kuverenga inzwi kwapera",
    "分类名称为空" => "Category name is empty",
    "创建分类成功" => "Kuronga kwakagadzirwa zvinobudirira",
    "创建分类失败" => "Tatadza kugadzira chikamu",
    "编辑分类成功" => "Gadzirisa chikamu zvakabudirira",
    "回复评论为空" => "Mhinduro kune mhinduro haina chinhu",
    "回复评论成功" => "Pindura kupa mhinduro unobudirira",
    "回复评论失败" => "Mhinduro yekutaura yakundikana",
    "评论内容为空" => "Zvekutaura hazvina chinhu",
    "编辑评论成功" => "Gadzirisa mhinduro",
    "待审评论成功" => "Mhinduro yakamirira ongororo yabudirira",
    "待审评论失败" => "Mhinduro yakamirira kuongororwa yakundikana",
    "审核通过评论" => "Maonero akatenderwa",
    "审核评论成功" => "Wongororo yabudirira",
    "审核评论失败" => "Ongororo yemhinduro yakundikana",
    "删除评论失败" => "Tatadza kudzima mhinduro",
    "请选择源语言" => "Ndapota sarudza mutauro wekutanga",
    "别名不可更改" => "Mazita haagone kuchinjwa",
    "标签名称为空" => "Tag name is empty",
    "创建链接成功" => "Link yagadzirwa zvinobudirira",
    "创建链接失败" => "Tatadza kugadzira link",
    "编辑链接成功" => "Gadzirisa link",
    "网站名称重名" => "Duplicate mazita ewebhusaiti",
    "网站地址重复" => "Duplicate kero yewebhusaiti",
    "图片压缩失败" => "Kudzvanya kwemufananidzo kwatadza",
    "移动文件失败" => "Tatadza kufambisa faira",
    "上传文件成功" => "Faira rakakwidzwa zvabudirira",
    "上传文件失败" => "Kurodha mafaira kwatadza",
    "共找到文件：" => "Mafaira ese awanikwa:",
    "创建页面成功" => "Peji yagadzirwa zvinobudirira",
    "创建页面失败" => "Kugadzira peji kwakundikana",
    "编辑页面成功" => "Gadzirisa peji",
    "权限数据错误" => "Kukanganisa kwedata remvumo",
    "创建角色成功" => "Basa rakagadzirwa zvinobudirira",
    "创建角色失败" => "Tatadza kugadzira basa",
    "编辑角色成功" => "Gadzirisa basa rakabudirira",
    "游客不可删除" => "Vashanyi havagone kudzima",
    "创建标签成功" => "Tag yakagadzirwa zvakabudirira",
    "创建标签失败" => "Tatadza kugadzira label",
    "编辑标签成功" => "Gadzirisa tag yabudirira",
    "角色数据错误" => "Character data error",
    "语言数据错误" => "Kukanganisa kwedata remutauro",
    "状态数据错误" => "Status data kukanganisa",
    "创建用户成功" => "Mushandisi akagadzirwa zvinobudirira",
    "创建用户失败" => "Yatadza kugadzira mushandisi",
    "编辑用户成功" => "Gadzirisa mushandisi zvakanaka",
    "本文博客网址" => "URL yechinyorwa ichi",
    "评论内容重复" => "Duplicate comment content",
    "回复内容重复" => "Duplicate mhinduro dzemukati",
    "评论发表成功" => "Mhinduro yakatumirwa zvinobudirira",
    "发表评论失败" => "Tatadza kutumira mhinduro",
    "前端删除评论" => "Kudzima makomendi kumberi",
    "中文（简体）" => "ChiChinese (Chakareruka)",
    "加泰罗尼亚语" => "Katarani",
    "苏格兰盖尔语" => "ChiGaelic cheScottish",
    "中文（繁体）" => "chiChinese chetsika)",
    "马拉雅拉姆语" => "ChiMalayalam",
    "斯洛文尼亚语" => "ChiSlovanian",
    "阿尔巴尼亚语" => "ChiAlbanian",
    "密码至少5位" => "Password inofanira kunge iine mavara mashanu",
    "你已经登录" => "Watopinda",
    "账号被禁用" => "Akaunti yakadzimwa",
    "请输入密码" => "Ndapota isa password",
    "留空不修改" => "Siya pasina uye usagadzirise",
    "请输入标题" => "Ndapota isa zita",
    "请输入内容" => "Ndapota isa zviri mukati",
    "多标签半角" => "Multi-label hafu-upamhi",
    "关键词建议" => "Keyword suggestions",
    "请输入作者" => "Ndapota isa munyori",
    "请选择数据" => "Ndapota sarudza data",
    "软删除文章" => "soft delete article",
    "软删除成功" => "Kudzima kwakapfava",
    "软删除失败" => "Kudzima zvinyoro zvakundikana",
    "角色不存在" => "basa harisipo",
    "角色被禁用" => "Basa rakaremara",
    "功能不存在" => "Basa harisipo",
    "功能被禁用" => "Basa rakadzimwa",
    "国际化帮助" => "Internationalization rubatsiro",
    "未选择文本" => "Hapana mavara akasarudzwa",
    "请选择语言" => "Ndapota sarudza mutauro",
    "请输入排序" => "Ndapota pinza pakuronga",
    "未修改属性" => "zvivakwa hazvina kuchinjwa",
    "机器人评论" => "Robhoti Ongororo",
    "生成语言包" => "Gadzira paki yemutauro",
    "国际化分类" => "International classification",
    "国际化标签" => "internationalization tag",
    "国际化文章" => "International articles",
    "国际化页面" => "International page",
    "服务器环境" => "Server environment",
    "数据库信息" => "Database ruzivo",
    "服务器时间" => "server nguva",
    "还没有评论" => "Hapasati pave nemacomments",
    "还没有数据" => "Hapana data parizvino",
    "网址不合法" => "URL haisi pamutemo",
    "选择多文件" => "Sarudza mafaira akawanda",
    "个未使用，" => "isina kushandiswa,",
    "文件不存在" => "faira harisipo",
    "重命名失败" => "Rename zita zvaramba",
    "重命名成功" => "Kupa zita rakabudirira",
    "角色已存在" => "Basa riripo kare",
    "蜘蛛不索引" => "dandemutande kwete indexing",
    "请输入数量" => "Ndapota isa huwandu",
    "昵称已存在" => "Nickname yatovapo",
    "页面没找到" => "Peji haina kuwanikwa",
    "还没有文章" => "Hapasati pave nezvinyorwa",
    "还没有页面" => "Hapana peji parizvino",
    "还没有分类" => "Hapasati pava nechikamu",
    "还没有标签" => "Hapana ma tags parizvino",
    "还没有热门" => "Haisati yazivikanwa",
    "还没有更新" => "Haisati yavandudzwa",
    "还没有网址" => "Hapana URL parizvino",
    "请写点评论" => "Ndapota nyora mhinduro",
    "，等待审核" => ",Moderated",
    "你已经注册" => "wakatonyoresa",
    "提取语言包" => "Bvisa mutauro paki",
    "分类国际化" => "Classification Internationalization",
    "标签国际化" => "Label internationalization",
    "文章国际化" => "Article internationalization",
    "页面国际化" => "Page internationalization",
    "格鲁吉亚语" => "ChiGeorgian",
    "博杰普尔语" => "Bhojpuri",
    "白俄罗斯语" => "Chiberaruzi",
    "斯瓦希里语" => "ChiSwahili",
    "古吉拉特语" => "ChiGujarati",
    "斯洛伐克语" => "ChiSlovak",
    "阿塞拜疆语" => "Azerbaijani",
    "印尼巽他语" => "Indonesian Sundanese",
    "加利西亚语" => "ChiGalician",
    "拉脱维亚语" => "ChiLatvian",
    "罗马尼亚语" => "ChiRomanian",
    "亚美尼亚语" => "ChiArmenian",
    "保加利亚语" => "ChiBulgarian",
    "爱沙尼亚语" => "ChiEstonian",
    "阿姆哈拉语" => "ChiAmharic",
    "吉尔吉斯语" => "Kiyagizi",
    "克罗地亚语" => "ChiCroatian",
    "波斯尼亚语" => "ChiBosnian",
    "蒂格尼亚语" => "Tignan",
    "克里奥尔语" => "Creole",
    "马尔加什语" => "Malagasy",
    "南非科萨语" => "South African Xhosa",
    "南非祖鲁语" => "Zulu, South Africa",
    "塞尔维亚语" => "ChiSebhiya",
    "乌兹别克语" => "Uzbek",
    "伊洛卡诺语" => "Ilocano",
    "印尼爪哇语" => "Javanese Indonesian",
    "回复ID错误" => "Pindura ID kukanganisa",
    "非法文章ID" => "Chinyorwa chisiri pamutemo ID",
    "管理后台" => "Management background",
    "管理登录" => "Admin kupinda",
    "登录成功" => "kupinda kwakabudirira",
    "切换注册" => "Chinja kunyoresa",
    "你已登出" => "Wabuda",
    "登出成功" => "Kubuda kwakabudirira",
    "用户注册" => "Kunyoresa kwemushandisi",
    "注册成功" => "kunyoresa kubudirira",
    "注册失败" => "kunyoresa kwakundikana",
    "重复密码" => "Dzokorora password",
    "切换登录" => "Chinja login",
    "请先登录" => "ndapota pinda mukati",
    "修改资料" => "Shandura ruzivo",
    "没有改变" => "Hapana shanduko",
    "不可修改" => "Unchangeable",
    "上传失败" => "kukwidza kwatadza",
    "清除成功" => "Bvisa zvinobudirira",
    "暂无记录" => "Hapana zvinyorwa",
    "批量删除" => "batch deletion",
    "文章列表" => "Chinyorwa chinyorwa",
    "发布时间" => "nguva yekusunungurwa",
    "修改时间" => "Shandura nguva",
    "文章标题" => "Musoro wechinyorwa",
    "标题重名" => "Duplicate title",
    "别名重复" => "Duplicate alias",
    "创建文章" => "Gadzira chinyorwa",
    "编辑文章" => "Rongedza chinyorwa",
    "非法字段" => "Munda usiri pamutemo",
    "填写整数" => "Zadza nhamba",
    "修改属性" => "Shandura zvivakwa",
    "修改成功" => "Kugadziridzwa zvakabudirira",
    "修改失败" => "kutadza kugadzirisa",
    "批量还原" => "Batch kudzorera",
    "还原文章" => "Dzorera chinyorwa",
    "还原成功" => "Kudzoreredza kwakabudirira",
    "还原失败" => "Kudzoreredza kwakundikana",
    "删除文章" => "Delete article",
    "删除成功" => "zvakadzimwa zvinobudirira",
    "删除失败" => "atadza kudzima",
    "全部展开" => "Wedzera zvese",
    "全部折叠" => "Koroka zvese",
    "添加下级" => "Wedzera pasi",
    "编辑菜单" => "Rongedza menyu",
    "菜单名称" => "Menu name",
    "父级菜单" => "Menyu yevabereki",
    "顶级菜单" => "top menu",
    "创建菜单" => "Gadzira menyu",
    "删除菜单" => "delete menyu",
    "非法访问" => "Kupinda kusina mvumo",
    "拒绝访问" => "kupinda kwarambwa",
    "没有权限" => "mvumo yaramba",
    "彻底删除" => "Bvisa zvachose",
    "批量待审" => "Batch rakamirira kuongororwa",
    "批量审核" => "Batch wongororo",
    "垃圾评论" => "Spam comments",
    "分类名称" => "Category Name",
    "分类列表" => "Category List",
    "父级分类" => "Chikamu chevabereki",
    "顶级分类" => "Top classification",
    "分类重名" => "Categories ane mazita akapetwa",
    "创建分类" => "Gadzira zvikamu",
    "编辑分类" => "Rongedza chikamu",
    "删除分类" => "Delete category",
    "评论作者" => "Ongorora munyori",
    "评论内容" => "comments",
    "评论列表" => "comment list",
    "评论文章" => "Ongorora zvinyorwa",
    "回复内容" => "Pindura zvirimo",
    "回复评论" => "Pindura kuti utaure",
    "编辑评论" => "Mhinduro dzemupepeti",
    "待审评论" => "Mhinduro dzakamirira",
    "待审成功" => "Takamirira kutongwa zvabudirira",
    "待审失败" => "Kutadza kumirira kutongwa",
    "审核成功" => "Wongororo yabudirira",
    "审核失败" => "Audit kutadza",
    "删除评论" => "Delete comment",
    "谷歌翻译" => "Shandurudzo google",
    "检测语言" => "Ziva mutauro",
    "别名错误" => "Alias ​​kukanganisa",
    "语言错误" => "Kukanganisa kwemutauro",
    "标签名称" => "Tag name",
    "标签列表" => "tag list",
    "标签重名" => "Tags ane duplicate mazita",
    "页面列表" => "Peji list",
    "页面标题" => "musoro wepeji",
    "父级页面" => "peji remubereki",
    "顶级页面" => "peji yepamusoro",
    "清除缓存" => "bvisa cache",
    "当前版本" => "shanduro yazvino",
    "重置系统" => "Reset system",
    "最新评论" => "latest comment",
    "联系我们" => "taura nesu",
    "数据统计" => "Statistics",
    "网站名称" => "Zita rewebhusaiti",
    "网站地址" => "website address",
    "链接列表" => "Linked list",
    "创建链接" => "Gadzira chinongedzo",
    "编辑链接" => "Edit link",
    "删除链接" => "Bvisa chinongedzo",
    "日志标题" => "Log title",
    "消息内容" => "Message content",
    "请求方法" => "Kukumbira nzira",
    "请求路径" => "Kumbira nzira",
    "日志列表" => "Log list",
    "上传成功" => "Kurodha kwabudirira",
    "媒体列表" => "media list",
    "开始上传" => "Tanga kurodha",
    "等待上传" => "Kumirira kuiswa",
    "重新上传" => "re-upload",
    "上传完成" => "upload yapera",
    "系统保留" => "System reserved",
    "发现重复" => "Duplicate yawanikwa",
    "上传文件" => "upload mafaira",
    "个在用，" => "Imwe iri kushandiswa,",
    "文件重名" => "Duplicate mazita emafaira",
    "删除文件" => "Delete Files",
    "创建页面" => "Gadzira peji",
    "编辑页面" => "Rongedza peji",
    "删除页面" => "delete peji",
    "角色列表" => "role list",
    "角色名称" => "Zita rebasa",
    "权限分配" => "Mvumo yekupihwa",
    "创建角色" => "Kugadzira Basa",
    "编辑角色" => "Rongedza basa",
    "删除角色" => "Delete basa",
    "基础设置" => "Basic Settings",
    "高级设置" => "advanced settings",
    "其他设置" => "mamwe marongero",
    "上传类型" => "Upload type",
    "限制大小" => "Muganhu saizi",
    "默认最大" => "Default maximum",
    "单点登录" => "nyorera mu",
    "版权信息" => "Copyright Information",
    "链接地址" => "link address",
    "热门文章" => "zvinyorwa zvakakurumbira",
    "首页评论" => "Maonero epeji yekumba",
    "内容相关" => "Zvemukati zvinoenderana",
    "图片分页" => "Kupeja mifananidzo",
    "友情链接" => "Links",
    "语音朗读" => "Kuverenga nenzwi",
    "评论登录" => "Comment login",
    "评论待审" => "Comment zvakamirira",
    "保存成功" => "Yakachengetedzwa zvinobudirira",
    "创建标签" => "Gadzira ma tags",
    "编辑标签" => "Edit tag",
    "删除标签" => "Delete tag",
    "用户列表" => "user list",
    "注册时间" => "Nguva yekunyoresa",
    "首次登陆" => "Kekutanga kupinda",
    "最后登陆" => "last login",
    "语言权限" => "Mvumo yemutauro",
    "创建用户" => "Gadzira mushandisi",
    "不能修改" => "Haikwanise kugadziridzwa",
    "编辑用户" => "Rongedza mushandisi",
    "删除用户" => "bvisa vashandisi",
    "最新文章" => "zvinyorwa zvitsva",
    "搜索结果" => "tsvaga mhinduro",
    "选择语言" => "Sarudza mutauro",
    "分类为空" => "Chikamu hachina chinhu",
    "收藏文章" => "Unganidza zvinyorwa",
    "收藏成功" => "Kuunganidza kwabudirira",
    "取消收藏" => "Cancel favourites",
    "取消回复" => "Kanzura mhinduro",
    "发表评论" => "Comment",
    "语音播放" => "Kutamba nenzwi",
    "回到顶部" => "kudzokera kumusoro",
    "分类目录" => "Categories",
    "下载内容" => "Dhaunirodha zvirimo",
    "相关文章" => "zvinoenderana nenyaya",
    "媒体合成" => "media synthesis",
    "语音合成" => "synthesis yekutaura",
    "前端登录" => "Pamberi pekupedzisira login",
    "前端注册" => "Kunyoresa kumberi",
    "前端登出" => "Frontend logout",
    "后台首页" => "Background Kumba",
    "欢迎页面" => "peji rekugamuchira",
    "权限管理" => "hutungamiri hwehutungamiri",
    "用户管理" => "User Management",
    "角色管理" => "basa rekutungamirira",
    "权限菜单" => "Mvumo menyu",
    "分类管理" => "Classification management",
    "标签管理" => "tag management",
    "文章管理" => "Kutarisira zvinyorwa",
    "页面管理" => "Page management",
    "媒体管理" => "media management",
    "上传接口" => "Upload interface",
    "链接管理" => "Link management",
    "评论管理" => "Comment management",
    "审核通过" => "kuongororwa kwakapfuura",
    "机器评论" => "ongororo yemuchina",
    "系统管理" => "System Management",
    "系统设置" => "Zvirongwa zveSystem",
    "保存设置" => "Sevha Zvirongwa",
    "恢复出厂" => "Dzorera Factory",
    "操作日志" => "Operation log",
    "删除日志" => "Delete log",
    "科西嘉语" => "Kosikeni",
    "瓜拉尼语" => "Guaraní",
    "卢旺达语" => "Kinyarwanda",
    "约鲁巴语" => "yoruba",
    "尼泊尔语" => "ChiNepali",
    "夏威夷语" => "Chihawayi",
    "意第绪语" => "Yiddish",
    "爱尔兰语" => "ChiIrish",
    "希伯来语" => "ChiHebhuru",
    "卡纳达语" => "ChiKannada",
    "匈牙利语" => "ChiHungarian",
    "泰米尔语" => "ChiTamil",
    "阿拉伯语" => "ChiArabic",
    "孟加拉语" => "ChiBengali",
    "萨摩亚语" => "Samoan",
    "班巴拉语" => "Bambara",
    "立陶宛语" => "Ritunia",
    "马耳他语" => "ChiMaltese",
    "土库曼语" => "ChiTeki",
    "阿萨姆语" => "Assamese",
    "僧伽罗语" => "sinhala",
    "乌克兰语" => "Ukrainian",
    "威尔士语" => "Welsh",
    "菲律宾语" => "Filipino",
    "艾马拉语" => "Aymara",
    "泰卢固语" => "ChiTelugu",
    "多格来语" => "dogglai",
    "迈蒂利语" => "Maithili",
    "普什图语" => "Pashito",
    "迪维希语" => "Dhivehi",
    "卢森堡语" => "Rukusembogi",
    "土耳其语" => "Turkish",
    "马其顿语" => "ChiMacedonian",
    "卢干达语" => "Luganda",
    "马拉地语" => "ChiMarati",
    "乌尔都语" => "Urdu",
    "葡萄牙语" => "ChiPutukezi",
    "奥罗莫语" => "Oromo",
    "西班牙语" => "ChiSpanish",
    "弗里西语" => "Frisian",
    "索马里语" => "Somari",
    "齐切瓦语" => "Chichewa",
    "旁遮普语" => "Punjabi",
    "巴斯克语" => "ChiBasque",
    "意大利语" => "ChiItalian",
    "塔吉克语" => "Tajik",
    "克丘亚语" => "Quechua",
    "奥利亚语" => "ChiOrya",
    "哈萨克语" => "Kazaki",
    "林格拉语" => "Lingala",
    "塞佩蒂语" => "Sepeti",
    "塞索托语" => "Sesotho",
    "维吾尔语" => "Uighur",
    "ICP No.001" => "ICP No.001",
    "用户名" => "username",
    "验证码" => "Verification code",
    "记住我" => "ndirangarire",
    "手机号" => "Nhamba yenhare",
    "请输入" => "pinda hako",
    "请选择" => "ndapota sarudza",
    "浏览量" => "Views",
    "回收站" => "recycle bin",
    "菜单树" => "menu tree",
    "控制器" => "controller",
    "第一页" => "Peji yekutanga",
    "最后页" => "peji yekupedzisira",
    "筛选列" => "Sefa mbiru",
    "删除行" => "Delete row",
    "还原行" => "Bvisa mutsara",
    "重命名" => "Rename",
    "国际化" => "kudyidzana kwenyika dzepasi pose",
    "文章数" => "Nhamba yezvinyorwa",
    "机器人" => "robhoti",
    "星期日" => "Svondo",
    "星期一" => "Muvhuro",
    "星期二" => "Chipiri",
    "星期三" => "Chitatu",
    "星期四" => "China",
    "星期五" => "Chishanu",
    "星期六" => "Mugovera",
    "会员数" => "Nhamba yenhengo",
    "评论数" => "Nhamba yemhinduro",
    "文件名" => "zita refaira",
    "音视频" => "Audio uye vhidhiyo",
    "扩展名" => "zita rekuwedzera",
    "未使用" => "Zvisina kushandiswa",
    "个无效" => "invalid",
    "共删除" => "Zvose zvadzimwa",
    "个文件" => "mafaira",
    "备案号" => "nhamba yemhosva",
    "轮播图" => "carousel",
    "条显示" => "bar kuratidza",
    "上一页" => "Peji yapfuura",
    "下一页" => "Peji inotevera",
    "未分类" => "uncategorized",
    "空标签" => "isina chinhu",
    "无标题" => "Untitled",
    "控制台" => "console",
    "登录中" => "kupinda mukati",
    "注册中" => "Kunyoresa",
    "跳转中" => "Redirecting",
    "提交中" => "kuendesa",
    "审核中" => "iri kuongororwa",
    "请稍后" => "chimbomirai henyu",
    "篇文章" => "Article",
    "客户端" => "client",
    "没有了" => "hakuna akasara",
    "后评论" => "post comments",
    "未命名" => "asina zita",
    "软删除" => "soft delete",
    "语言包" => "Mutauro paki",
    "豪萨语" => "ChiHausa",
    "挪威语" => "ChiNorwegian",
    "贡根语" => "gonggan mutauro",
    "拉丁语" => "Ratini",
    "捷克语" => "ChiCzech",
    "波斯语" => "ChiPersian",
    "印地语" => "ChiHindi",
    "冰岛语" => "ChiIcelandic",
    "契维语" => "Twi",
    "高棉语" => "Cambodian",
    "丹麦语" => "ChiDanish",
    "修纳语" => "Shona",
    "越南语" => "Vietnamese",
    "宿务语" => "Cebuano",
    "波兰语" => "ChiPolish",
    "鞑靼语" => "Tatar",
    "老挝语" => "Layo",
    "瑞典语" => "ChiSwedish",
    "缅甸语" => "ChiBurmese",
    "信德语" => "ChiSindhi",
    "马来语" => "ChiMalay",
    "伊博语" => "Igbo",
    "希腊语" => "ChiGiriki",
    "芬兰语" => "ChiFinish",
    "埃维语" => "Ewe",
    "毛利语" => "Maori",
    "荷兰语" => "ChiDutch",
    "蒙古语" => "ChiMongoria",
    "米佐语" => "Mizo",
    "世界语" => "Esiperando",
    "印尼语" => "ChiIndonesian",
    "宗加语" => "Dzonga",
    "密码" => "pasiwedhi",
    "登录" => "Log in",
    "重置" => "reset",
    "信息" => "ruzivo",
    "成功" => "kubudirira",
    "失败" => "kukundikana",
    "昵称" => "Nick name",
    "选填" => "Optional",
    "注册" => "rejista",
    "错误" => "kukanganisa",
    "头像" => "avatar",
    "上传" => "upload",
    "保存" => "save",
    "标题" => "title",
    "别名" => "Alias",
    "描述" => "tsanangura",
    "封面" => "cover",
    "分类" => "Classification",
    "状态" => "state",
    "正常" => "normal",
    "禁用" => "Disable",
    "作者" => "munyori",
    "日期" => "date",
    "添加" => "Wedzera kune",
    "刷新" => "zorodza",
    "标签" => "Label",
    "浏览" => "Browse",
    "点赞" => "kufanana",
    "评论" => "Comment",
    "操作" => "shanda",
    "阅读" => "verenga",
    "编辑" => "edit",
    "删除" => "delete",
    "翻译" => "shandura",
    "内容" => "content",
    "分割" => "segmentation",
    "建议" => "zano",
    "非法" => "zvisiri pamutemo",
    "还原" => "kuderedza",
    "方法" => "nzira",
    "排序" => "sort",
    "图标" => "icon",
    "菜单" => "menyu",
    "时间" => "nguva",
    "匿名" => "anonymous",
    "确定" => "Chokwadi",
    "取消" => "Kanzura",
    "跳转" => "Jump",
    "页码" => "peji nhamba",
    "共计" => "zvachose",
    "每页" => "papeji",
    "导出" => "Export",
    "打印" => "Dhinda",
    "回复" => "pindura",
    "路径" => "nzira",
    "预览" => "Preview",
    "名称" => "zita",
    "行数" => "Mitsara",
    "相册" => "photo album",
    "文章" => "article",
    "待审" => "Pending",
    "通过" => "pass",
    "垃圾" => "Rubbish",
    "提取" => "extract",
    "生成" => "gadzira",
    "定位" => "chinzvimbo",
    "填充" => "kuzadza",
    "帮助" => "help",
    "返回" => "return",
    "语言" => "mutauro",
    "宽屏" => "widescreen",
    "普通" => "zvakajairika",
    "后台" => "Backstage",
    "前台" => "tafura yepamberi",
    "登出" => "Buda",
    "版本" => "Version",
    "关于" => "nezve",
    "微信" => "WeChat",
    "会员" => "nhengo",
    "地址" => "kero",
    "类型" => "type",
    "图片" => "mufananidzo",
    "文件" => "document",
    "失效" => "Hazvina basa",
    "大小" => "saizi",
    "下载" => "download",
    "导航" => "navigation",
    "站标" => "Site logo",
    "单位" => "unit",
    "加速" => "accelerate",
    "角色" => "Basa",
    "首页" => "peji Rekutanga",
    "个数" => "nhamba",
    "我的" => "yangu",
    "说道" => "akadaro",
    "手机" => "nharembozha",
    "可选" => "Optional",
    "搜索" => "tsvaga",
    "页面" => "peji",
    "查看" => "Check",
    "创建" => "gadzira",
    "更新" => "vandudza",
    "英语" => "Chirungu",
    "法语" => "ChiFrench",
    "俄语" => "ChiRussian",
    "梵语" => "Sanzikiriti",
    "日语" => "ChiJapanese",
    "泰语" => "ChiThai",
    "苗语" => "ChiHmong",
    "德语" => "ChiJerimani",
    "韩语" => "ChiKorean",
    "请" => "ndapota",
];
