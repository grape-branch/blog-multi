<?php
/**
 * @author admin
 * @date 2024-06-05 12:44:57
 * @desc 匈牙利语语言包
 */
return [
    "迷你mvc小框架，一套用于开发各类管理系统的通用型后台管理系统模板，基于开源免费的 layui 的制作，内置各类实际业务场景下相对丰富的示例。" => "A Mini MVC egy kis keretrendszer, univerzális háttérkezelő rendszer sablonok készlete különböző felügyeleti rendszerek fejlesztéséhez. Nyílt forráskódú és ingyenes layui-on alapul, és viszonylag gazdag példákkal rendelkezik a különféle tényleges üzleti forgatókönyvekben.",
    "除非特别说明，本博客均为原创，如需转载，请以链接形式标明来源出处。" => "Ha nincs másképp jelezve, ez a blog eredeti.",
    "重置系统，将删除用户所有数据和附件，恢复到出厂设置。" => "A rendszer visszaállítása törli az összes felhasználói adatot és mellékletet, és visszaállítja a gyári beállításokat.",
    "用户名只能英文字母数字下划线或中划线，位数" => "A felhasználónév csak angol betűket, számokat, alá- vagy aláhúzásjeleket, valamint a számjegyek számát tartalmazhatja",
    "昵称只能中英文字母数字下划线中划线，位数" => "A becenév csak kínai és angol betűket, számokat, aláhúzásjeleket, aláhúzásjeleket és számjegyeket tartalmazhat.",
    "有道接口不支持，需科学上网使用谷歌接口。" => "A Youdao felület nem támogatott Az internet tudományos eléréséhez a Google felületet kell használnia.",
    "别名只能英文字母数字下划线中划线，位数" => "Az álnevek csak angol betűket, számokat, aláhúzásjeleket, aláhúzásjeleket és számjegyeket tartalmazhatnak.",
    "计算孤立文件需要较长时间，确定继续吗？" => "Sok időt vesz igénybe az árva fájlok kiszámítása. Biztosan folytatja?",
    "语音不支持，需科学上网使用谷歌接口。" => "A hang nem támogatott, ezért az internet tudományos eléréséhez a Google felületét kell használnia.",
    "您的账号已在别处登录，请重新登录！" => "Fiókja máshol be van jelentkezve, kérjük, jelentkezzen be újra!",
    "你的账号已被禁用，请联系管理员" => "Fiókját letiltottuk, kérjük, forduljon a rendszergazdához",
    "文件有在使用中，请重新计算状态" => "A fájl használatban van, kérjük, számítsa újra az állapotot",
    "目标语言权限不够，请联系管理员" => "Nem megfelelő célnyelvi engedélyek, forduljon a rendszergazdához",
    "真的标记垃圾评论选中的行吗？" => "Tényleg spamként jelöli meg a kiválasztott sort?",
    "源语言别名为空，请联系源作者" => "A forrásnyelvi álnév üres, kérjük, forduljon a forrás szerzőjéhez",
    "点击上传，或将文件拖拽到此处" => "Kattintson a feltöltéshez, vagy húzza ide a fájlt",
    "合成成功！再次点击按钮下载。" => "A szintézis sikeres! A letöltéshez kattintson újra a gombra.",
    "没有语言权限，请联系管理员" => "Nincs nyelvi engedély, forduljon a rendszergazdához",
    "源语言别名为空，不能国际化" => "A forrásnyelvi álnév üres, és nem nemzetköziesíthető.",
    "源语言与目标语言都不能为空" => "Sem a forrásnyelv, sem a célnyelv nem lehet üres.",
    "源语言与目标语言数量不一致" => "A forrás- és célnyelvek száma inkonzisztens",
    "删除文件失败，文件有在使用" => "Nem sikerült törölni a fájlt, a fájl használatban van",
    "角色不存在，请联系管理员" => "A szerepkör nem létezik, forduljon a rendszergazdához",
    "角色被禁用，请联系管理员" => "A szerepkör le van tiltva, forduljon a rendszergazdához",
    "功能不存在，请联系管理员" => "A funkció nem létezik, forduljon a rendszergazdához",
    "功能被禁用，请联系管理员" => "A funkció le van tiltva, forduljon a rendszergazdához",
    "真的彻底删除选中的行吗？" => "Tényleg teljesen törli a kijelölt sort?",
    "真的审核通过选中的行吗？" => "Valóban jóváhagyták a kiválasztott sorokat?",
    "目标语言与源语言不能相同" => "A célnyelv nem lehet azonos a forrásnyelvvel",
    "提取语言包与源语言不一致" => "A kibontott nyelvi csomag nem egyeztethető össze a forrásnyelvvel",
    "文件有在使用，删除失败！" => "A fájl használatban van, és a törlés nem sikerült!",
    "没有权限，请联系管理员" => "Nincs engedélye, forduljon a rendszergazdához",
    "真的待审核选中的行吗？" => "Tényleg egy sor, amit át kell tekinteni és ki kell választani?",
    "目标语言包有空行，行数" => "A célnyelvi csomagban üres sorok vannak, a sorok száma",
    "此操作恢复到出厂设置？" => "Ez a művelet visszaállítja a gyári beállításokat?",
    "朗读错误，请联网后再试" => "Olvasási hiba. Kérjük, csatlakozzon az internethez, és próbálja újra",
    "还没有添加分类描述信息" => "A kategória leírására vonatkozó információk még nincsenek hozzáadva",
    "本地媒体已失效或不存在" => "A helyi adathordozó érvénytelen vagy nem létezik",
    "库尔德语（库尔曼吉语）" => "kurd (kurmandzsi)",
    "作者名未注册或被禁用" => "A szerző neve nincs regisztrálva vagy letiltva",
    "真的删除选中的行吗？" => "Tényleg törli a kijelölt sort?",
    "真的还原选中的行吗？" => "Valóban visszaállítja a kijelölt sorokat?",
    "真的删除行或子行么？" => "Valóban törölni szeretne sorokat vagy részsorokat?",
    "目标语言包未发现空行" => "Az üres sor nem található a célnyelvi csomagban",
    "该语言不支持语音朗读" => "Ez a nyelv nem támogatja a hangolvasást",
    "语言包数据未发生改变" => "A nyelvi csomag adatai nem változtak",
    "欢迎使用后台管理系统" => "Üdvözöljük a háttérkezelő rendszerben",
    "文件有在使用或已失效" => "A fájl használatban van vagy lejárt",
    "合成失败，请稍后再试" => "A szintézis nem sikerült, próbálkozzon újra később",
    "&copy; 2021-2023 Company, Inc." => "© 2021-2023 Company, Inc.",
    "梅泰语（曼尼普尔语）" => "meitei (manipuri)",
    "两次密码输入不一致" => "A két jelszóbevitel nem konzisztens",
    "该用户密码不可修改" => "Ez a felhasználói jelszó nem módosítható",
    "添加文章时创建标签" => "Hozzon létre címkéket cikkek hozzáadásakor",
    "删除文章时删除评论" => "A megjegyzések törlése cikk törlésekor",
    "控制器与方法已存在" => "A vezérlő és a módszer már létezik",
    "标题或名称不能为空" => "A cím vagy a név nem lehet üres",
    "请选择一种语言朗读" => "Kérjük, válasszon egy nyelvet a felolvasáshoz",
    "分类有文章不能删除" => "A kategóriában vannak olyan cikkek, amelyeket nem lehet törölni",
    "审核为垃圾评论成功" => "Sikeresen ellenőrizve spamként",
    "审核为垃圾评论失败" => "A spam jellegű megjegyzés ellenőrzése nem sikerült",
    "确定要登出站点吗？" => "Biztos, hogy ki akar jelentkezni az oldalról?",
    "网站名称或地址为空" => "A webhely neve vagy címe üres",
    "网站名称或地址重名" => "Ismétlődő webhelynév vagy cím",
    "允许上传的文件类型" => "Feltölthető fájltípusok",
    "标签有文章不能删除" => "A címkéket tartalmazó cikkek nem törölhetők",
    "人觉得这篇文章很赞" => "Az emberek szerint ez a cikk nagyszerű",
    "还没有页面描述信息" => "Az oldalleírásról még nincs információ",
    "回复与评论内容重复" => "A válasz és a megjegyzés ismétlődő tartalma",
    "失败！请稍后再试。" => "sikertelen! Kérlek, próbáld újra később.",
    "主耶稣基督里的教会" => "az egyház az Úr Jézus Krisztusban",
    "库尔德语（索拉尼）" => "kurd (sorani)",
    "布尔语(南非荷兰语)" => "logikai (afrikaans)",
    "用户名或密码错误" => "Rossz felhasználónév vagy jelszó",
    "源语言必须是中文" => "A forrásnyelvnek kínainak kell lennie",
    "目标语言别名为空" => "A célnyelvi alias üres",
    "国际化文章时标签" => "Nemzetközivé tett cikkcímkék",
    "确定清除缓存吗？" => "Biztosan törli a gyorsítótárat?",
    "超过的单文件大小" => "Egyetlen fájl mérete túllépve",
    "超过前端表单限制" => "A kezelőfelület űrlapkorlátja túllépve",
    "目标没有写入权限" => "A célnak nincs írási engedélye",
    "不允许的上传类型" => "Nem engedélyezett feltöltési típusok",
    "文件大小不能超过" => "A fájl mérete nem haladhatja meg",
    "保存基础设置成功" => "Az alapvető beállítások sikeresen elmentve",
    "首次基础设置成功" => "Az első alapbeállítás sikerült",
    "正在合成，请稍后" => "Szintetizálás folyamatban, kérem, várjon",
    "不合理的请求方法" => "Indokolatlan kérési mód",
    "Session无效或过期" => "A munkamenet érvénytelen vagy lejárt",
    "手机号码不正确" => "a telefonszám helytelen",
    "手机号码已存在" => "A mobilszám már létezik",
    "标题或内容为空" => "A cím vagy a tartalom üres",
    "创建文章时标签" => "Címkék cikk létrehozásakor",
    "编辑文章时标签" => "Címkék a cikk szerkesztésekor",
    "真的删除行么？" => "Valóban törölni szeretné a sort?",
    "请输入菜单名称" => "Kérjük, adja meg a menü nevét",
    "请先删除子菜单" => "Kérjük, először törölje az almenüt",
    "未登录访问后台" => "Hozzáférés a háttérrendszerhez bejelentkezés nélkül",
    "Cookie无效或过期" => "A cookie érvénytelen vagy lejárt",
    "真的还原行么？" => "Valóban vissza lehet állítani?",
    "编辑器内容为空" => "A szerkesztő tartalma üres",
    "未选择整行文本" => "A szöveg teljes sora nincs kiválasztva",
    "划词选择行错误" => "Szókiválasztó sor hiba",
    "数据源发生改变" => "Adatforrás változásai",
    "填充成功，行号" => "Sikeresen kitöltve, sorszám",
    "请输入分类名称" => "Kérjük, adja meg a kategória nevét",
    "分类名不能为空" => "A kategória neve nem lehet üres",
    "排序只能是数字" => "A rendezés csak numerikus lehet",
    "请先删除子分类" => "Kérjük, először törölje az alkategóriákat",
    "请输入评论作者" => "Kérjük, adja meg a megjegyzés szerzőjét",
    "请选择目标语言" => "Kérjük, válassza ki a célnyelvet",
    "语言包生成成功" => "A nyelvi csomag sikeresen generálva",
    "语言包生成失败" => "A nyelvi csomag létrehozása nem sikerült",
    "国际化分类成功" => "Sikeres nemzetközi osztályozás",
    "国际化分类失败" => "A nemzetközi osztályozás nem sikerült",
    "请输入标签名称" => "Adjon meg egy címkenevet",
    "国际化标签成功" => "Siker a nemzetközi kiadónál",
    "国际化标签失败" => "A nemzetköziesítési címke nem sikerült",
    "国际化文章成功" => "Nemzetközi cikksiker",
    "国际化文章失败" => "Nem sikerült a nemzetközi cikk",
    "请输入页面标题" => "Kérjük, adja meg az oldal címét",
    "国际化页面成功" => "Sikerült a nemzetköziesítési oldal",
    "国际化页面失败" => "A nemzetköziesítési oldal nem sikerült",
    "作者：葡萄枝子" => "Szerző: Szőlőág",
    "请输入网站名称" => "Kérjük, adja meg a webhely nevét",
    "请输入网站地址" => "Kérjük, adja meg a webhely címét",
    "链接名不能为空" => "A link neve nem lehet üres",
    "上传文件不完整" => "A feltöltött fájl hiányos",
    "没有文件被上传" => "Nem töltöttek fel fájlokat",
    "找不到临时目录" => "Az ideiglenes könyvtár nem található",
    "未知的文件类型" => "Ismeretlen fájltípus",
    "文件名不能为空" => "A fájlnév nem lehet üres",
    "个文件有在使用" => "fájlok használatban vannak",
    "请先删除子页面" => "Kérjük, először törölje az aloldalt",
    "请输入角色名称" => "Kérjük, adjon meg egy szerepnevet",
    "管理员不可禁用" => "A rendszergazdák nem tilthatják le",
    "管理员不可删除" => "A rendszergazdák nem törölhetik",
    "请输入限制大小" => "Kérjük, adja meg a limit méretet",
    "请输入版权信息" => "Kérjük, adja meg a szerzői jogi információkat",
    "恢复出厂成功！" => "Sikeres gyári visszaállítás!",
    "恢复出厂失败！" => "A gyári beállítások visszaállítása nem sikerült!",
    "标签名不能为空" => "A címke neve nem lehet üres",
    "还没有内容信息" => "Még nincs tartalmi információ",
    "这篇文章很有用" => "Ez a cikk nagyon hasznos",
    "保存分类国际化" => "Kategória nemzetköziesítésének mentése",
    "分类国际化帮助" => "Osztályozási internacionalizálási segítség",
    "保存标签国际化" => "Mentse el a címke nemzetközivé tételét",
    "标签国际化帮助" => "Címke nemzetköziesítési súgó",
    "保存文章国际化" => "Cikk nemzetközivé tételének mentése",
    "文章国际化帮助" => "Súgó cikk nemzetközivé tételéhez",
    "保存页面国际化" => "Mentse az oldal nemzetközivé tételét",
    "页面国际化帮助" => "Segítség az oldal nemzetközivé tételéhez",
    "海地克里奥尔语" => "Haiti kreol",
    "非法的ajax请求" => "Illegális ajax-kérés",
    "密码至少位数" => "A jelszónak legalább számjegyből kell állnia",
    "验证码不正确" => "Helytelen ellenőrző kód",
    "包含非法参数" => "Illegális paramétereket tartalmaz",
    "请输入用户名" => "kérjük adja meg a felhasználó nevét",
    "用户名已存在" => "Felhasználónév már létezik",
    "请重输入密码" => "Kérjük adja meg újra a jelszavát",
    "图片格式错误" => "Képformátum hiba",
    "修改资料成功" => "Az adatok sikeresen módosítva",
    "没有改变信息" => "Az információ nem változott",
    "请输入浏览量" => "Kérjük, adja meg a megtekintések számát",
    "请输入点赞数" => "Kérjük, adja meg a kedvelések számát",
    "请选择子分类" => "Kérjük, válasszon egy alkategóriát",
    "创建文章成功" => "A cikk sikeresen elkészült",
    "创建文章失败" => "Nem sikerült létrehozni a cikket",
    "编辑文章成功" => "A cikk szerkesztése sikeres volt",
    "标题不能为空" => "A cím nem lehet üres",
    "菜单名称重复" => "Ismétlődő menünév",
    "创建菜单成功" => "A menü sikeresen létrehozva",
    "创建菜单失败" => "Nem sikerült létrehozni a menüt",
    "编辑菜单成功" => "A menü szerkesztése sikeres",
    "请选择行数据" => "Kérjük, válassza ki a sor adatait",
    "计算孤立文件" => "Számolja meg az árva fájlokat",
    "划词选择错误" => "Rossz szóválasztás",
    "请提取语言包" => "Kérjük, bontsa ki a nyelvi csomagot",
    "没有语音文字" => "Nincs hangszöveg",
    "语音朗读完成" => "A hangolvasás befejeződött",
    "分类名称为空" => "A kategória neve üres",
    "创建分类成功" => "Az osztályozás sikeresen létrehozva",
    "创建分类失败" => "Nem sikerült létrehozni a kategóriát",
    "编辑分类成功" => "A kategória szerkesztése sikeres volt",
    "回复评论为空" => "A hozzászólásra adott válasz üres",
    "回复评论成功" => "Sikeres válasz a hozzászóláshoz",
    "回复评论失败" => "Nem sikerült válaszolni a megjegyzésre",
    "评论内容为空" => "A megjegyzés tartalma üres",
    "编辑评论成功" => "A megjegyzés szerkesztése sikeres volt",
    "待审评论成功" => "Az ellenőrzésre váró megjegyzés sikeres",
    "待审评论失败" => "Az ellenőrzésre váró megjegyzés meghiúsult",
    "审核通过评论" => "Jóváhagyott megjegyzések",
    "审核评论成功" => "A felülvizsgálat sikeres",
    "审核评论失败" => "A megjegyzés ellenőrzése sikertelen",
    "删除评论失败" => "Nem sikerült törölni a megjegyzést",
    "请选择源语言" => "Kérjük, válassza ki a forrásnyelvet",
    "别名不可更改" => "Az álnevek nem módosíthatók",
    "标签名称为空" => "A címke neve üres",
    "创建链接成功" => "Link sikeresen létrehozva",
    "创建链接失败" => "Nem sikerült létrehozni a linket",
    "编辑链接成功" => "Sikeres link szerkesztése",
    "网站名称重名" => "Ismétlődő webhelynevek",
    "网站地址重复" => "Ismétlődő webhelycím",
    "图片压缩失败" => "A képtömörítés nem sikerült",
    "移动文件失败" => "Nem sikerült áthelyezni a fájlt",
    "上传文件成功" => "fájl sikeresen feltöltve",
    "上传文件失败" => "A fájl feltöltése sikertelen",
    "共找到文件：" => "Összes talált fájl:",
    "创建页面成功" => "Az oldal sikeresen létrehozva",
    "创建页面失败" => "Az oldal létrehozása nem sikerült",
    "编辑页面成功" => "Az oldal szerkesztése sikeres volt",
    "权限数据错误" => "Engedélyadatok hiba",
    "创建角色成功" => "A szerepkör sikeresen létrehozva",
    "创建角色失败" => "Nem sikerült létrehozni a szerepet",
    "编辑角色成功" => "A szerepkör szerkesztése sikeres volt",
    "游客不可删除" => "A látogatók nem törölhetik",
    "创建标签成功" => "Címke sikeresen létrehozva",
    "创建标签失败" => "Nem sikerült létrehozni a címkét",
    "编辑标签成功" => "A címke szerkesztése sikeres volt",
    "角色数据错误" => "Karakteradat hiba",
    "语言数据错误" => "Nyelvi adathiba",
    "状态数据错误" => "Állapot adat hiba",
    "创建用户成功" => "Felhasználó sikeresen létrehozva",
    "创建用户失败" => "Nem sikerült létrehozni a felhasználót",
    "编辑用户成功" => "Felhasználó szerkesztése sikeres",
    "本文博客网址" => "A cikk blog URL-je",
    "评论内容重复" => "Ismétlődő megjegyzéstartalom",
    "回复内容重复" => "Ismétlődő választartalom",
    "评论发表成功" => "A megjegyzés sikeresen elküldve",
    "发表评论失败" => "Nem sikerült a megjegyzés közzététele",
    "前端删除评论" => "Megjegyzések törlése a kezelőfelületről",
    "中文（简体）" => "kínai (egyszerűsített)",
    "加泰罗尼亚语" => "katalán",
    "苏格兰盖尔语" => "skót gael",
    "中文（繁体）" => "tradicionális kínai)",
    "马拉雅拉姆语" => "Malayalam",
    "斯洛文尼亚语" => "szlovén",
    "阿尔巴尼亚语" => "albán",
    "密码至少5位" => "A jelszónak legalább 5 karakterből kell állnia",
    "你已经登录" => "Ön már bejelentkezett",
    "账号被禁用" => "A fiók le van tiltva",
    "请输入密码" => "Kérjük, írja be a jelszót",
    "留空不修改" => "Hagyja üresen, és ne módosítsa",
    "请输入标题" => "Kérjük, adjon meg egy címet",
    "请输入内容" => "Kérjük, adja meg a tartalmat",
    "多标签半角" => "Több címke félszélességben",
    "关键词建议" => "Kulcsszójavaslatok",
    "请输入作者" => "Kérjük, adja meg a szerzőt",
    "请选择数据" => "Kérjük, válasszon adatokat",
    "软删除文章" => "puha cikk törlése",
    "软删除成功" => "A lágy törlés sikeres",
    "软删除失败" => "A lágy törlés nem sikerült",
    "角色不存在" => "szerep nem létezik",
    "角色被禁用" => "A szerepkör le van tiltva",
    "功能不存在" => "A funkció nem létezik",
    "功能被禁用" => "A funkció le van tiltva",
    "国际化帮助" => "Nemzetközi segítségnyújtás",
    "未选择文本" => "Nincs kiválasztva szöveg",
    "请选择语言" => "Kérjük, válasszon nyelvet",
    "请输入排序" => "Kérjük, adja meg a rendezést",
    "未修改属性" => "tulajdonságai nem változtak",
    "机器人评论" => "Robot vélemények",
    "生成语言包" => "Nyelvi csomag létrehozása",
    "国际化分类" => "Nemzetközi osztályozás",
    "国际化标签" => "nemzetköziesítési címke",
    "国际化文章" => "Nemzetközi cikkek",
    "国际化页面" => "Nemzetközi oldal",
    "服务器环境" => "Szerver környezet",
    "数据库信息" => "Adatbázis információk",
    "服务器时间" => "szerver idő",
    "还没有评论" => "Még nincsenek hozzászólások",
    "还没有数据" => "Még nincsenek adatok",
    "网址不合法" => "Az URL illegális",
    "选择多文件" => "Válasszon ki több fájlt",
    "个未使用，" => "felhasználatlan,",
    "文件不存在" => "a fájl nem létezik",
    "重命名失败" => "Az átnevezés nem sikerült",
    "重命名成功" => "Az átnevezés sikeres",
    "角色已存在" => "A szerepkör már létezik",
    "蜘蛛不索引" => "pók nem indexel",
    "请输入数量" => "Kérjük, adja meg a mennyiséget",
    "昵称已存在" => "A becenév már létezik",
    "页面没找到" => "Az oldal nem található",
    "还没有文章" => "Még nincsenek cikkek",
    "还没有页面" => "Még nincs oldal",
    "还没有分类" => "Még nincs besorolás",
    "还没有标签" => "Még nincsenek címkék",
    "还没有热门" => "Még nem népszerű",
    "还没有更新" => "Még nincs frissítve",
    "还没有网址" => "Még nincs URL",
    "请写点评论" => "Kérjük, írjon megjegyzést",
    "，等待审核" => ",Moderálva",
    "你已经注册" => "már regisztráltál",
    "提取语言包" => "Nyelvi csomag kibontása",
    "分类国际化" => "Osztályozás Nemzetköziesítés",
    "标签国际化" => "Label nemzetközivé",
    "文章国际化" => "Cikk nemzetközivé",
    "页面国际化" => "Oldal nemzetközivé",
    "格鲁吉亚语" => "grúz",
    "博杰普尔语" => "Bhojpuri",
    "白俄罗斯语" => "fehérorosz",
    "斯瓦希里语" => "szuahéli",
    "古吉拉特语" => "gudzsaráti",
    "斯洛伐克语" => "szlovák",
    "阿塞拜疆语" => "azerbajdzsáni",
    "印尼巽他语" => "indonéz szundanai",
    "加利西亚语" => "galíciai",
    "拉脱维亚语" => "lett",
    "罗马尼亚语" => "román",
    "亚美尼亚语" => "örmény",
    "保加利亚语" => "bolgár",
    "爱沙尼亚语" => "észt",
    "阿姆哈拉语" => "amhara",
    "吉尔吉斯语" => "kirgiz",
    "克罗地亚语" => "horvát",
    "波斯尼亚语" => "bosnyák",
    "蒂格尼亚语" => "Tignan",
    "克里奥尔语" => "kreol",
    "马尔加什语" => "madagaszkári",
    "南非科萨语" => "dél-afrikai xhosa",
    "南非祖鲁语" => "Zulu, Dél-Afrika",
    "塞尔维亚语" => "szerb",
    "乌兹别克语" => "üzbég",
    "伊洛卡诺语" => "Ilocano",
    "印尼爪哇语" => "jávai indonéz",
    "回复ID错误" => "Válaszazonosító hiba",
    "非法文章ID" => "Illegális cikkazonosító",
    "管理后台" => "Menedzsment háttér",
    "管理登录" => "Admin bejelentkezés",
    "登录成功" => "sikeres bejelentkezés",
    "切换注册" => "Váltó regisztráció",
    "你已登出" => "Ki vagy jelentkezve",
    "登出成功" => "Sikeres kijelentkezés",
    "用户注册" => "Felhasználó regisztráció",
    "注册成功" => "sikeres regisztráció",
    "注册失败" => "Regisztráció sikertelen",
    "重复密码" => "Jelszó újra",
    "切换登录" => "Válts bejelentkezést",
    "请先登录" => "kérlek, jelentkezz be először",
    "修改资料" => "Módosítsa az információkat",
    "没有改变" => "Nincs változás",
    "不可修改" => "Változhatatlan",
    "上传失败" => "Feltöltés sikertelen",
    "清除成功" => "Sikeres törlés",
    "暂无记录" => "Nincsenek rekordok",
    "批量删除" => "köteg törlése",
    "文章列表" => "Cikk lista",
    "发布时间" => "kiadás ideje",
    "修改时间" => "Változtassa meg az időt",
    "文章标题" => "Cikk címe",
    "标题重名" => "Ismétlődő cím",
    "别名重复" => "Ismétlődő alias",
    "创建文章" => "Cikk létrehozása",
    "编辑文章" => "Cikk szerkesztése",
    "非法字段" => "Illegális mező",
    "填写整数" => "Töltse ki az egész számot",
    "修改属性" => "Tulajdonságok módosítása",
    "修改成功" => "Sikeresen módosítva",
    "修改失败" => "nem sikerül szerkeszteni",
    "批量还原" => "Kötegelt visszaállítás",
    "还原文章" => "Cikk visszaállítása",
    "还原成功" => "Sikeres visszaállítás",
    "还原失败" => "A visszaállítás nem sikerült",
    "删除文章" => "Cikk törlése",
    "删除成功" => "sikeresen törölve",
    "删除失败" => "nem sikerült törölni",
    "全部展开" => "Az összes kibontása",
    "全部折叠" => "Mindet összecsuk",
    "添加下级" => "Beosztott hozzáadása",
    "编辑菜单" => "Szerkesztés menü",
    "菜单名称" => "Menü neve",
    "父级菜单" => "Szülő menü",
    "顶级菜单" => "főmenü",
    "创建菜单" => "Menü létrehozása",
    "删除菜单" => "menü törlése",
    "非法访问" => "Illetéktelen hozzáférés",
    "拒绝访问" => "hozzáférés megtagadva",
    "没有权限" => "hozzáférés megtagadva",
    "彻底删除" => "Távolítsa el teljesen",
    "批量待审" => "A tétel felülvizsgálatra vár",
    "批量审核" => "Batch felülvizsgálat",
    "垃圾评论" => "Spam megjegyzések",
    "分类名称" => "kategória név",
    "分类列表" => "Kategória lista",
    "父级分类" => "Szülő kategória",
    "顶级分类" => "Legjobb besorolás",
    "分类重名" => "Ismétlődő névvel rendelkező kategóriák",
    "创建分类" => "Hozzon létre kategóriákat",
    "编辑分类" => "Kategória szerkesztése",
    "删除分类" => "Kategória törlése",
    "评论作者" => "Vélemény szerzője",
    "评论内容" => "Hozzászólások",
    "评论列表" => "megjegyzéslista",
    "评论文章" => "Cikkek áttekintése",
    "回复内容" => "Válasz tartalma",
    "回复评论" => "Válasz a hozzászólásra",
    "编辑评论" => "Szerkesztői megjegyzések",
    "待审评论" => "Függőben lévő megjegyzések",
    "待审成功" => "Sikeres próbaidőszak",
    "待审失败" => "Sikertelenség tárgyalás alatt",
    "审核成功" => "Az ellenőrzés sikeres",
    "审核失败" => "Ellenőrzési hiba",
    "删除评论" => "Megjegyzés törlése",
    "谷歌翻译" => "Google Fordító",
    "检测语言" => "Nyelvfelismerés",
    "别名错误" => "Alias ​​hiba",
    "语言错误" => "Nyelvi hiba",
    "标签名称" => "Címke neve",
    "标签列表" => "címke lista",
    "标签重名" => "Ismétlődő névvel rendelkező címkék",
    "页面列表" => "Oldallista",
    "页面标题" => "lap cím",
    "父级页面" => "szülőoldal",
    "顶级页面" => "felső oldal",
    "清除缓存" => "gyorsítótár törlése",
    "当前版本" => "jelenlegi verzió",
    "重置系统" => "Rendszer visszaállítása",
    "最新评论" => "legújabb hozzászólás",
    "联系我们" => "lépjen kapcsolatba velünk",
    "数据统计" => "Statisztika",
    "网站名称" => "Webhely neve",
    "网站地址" => "honlap címe",
    "链接列表" => "Linkelt lista",
    "创建链接" => "Link létrehozása",
    "编辑链接" => "Link szerkesztése",
    "删除链接" => "Link eltávolítása",
    "日志标题" => "Napló címe",
    "消息内容" => "Üzenet tartalma",
    "请求方法" => "Kérési mód",
    "请求路径" => "Kérjen elérési utat",
    "日志列表" => "Napló lista",
    "上传成功" => "Feltöltés sikeres",
    "媒体列表" => "média lista",
    "开始上传" => "Indítsa el a feltöltést",
    "等待上传" => "Feltöltésre vár",
    "重新上传" => "töltse fel újra",
    "上传完成" => "feltöltés befejeződött",
    "系统保留" => "Rendszer fenntartva",
    "发现重复" => "Ismétlődés található",
    "上传文件" => "fájlok feltöltése",
    "个在用，" => "Az egyik használatban van,",
    "文件重名" => "Ismétlődő fájlnevek",
    "删除文件" => "Fájlok törlése",
    "创建页面" => "Oldal létrehozása",
    "编辑页面" => "Oldal szerkesztése",
    "删除页面" => "oldal törlése",
    "角色列表" => "szereplista",
    "角色名称" => "Szerep neve",
    "权限分配" => "Engedélykiosztás",
    "创建角色" => "Szerep létrehozása",
    "编辑角色" => "Szerep szerkesztése",
    "删除角色" => "Szerep törlése",
    "基础设置" => "Alapbeállítások",
    "高级设置" => "további beállítások",
    "其他设置" => "egyéb beállitások",
    "上传类型" => "Feltöltés típusa",
    "限制大小" => "Limit méret",
    "默认最大" => "Alapértelmezett maximum",
    "单点登录" => "Bejelentkezés",
    "版权信息" => "Szerzői jogi információk",
    "链接地址" => "link címe",
    "热门文章" => "népszerű cikkek",
    "首页评论" => "A kezdőlap megjegyzései",
    "内容相关" => "Tartalommal kapcsolatos",
    "图片分页" => "Kép lapozás",
    "友情链接" => "Linkek",
    "语音朗读" => "Hangolvasás",
    "评论登录" => "Megjegyzés hozzászólás",
    "评论待审" => "Megjegyzés függőben",
    "保存成功" => "sikeresen elmentve",
    "创建标签" => "Hozzon létre címkéket",
    "编辑标签" => "Címke szerkesztése",
    "删除标签" => "Címke törlése",
    "用户列表" => "felhasználói lista",
    "注册时间" => "Regisztráció ideje",
    "首次登陆" => "Első bejelentkezés",
    "最后登陆" => "utolsó bejelentkezés",
    "语言权限" => "Nyelvi engedélyek",
    "创建用户" => "Felhasználó létrehozása",
    "不能修改" => "Nem módosítható",
    "编辑用户" => "Felhasználó szerkesztése",
    "删除用户" => "törölje a felhasználókat",
    "最新文章" => "Legfrissebb cikkek",
    "搜索结果" => "Keresési eredmények",
    "选择语言" => "Válasszon nyelvet",
    "分类为空" => "A kategória üres",
    "收藏文章" => "Gyűjts cikkeket",
    "收藏成功" => "Sikeres gyűjtés",
    "取消收藏" => "Törölje a kedvenceket",
    "取消回复" => "válasz visszavonása",
    "发表评论" => "Megjegyzés",
    "语音播放" => "Hanglejátszás",
    "回到顶部" => "vissza a tetejére",
    "分类目录" => "Kategóriák",
    "下载内容" => "Tartalom letöltése",
    "相关文章" => "kapcsolódó cikkek",
    "媒体合成" => "médiaszintézis",
    "语音合成" => "beszédszintézis",
    "前端登录" => "Front-end bejelentkezés",
    "前端注册" => "Front-end regisztráció",
    "前端登出" => "Frontend kijelentkezés",
    "后台首页" => "Háttér Home",
    "欢迎页面" => "üdvözlő oldal",
    "权限管理" => "hatósági menedzsment",
    "用户管理" => "Felhasználókezelés",
    "角色管理" => "szerepkezelés",
    "权限菜单" => "Engedélyek menü",
    "分类管理" => "Osztályozás kezelése",
    "标签管理" => "címkekezelés",
    "文章管理" => "Cikkkezelés",
    "页面管理" => "Oldalkezelés",
    "媒体管理" => "médiamenedzsment",
    "上传接口" => "Feltöltési felület",
    "链接管理" => "Linkkezelés",
    "评论管理" => "Kommentkezelés",
    "审核通过" => "sikeres vizsga",
    "机器评论" => "gép áttekintése",
    "系统管理" => "Rendszer menedzsment",
    "系统设置" => "Rendszerbeállítások",
    "保存设置" => "Beállítások mentése",
    "恢复出厂" => "Gyár visszaállítása",
    "操作日志" => "Működési napló",
    "删除日志" => "Napló törlése",
    "科西嘉语" => "korzikai",
    "瓜拉尼语" => "Guaraní",
    "卢旺达语" => "Kinyarwanda",
    "约鲁巴语" => "joruba",
    "尼泊尔语" => "nepáli",
    "夏威夷语" => "hawaii",
    "意第绪语" => "jiddis",
    "爱尔兰语" => "ír",
    "希伯来语" => "héber",
    "卡纳达语" => "kannada",
    "匈牙利语" => "Magyar",
    "泰米尔语" => "tamil",
    "阿拉伯语" => "arab",
    "孟加拉语" => "bengáli",
    "萨摩亚语" => "szamoai",
    "班巴拉语" => "Bambara",
    "立陶宛语" => "litván",
    "马耳他语" => "máltai",
    "土库曼语" => "türkmén",
    "阿萨姆语" => "asszámi",
    "僧伽罗语" => "szinhala",
    "乌克兰语" => "ukrán",
    "威尔士语" => "walesi",
    "菲律宾语" => "filippínó",
    "艾马拉语" => "Aymara",
    "泰卢固语" => "telugu",
    "多格来语" => "dogglai",
    "迈蒂利语" => "Maithili",
    "普什图语" => "pastu",
    "迪维希语" => "Dhivehi",
    "卢森堡语" => "luxemburgi",
    "土耳其语" => "török",
    "马其顿语" => "macedón",
    "卢干达语" => "Luganda",
    "马拉地语" => "marathi",
    "乌尔都语" => "urdu",
    "葡萄牙语" => "portugál",
    "奥罗莫语" => "Oromo",
    "西班牙语" => "spanyol",
    "弗里西语" => "fríz",
    "索马里语" => "szomáliai",
    "齐切瓦语" => "Chichewa",
    "旁遮普语" => "pandzsábi",
    "巴斯克语" => "baszk",
    "意大利语" => "olasz",
    "塔吉克语" => "tadzsik",
    "克丘亚语" => "kecsua",
    "奥利亚语" => "Oriya",
    "哈萨克语" => "kazah",
    "林格拉语" => "Lingala",
    "塞佩蒂语" => "Sepeti",
    "塞索托语" => "Sesotho",
    "维吾尔语" => "ujgur",
    "ICP No.001" => "ICP No.001",
    "用户名" => "felhasználónév",
    "验证码" => "Ellenőrző kód",
    "记住我" => "Emlékezz rám",
    "手机号" => "Telefonszám",
    "请输入" => "kérlek lépj be",
    "请选择" => "kérlek válassz",
    "浏览量" => "Nézetek",
    "回收站" => "újrahasznosító kuka",
    "菜单树" => "menü fa",
    "控制器" => "vezérlő",
    "第一页" => "Első oldal",
    "最后页" => "utolsó oldal",
    "筛选列" => "Oszlopok szűrése",
    "删除行" => "Sor törlése",
    "还原行" => "Sor visszavonása",
    "重命名" => "Átnevezés",
    "国际化" => "globalizáció",
    "文章数" => "Cikkek száma",
    "机器人" => "robot",
    "星期日" => "vasárnap",
    "星期一" => "hétfő",
    "星期二" => "kedd",
    "星期三" => "szerda",
    "星期四" => "csütörtök",
    "星期五" => "péntek",
    "星期六" => "szombat",
    "会员数" => "A tagok száma",
    "评论数" => "Hozzászólások száma",
    "文件名" => "fájl név",
    "音视频" => "Hang és videó",
    "扩展名" => "kiterjesztés neve",
    "未使用" => "Felhasználatlan",
    "个无效" => "érvénytelen",
    "共删除" => "Összesen törölve",
    "个文件" => "fájlokat",
    "备案号" => "ügyszám",
    "轮播图" => "körhinta",
    "条显示" => "bár kijelző",
    "上一页" => "Előző oldal",
    "下一页" => "Következő oldal",
    "未分类" => "kategorizálatlan",
    "空标签" => "üres címke",
    "无标题" => "Névtelen",
    "控制台" => "konzol",
    "登录中" => "bejelentkezés",
    "注册中" => "Regisztráció",
    "跳转中" => "Átirányítás",
    "提交中" => "benyújtása",
    "审核中" => "Felülvizsgálat alatt",
    "请稍后" => "kérlek várj",
    "篇文章" => "Cikk",
    "客户端" => "ügyfél",
    "没有了" => "egy sem maradt",
    "后评论" => "hozzászólásokat írj",
    "未命名" => "névtelen",
    "软删除" => "lágy törlés",
    "语言包" => "Nyelv csomag",
    "豪萨语" => "Hausa",
    "挪威语" => "norvég",
    "贡根语" => "gonggan nyelv",
    "拉丁语" => "latin",
    "捷克语" => "cseh",
    "波斯语" => "perzsa",
    "印地语" => "hindi",
    "冰岛语" => "izlandi",
    "契维语" => "Twi",
    "高棉语" => "kambodzsai",
    "丹麦语" => "dán",
    "修纳语" => "Shona",
    "越南语" => "vietnami",
    "宿务语" => "Cebuano",
    "波兰语" => "fényesít",
    "鞑靼语" => "tatár",
    "老挝语" => "Lao",
    "瑞典语" => "svéd",
    "缅甸语" => "burmai",
    "信德语" => "sindhi",
    "马来语" => "maláj",
    "伊博语" => "Igbo",
    "希腊语" => "görög",
    "芬兰语" => "finn",
    "埃维语" => "Anyajuh",
    "毛利语" => "maori",
    "荷兰语" => "holland",
    "蒙古语" => "mongol",
    "米佐语" => "Mizo",
    "世界语" => "eszperantó",
    "印尼语" => "indonéz",
    "宗加语" => "Dzonga",
    "密码" => "Jelszó",
    "登录" => "Belépés",
    "重置" => "Visszaállítás",
    "信息" => "információ",
    "成功" => "siker",
    "失败" => "nem sikerül",
    "昵称" => "Nick név",
    "选填" => "Választható",
    "注册" => "Regisztráció",
    "错误" => "hiba",
    "头像" => "avatar",
    "上传" => "feltölteni",
    "保存" => "megment",
    "标题" => "cím",
    "别名" => "Álnév",
    "描述" => "leírni",
    "封面" => "borító",
    "分类" => "Osztályozás",
    "状态" => "állapot",
    "正常" => "Normál",
    "禁用" => "Letiltás",
    "作者" => "szerző",
    "日期" => "dátum",
    "添加" => "Hozzáadás",
    "刷新" => "Frissítés",
    "标签" => "Címke",
    "浏览" => "Tallózás",
    "点赞" => "mint",
    "评论" => "Megjegyzés",
    "操作" => "működtet",
    "阅读" => "olvas",
    "编辑" => "szerkeszteni",
    "删除" => "töröl",
    "翻译" => "fordít",
    "内容" => "tartalom",
    "分割" => "szegmentálás",
    "建议" => "javaslat",
    "非法" => "illegális",
    "还原" => "csökkentés",
    "方法" => "módszer",
    "排序" => "fajta",
    "图标" => "ikon",
    "菜单" => "menü",
    "时间" => "idő",
    "匿名" => "névtelen",
    "确定" => "biztos",
    "取消" => "Megszünteti",
    "跳转" => "Ugrás",
    "页码" => "oldalszám",
    "共计" => "teljes",
    "每页" => "oldalanként",
    "导出" => "Export",
    "打印" => "Nyomtatás",
    "回复" => "válasz",
    "路径" => "pálya",
    "预览" => "Előnézet",
    "名称" => "név",
    "行数" => "Sorok",
    "相册" => "fotóalbum",
    "文章" => "cikk",
    "待审" => "Függőben levő",
    "通过" => "pass",
    "垃圾" => "Szemét",
    "提取" => "kivonat",
    "生成" => "generál",
    "定位" => "pozíció",
    "填充" => "töltő",
    "帮助" => "Segítség",
    "返回" => "Visszatérés",
    "语言" => "nyelv",
    "宽屏" => "szélesvásznú",
    "普通" => "rendes",
    "后台" => "Színfalak mögötti",
    "前台" => "recepció",
    "登出" => "Kijelentkezés",
    "版本" => "Változat",
    "关于" => "ról ről",
    "微信" => "WeChat",
    "会员" => "tag",
    "地址" => "cím",
    "类型" => "típus",
    "图片" => "kép",
    "文件" => "dokumentum",
    "失效" => "Érvénytelen",
    "大小" => "méret",
    "下载" => "Letöltés",
    "导航" => "navigáció",
    "站标" => "Webhely logója",
    "单位" => "Mértékegység",
    "加速" => "felgyorsítani",
    "角色" => "Szerep",
    "首页" => "Címlap",
    "个数" => "szám",
    "我的" => "enyém",
    "说道" => "mondott",
    "手机" => "mobiltelefon",
    "可选" => "Választható",
    "搜索" => "keresés",
    "页面" => "oldalon",
    "查看" => "Jelölje be",
    "创建" => "teremt",
    "更新" => "megújítani",
    "英语" => "angol",
    "法语" => "Francia",
    "俄语" => "orosz",
    "梵语" => "szanszkrit",
    "日语" => "japán",
    "泰语" => "thai",
    "苗语" => "hmong",
    "德语" => "német",
    "韩语" => "koreai",
    "请" => "kérem",
];
