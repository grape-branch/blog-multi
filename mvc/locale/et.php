<?php
/**
 * @author admin
 * @date 2024-06-05 12:56:43
 * @desc 爱沙尼亚语语言包
 */
return [
    "迷你mvc小框架，一套用于开发各类管理系统的通用型后台管理系统模板，基于开源免费的 layui 的制作，内置各类实际业务场景下相对丰富的示例。" => "Mini MVC on väike raamistik, universaalsete taustahaldussüsteemide mallide komplekt erinevate haldussüsteemide arendamiseks. See põhineb avatud lähtekoodil ja vabal paigutusel ning sellel on suhteliselt rikkalik näidete komplekt erinevatest tegelikest äristsenaariumitest.",
    "除非特别说明，本博客均为原创，如需转载，请以链接形式标明来源出处。" => "Kui pole märgitud teisiti, on see ajaveeb originaal.",
    "重置系统，将删除用户所有数据和附件，恢复到出厂设置。" => "Süsteemi lähtestamine kustutab kõik kasutajaandmed ja manused ning taastab selle tehaseseadetele.",
    "用户名只能英文字母数字下划线或中划线，位数" => "Kasutajanimi võib sisaldada ainult ingliskeelseid tähti, numbreid, all- või allkriipse ning numbrite arvu",
    "昵称只能中英文字母数字下划线中划线，位数" => "Hüüdnimi võib sisaldada ainult hiina- ja ingliskeelseid tähti, numbreid, allkriipse, alakriipse ja numbreid.",
    "有道接口不支持，需科学上网使用谷歌接口。" => "Youdao liidest ei toetata Interneti teaduslikuks kasutamiseks peate kasutama Google\'i liidest.",
    "别名只能英文字母数字下划线中划线，位数" => "Varjunimed võivad sisaldada ainult ingliskeelseid tähti, numbreid, alakriipse, alakriipse ja numbreid.",
    "计算孤立文件需要较长时间，确定继续吗？" => "Orbfailide arvutamine võtab kaua aega. Kas olete kindel, et soovite jätkata?",
    "语音不支持，需科学上网使用谷歌接口。" => "Häält ei toetata, seega peate Interneti teaduslikuks kasutamiseks kasutama Google\'i liidest.",
    "您的账号已在别处登录，请重新登录！" => "Teie konto on mujal sisse logitud, palun logige uuesti sisse!",
    "你的账号已被禁用，请联系管理员" => "Teie konto on keelatud, võtke ühendust administraatoriga",
    "文件有在使用中，请重新计算状态" => "Fail on kasutusel, palun arvuta olek ümber",
    "目标语言权限不够，请联系管理员" => "Ebapiisavad sihtkeele õigused, võtke ühendust administraatoriga",
    "真的标记垃圾评论选中的行吗？" => "Kas tõesti märgite valitud rea rämpspostiks?",
    "源语言别名为空，请联系源作者" => "Lähtekeele alias on tühi, võtke ühendust allika autoriga",
    "点击上传，或将文件拖拽到此处" => "Klõpsake üleslaadimiseks või lohistage fail siia",
    "合成成功！再次点击按钮下载。" => "Süntees õnnestus! Allalaadimiseks klõpsake nuppu uuesti.",
    "没有语言权限，请联系管理员" => "Keeleõigus puudub, võtke ühendust administraatoriga",
    "源语言别名为空，不能国际化" => "Lähtekeele alias on tühi ja seda ei saa rahvusvahelistuda.",
    "源语言与目标语言都不能为空" => "Ei lähtekeel ega sihtkeel ei tohi olla tühjad.",
    "源语言与目标语言数量不一致" => "Lähte- ja sihtkeelte arv on ebaühtlane",
    "删除文件失败，文件有在使用" => "Faili kustutamine ebaõnnestus, fail on kasutusel",
    "角色不存在，请联系管理员" => "Rolli pole olemas, võtke ühendust administraatoriga",
    "角色被禁用，请联系管理员" => "Roll on keelatud, võtke ühendust administraatoriga",
    "功能不存在，请联系管理员" => "Funktsioon puudub, võtke ühendust administraatoriga",
    "功能被禁用，请联系管理员" => "Funktsioon on keelatud, võtke ühendust administraatoriga",
    "真的彻底删除选中的行吗？" => "Kas see tõesti kustutab valitud rea täielikult?",
    "真的审核通过选中的行吗？" => "Kas valitud read on tõesti heaks kiidetud?",
    "目标语言与源语言不能相同" => "Sihtkeel ei saa olla sama mis lähtekeel",
    "提取语言包与源语言不一致" => "Ekstraheeritud keelepakett ei ole lähtekeelega kooskõlas",
    "文件有在使用，删除失败！" => "Fail on kasutusel ja kustutamine ebaõnnestus!",
    "没有权限，请联系管理员" => "Luba pole, võtke ühendust administraatoriga",
    "真的待审核选中的行吗？" => "Kas see on tõesti rida, mis tuleb üle vaadata ja välja valida?",
    "目标语言包有空行，行数" => "Sihtkeele paketis on tühjad read, ridade arv",
    "此操作恢复到出厂设置？" => "Kas see toiming taastab tehaseseaded?",
    "朗读错误，请联网后再试" => "Lugemisviga, looge Interneti-ühendus ja proovige uuesti",
    "还没有添加分类描述信息" => "Kategooriakirjelduse teavet pole veel lisatud",
    "本地媒体已失效或不存在" => "Kohalik meedia on kehtetu või seda pole olemas",
    "库尔德语（库尔曼吉语）" => "kurdi (kurmanji)",
    "作者名未注册或被禁用" => "Autori nimi pole registreeritud ega keelatud",
    "真的删除选中的行吗？" => "Kas tõesti kustutate valitud rea?",
    "真的还原选中的行吗？" => "Kas tõesti taastate valitud read?",
    "真的删除行或子行么？" => "Kas soovite tõesti ridu või alamridu kustutada?",
    "目标语言包未发现空行" => "Sihtkeelepaketis ei leitud tühja rida",
    "该语言不支持语音朗读" => "See keel ei toeta häällugemist",
    "语言包数据未发生改变" => "Keelepaketi andmed ei ole muutunud",
    "欢迎使用后台管理系统" => "Tere tulemast taustahaldussüsteemi",
    "文件有在使用或已失效" => "Fail on kasutusel või aegunud",
    "合成失败，请稍后再试" => "Süntees ebaõnnestus, proovige hiljem uuesti",
    "&copy; 2021-2023 Company, Inc." => "© 2021–2023 Company, Inc.",
    "梅泰语（曼尼普尔语）" => "meitei (manipuri)",
    "两次密码输入不一致" => "Kaks paroolisisendit on vastuolulised",
    "该用户密码不可修改" => "Seda kasutaja parooli ei saa muuta",
    "添加文章时创建标签" => "Looge artiklite lisamisel silte",
    "删除文章时删除评论" => "Kustutage artiklit kustutades kommentaarid",
    "控制器与方法已存在" => "Kontroller ja meetod on juba olemas",
    "标题或名称不能为空" => "Pealkiri või nimi ei tohi olla tühi",
    "请选择一种语言朗读" => "Valige ettelugemiseks keel",
    "分类有文章不能删除" => "Selles kategoorias on artikleid, mida ei saa kustutada",
    "审核为垃圾评论成功" => "Rämpspostina üle vaadatud",
    "审核为垃圾评论失败" => "Rämpspostikommentaari ülevaatamine ebaõnnestus",
    "确定要登出站点吗？" => "Kas olete kindel, et soovite saidilt välja logida?",
    "网站名称或地址为空" => "Veebisaidi nimi või aadress on tühi",
    "网站名称或地址重名" => "Veebisaidi nime või aadressi duplikaat",
    "允许上传的文件类型" => "Failitüübid on lubatud üles laadida",
    "标签有文章不能删除" => "Märgenditega artikleid ei saa kustutada",
    "人觉得这篇文章很赞" => "Inimesed arvavad, et see artikkel on suurepärane",
    "还没有页面描述信息" => "Lehe kirjelduse teave puudub",
    "回复与评论内容重复" => "Vastuse ja kommentaari dubleeritud sisu",
    "失败！请稍后再试。" => "ebaõnnestuda! Palun proovi hiljem uuesti.",
    "主耶稣基督里的教会" => "kirik isandas Jeesus Kristuses",
    "库尔德语（索拉尼）" => "kurdi (sorani)",
    "布尔语(南非荷兰语)" => "Boole (afrikaani)",
    "用户名或密码错误" => "Vale kasutajanimi või parool",
    "源语言必须是中文" => "Lähtekeel peab olema hiina keel",
    "目标语言别名为空" => "Sihtkeele alias on tühi",
    "国际化文章时标签" => "Rahvusvaheliste artiklite sildid",
    "确定清除缓存吗？" => "Kas olete kindel, et soovite vahemälu tühjendada?",
    "超过的单文件大小" => "Üksiku faili suurus on ületatud",
    "超过前端表单限制" => "Esiliidese vormi limiit on ületatud",
    "目标没有写入权限" => "Sihtmärgil pole kirjutamisõigust",
    "不允许的上传类型" => "Pole lubatud üleslaadimise tüübid",
    "文件大小不能超过" => "Faili suurus ei tohi ületada",
    "保存基础设置成功" => "Põhiseaded salvestati edukalt",
    "首次基础设置成功" => "Esimene põhiseade õnnestus",
    "正在合成，请稍后" => "Sünteesimine, palun oodake",
    "不合理的请求方法" => "Ebamõistlik taotlemisviis",
    "Session无效或过期" => "Seanss on kehtetu või aegunud",
    "手机号码不正确" => "telefoninumber on vale",
    "手机号码已存在" => "Mobiilinumber on juba olemas",
    "标题或内容为空" => "Pealkiri või sisu on tühi",
    "创建文章时标签" => "Sildid artikli loomisel",
    "编辑文章时标签" => "Sildid artikli redigeerimisel",
    "真的删除行么？" => "Kas soovite tõesti rea kustutada?",
    "请输入菜单名称" => "Sisestage menüü nimi",
    "请先删除子菜单" => "Palun kustuta esmalt alammenüü",
    "未登录访问后台" => "Juurdepääs taustaprogrammile sisse logimata",
    "Cookie无效或过期" => "Küpsis on kehtetu või aegunud",
    "真的还原行么？" => "Kas seda on tõesti võimalik taastada?",
    "编辑器内容为空" => "Redigeerija sisu on tühi",
    "未选择整行文本" => "Tervet tekstirida pole valitud",
    "划词选择行错误" => "Sõnavaliku rea viga",
    "数据源发生改变" => "Andmeallika muudatused",
    "填充成功，行号" => "Täidetud edukalt, rea number",
    "请输入分类名称" => "Sisestage kategooria nimi",
    "分类名不能为空" => "Kategooria nime väli ei tohi olla tühi",
    "排序只能是数字" => "Sorteerimine saab olla ainult numbriline",
    "请先删除子分类" => "Palun kustuta esmalt alamkategooriad",
    "请输入评论作者" => "Sisestage kommentaari autor",
    "请选择目标语言" => "Valige sihtkeel",
    "语言包生成成功" => "Keelepaketi genereerimine õnnestus",
    "语言包生成失败" => "Keelepaketi loomine ebaõnnestus",
    "国际化分类成功" => "Rahvusvaheline klassifikatsioon õnnestus",
    "国际化分类失败" => "Rahvusvaheline klassifikatsioon ebaõnnestus",
    "请输入标签名称" => "Sisestage sildi nimi",
    "国际化标签成功" => "Rahvusvahelise märgi edu",
    "国际化标签失败" => "Rahvusvahelistamise silt ebaõnnestus",
    "国际化文章成功" => "Rahvusvaheline artiklite edu",
    "国际化文章失败" => "Rahvusvaheline artikkel ebaõnnestus",
    "请输入页面标题" => "Sisestage lehe pealkiri",
    "国际化页面成功" => "Rahvusvahelistumise leht õnnestus",
    "国际化页面失败" => "Rahvusvahelistamise leht ebaõnnestus",
    "作者：葡萄枝子" => "Autor: Viinamarjade oks",
    "请输入网站名称" => "Sisestage veebisaidi nimi",
    "请输入网站地址" => "Sisestage veebisaidi aadress",
    "链接名不能为空" => "Lingi nimi ei tohi olla tühi",
    "上传文件不完整" => "Üleslaaditud fail on mittetäielik",
    "没有文件被上传" => "Faile ei laaditud üles",
    "找不到临时目录" => "Ajutist kataloogi ei leitud",
    "未知的文件类型" => "Tundmatu failitüüp",
    "文件名不能为空" => "Faili nimi ei tohi olla tühi",
    "个文件有在使用" => "failid on kasutusel",
    "请先删除子页面" => "Palun kustuta esmalt alamleht",
    "请输入角色名称" => "Sisestage rolli nimi",
    "管理员不可禁用" => "Administraatorid ei saa keelata",
    "管理员不可删除" => "Administraatorid ei saa kustutada",
    "请输入限制大小" => "Sisestage piirsuurus",
    "请输入版权信息" => "Sisestage autoriõiguse teave",
    "恢复出厂成功！" => "Tehase lähtestamine õnnestus!",
    "恢复出厂失败！" => "Tehaseseadetele lähtestamine ebaõnnestus!",
    "标签名不能为空" => "Sildi nimi ei tohi olla tühi",
    "还没有内容信息" => "Sisuinfot veel pole",
    "这篇文章很有用" => "See artikkel on väga kasulik",
    "保存分类国际化" => "Salvesta kategooria rahvusvahelistumine",
    "分类国际化帮助" => "Klassifikatsiooni rahvusvahelistumise abi",
    "保存标签国际化" => "Salvesta sildi rahvusvahelistumine",
    "标签国际化帮助" => "Sildi rahvusvahelistumise abi",
    "保存文章国际化" => "Salvesta artikli rahvusvahelistumine",
    "文章国际化帮助" => "Artiklite rahvusvahelistumise abi",
    "保存页面国际化" => "Salvesta lehe rahvusvahelistumine",
    "页面国际化帮助" => "Abi lehe rahvusvahelistumisest",
    "海地克里奥尔语" => "Haiti kreool",
    "非法的ajax请求" => "Ebaseaduslik ajaxi taotlus",
    "密码至少位数" => "Parool peab koosnema vähemalt numbritest",
    "验证码不正确" => "Vale kinnituskood",
    "包含非法参数" => "Sisaldab ebaseaduslikke parameetreid",
    "请输入用户名" => "palun sisesta kasutajanimi",
    "用户名已存在" => "Kasutajanimi on juba olemas",
    "请重输入密码" => "Palun sisestage oma parool uuesti",
    "图片格式错误" => "Pildi vormingu viga",
    "修改资料成功" => "Andmete muutmine õnnestus",
    "没有改变信息" => "Teave ei muutunud",
    "请输入浏览量" => "Sisestage vaatamiste arv",
    "请输入点赞数" => "Palun sisesta meeldimiste arv",
    "请选择子分类" => "Valige alamkategooria",
    "创建文章成功" => "Artikli loomine õnnestus",
    "创建文章失败" => "Artikli loomine ebaõnnestus",
    "编辑文章成功" => "Artikli redigeerimine õnnestus",
    "标题不能为空" => "Pealkirja väli ei tohi olla tühi",
    "菜单名称重复" => "Menüü nime duplikaat",
    "创建菜单成功" => "Menüü loomine õnnestus",
    "创建菜单失败" => "Menüü loomine ebaõnnestus",
    "编辑菜单成功" => "Menüü redigeerimine õnnestus",
    "请选择行数据" => "Valige rea andmed",
    "计算孤立文件" => "Loenda orb faile",
    "划词选择错误" => "Vale sõnavalik",
    "请提取语言包" => "Ekstraktige keelepakett",
    "没有语音文字" => "Häälteksti pole",
    "语音朗读完成" => "Häällugemine lõpetatud",
    "分类名称为空" => "Kategooria nimi on tühi",
    "创建分类成功" => "Klassifikatsiooni loomine õnnestus",
    "创建分类失败" => "Kategooria loomine ebaõnnestus",
    "编辑分类成功" => "Kategooria redigeerimine õnnestus",
    "回复评论为空" => "Vastus kommentaarile on tühi",
    "回复评论成功" => "Kommentaarile vastamine õnnestus",
    "回复评论失败" => "Kommentaarile vastamine ebaõnnestus",
    "评论内容为空" => "Kommentaari sisu on tühi",
    "编辑评论成功" => "Kommentaari redigeerimine õnnestus",
    "待审评论成功" => "Ülevaatuse ootel kommentaar õnnestus",
    "待审评论失败" => "Ülevaatuse ootel kommentaar ebaõnnestus",
    "审核通过评论" => "Kinnitatud kommentaarid",
    "审核评论成功" => "Ülevaatus õnnestus",
    "审核评论失败" => "Kommentaaride ülevaatus ebaõnnestus",
    "删除评论失败" => "Kommentaari kustutamine ebaõnnestus",
    "请选择源语言" => "Valige lähtekeel",
    "别名不可更改" => "Varjunimesid ei saa muuta",
    "标签名称为空" => "Sildi nimi on tühi",
    "创建链接成功" => "Link edukalt loodud",
    "创建链接失败" => "Lingi loomine ebaõnnestus",
    "编辑链接成功" => "Lingi muutmine õnnestus",
    "网站名称重名" => "Dubleerivad veebisaitide nimed",
    "网站地址重复" => "Veebisaidi aadressi duplikaat",
    "图片压缩失败" => "Pildi tihendamine ebaõnnestus",
    "移动文件失败" => "Faili teisaldamine ebaõnnestus",
    "上传文件成功" => "Fail on edukalt üles laaditud",
    "上传文件失败" => "Faili üleslaadimine ebaõnnestus",
    "共找到文件：" => "Leitud faile kokku:",
    "创建页面成功" => "Lehekülje loomine õnnestus",
    "创建页面失败" => "Lehe loomine ebaõnnestus",
    "编辑页面成功" => "Lehe redigeerimine õnnestus",
    "权限数据错误" => "Loa andmete viga",
    "创建角色成功" => "Rolli loomine õnnestus",
    "创建角色失败" => "Rolli loomine ebaõnnestus",
    "编辑角色成功" => "Rolli muutmine õnnestus",
    "游客不可删除" => "Külastajad ei saa kustutada",
    "创建标签成功" => "Sildi loomine õnnestus",
    "创建标签失败" => "Sildi loomine ebaõnnestus",
    "编辑标签成功" => "Märgendi muutmine õnnestus",
    "角色数据错误" => "Tähemärgi andmete viga",
    "语言数据错误" => "Keeleandmete viga",
    "状态数据错误" => "Olekuandmete viga",
    "创建用户成功" => "Kasutaja loomine õnnestus",
    "创建用户失败" => "Kasutaja loomine ebaõnnestus",
    "编辑用户成功" => "Kasutaja redigeerimine õnnestus",
    "本文博客网址" => "Selle artikli ajaveebi URL",
    "评论内容重复" => "Kommentaari sisu dubleerimine",
    "回复内容重复" => "Dubleeri vastuse sisu",
    "评论发表成功" => "Kommentaari postitamine õnnestus",
    "发表评论失败" => "Kommentaari postitamine ebaõnnestus",
    "前端删除评论" => "Kommentaaride kustutamine esiosast",
    "中文（简体）" => "Lihtsustatud hiina keel)",
    "加泰罗尼亚语" => "katalaani",
    "苏格兰盖尔语" => "šoti gaeli keel",
    "中文（繁体）" => "traditsiooniline hiina)",
    "马拉雅拉姆语" => "malajalami",
    "斯洛文尼亚语" => "sloveeni keel",
    "阿尔巴尼亚语" => "albaanlane",
    "密码至少5位" => "Parool peab olema vähemalt 5 tähemärki pikk",
    "你已经登录" => "Sa oled juba sisse logitud",
    "账号被禁用" => "Konto on keelatud",
    "请输入密码" => "Palun sisesta parool",
    "留空不修改" => "Jätke tühjaks ja ärge muutke",
    "请输入标题" => "Palun sisestage pealkiri",
    "请输入内容" => "Sisestage sisu",
    "多标签半角" => "Mitme sildi poollaius",
    "关键词建议" => "Märksõna soovitused",
    "请输入作者" => "Palun sisestage autor",
    "请选择数据" => "Valige andmed",
    "软删除文章" => "pehme kustuta artikkel",
    "软删除成功" => "Pehme kustutamine õnnestus",
    "软删除失败" => "Pehme kustutamine ebaõnnestus",
    "角色不存在" => "rolli ei eksisteeri",
    "角色被禁用" => "Roll on keelatud",
    "功能不存在" => "Funktsiooni pole olemas",
    "功能被禁用" => "Funktsioon on keelatud",
    "国际化帮助" => "Abi rahvusvahelistumisest",
    "未选择文本" => "Teksti pole valitud",
    "请选择语言" => "Valige keel",
    "请输入排序" => "Palun sisestage sortimine",
    "未修改属性" => "omadusi ei muudeta",
    "机器人评论" => "Robotide ülevaated",
    "生成语言包" => "Loo keelepakett",
    "国际化分类" => "Rahvusvaheline klassifikatsioon",
    "国际化标签" => "rahvusvahelistumise silt",
    "国际化文章" => "Rahvusvahelised artiklid",
    "国际化页面" => "Rahvusvaheline leht",
    "服务器环境" => "Serveri keskkond",
    "数据库信息" => "Andmebaasi teave",
    "服务器时间" => "serveri aeg",
    "还没有评论" => "Kommentaarid veel puuduvad",
    "还没有数据" => "Andmeid veel pole",
    "网址不合法" => "URL on ebaseaduslik",
    "选择多文件" => "Valige mitu faili",
    "个未使用，" => "kasutamata,",
    "文件不存在" => "faili ei ole olemas",
    "重命名失败" => "Ümbernimetamine ebaõnnestus",
    "重命名成功" => "Ümbernimetamine õnnestus",
    "角色已存在" => "Roll on juba olemas",
    "蜘蛛不索引" => "ämblik ei indekseeri",
    "请输入数量" => "Palun sisesta kogus",
    "昵称已存在" => "Hüüdnimi on juba olemas",
    "页面没找到" => "lehte ei leitud",
    "还没有文章" => "Artikleid veel pole",
    "还没有页面" => "Lehte pole veel",
    "还没有分类" => "Klassifikatsiooni veel pole",
    "还没有标签" => "Silte pole veel",
    "还没有热门" => "Pole veel populaarne",
    "还没有更新" => "Pole veel värskendatud",
    "还没有网址" => "URL-i veel pole",
    "请写点评论" => "Palun kirjutage kommentaar",
    "，等待审核" => ",Modereeritud",
    "你已经注册" => "olete juba registreerunud",
    "提取语言包" => "Keelepaketi väljavõte",
    "分类国际化" => "Klassifikatsioon Rahvusvahelistumine",
    "标签国际化" => "Sildi rahvusvahelistumine",
    "文章国际化" => "Artikli rahvusvahelistumine",
    "页面国际化" => "Lehekülje rahvusvahelistumine",
    "格鲁吉亚语" => "Gruusia keel",
    "博杰普尔语" => "Bhojpuri",
    "白俄罗斯语" => "valgevenelane",
    "斯瓦希里语" => "suahiili keel",
    "古吉拉特语" => "gudžarati keel",
    "斯洛伐克语" => "slovaki",
    "阿塞拜疆语" => "Aserbaidžaan",
    "印尼巽他语" => "Indoneesia sundaani keel",
    "加利西亚语" => "Galicia",
    "拉脱维亚语" => "lätlane",
    "罗马尼亚语" => "rumeenlane",
    "亚美尼亚语" => "armeenlane",
    "保加利亚语" => "bulgaaria keel",
    "爱沙尼亚语" => "eesti keel",
    "阿姆哈拉语" => "amhari",
    "吉尔吉斯语" => "kirgiisi",
    "克罗地亚语" => "Horvaatia",
    "波斯尼亚语" => "bosnia keel",
    "蒂格尼亚语" => "Tignan",
    "克里奥尔语" => "kreool",
    "马尔加什语" => "Madagaskari",
    "南非科萨语" => "Lõuna-Aafrika xhosa",
    "南非祖鲁语" => "Zulu, Lõuna-Aafrika Vabariik",
    "塞尔维亚语" => "serblane",
    "乌兹别克语" => "usbeki",
    "伊洛卡诺语" => "Ilocano",
    "印尼爪哇语" => "jaava indoneesia keel",
    "回复ID错误" => "Vastuse ID viga",
    "非法文章ID" => "Illegaalne artikli ID",
    "管理后台" => "Juhtkonna taust",
    "管理登录" => "Administraatori sisselogimine",
    "登录成功" => "sisselogimine õnnestus",
    "切换注册" => "Lülitage registreerimine",
    "你已登出" => "Olete välja logitud",
    "登出成功" => "Väljalogimine õnnestus",
    "用户注册" => "Kasutaja registreerimine",
    "注册成功" => "registreerumine õnnestus",
    "注册失败" => "Registreerimine ebaõnnestus",
    "重复密码" => "Korda salasõna",
    "切换登录" => "Vaheta sisselogimist",
    "请先登录" => "Palun logi enne sisse",
    "修改资料" => "Muuda teavet",
    "没有改变" => "Ei ole muutust",
    "不可修改" => "Muutumatu",
    "上传失败" => "üleslaadimine ebaõnnestus",
    "清除成功" => "Tühjendamine õnnestus",
    "暂无记录" => "Kirjeid pole",
    "批量删除" => "partii kustutamine",
    "文章列表" => "Artiklite loend",
    "发布时间" => "vabastamise aeg",
    "修改时间" => "Muutke kellaaega",
    "文章标题" => "Artikli pealkiri",
    "标题重名" => "Topeltpealkiri",
    "别名重复" => "Pseudonüümi duplikaat",
    "创建文章" => "Loo artikkel",
    "编辑文章" => "Redigeeri artiklit",
    "非法字段" => "Ebaseaduslik väli",
    "填写整数" => "Täitke täisarv",
    "修改属性" => "Muuda omadusi",
    "修改成功" => "Edukalt muudetud",
    "修改失败" => "ei õnnestu redigeerida",
    "批量还原" => "Partii taastamine",
    "还原文章" => "Taasta artikkel",
    "还原成功" => "Taastamine õnnestus",
    "还原失败" => "Taastamine ebaõnnestus",
    "删除文章" => "Kustuta artikkel",
    "删除成功" => "edukalt kustutatud",
    "删除失败" => "kustutamine ebaõnnestus",
    "全部展开" => "Laienda kõik",
    "全部折叠" => "Ahenda kõik",
    "添加下级" => "Lisage alluv",
    "编辑菜单" => "Redigeeri menüü",
    "菜单名称" => "Menüü nimi",
    "父级菜单" => "Lapsevanema menüü",
    "顶级菜单" => "ülemine menüü",
    "创建菜单" => "Loo menüü",
    "删除菜单" => "kustuta menüü",
    "非法访问" => "Volitamata juurdepääs",
    "拒绝访问" => "Ligipääs keelatud",
    "没有权限" => "luba keelatud",
    "彻底删除" => "Eemaldage täielikult",
    "批量待审" => "Partii ülevaatuse ootel",
    "批量审核" => "Partii ülevaade",
    "垃圾评论" => "Rämpsposti kommentaarid",
    "分类名称" => "Kategooria nimi",
    "分类列表" => "Kategooriate loend",
    "父级分类" => "Vanemkategooria",
    "顶级分类" => "Tippklassifikatsioon",
    "分类重名" => "Topeltnimedega kategooriad",
    "创建分类" => "Loo kategooriaid",
    "编辑分类" => "Muuda kategooriat",
    "删除分类" => "Kustuta kategooria",
    "评论作者" => "Arvustuse autor",
    "评论内容" => "kommentaarid",
    "评论列表" => "kommentaaride loend",
    "评论文章" => "Arvustage artiklid",
    "回复内容" => "Vasta sisu",
    "回复评论" => "Vasta kommentaarile",
    "编辑评论" => "Toimetuse kommentaarid",
    "待审评论" => "Ootel kommentaarid",
    "待审成功" => "Prooviperioodi ootel õnnestus",
    "待审失败" => "Ebaõnnestumine on kohtuprotsessi ootel",
    "审核成功" => "Ülevaatus õnnestus",
    "审核失败" => "Auditi ebaõnnestumine",
    "删除评论" => "Kustuta kommentaar",
    "谷歌翻译" => "Google\'i tõlge",
    "检测语言" => "Tuvasta keel",
    "别名错误" => "Aliase viga",
    "语言错误" => "Keeleviga",
    "标签名称" => "Sildi nimi",
    "标签列表" => "siltide loend",
    "标签重名" => "Korduvate nimedega sildid",
    "页面列表" => "Lehekülgede loend",
    "页面标题" => "lehe pealkiri",
    "父级页面" => "emaleht",
    "顶级页面" => "ülemine leht",
    "清除缓存" => "tühjendage vahemälu",
    "当前版本" => "praegune versioon",
    "重置系统" => "Lähtestage süsteem",
    "最新评论" => "viimane kommentaar",
    "联系我们" => "võta meiega ühendust",
    "数据统计" => "Statistika",
    "网站名称" => "Veebisaidi nimi",
    "网站地址" => "veebilehe aadress",
    "链接列表" => "Lingitud loend",
    "创建链接" => "Loo link",
    "编辑链接" => "Redigeeri linki",
    "删除链接" => "Eemalda link",
    "日志标题" => "Logi pealkiri",
    "消息内容" => "Sõnumi sisu",
    "请求方法" => "Taotluse meetod",
    "请求路径" => "Taotlege teed",
    "日志列表" => "Logi nimekiri",
    "上传成功" => "Üleslaadimine õnnestus",
    "媒体列表" => "meedialoend",
    "开始上传" => "Alusta üleslaadimist",
    "等待上传" => "Ootab üleslaadimist",
    "重新上传" => "uuesti üles laadida",
    "上传完成" => "üleslaadimine lõpetatud",
    "系统保留" => "Süsteem reserveeritud",
    "发现重复" => "Leiti duplikaat",
    "上传文件" => "faile üles laadima",
    "个在用，" => "Üks on kasutusel,",
    "文件重名" => "Dubleerivad failinimed",
    "删除文件" => "Kustuta failid",
    "创建页面" => "Loo leht",
    "编辑页面" => "Redigeeri lehte",
    "删除页面" => "kustuta leht",
    "角色列表" => "rollide loetelu",
    "角色名称" => "Rolli nimi",
    "权限分配" => "Loa määramine",
    "创建角色" => "Rolli loomine",
    "编辑角色" => "Muuda rolli",
    "删除角色" => "Kustuta roll",
    "基础设置" => "Põhiseaded",
    "高级设置" => "täpsemad seaded",
    "其他设置" => "muud seaded",
    "上传类型" => "Üleslaadimise tüüp",
    "限制大小" => "Piiratud suurus",
    "默认最大" => "Vaikimisi maksimum",
    "单点登录" => "Logi sisse",
    "版权信息" => "Autoriõiguse teave",
    "链接地址" => "lingi aadress",
    "热门文章" => "populaarsed artiklid",
    "首页评论" => "Kodulehe kommentaarid",
    "内容相关" => "Sisuga seotud",
    "图片分页" => "Piltide otsimine",
    "友情链接" => "Lingid",
    "语音朗读" => "Häällugemine",
    "评论登录" => "Kommenteeri sisselogimist",
    "评论待审" => "Kommentaar on ootel",
    "保存成功" => "Salvestamine õnnestus",
    "创建标签" => "Loo sildid",
    "编辑标签" => "Muuda silti",
    "删除标签" => "Kustuta silt",
    "用户列表" => "kasutajate loend",
    "注册时间" => "Registreerimise aeg",
    "首次登陆" => "Esmakordne sisselogimine",
    "最后登陆" => "viimane sisselogimine",
    "语言权限" => "Keeleõigused",
    "创建用户" => "Loo kasutaja",
    "不能修改" => "Ei saa muuta",
    "编辑用户" => "Muuda kasutajat",
    "删除用户" => "kasutajaid kustutada",
    "最新文章" => "viimased artiklid",
    "搜索结果" => "Otsingu tulemused",
    "选择语言" => "Valige keel",
    "分类为空" => "Kategooria on tühi",
    "收藏文章" => "Koguge artikleid",
    "收藏成功" => "Kogumine õnnestus",
    "取消收藏" => "Tühista lemmikud",
    "取消回复" => "Tühista vastus",
    "发表评论" => "Kommenteeri",
    "语音播放" => "Hääle taasesitus",
    "回到顶部" => "tagasi tippu",
    "分类目录" => "Kategooriad",
    "下载内容" => "Sisu allalaadimine",
    "相关文章" => "seotud artiklid",
    "媒体合成" => "meedia süntees",
    "语音合成" => "kõne süntees",
    "前端登录" => "Esiotsa sisselogimine",
    "前端注册" => "Esiotsa registreerimine",
    "前端登出" => "Esikülje väljalogimine",
    "后台首页" => "Taust Kodu",
    "欢迎页面" => "tervitusleht",
    "权限管理" => "asutuse juhtimine",
    "用户管理" => "Kasutajate haldamine",
    "角色管理" => "rolli juhtimine",
    "权限菜单" => "Lubade menüü",
    "分类管理" => "Klassifikatsiooni haldamine",
    "标签管理" => "siltide haldamine",
    "文章管理" => "Artiklite haldamine",
    "页面管理" => "Lehekülje haldamine",
    "媒体管理" => "meedia juhtimine",
    "上传接口" => "Laadi üles liides",
    "链接管理" => "Linkide haldamine",
    "评论管理" => "Kommentaaride haldamine",
    "审核通过" => "eksam sooritatud",
    "机器评论" => "masina ülevaatus",
    "系统管理" => "Süsteemihaldus",
    "系统设置" => "Süsteemisätted",
    "保存设置" => "Salvesta sätted",
    "恢复出厂" => "Tehase taastamine",
    "操作日志" => "Toimingute logi",
    "删除日志" => "Kustuta logi",
    "科西嘉语" => "Korsika",
    "瓜拉尼语" => "Guaraní",
    "卢旺达语" => "Kinyarwanda",
    "约鲁巴语" => "joruba",
    "尼泊尔语" => "Nepali",
    "夏威夷语" => "Havai",
    "意第绪语" => "jidiš",
    "爱尔兰语" => "iirlane",
    "希伯来语" => "heebrea keel",
    "卡纳达语" => "kannada",
    "匈牙利语" => "ungarlane",
    "泰米尔语" => "tamiil",
    "阿拉伯语" => "araabia keel",
    "孟加拉语" => "bengali",
    "萨摩亚语" => "Samoa",
    "班巴拉语" => "Bambara",
    "立陶宛语" => "Leedu",
    "马耳他语" => "malta keel",
    "土库曼语" => "türkmeen",
    "阿萨姆语" => "Assami",
    "僧伽罗语" => "sinhala",
    "乌克兰语" => "ukrainlane",
    "威尔士语" => "kõmri",
    "菲律宾语" => "filipiinlane",
    "艾马拉语" => "Aymara",
    "泰卢固语" => "telugu",
    "多格来语" => "dogglai",
    "迈蒂利语" => "Maithili",
    "普什图语" => "puštu",
    "迪维希语" => "Dhivehi",
    "卢森堡语" => "Luksemburgi keel",
    "土耳其语" => "türgi keel",
    "马其顿语" => "makedoonlane",
    "卢干达语" => "Luganda",
    "马拉地语" => "marati",
    "乌尔都语" => "urdu",
    "葡萄牙语" => "portugali keel",
    "奥罗莫语" => "Oromo",
    "西班牙语" => "hispaania keel",
    "弗里西语" => "friisi keel",
    "索马里语" => "Somaalia",
    "齐切瓦语" => "Chichewa",
    "旁遮普语" => "pandžabi",
    "巴斯克语" => "baski keel",
    "意大利语" => "itaalia keel",
    "塔吉克语" => "tadžiki",
    "克丘亚语" => "ketšua",
    "奥利亚语" => "Oriya",
    "哈萨克语" => "kasahhi",
    "林格拉语" => "Lingala",
    "塞佩蒂语" => "Sepeti",
    "塞索托语" => "Sesotho",
    "维吾尔语" => "uiguurid",
    "ICP No.001" => "ICP nr 001",
    "用户名" => "kasutajanimi",
    "验证码" => "Kinnituskood",
    "记住我" => "mäleta mind",
    "手机号" => "Telefoninumber",
    "请输入" => "Palun sisesta",
    "请选择" => "palun vali",
    "浏览量" => "Vaated",
    "回收站" => "Prügikast",
    "菜单树" => "Menüüpuu",
    "控制器" => "kontroller",
    "第一页" => "Esimene lehekülg",
    "最后页" => "viimane lehekülg",
    "筛选列" => "Filtreeri veerge",
    "删除行" => "Kustuta rida",
    "还原行" => "Võta rida tagasi",
    "重命名" => "Nimeta ümber",
    "国际化" => "üleilmastumine",
    "文章数" => "Artiklite arv",
    "机器人" => "robot",
    "星期日" => "pühapäev",
    "星期一" => "esmaspäev",
    "星期二" => "teisipäeval",
    "星期三" => "kolmapäeval",
    "星期四" => "neljapäeval",
    "星期五" => "reedel",
    "星期六" => "laupäeval",
    "会员数" => "Liikmete arv",
    "评论数" => "Kommentaaride arv",
    "文件名" => "faili nimi",
    "音视频" => "Audio ja video",
    "扩展名" => "laiendi nimi",
    "未使用" => "Kasutamata",
    "个无效" => "kehtetu",
    "共删除" => "Kokku kustutatud",
    "个文件" => "failid",
    "备案号" => "Kohtuasja number",
    "轮播图" => "karussell",
    "条显示" => "riba ekraan",
    "上一页" => "Eelmine leht",
    "下一页" => "Järgmine leht",
    "未分类" => "kategoriseerimata",
    "空标签" => "tühi silt",
    "无标题" => "Pealkirjata",
    "控制台" => "konsool",
    "登录中" => "sisse logima",
    "注册中" => "Registreerimine",
    "跳转中" => "Ümbersuunamine",
    "提交中" => "esitamine",
    "审核中" => "ülevaatlusel",
    "请稍后" => "Palun oota",
    "篇文章" => "Artikkel",
    "客户端" => "klient",
    "没有了" => "pole enam ühtegi",
    "后评论" => "postita kommentaare",
    "未命名" => "nimetu",
    "软删除" => "pehme kustutamine",
    "语言包" => "Keelepakett",
    "豪萨语" => "Hausa",
    "挪威语" => "norra keel",
    "贡根语" => "gongani keel",
    "拉丁语" => "ladina keel",
    "捷克语" => "tšehhi",
    "波斯语" => "pärslane",
    "印地语" => "hindi",
    "冰岛语" => "islandi",
    "契维语" => "Twi",
    "高棉语" => "Kambodža",
    "丹麦语" => "taani keel",
    "修纳语" => "Shona",
    "越南语" => "vietnamlane",
    "宿务语" => "Cebuano",
    "波兰语" => "poola keel",
    "鞑靼语" => "tatarlane",
    "老挝语" => "Lao",
    "瑞典语" => "rootsi keel",
    "缅甸语" => "birma",
    "信德语" => "sindhi",
    "马来语" => "malai",
    "伊博语" => "igbo",
    "希腊语" => "kreeka keel",
    "芬兰语" => "soome keel",
    "埃维语" => "Ewe",
    "毛利语" => "maoorid",
    "荷兰语" => "hollandi keel",
    "蒙古语" => "mongoli keel",
    "米佐语" => "Mizo",
    "世界语" => "esperanto",
    "印尼语" => "Indoneesia",
    "宗加语" => "Dzonga",
    "密码" => "parool",
    "登录" => "Logi sisse",
    "重置" => "lähtestada",
    "信息" => "teavet",
    "成功" => "edu",
    "失败" => "ebaõnnestuda",
    "昵称" => "Hüüdnimi",
    "选填" => "Valikuline",
    "注册" => "Registreeri",
    "错误" => "viga",
    "头像" => "avatari",
    "上传" => "Laadi üles",
    "保存" => "salvestada",
    "标题" => "pealkiri",
    "别名" => "Teise nimega",
    "描述" => "kirjeldada",
    "封面" => "kaas",
    "分类" => "Klassifikatsioon",
    "状态" => "olek",
    "正常" => "normaalne",
    "禁用" => "Keela",
    "作者" => "autor",
    "日期" => "kuupäeva",
    "添加" => "Lisa",
    "刷新" => "värskenda",
    "标签" => "Silt",
    "浏览" => "Sirvige",
    "点赞" => "meeldib",
    "评论" => "Kommenteeri",
    "操作" => "tegutseda",
    "阅读" => "lugeda",
    "编辑" => "muuda",
    "删除" => "kustutada",
    "翻译" => "tõlkida",
    "内容" => "sisu",
    "分割" => "segmenteerimine",
    "建议" => "soovitus",
    "非法" => "illegaalne",
    "还原" => "vähendamine",
    "方法" => "meetod",
    "排序" => "sorteerida",
    "图标" => "ikooni",
    "菜单" => "menüü",
    "时间" => "aega",
    "匿名" => "anonüümne",
    "确定" => "Muidugi",
    "取消" => "Tühista",
    "跳转" => "Hüppa",
    "页码" => "lehekülje number",
    "共计" => "kokku",
    "每页" => "lehekülje kohta",
    "导出" => "Ekspordi",
    "打印" => "Prindi",
    "回复" => "vastata",
    "路径" => "tee",
    "预览" => "Eelvaade",
    "名称" => "nimi",
    "行数" => "read",
    "相册" => "fotoalbum",
    "文章" => "artiklit",
    "待审" => "Ootel",
    "通过" => "üle andma",
    "垃圾" => "Prügi",
    "提取" => "väljavõte",
    "生成" => "genereerida",
    "定位" => "positsiooni",
    "填充" => "täitmine",
    "帮助" => "abi",
    "返回" => "tagasi",
    "语言" => "keel",
    "宽屏" => "laiekraan",
    "普通" => "tavaline",
    "后台" => "Lavatagused",
    "前台" => "vastuvõtulett",
    "登出" => "Logi välja",
    "版本" => "Versioon",
    "关于" => "umbes",
    "微信" => "WeChat",
    "会员" => "liige",
    "地址" => "aadress",
    "类型" => "tüüp",
    "图片" => "pilt",
    "文件" => "dokument",
    "失效" => "Kehtetu",
    "大小" => "suurus",
    "下载" => "lae alla",
    "导航" => "navigeerimine",
    "站标" => "Saidi logo",
    "单位" => "üksus",
    "加速" => "kiirendada",
    "角色" => "Roll",
    "首页" => "Esilehekülg",
    "个数" => "number",
    "我的" => "minu oma",
    "说道" => "ütles",
    "手机" => "mobiiltelefon",
    "可选" => "Valikuline",
    "搜索" => "otsing",
    "页面" => "lehel",
    "查看" => "Kontrollima",
    "创建" => "luua",
    "更新" => "uuendada",
    "英语" => "Inglise",
    "法语" => "prantsuse keel",
    "俄语" => "vene keel",
    "梵语" => "sanskrit",
    "日语" => "Jaapani",
    "泰语" => "Tai",
    "苗语" => "hmong",
    "德语" => "saksa keel",
    "韩语" => "korea keel",
    "请" => "palun",
];
