<?php
/**
 * @author admin
 * @date 2024-06-05 12:59:16
 * @desc 索马里语语言包
 */
return [
    "迷你mvc小框架，一套用于开发各类管理系统的通用型后台管理系统模板，基于开源免费的 layui 的制作，内置各类实际业务场景下相对丰富的示例。" => "Mini MVC waa qaab-dhismeed yar, oo ah habraacyada nidaamka maaraynta dhabarka caalamiga ah si loo horumariyo habab maamul oo kala duwan.",
    "除非特别说明，本博客均为原创，如需转载，请以链接形式标明来源出处。" => "Haddii aan si kale loo sheegin, balooggani waa asal.",
    "重置系统，将删除用户所有数据和附件，恢复到出厂设置。" => "Dib-u-dejinta nidaamka waxay tirtiri doontaa dhammaan xogta isticmaalaha iyo lifaaqyada waxayna ku celin doontaa goobaha warshadda.",
    "用户名只能英文字母数字下划线或中划线，位数" => "Magaca adeegsaduhu waxa uu ka koobnaan karaa oo keliya xarfo Ingiriisi, nambaro, hoosta ama hoosta, iyo tirada lambarrada",
    "昵称只能中英文字母数字下划线中划线，位数" => "Naanaysta waxay ka koobnaan kartaa oo keliya xarfo Shiineys iyo Ingiriisi ah, lambaro, hoosta, hoosta, iyo lambarrada.",
    "有道接口不支持，需科学上网使用谷歌接口。" => "Interface Youdao lama taageero Waxaad u baahan tahay inaad isticmaasho interneedka Google si aad u gasho si cilmiyeysan.",
    "别名只能英文字母数字下划线中划线，位数" => "Magacyadu waxay ka koobnaan karaan oo keliya xarfo Ingiriisi ah, lambaro, hoosta, hoosta, iyo lambarrada.",
    "计算孤立文件需要较长时间，确定继续吗？" => "Waxay qaadataa wakhti dheer in la xisaabiyo faylasha agoonta Ma hubtaa inaad rabto inaad sii waddo?",
    "语音不支持，需科学上网使用谷歌接口。" => "Codka lama taageero, marka waxaad u baahan tahay inaad isticmaasho interneedka Google-ka si aad si cilmiyeysan ugu gasho internetka.",
    "您的账号已在别处登录，请重新登录！" => "Koontadaada meel kale ayaa laga galay, fadlan mar kale soo gal!",
    "你的账号已被禁用，请联系管理员" => "Koontadaada waa la joojiyay, fadlan la xiriir maamulaha",
    "文件有在使用中，请重新计算状态" => "Faylka waa la isticmaalayaa, fadlan dib u xisaabi heerka",
    "目标语言权限不够，请联系管理员" => "Ogolaanshaha luqadda bartilmaameedka oo aan ku filnayn, fadlan la xiriir maamulaha",
    "真的标记垃圾评论选中的行吗？" => "Runtii ma ku calaamadisay khadka la doortay inuu yahay spam?",
    "源语言别名为空，请联系源作者" => "Luqadda isha ee naanaysta waa madhan, fadlan la xidhiidh qoraaga isha",
    "点击上传，或将文件拖拽到此处" => "Guji si aad u soo dejiso, ama u jiid faylka halkan",
    "合成成功！再次点击按钮下载。" => "Isku-dubaridku waa guulaystay! Guji badhanka mar labaad si aad u soo dejiso.",
    "没有语言权限，请联系管理员" => "Ma jiro fasax luqadeed, fadlan la xiriir maamulaha",
    "源语言别名为空，不能国际化" => "Luqadda isha ee naanaysta waa madhan lamana dhigi karo mid caalami ah.",
    "源语言与目标语言都不能为空" => "Luqadda asalka ah iyo luqadda la beegsanayaa midna ma noqon karto.",
    "源语言与目标语言数量不一致" => "Tirada ilaha iyo luqadaha bartilmaameedku waa iswaafaqayn",
    "删除文件失败，文件有在使用" => "Ku guuldareystay in la tirtiro faylka, faylka waa la isticmaalayaa",
    "角色不存在，请联系管理员" => "Doorku ma jiro, fadlan la xidhiidh maamulaha",
    "角色被禁用，请联系管理员" => "Doorku waa naafo, fadlan la xidhiidh maamulaha",
    "功能不存在，请联系管理员" => "Shaqadu ma jirto, fadlan la xidhiidh maamulaha",
    "功能被禁用，请联系管理员" => "Shaqadu waa naafo, fadlan la xidhiidh maamulaha",
    "真的彻底删除选中的行吗？" => "Runtii miyay gebi ahaanba tirtirtaa safka la doortay?",
    "真的审核通过选中的行吗？" => "Safafka la doortay runtii ma la ansixiyay?",
    "目标语言与源语言不能相同" => "Luqadda bartilmaameedku ma noqon karto mid la mid ah luqadda asalka ah",
    "提取语言包与源语言不一致" => "Xirmada luqadda la soo saaray waa mid aan waafaqsanayn luqadda isha",
    "文件有在使用，删除失败！" => "Faylka waa la isticmaalayaa oo tirtirid waa fashilmay!",
    "没有权限，请联系管理员" => "Ogolaansho maleh, fadlan la xidhiidh maamulaha",
    "真的待审核选中的行吗？" => "Runtii ma xariiq u baahan in dib loo eego oo la xusho?",
    "目标语言包有空行，行数" => "Xidhmada luqadda bartilmaameedku waxay leedahay khadadka maran, tirada xariiqyada",
    "此操作恢复到出厂设置？" => "Hawlgalkani waxa uu ku soo celinayaa goobihii warshada?",
    "朗读错误，请联网后再试" => "Cilad akhriska, fadlan ku xidh internetka oo isku day mar kale",
    "还没有添加分类描述信息" => "Weli lama darin macluumaadka sifaha qaybta",
    "本地媒体已失效或不存在" => "Warbahinta maxaliga ahi waa been ama ma jiraan",
    "库尔德语（库尔曼吉语）" => "Kurdi (Kurmanji)",
    "作者名未注册或被禁用" => "Magaca qoraagu ma diiwaan gashanayn mana la joojin",
    "真的删除选中的行吗？" => "Runtii ma tirtirtaa safka la doortay?",
    "真的还原选中的行吗？" => "Runtii ma soo celisaa safafka la doortay?",
    "真的删除行或子行么？" => "Runtii ma rabtaa inaad tirtirto safafka ama saafka hoose?",
    "目标语言包未发现空行" => "Khad faaruq ah lagama helin xidhmada luqadda bartilmaameedka",
    "该语言不支持语音朗读" => "Luuqadani ma taageerto cod akhrinta",
    "语言包数据未发生改变" => "Xogta xirmada luqadda isma beddelin",
    "欢迎使用后台管理系统" => "Ku soo dhawoow nidaamka maaraynta dhabarka",
    "文件有在使用或已失效" => "Faylka waa la isticmaalayaa ama dhacay",
    "合成失败，请稍后再试" => "Isku-dubaridku wuu fashilmay, fadlan isku day mar kale hadhow",
    "&copy; 2021-2023 Company, Inc." => "© 2021-2023 Shirkadda, Inc.",
    "梅泰语（曼尼普尔语）" => "Meitei (Manipuri)",
    "两次密码输入不一致" => "Labada gelinta erayga sirta ah waa kuwo aan is waafaqsanayn",
    "该用户密码不可修改" => "Furaha isticmaalaha lama beddeli karo",
    "添加文章时创建标签" => "Samee calaamado marka aad maqaallada ku darayso",
    "删除文章时删除评论" => "Tirtir faallooyinka marka maqaal la tirtirayo",
    "控制器与方法已存在" => "Kantaroolka iyo habka ayaa horeba u jiray",
    "标题或名称不能为空" => "Magaca ama magaca ma noqon karo madhan",
    "请选择一种语言朗读" => "Fadlan door luqad aad kor ugu akhrido",
    "分类有文章不能删除" => "Waxaa jira maqaallo qaybta ah oo aan la tirtiri karin",
    "审核为垃圾评论成功" => "Dib loogu eegay spam si guul leh",
    "审核为垃圾评论失败" => "Dib u eegista ayaa ku guuldareysatay faallada spamka",
    "确定要登出站点吗？" => "Ma hubtaa inaad doonayso inaad ka baxdo goobta?",
    "网站名称或地址为空" => "Magaca ama ciwaanka mareegaha waa madhan",
    "网站名称或地址重名" => "nuqul ka samee magaca ama ciwaanka mareegaha",
    "允许上传的文件类型" => "Noocyada faylka waa la oggol yahay in la soo geliyo",
    "标签有文章不能删除" => "Maqaallada leh summada lama tirtiri karo",
    "人觉得这篇文章很赞" => "Dadku waxay u maleynayaan in maqaalkani yahay mid weyn",
    "还没有页面描述信息" => "Weli ma jiro macluumaad sifayn bogga",
    "回复与评论内容重复" => "Nuqul ka mid ah jawaabta iyo faallooyinka",
    "失败！请稍后再试。" => "fashilanto! Fadlan isku day mar kale hadhow",
    "主耶稣基督里的教会" => "Kaniisadda ku taal Rabbi Ciise Masiix",
    "库尔德语（索拉尼）" => "Kurdish (Soraani)",
    "布尔语(南非荷兰语)" => "Boolean (Afrikaan)",
    "用户名或密码错误" => "Magaca isticmaale ama erayga sirta ah ee khaldan",
    "源语言必须是中文" => "Luuqadda laga soo xigtay waa in ay noqotaa Shiine",
    "目标语言别名为空" => "Luqadda bartilmaameedka ee naanaysta waa madhan",
    "国际化文章时标签" => "Qoraalada caalamiga ah",
    "确定清除缓存吗？" => "Ma hubtaa inaad rabto inaad tirtirto kaydka?",
    "超过的单文件大小" => "Cabbirka hal fayl waa la dhaafay",
    "超过前端表单限制" => "Xadka foomamka hore waa la dhaafay",
    "目标没有写入权限" => "Bartilmaameedka ma laha ogolaansho qoraal ah",
    "不允许的上传类型" => "Aan la oggolayn noocyada raritaanka",
    "文件大小不能超过" => "Cabbirka faylka ma dhaafi karo",
    "保存基础设置成功" => "Dejinta aasaasiga ah ayaa loo badbaadiyay si guul leh",
    "首次基础设置成功" => "Dajinta aasaasiga ah ee ugu horeysay waa lagu guulaystay",
    "正在合成，请稍后" => "Synthesing, fadlan sug",
    "不合理的请求方法" => "Habka codsi aan macquul ahayn",
    "Session无效或过期" => "Fadhigu waa khalad ama dhacay",
    "手机号码不正确" => "nambarka taleefoonku waa khaldan yahay",
    "手机号码已存在" => "Lambarka moobilka ayaa hore u jiray",
    "标题或内容为空" => "Cinwaanka ama waxa ku jira waa madhan",
    "创建文章时标签" => "Tags marka la abuurayo maqaal",
    "编辑文章时标签" => "Tags marka maqaal la hagaajinayo",
    "真的删除行么？" => "Runtii ma rabtaa in aad tirtirto khadka?",
    "请输入菜单名称" => "Fadlan geli magaca menu",
    "请先删除子菜单" => "Fadlan marka hore tirtir liiska hoose",
    "未登录访问后台" => "Geli dhabarka dambe adoon soo galin",
    "Cookie无效或过期" => "Kukigu waa sax ama wuu dhacay",
    "真的还原行么？" => "Runtii suurtagal ma tahay in dib loo soo celiyo?",
    "编辑器内容为空" => "Xogta tifaftiruhu waa madhan",
    "未选择整行文本" => "Dhammaan khadka qoraalka lama dooran",
    "划词选择行错误" => "Khaladka khadka xulashada ereyga",
    "数据源发生改变" => "Isha xogta ayaa isbedesha",
    "填充成功，行号" => "Buuxiyey si guul leh, lambarka khadka",
    "请输入分类名称" => "Fadlan geli magaca qaybta",
    "分类名不能为空" => "Magaca qaybta ma noqon karo madhan",
    "排序只能是数字" => "Kala soocida waxay noqon kartaa oo kaliya tirooyin",
    "请先删除子分类" => "Fadlan marka hore tirtir qaybaha hoose",
    "请输入评论作者" => "Fadlan geli qoraaga faallooyinka",
    "请选择目标语言" => "Fadlan door luqadda bartilmaameedka ah",
    "语言包生成成功" => "Xidhmada luqadda si guul leh ayaa loo soo saaray",
    "语言包生成失败" => "Soo saarista xirmada luqadda waa guuldareystay",
    "国际化分类成功" => "Kala saarista caalamiga ah waa lagu guulaystay",
    "国际化分类失败" => "Kala soocida caalamiga ah waa fashilantay",
    "请输入标签名称" => "Fadlan geli magac calaamad",
    "国际化标签成功" => "Guul summada caalamiga",
    "国际化标签失败" => "Summada caalimaynta way guuldarraysatay",
    "国际化文章成功" => "Guul maqaal caalami ah",
    "国际化文章失败" => "Maqaal caalami ah ayaa fashilmay",
    "请输入页面标题" => "Fadlan geli cinwaanka bogga",
    "国际化页面成功" => "Bogga caalimiyaynta waa lagu guulaystay",
    "国际化页面失败" => "Bogga caalamiga ah ayaa guuldarraystay",
    "作者：葡萄枝子" => "Qore: Laanta Canabka",
    "请输入网站名称" => "Fadlan gali magaca shabakada",
    "请输入网站地址" => "Fadlan geli ciwaanka mareegaha",
    "链接名不能为空" => "Magaca isku xidhka ma noqon karo madhan",
    "上传文件不完整" => "Faylka la soo geliyay waa mid aan dhameystirneyn",
    "没有文件被上传" => "Wax galal ah lama rarin",
    "找不到临时目录" => "Hagaha heerkulka lama helin",
    "未知的文件类型" => "Nooca faylka aan la garanayn",
    "文件名不能为空" => "Magaca faylka ma noqon karo maran",
    "个文件有在使用" => "faylasha waa la isticmaalayaa",
    "请先删除子页面" => "Fadlan marka hore tirtir bogga hoose",
    "请输入角色名称" => "Fadlan geli magac door",
    "管理员不可禁用" => "Maamulayaashu ma joojin karaan",
    "管理员不可删除" => "Maamulayaashu ma tirtiri karaan",
    "请输入限制大小" => "Fadlan geli cabbirka xadka",
    "请输入版权信息" => "Fadlan geli macluumaadka xuquuqda daabacaada",
    "恢复出厂成功！" => "Dib u habayn warshadeed waa lagu guulaystay!",
    "恢复出厂失败！" => "Dib u habayn warshadeed waa fashilantay!",
    "标签名不能为空" => "Magaca tag ma noqon karo maran",
    "还没有内容信息" => "Weli ma jiro xog macluumaad ah",
    "这篇文章很有用" => "Maqaalkani waa mid aad waxtar u leh",
    "保存分类国际化" => "Badbaadi heerka caalamiga ah",
    "分类国际化帮助" => "Caawinta caalamiga ah ee kala saarista",
    "保存标签国际化" => "Keydi calaamadda caalamiga ah",
    "标签国际化帮助" => "Tag caawimada caalamiga ah",
    "保存文章国际化" => "Badbaadinta maqaallada caalamiga ah",
    "文章国际化帮助" => "Caawinta caalamiga ah ee qodobka",
    "保存页面国际化" => "Kaydi bogga caalamiga",
    "页面国际化帮助" => "Caawinta caalamiga ah ee bogga",
    "海地克里奥尔语" => "Haitian Creole",
    "非法的ajax请求" => "Codsiga ajax sharci darro ah",
    "密码至少位数" => "Furaha sirta ah waa inuu lahaadaa ugu yaraan lambar",
    "验证码不正确" => "Koodhka xaqiijinta ee khaldan",
    "包含非法参数" => "Waxay ka kooban tahay cabbirro sharci darro ah",
    "请输入用户名" => "fadlan geli magaca isticmaale",
    "用户名已存在" => "Magaca isticmaalaha ayaa hore u jiray",
    "请重输入密码" => "Fadlan dib u geli eraygaaga sirta ah",
    "图片格式错误" => "Khaladka qaabka sawirka",
    "修改资料成功" => "Xogta si guul leh ayaa wax looga beddelay",
    "没有改变信息" => "Wax xog ah oo iska beddelay ma jiraan",
    "请输入浏览量" => "Fadlan geli tirada aragtida",
    "请输入点赞数" => "Fadlan geli tirada jecel",
    "请选择子分类" => "Fadlan door qayb-hoosaad",
    "创建文章成功" => "Maqaalka ayaa si guul leh loo sameeyay",
    "创建文章失败" => "Ku guul daraystay in la abuuro maqaal",
    "编辑文章成功" => "Wax ka beddel maqaalka si guul leh",
    "标题不能为空" => "Cinwaanka ma noqon karo maran",
    "菜单名称重复" => "Nuqul magaca menu",
    "创建菜单成功" => "Menu si guul leh ayaa loo sameeyay",
    "创建菜单失败" => "Ku guuldareystay in la abuuro menu",
    "编辑菜单成功" => "Menu wax ka beddel waa lagu guulaystay",
    "请选择行数据" => "Fadlan dooro xogta safka",
    "计算孤立文件" => "Tiri galalka agoonta",
    "划词选择错误" => "Xulashada erayada khaldan",
    "请提取语言包" => "Fadlan soo saar xidhmada luqadda",
    "没有语音文字" => "Ma jiro qoraal cod ah",
    "语音朗读完成" => "Akhriska codka waa la dhameeyay",
    "分类名称为空" => "Magaca qaybta waa madhan",
    "创建分类成功" => "Kala saarista si guul leh ayaa loo sameeyay",
    "创建分类失败" => "Ku guuldareystay in la abuuro qaybta",
    "编辑分类成功" => "Wax ka beddel qaybta si guul leh",
    "回复评论为空" => "Jawaabta faallooyinka waa madhan",
    "回复评论成功" => "Si guul leh uga jawaab faallooyinka",
    "回复评论失败" => "Jawaabta faallooyinka waa guuldareystay",
    "评论内容为空" => "Waxyaabaha faallooyinka ku jira waa madhan",
    "编辑评论成功" => "Wax ka beddel faallada si guul leh",
    "待审评论成功" => "Faallooyinku waa lagu guulaystay",
    "待审评论失败" => "Faallooyinka la sugayo eegidda waa guuldarreystay",
    "审核通过评论" => "Faallooyinka la ansixiyay",
    "审核评论成功" => "Dib u eegis waa lagu guulaystay",
    "审核评论失败" => "Faallooyinka ayaa fashilmay",
    "删除评论失败" => "Waa lagu guul daraystay in la tirtiro faallooyinka",
    "请选择源语言" => "Fadlan door luqadda isha",
    "别名不可更改" => "Magacyada lagu naanayso lama beddeli karo",
    "标签名称为空" => "Magaca tag waa madhan",
    "创建链接成功" => "Xiriirinta si guul leh ayaa loo sameeyay",
    "创建链接失败" => "Ku guuldareystay inuu abuuro isku xirka",
    "编辑链接成功" => "Wax ka beddel xiriirinta si guul leh",
    "网站名称重名" => "Magacyada shabakadaha nuqul ka mid ah",
    "网站地址重复" => "Cinwaanka mareegaha nuqul ka mid ah",
    "图片压缩失败" => "Isku-buufinta sawirka ayaa guuldareystay",
    "移动文件失败" => "Ku guuldareystay in la raro faylka",
    "上传文件成功" => "Faylka si guul leh ayaa loo soo geliyey",
    "上传文件失败" => "Soo dejintii faylka waa guuldareystay",
    "共找到文件：" => "Wadarta faylasha la helay:",
    "创建页面成功" => "Bogga si guul leh ayaa loo sameeyay",
    "创建页面失败" => "Abuuritaanka bogga waa guul daraystay",
    "编辑页面成功" => "Wax ka beddel bogga si guul leh",
    "权限数据错误" => "Cilada xogta ogolaanshaha",
    "创建角色成功" => "Doorka si guul leh ayaa loo abuuray",
    "创建角色失败" => "Ku guuldareystay inuu abuuro door",
    "编辑角色成功" => "Doorka wax ka beddel si guul leh",
    "游客不可删除" => "Soo booqdayaashu ma tirtiri karaan",
    "创建标签成功" => "Tag ayaa loo sameeyay si guul leh",
    "创建标签失败" => "Ku guuldareystay inuu abuuro calaamad",
    "编辑标签成功" => "Wax ka beddel summada si guul leh",
    "角色数据错误" => "Khaladka xogta jilaha",
    "语言数据错误" => "Cilada xogta luqadda",
    "状态数据错误" => "Khaladka xogta heerka",
    "创建用户成功" => "Isticmaalaha si guul leh ayuu u sameeyay",
    "创建用户失败" => "Ku guuldareystay inuu abuuro isticmaale",
    "编辑用户成功" => "Wax ka beddel isticmaale si guul leh",
    "本文博客网址" => "Maqaalka URL URL",
    "评论内容重复" => "Nuqul faallooyinka",
    "回复内容重复" => "Nuqul jawaab celin ah",
    "评论发表成功" => "Faallada ayaa si guul leh loo dhajiyay",
    "发表评论失败" => "Ku guul daraystay in uu faallooyinka soo dhigo",
    "前端删除评论" => "Tirtiridda faallooyinka xagga hore",
    "中文（简体）" => "Shiinees (La fududeeyay)",
    "加泰罗尼亚语" => "Catalan",
    "苏格兰盖尔语" => "Scottish Gaelic",
    "中文（繁体）" => "Shiinees dhaqameed)",
    "马拉雅拉姆语" => "Malayalam",
    "斯洛文尼亚语" => "sloveniya",
    "阿尔巴尼亚语" => "Albaaniyaan",
    "密码至少5位" => "Erayga sirta ah waa inuu ahaadaa ugu yaraan 5 xaraf",
    "你已经登录" => "Durba waad soo gashay",
    "账号被禁用" => "Akoonka waa naafo",
    "请输入密码" => "Fadlan geli erayga sirta ah",
    "留空不修改" => "Banaan ku dhaaf oo ha beddelin",
    "请输入标题" => "Fadlan geli cinwaan",
    "请输入内容" => "Fadlan gali nuxurka",
    "多标签半角" => "Calaamado badan oo kala badh ballac ah",
    "关键词建议" => "Soo jeedinta erayga muhiimka ah",
    "请输入作者" => "Fadlan geli qoraaga",
    "请选择数据" => "Fadlan dooro xogta",
    "软删除文章" => "jilicsan article tirtirto",
    "软删除成功" => "Tirtiridda jilicsan waa lagu guuleystay",
    "软删除失败" => "Tirtiridda jilicsan waa guuldareystay",
    "角色不存在" => "door ma jiro",
    "角色被禁用" => "Doorku waa naafo",
    "功能不存在" => "Shaqadu ma jirto",
    "功能被禁用" => "Shaqadu waa naafo",
    "国际化帮助" => "Caawinta caalamiga ah",
    "未选择文本" => "Ma jiro qoraal la doortay",
    "请选择语言" => "Fadlan door luqadda",
    "请输入排序" => "Fadlan geli kala-soocidda",
    "未修改属性" => "guryaha aan la beddelin",
    "机器人评论" => "Reviews Robot",
    "生成语言包" => "Samee xirmo luqadeed",
    "国际化分类" => "Kala saarista caalamiga ah",
    "国际化标签" => "caalamiyeynta",
    "国际化文章" => "Maqaallo caalami ah",
    "国际化页面" => "Bogga caalamiga ah",
    "服务器环境" => "Deegaanka server-ka",
    "数据库信息" => "Xogta kaydka",
    "服务器时间" => "waqtiga serverka",
    "还没有评论" => "Weli faallooyin ma jiraan",
    "还没有数据" => "Wali xog malaha",
    "网址不合法" => "URL waa sharci darro",
    "选择多文件" => "Dooro faylal badan",
    "个未使用，" => "aan la isticmaalin,",
    "文件不存在" => "file ma jiro",
    "重命名失败" => "Magacaabista waa guuldareystay",
    "重命名成功" => "Magacaabista waa lagu guulaystay",
    "角色已存在" => "Door ayaa horay u jirtay",
    "蜘蛛不索引" => "caaro aan tilmaamin",
    "请输入数量" => "Fadlan geli tirada",
    "昵称已存在" => "Naanaysta ayaa hore u jirtay",
    "页面没找到" => "Bogga lama helin",
    "还没有文章" => "Ma jiraan maqaallo weli",
    "还没有页面" => "Bog wali ma jiro",
    "还没有分类" => "Weli ma kala sooc",
    "还没有标签" => "Wali lama hayo",
    "还没有热门" => "Weli caan ma aha",
    "还没有更新" => "Weli lama cusboonaysiin",
    "还没有网址" => "Wali URL ma jiro",
    "请写点评论" => "Fadlan qor faallo",
    "，等待审核" => ", Dhexdhexaad",
    "你已经注册" => "mar hore ayaad is diiwaan galisay",
    "提取语言包" => "Soo saar xirmo luqadeed",
    "分类国际化" => "Kala soocidda Internationalization",
    "标签国际化" => "Sumad caalamiyaynta",
    "文章国际化" => "Maqaal caalamiyeynta",
    "页面国际化" => "Page-ka caalamiga ah",
    "格鲁吉亚语" => "Joorjiyaan",
    "博杰普尔语" => "Bhojpuri",
    "白俄罗斯语" => "Belarusiyaanka",
    "斯瓦希里语" => "Sawaaxili",
    "古吉拉特语" => "gujarati",
    "斯洛伐克语" => "Slovakia",
    "阿塞拜疆语" => "Asarbayjaan",
    "印尼巽他语" => "Indonesian Sundan",
    "加利西亚语" => "Galiciyan",
    "拉脱维亚语" => "Latvia",
    "罗马尼亚语" => "Romanian",
    "亚美尼亚语" => "Armeeniyaan",
    "保加利亚语" => "Bulgaariya",
    "爱沙尼亚语" => "Istooniyaan",
    "阿姆哈拉语" => "Amxaari",
    "吉尔吉斯语" => "Kyrgyz",
    "克罗地亚语" => "Croatian",
    "波斯尼亚语" => "Bosnia",
    "蒂格尼亚语" => "Tignan",
    "克里奥尔语" => "Creole",
    "马尔加什语" => "Malagasy",
    "南非科萨语" => "Koonfur Afrika Xhosa",
    "南非祖鲁语" => "Zulu, Koonfur Afrika",
    "塞尔维亚语" => "Seerbiyaan",
    "乌兹别克语" => "Uzbekistan",
    "伊洛卡诺语" => "Ilocano",
    "印尼爪哇语" => "Indonesian Javanese ah",
    "回复ID错误" => "Cilad aqoonsi ka jawaab",
    "非法文章ID" => "Aqoonsiga maqaalka sharci-darrada ah",
    "管理后台" => "Asalkii maamulka",
    "管理登录" => "admin login",
    "登录成功" => "galitaanka guul",
    "切换注册" => "Isdiiwaangelinta beddel",
    "你已登出" => "Waa lagaa baxay",
    "登出成功" => "Ka bixida waa lagu guulaystay",
    "用户注册" => "Diiwaangelinta isticmaalaha",
    "注册成功" => "guusha diiwaangelinta",
    "注册失败" => "diiwaangelintu way fashilantay",
    "重复密码" => "Ku celi erayga sirta ah",
    "切换登录" => "Bedeli galitaanka",
    "请先登录" => "fadlan marka hore gal",
    "修改资料" => "Wax ka beddel xogta",
    "没有改变" => "Isbadal ma jiro",
    "不可修改" => "Aan la beddeli karin",
    "上传失败" => "soo galinta ayaa fashilantay",
    "清除成功" => "Nadiifi si guul leh",
    "暂无记录" => "Ma jiro diiwaan",
    "批量删除" => "tirtirka dufcada",
    "文章列表" => "Liiska maqaallada",
    "发布时间" => "wakhtiga sii daynta",
    "修改时间" => "Bedel wakhtiga",
    "文章标题" => "Cinwaanka maqaalka",
    "标题重名" => "Cinwaanka nuqul ka mid ah",
    "别名重复" => "Naanaysta nuqul ka samaysan",
    "创建文章" => "Abuur maqaal",
    "编辑文章" => "Wax ka beddel maqaal",
    "非法字段" => "Beer sharci-darro ah",
    "填写整数" => "Buuxi lambarrada",
    "修改属性" => "Wax ka beddel guryaha",
    "修改成功" => "Si guul leh ayaa wax laga beddelay",
    "修改失败" => "ku guul darreysato inaad wax ka beddesho",
    "批量还原" => "Dufcaddii soo celinta",
    "还原文章" => "Soo celi maqaalka",
    "还原成功" => "Soo celinta guul",
    "还原失败" => "Soo celinta waa fashilantay",
    "删除文章" => "Tirtir maqaalka",
    "删除成功" => "si guul leh ayaa loo tirtiray",
    "删除失败" => "ku guuldareystay in la tirtiro",
    "全部展开" => "Balaadhi dhammaan",
    "全部折叠" => "Burbur dhammaan",
    "添加下级" => "Kudar hoose",
    "编辑菜单" => "Wax ka beddel liiska",
    "菜单名称" => "Magaca menu",
    "父级菜单" => "Liiska waalidka",
    "顶级菜单" => "menu sare",
    "创建菜单" => "Samee liiska",
    "删除菜单" => "menu tirtir",
    "非法访问" => "Gelitaan aan la ogolayn",
    "拒绝访问" => "diidmo gelid",
    "没有权限" => "ogolaanshaha loo diiday",
    "彻底删除" => "Si buuxda uga saar",
    "批量待审" => "Dufcadu waxay sugaysaa dib u eegis",
    "批量审核" => "Dufcaddii dib u eegista",
    "垃圾评论" => "Faallooyinka spam",
    "分类名称" => "Magaca Qaybta",
    "分类列表" => "Liiska Qaybta",
    "父级分类" => "Qaybta waalidka",
    "顶级分类" => "Kala soocida ugu sareysa",
    "分类重名" => "Qeybaha leh magacyo nuqul ah",
    "创建分类" => "Abuur qaybaha",
    "编辑分类" => "Wax ka beddel qaybta",
    "删除分类" => "Tirtir qaybta",
    "评论作者" => "Dib u eegis qoraaga",
    "评论内容" => "faallooyin",
    "评论列表" => "liiska faallooyinka",
    "评论文章" => "Maqaallada dib u eegista",
    "回复内容" => "Ka jawaab nuxurka",
    "回复评论" => "Ka jawaab faallooyinka",
    "编辑评论" => "Faallooyinka tifaftirka",
    "待审评论" => "Faallooyinka sugaya",
    "待审成功" => "Tijaabada sugaysa si guul leh",
    "待审失败" => "Fashilku wuxuu sugayaa maxkamad",
    "审核成功" => "Dib u eegis guul ah",
    "审核失败" => "Guuldarradii hanti-dhawrka",
    "删除评论" => "Tirti faallooyinka",
    "谷歌翻译" => "Google turjumi",
    "检测语言" => "Soo ogow luqadda",
    "别名错误" => "Khalad magac u yaal ah",
    "语言错误" => "Cilad luqadeed",
    "标签名称" => "Magaca tag",
    "标签列表" => "liiska tag",
    "标签重名" => "Tags leh magacyo nuqul ah",
    "页面列表" => "Liiska bogga",
    "页面标题" => "cinwaanka bogga",
    "父级页面" => "bogga waalidka",
    "顶级页面" => "bogga sare",
    "清除缓存" => "kayd cad",
    "当前版本" => "nooca hadda jira",
    "重置系统" => "Nidaamka dib u dajinta",
    "最新评论" => "faallooyinka ugu dambeeyay",
    "联系我们" => "nala soo xiriir",
    "数据统计" => "Tirakoobka",
    "网站名称" => "Magaca shabakada",
    "网站地址" => "ciwaanka mareegaha",
    "链接列表" => "Liiska ku xidhan",
    "创建链接" => "Abuur xiriiriye",
    "编辑链接" => "Wax ka beddel xiriiriyaha",
    "删除链接" => "Ka saar xiriirka",
    "日志标题" => "Magaca gal",
    "消息内容" => "Nuxurka fariinta",
    "请求方法" => "Habka codsiga",
    "请求路径" => "Dariiqa codsiga",
    "日志列表" => "Liiska gal",
    "上传成功" => "Soo dejinta guul",
    "媒体列表" => "liiska warbaahinta",
    "开始上传" => "Bilow soo dejinta",
    "等待上传" => "Sugitaanka soo dejinta",
    "重新上传" => "dib u soo rarid",
    "上传完成" => "rarkii la dhameeyay",
    "系统保留" => "Nidaamka waa xafidan yahay",
    "发现重复" => "nuqul ayaa la helay",
    "上传文件" => "geli faylasha",
    "个在用，" => "Mid waa la isticmaalayaa,",
    "文件重名" => "Magacyada faylka nuqul",
    "删除文件" => "Tirtir faylalka",
    "创建页面" => "Samee bog",
    "编辑页面" => "Wax ka beddel bogga",
    "删除页面" => "tirtir bogga",
    "角色列表" => "liiska doorka",
    "角色名称" => "Magaca Doorka",
    "权限分配" => "Shaqada ogolaanshaha",
    "创建角色" => "Abuurista Door",
    "编辑角色" => "Doorka wax ka beddel",
    "删除角色" => "Tirtir doorka",
    "基础设置" => "Dejinta aasaasiga ah",
    "高级设置" => "goobaha horumarsan",
    "其他设置" => "goobaha kale",
    "上传类型" => "Nooca soo dejinta",
    "限制大小" => "Xaddid cabbirka",
    "默认最大" => "Ugu badnaan default",
    "单点登录" => "saxiix",
    "版权信息" => "Macluumaadka Xuquuqda Daabacaada",
    "链接地址" => "cinwaanka xiriirka",
    "热门文章" => "maqaallada caanka ah",
    "首页评论" => "Faallooyinka bogga hoyga",
    "内容相关" => "Waxa la xidhiidha",
    "图片分页" => "Bogga sawirka",
    "友情链接" => "Xiriirinta",
    "语音朗读" => "Cod akhris",
    "评论登录" => "Faallo gal",
    "评论待审" => "Faallo waa la sugayaa",
    "保存成功" => "Si guul leh loo badbaadiyay",
    "创建标签" => "Samee tags",
    "编辑标签" => "Tafatir tag",
    "删除标签" => "Tirtir summada",
    "用户列表" => "liiska isticmaalaha",
    "注册时间" => "Waqtiga diiwaangelinta",
    "首次登陆" => "Markii ugu horeysay login",
    "最后登陆" => "login u dambeeya",
    "语言权限" => "Ogolaanshaha luqadda",
    "创建用户" => "Abuur isticmaale",
    "不能修改" => "Lama bedeli karo",
    "编辑用户" => "Wax ka beddel isticmaale",
    "删除用户" => "tirtir isticmaalayaasha",
    "最新文章" => "articles ugu dambeeyay",
    "搜索结果" => "natiijooyinka raadinta",
    "选择语言" => "Dooro luqad",
    "分类为空" => "Qaybta madhan",
    "收藏文章" => "Ururi maqaallo",
    "收藏成功" => "Ururinta waa lagu guulaystay",
    "取消收藏" => "Jooji kuwa ugu cadcad",
    "取消回复" => "Jooji jawaabta",
    "发表评论" => "Faallo",
    "语音播放" => "Dib u soo celinta codka",
    "回到顶部" => "dib ugu noqo xagga sare",
    "分类目录" => "Qaybaha",
    "下载内容" => "Soo deji nuxurka",
    "相关文章" => "maqaallada la xidhiidha",
    "媒体合成" => "isku dhafka warbaahinta",
    "语音合成" => "hadalka hadalka",
    "前端登录" => "Gelitaanka-dhamaadka hore",
    "前端注册" => "Diiwaangelinta-dhamaadka hore",
    "前端登出" => "Calaamadaynta hore",
    "后台首页" => "Guriga asalka ah",
    "欢迎页面" => "bogga soo dhawoow",
    "权限管理" => "maamulka maamulka",
    "用户管理" => "Maamulka Isticmaalaha",
    "角色管理" => "maaraynta doorka",
    "权限菜单" => "Liiska ogolaanshaha",
    "分类管理" => "Maareynta kala soocidda",
    "标签管理" => "maamulka tag",
    "文章管理" => "Maareynta qodobka",
    "页面管理" => "Maamulka bogga",
    "媒体管理" => "maamulka warbaahinta",
    "上传接口" => "Soo rar interface",
    "链接管理" => "Maareynta isku xirka",
    "评论管理" => "Maareynta faallooyinka",
    "审核通过" => "imtixaan baa maray",
    "机器评论" => "dib u eegista mashiinka",
    "系统管理" => "Maamulka Nidaamka",
    "系统设置" => "Dejinta nidaamka",
    "保存设置" => "Keydi Settings",
    "恢复出厂" => "Soo Celinta Warshada",
    "操作日志" => "Diiwaanka hawlgalka",
    "删除日志" => "Tirtir qoraalka",
    "科西嘉语" => "Korsican",
    "瓜拉尼语" => "Guarani",
    "卢旺达语" => "Kinyarwanda",
    "约鲁巴语" => "yoruba",
    "尼泊尔语" => "Nepali",
    "夏威夷语" => "Hawaawaan",
    "意第绪语" => "Yidish",
    "爱尔兰语" => "iriish",
    "希伯来语" => "cibraan",
    "卡纳达语" => "kannada",
    "匈牙利语" => "hunguri",
    "泰米尔语" => "tamil",
    "阿拉伯语" => "Carabi",
    "孟加拉语" => "Bengali",
    "萨摩亚语" => "Samoan",
    "班巴拉语" => "Bambara",
    "立陶宛语" => "Lithuanian",
    "马耳他语" => "Malta",
    "土库曼语" => "Turkmen",
    "阿萨姆语" => "Assamese",
    "僧伽罗语" => "sinhala",
    "乌克兰语" => "Yukreeniyaan",
    "威尔士语" => "Welsh",
    "菲律宾语" => "Filibiin",
    "艾马拉语" => "Aymara",
    "泰卢固语" => "telugu",
    "多格来语" => "dogglai",
    "迈蒂利语" => "Maithili",
    "普什图语" => "Pashto",
    "迪维希语" => "Dhivehi",
    "卢森堡语" => "Luxembourgish",
    "土耳其语" => "turkiga",
    "马其顿语" => "Masedooniyaan",
    "卢干达语" => "Luganda",
    "马拉地语" => "marathi",
    "乌尔都语" => "Urduu",
    "葡萄牙语" => "Boortaqiis",
    "奥罗莫语" => "Oromo",
    "西班牙语" => "isbaanish",
    "弗里西语" => "Frisian",
    "索马里语" => "Soomaali",
    "齐切瓦语" => "Chichewa",
    "旁遮普语" => "punjabi",
    "巴斯克语" => "basque",
    "意大利语" => "Talyaani",
    "塔吉克语" => "Taajik",
    "克丘亚语" => "Quechua",
    "奥利亚语" => "Ooriya",
    "哈萨克语" => "Kazakh",
    "林格拉语" => "Lingala",
    "塞佩蒂语" => "Sepeti",
    "塞索托语" => "Sesotho",
    "维吾尔语" => "Uighur",
    "ICP No.001" => "ICP No.001",
    "用户名" => "username",
    "验证码" => "Koodhka xaqiijinta",
    "记住我" => "i xasuuso",
    "手机号" => "Lambarka taleefanka",
    "请输入" => "fadlan gal",
    "请选择" => "fadlan dooro",
    "浏览量" => "Muuqaalo",
    "回收站" => "qashinka dib loo warshadeeyo",
    "菜单树" => "Geedka menu",
    "控制器" => "xakameeye",
    "第一页" => "Bogga koowaad",
    "最后页" => "bogga ugu dambeeya",
    "筛选列" => "Tiirarka shaandhaynta",
    "删除行" => "Tirtir safka",
    "还原行" => "Ka noqo safka",
    "重命名" => "Magacaabista",
    "国际化" => "caalamiyaynta",
    "文章数" => "Tirada maqaallada",
    "机器人" => "robot",
    "星期日" => "Axad",
    "星期一" => "Isniin",
    "星期二" => "Talaado",
    "星期三" => "Arbacada",
    "星期四" => "Khamiis",
    "星期五" => "Jimcaha",
    "星期六" => "Sabti",
    "会员数" => "Tirada xubnaha",
    "评论数" => "Tirada faallooyinka",
    "文件名" => "Magaca faylka",
    "音视频" => "Maqal iyo muuqaal",
    "扩展名" => "magaca kordhinta",
    "未使用" => "Aan la isticmaalin",
    "个无效" => "aan sax ahayn",
    "共删除" => "Wadarta waa la tirtiray",
    "个文件" => "faylasha",
    "备案号" => "nambar kiis",
    "轮播图" => "carousel",
    "条显示" => "bandhig baar",
    "上一页" => "Boggii hore",
    "下一页" => "Bogga xiga",
    "未分类" => "aan la kala saarin",
    "空标签" => "calaamad maran",
    "无标题" => "Aan cinwaan lahayn",
    "控制台" => "console",
    "登录中" => "galitaanka",
    "注册中" => "Diiwaangelinta",
    "跳转中" => "Jihaynta",
    "提交中" => "soo gudbin",
    "审核中" => "hoos u eegista",
    "请稍后" => "Fadlan sug",
    "篇文章" => "Maqaal",
    "客户端" => "macmiilka",
    "没有了" => "midna ma hadhin",
    "后评论" => "comments post",
    "未命名" => "aan la magacaabin",
    "软删除" => "tirtir jilicsan",
    "语言包" => "Xidhmada luqadda",
    "豪萨语" => "Hausa",
    "挪威语" => "norwiiji",
    "贡根语" => "luqadda gonggan",
    "拉丁语" => "laatiinka",
    "捷克语" => "Czech",
    "波斯语" => "Faaris",
    "印地语" => "Hindi",
    "冰岛语" => "Iceland",
    "契维语" => "Twi",
    "高棉语" => "Kamboodiya",
    "丹麦语" => "deenishka",
    "修纳语" => "Shona",
    "越南语" => "Vietnamese",
    "宿务语" => "Cebuano",
    "波兰语" => "Polish",
    "鞑靼语" => "Tataarka",
    "老挝语" => "Lao",
    "瑞典语" => "Iswidish",
    "缅甸语" => "Burma",
    "信德语" => "sindhi",
    "马来语" => "Malaay",
    "伊博语" => "Igbo",
    "希腊语" => "Giriig",
    "芬兰语" => "Finnish",
    "埃维语" => "Ewe",
    "毛利语" => "Maori",
    "荷兰语" => "Dutch",
    "蒙古语" => "Mongoliyaan",
    "米佐语" => "Mizo",
    "世界语" => "Esperanto",
    "印尼语" => "Indonesian",
    "宗加语" => "Dzonga",
    "密码" => "erayga sirta ah",
    "登录" => "Soo gal",
    "重置" => "dib u habeyn",
    "信息" => "xog",
    "成功" => "guul",
    "失败" => "fashilanto",
    "昵称" => "Magaca Nick",
    "选填" => "Ikhtiyaari",
    "注册" => "diwaangeli",
    "错误" => "qalad",
    "头像" => "avatar",
    "上传" => "ku shubid",
    "保存" => "badbaadi",
    "标题" => "horyaalka",
    "别名" => "Magac loo yaqaan",
    "描述" => "qeexid",
    "封面" => "daboolid",
    "分类" => "Kala soocidda",
    "状态" => "gobol",
    "正常" => "caadi",
    "禁用" => "Hawl gab",
    "作者" => "qoraa",
    "日期" => "taariikhda",
    "添加" => "Ku dar",
    "刷新" => "dib u cusboonaysii",
    "标签" => "Summada",
    "浏览" => "baadh",
    "点赞" => "sida",
    "评论" => "Faallo",
    "操作" => "hawlgeli",
    "阅读" => "akhri",
    "编辑" => "wax ka beddel",
    "删除" => "tirtirto",
    "翻译" => "turjun",
    "内容" => "nuxurka",
    "分割" => "kala qaybsanaan",
    "建议" => "soo jeedin",
    "非法" => "sharci darro ah",
    "还原" => "dhimis",
    "方法" => "habka",
    "排序" => "kala soocid",
    "图标" => "icon",
    "菜单" => "menu",
    "时间" => "waqti",
    "匿名" => "qarsoodi",
    "确定" => "Hubaal",
    "取消" => "Jooji",
    "跳转" => "Bood",
    "页码" => "lambarka bogga",
    "共计" => "wadar ahaan",
    "每页" => "boggiiba",
    "导出" => "Dhoofinta",
    "打印" => "Daabac",
    "回复" => "jawaab",
    "路径" => "waddo",
    "预览" => "Horudhac",
    "名称" => "magac",
    "行数" => "Saf",
    "相册" => "album sawir",
    "文章" => "maqaal",
    "待审" => "La sugayo",
    "通过" => "dhaaf",
    "垃圾" => "Qashinka",
    "提取" => "soosaarid",
    "生成" => "dhalin",
    "定位" => "booska",
    "填充" => "buuxin",
    "帮助" => "I caawi",
    "返回" => "soo noqod",
    "语言" => "luqadda",
    "宽屏" => "muraayad ballaadhan",
    "普通" => "caadiga ah",
    "后台" => "Marxaladda dambe",
    "前台" => "miiska hore",
    "登出" => "Saxiix",
    "版本" => "Nooca",
    "关于" => "ku saabsan",
    "微信" => "WeChat",
    "会员" => "xubin",
    "地址" => "ciwaanka",
    "类型" => "nooca",
    "图片" => "sawir",
    "文件" => "dukumeenti",
    "失效" => "Aan sax ahayn",
    "大小" => "cabbirka",
    "下载" => "soo dejiso",
    "导航" => "navigation",
    "站标" => "Summada goobta",
    "单位" => "unug",
    "加速" => "dedejiso",
    "角色" => "Doorka",
    "首页" => "bogga hore",
    "个数" => "tirada",
    "我的" => "anigaa iska leh",
    "说道" => "ayuu yidhi",
    "手机" => "telefoonka gacanta",
    "可选" => "Ikhtiyaari",
    "搜索" => "raadi",
    "页面" => "bogga",
    "查看" => "Hubi",
    "创建" => "abuurto",
    "更新" => "cusbooneysiin",
    "英语" => "Ingiriis",
    "法语" => "Faransiis",
    "俄语" => "Ruush",
    "梵语" => "Sanskrit",
    "日语" => "Jabbaan",
    "泰语" => "Thai",
    "苗语" => "Hmong",
    "德语" => "Jarmal",
    "韩语" => "Kuuriyaan",
    "请" => "fadlan",
];
