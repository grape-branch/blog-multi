<?php
/**
 * @author admin
 * @date 2024-06-06 05:03:57
 * @desc 梅泰语（曼尼普尔语）语言包
 */
return [
    "迷你mvc小框架，一套用于开发各类管理系统的通用型后台管理系统模板，基于开源免费的 layui 的制作，内置各类实际业务场景下相对丰富的示例。" => "ꯃꯤꯅꯤ ꯑꯦꯝ.ꯚꯤ.ꯁꯤ.ꯅꯥ ꯑꯄꯤꯀꯄꯥ ꯐ꯭ꯔꯦꯃꯋꯥꯔꯛ ꯑꯃꯅꯤ, ꯃꯁꯤ ꯇꯣꯉꯥꯟ ꯇꯣꯉꯥꯅꯕꯥ ꯃꯦꯅꯦꯖꯃꯦꯟꯇ ꯁꯤꯁ꯭ꯇꯦꯃꯁꯤꯡ ꯁꯦꯃꯒꯠꯅꯕꯥ ꯌꯨꯅꯤꯚꯔꯁꯦꯜ ꯕꯦꯀꯑꯦꯟꯗ ꯃꯦꯅꯦꯖꯃꯦꯟꯇ ꯁꯤꯁ꯭ꯇꯦꯝ ꯇꯦꯃꯞꯂꯦꯠꯁꯤꯡꯒꯤ ꯁꯦꯠ ꯑꯃꯅꯤ꯫",
    "除非特别说明，本博客均为原创，如需转载，请以链接形式标明来源出处。" => "ꯑꯇꯣꯞꯄꯥ ꯃꯑꯣꯡ ꯑꯃꯗꯥ ꯍꯥꯌꯗ꯭ꯔꯕꯗꯤ, ꯕ꯭ꯂꯣꯒ ꯑꯁꯤ ꯑꯔꯤꯖꯤꯅꯦꯂꯅꯤ, ꯀꯔꯤꯒꯨꯝꯕꯥ ꯅꯍꯥꯛꯅꯥ ꯑꯃꯨꯛ ꯍꯟꯅꯥ ꯐꯣꯡꯗꯣꯀꯄꯥ ꯃꯊꯧ ꯇꯥꯔꯕꯗꯤ, ꯆꯥꯅꯕꯤꯗꯨꯅꯥ ꯂꯤꯉ꯭ꯛ ꯑꯃꯒꯤ ꯃꯑꯣꯡꯗꯥ ꯁꯣꯔꯁ ꯑꯗꯨ ꯇꯥꯀꯄꯤꯌꯨ꯫",
    "重置系统，将删除用户所有数据和附件，恢复到出厂设置。" => "ꯁꯤꯁ꯭ꯇꯦꯝ ꯑꯁꯤ ꯔꯤꯁꯦꯠ ꯇꯧꯕꯅꯥ ꯌꯨꯖꯔꯒꯤ ꯗꯦꯇꯥ ꯑꯃꯁꯨꯡ ꯑꯦꯇꯦꯆꯃꯦꯟꯇ ꯄꯨꯝꯅꯃꯛ ꯃꯨꯠꯊꯠꯀꯅꯤ ꯑꯃꯁꯨꯡ ꯐꯦꯛꯇꯔꯤꯒꯤ ꯁꯦꯇꯤꯡꯁꯤꯡꯗꯥ ꯑꯃꯨꯛ ꯍꯟꯅꯥ ꯄꯨꯔꯛꯀꯅꯤ꯫",
    "用户名只能英文字母数字下划线或中划线，位数" => "ꯌꯨꯖꯔꯅꯦꯝ ꯑꯁꯤꯗꯥ ꯏꯪꯂꯤꯁꯀꯤ ꯑꯛꯁꯔ, ꯅꯝꯕꯔ, ꯑꯟꯗꯔꯁ꯭ꯀꯣꯔ ꯅꯠꯔꯒꯥ ꯑꯟꯗꯔꯁ꯭ꯀꯣꯔ, ꯑꯃꯁꯨꯡ ꯗꯤꯖꯤꯠꯀꯤ ꯃꯁꯤꯡ ꯈꯛꯇꯃꯛ ꯌꯥꯑꯣꯕꯥ ꯌꯥꯏ꯫",
    "昵称只能中英文字母数字下划线中划线，位数" => "ꯅꯤꯀꯅꯦꯝ ꯑꯁꯤꯗꯥ ꯆꯥꯏꯅꯥ ꯑꯃꯁꯨꯡ ꯏꯪꯂꯤꯁꯀꯤ ꯑꯛꯁꯔ, ꯅꯝꯕꯔ, ꯑꯟꯗꯔꯁ꯭ꯀꯣꯔ, ꯑꯟꯗꯔꯁ꯭ꯀꯣꯔ, ꯑꯃꯁꯨꯡ ꯗꯤꯖꯤꯠ ꯈꯛꯇꯃꯛ ꯌꯥꯑꯣꯕꯥ ꯌꯥꯏ꯫",
    "有道接口不支持，需科学上网使用谷歌接口。" => "Youdao ꯏꯟꯇꯔꯐꯦꯁ ꯑꯁꯤ ꯁꯄꯣꯔꯠ ꯇꯧꯗꯦ ꯅꯍꯥꯛꯅꯥ ꯏꯟꯇꯔꯅꯦꯠ ꯑꯁꯤ ꯁꯥꯏꯟꯇꯤꯐꯤꯛ ꯑꯣꯏꯅꯥ ꯑꯦꯛꯁꯦꯁ ꯇꯧꯅꯕꯒꯤꯗꯃꯛ ꯒꯨꯒꯜ ꯏꯟꯇꯔꯐꯦꯁ ꯁꯤꯖꯤꯟꯅꯕꯥ ꯃꯊꯧ ꯇꯥꯏ꯫",
    "别名只能英文字母数字下划线中划线，位数" => "ꯑꯦꯂꯤꯌꯥꯁꯁꯤꯡꯗꯥ ꯏꯪꯂꯤꯁꯀꯤ ꯑꯛꯁꯔ, ꯅꯝꯕꯔ, ꯑꯟꯗꯔꯁ꯭ꯀꯣꯔ, ꯑꯟꯗꯔꯁ꯭ꯀꯣꯔ, ꯑꯃꯁꯨꯡ ꯗꯤꯖꯤꯠ ꯈꯛꯇꯃꯛ ꯌꯥꯑꯣꯕꯥ ꯌꯥꯏ꯫",
    "计算孤立文件需要较长时间，确定继续吗？" => "ꯑꯉꯥꯡ ꯑꯣꯏꯔꯤꯉꯩꯒꯤ ꯐꯥꯏꯂꯁꯤꯡ ꯂꯦꯄꯊꯣꯀꯄꯗꯥ ꯃꯇꯝ ꯁꯥꯡꯅꯥ ꯆꯪꯏ ꯅꯍꯥꯛꯅꯥ ꯃꯈꯥ ꯆꯠꯊꯕꯥ ꯄꯥꯝꯂꯤꯕꯔꯥ?",
    "语音不支持，需科学上网使用谷歌接口。" => "ꯚꯣꯏꯁ ꯑꯁꯤ ꯁꯄꯣꯔꯠ ꯇꯧꯗꯦ, ꯃꯔꯝ ꯑꯗꯨꯅꯥ ꯅꯍꯥꯛꯅꯥ ꯏꯟꯇꯔꯅꯦꯠ ꯑꯁꯤ ꯁꯥꯏꯟꯇꯤꯐꯤꯛ ꯑꯣꯏꯅꯥ ꯑꯦꯛꯁꯦꯁ ꯇꯧꯅꯕꯒꯤꯗꯃꯛ ꯒꯨꯒꯜ ꯏꯟꯇꯔꯐꯦꯁ ꯁꯤꯖꯤꯟꯅꯕꯥ ꯃꯊꯧ ꯇꯥꯏ꯫",
    "您的账号已在别处登录，请重新登录！" => "ꯅꯍꯥꯛꯀꯤ ꯑꯦꯀꯥꯎꯟꯇ ꯑꯗꯨ ꯑꯇꯣꯞꯄꯥ ꯃꯐꯝ ꯑꯃꯗꯥ ꯂꯣꯒ ꯏꯟ ꯇꯧꯈ꯭ꯔꯦ, ꯆꯥꯅꯕꯤꯗꯨꯅꯥ ꯑꯃꯨꯛ ꯍꯟꯅꯥ ꯂꯣꯒ ꯏꯟ ꯇꯧꯕꯤꯌꯨ!",
    "你的账号已被禁用，请联系管理员" => "ꯅꯍꯥꯛꯀꯤ ꯑꯦꯀꯥꯎꯟꯇ ꯑꯗꯨ ꯗꯤꯁꯦꯕꯜ ꯇꯧꯔꯦ, ꯆꯥꯅꯕꯤꯗꯨꯅꯥ ꯑꯦꯗꯃꯤꯅꯤꯁ꯭ꯠꯔꯦꯇꯔꯒꯥ ꯄꯥꯎ ꯐꯥꯎꯅꯕꯤꯌꯨ꯫",
    "文件有在使用中，请重新计算状态" => "ꯐꯥꯏꯜ ꯑꯁꯤ ꯁꯤꯖꯤꯟꯅꯔꯤ, ꯆꯥꯅꯕꯤꯗꯨꯅꯥ ꯁ꯭ꯇꯦꯇꯁ ꯑꯗꯨ ꯑꯃꯨꯛ ꯍꯟꯅꯥ ꯂꯦꯄꯊꯣꯀꯄꯤꯌꯨ",
    "目标语言权限不够，请联系管理员" => "ꯇꯥꯔꯒꯦꯠ ꯂꯣꯂꯒꯤ ꯑꯌꯥꯕꯥ ꯃꯔꯥꯡ ꯀꯥꯌꯅꯥ ꯂꯩꯇꯕꯅꯥ, ꯆꯥꯅꯕꯤꯗꯨꯅꯥ ꯑꯦꯗꯃꯤꯅꯤꯁ꯭ꯠꯔꯦꯇꯔꯒꯥ ꯄꯥꯎ ꯐꯥꯎꯅꯕꯤꯌꯨ꯫",
    "真的标记垃圾评论选中的行吗？" => "ꯅꯍꯥꯛꯅꯥ ꯇꯁꯦꯡꯅꯥ ꯈꯅꯒꯠꯂꯕꯥ ꯂꯥꯏꯅꯁꯤꯡ ꯑꯗꯨ ꯁ꯭ꯄꯦꯝ ꯑꯣꯏꯅꯥ ꯃꯥꯔꯛ ꯇꯧꯕ꯭ꯔꯥ?",
    "源语言别名为空，请联系源作者" => "ꯁꯣꯔꯁ ꯂꯦꯉ꯭ꯒꯨꯌꯦꯖ ꯑꯦꯂꯤꯌꯥꯁ ꯑꯁꯤ ꯏꯝꯄꯣꯔꯠ ꯑꯣꯏꯔꯦ, ꯆꯥꯅꯕꯤꯗꯨꯅꯥ ꯁꯣꯔꯁ ꯑꯣꯊꯣꯔ ꯑꯗꯨꯒꯥ ꯄꯥꯎ ꯐꯥꯑꯣꯅꯕꯤꯌꯨ꯫",
    "点击上传，或将文件拖拽到此处" => "ꯑꯄꯂꯣꯗ ꯇꯧꯅꯕꯥ ꯀ꯭ꯂꯤꯛ ꯇꯧꯔꯣ, ꯅꯠꯔꯒꯥ ꯐꯥꯏꯜ ꯑꯗꯨ ꯃꯐꯃꯁꯤꯗꯥ ꯂꯧꯊꯣꯀꯎ꯫",
    "合成成功！再次点击按钮下载。" => "ꯁꯤꯟꯊꯦꯁꯤꯁ ꯃꯥꯌ ꯄꯥꯛꯂꯦ! ꯗꯥꯎꯅꯂꯣꯗ ꯇꯧꯅꯕꯥ ꯕꯇꯟ ꯑꯗꯨ ꯑꯃꯨꯛ ꯍꯟꯅꯥ ꯀ꯭ꯂꯤꯛ ꯇꯧꯔꯣ꯫",
    "没有语言权限，请联系管理员" => "ꯂꯣꯂꯒꯤ ꯑꯌꯥꯕꯥ ꯑꯃꯠꯇꯥ ꯂꯩꯇꯦ, ꯆꯥꯅꯕꯤꯗꯨꯅꯥ ꯑꯦꯗꯃꯤꯅꯤꯁ꯭ꯠꯔꯦꯇꯔꯒꯥ ꯄꯥꯎ ꯐꯥꯑꯣꯅꯕꯤꯌꯨ꯫",
    "源语言别名为空，不能国际化" => "ꯁꯣꯔꯁ ꯂꯦꯉ꯭ꯒꯨꯌꯦꯖ ꯑꯦꯂꯤꯌꯥꯁ ꯑꯁꯤ ꯏꯝꯄꯣꯔꯠ ꯑꯣꯏ ꯑꯃꯁꯨꯡ ꯏꯟꯇꯔꯅꯦꯁ꯭ꯅꯦꯂꯥꯏꯖ ꯇꯧꯕꯥ ꯌꯥꯗꯦ꯫",
    "源语言与目标语言都不能为空" => "ꯁꯣꯔꯁ ꯂꯦꯉ꯭ꯒꯨꯌꯦꯖ ꯅꯠꯔꯒꯥ ꯇꯥꯔꯒꯦꯠ ꯂꯦꯉ꯭ꯒꯨꯌꯦꯖ ꯑꯃꯠꯇꯥ ꯏꯪꯅꯥ ꯂꯩꯕꯥ ꯌꯥꯔꯣꯏ꯫",
    "源语言与目标语言数量不一致" => "ꯁꯣꯔꯁ ꯑꯃꯁꯨꯡ ꯇꯥꯔꯒꯦꯠ ꯂꯣꯂꯁꯤꯡꯒꯤ ꯃꯁꯤꯡ ꯑꯁꯤ ꯌꯥꯝꯅꯥ ꯃꯥꯟꯅꯗꯦ꯫",
    "删除文件失败，文件有在使用" => "ꯐꯥꯏꯜ ꯃꯨꯠꯊꯠꯄꯥ ꯉꯃꯗꯦ, ꯐꯥꯏꯜ ꯑꯁꯤ ꯁꯤꯖꯤꯟꯅꯔꯤ꯫",
    "角色不存在，请联系管理员" => "ꯊꯧꯗꯥꯡ ꯑꯁꯤ ꯂꯩꯇꯦ, ꯆꯥꯅꯕꯤꯗꯨꯅꯥ ꯑꯦꯗꯃꯤꯅꯤꯁ꯭ꯠꯔꯦꯇꯔꯒꯥ ꯄꯥꯎ ꯐꯥꯑꯣꯅꯕꯤꯌꯨ꯫",
    "角色被禁用，请联系管理员" => "ꯔꯣꯜ ꯑꯁꯤ ꯗꯤꯁꯦꯕꯜ ꯑꯣꯏꯔꯦ, ꯆꯥꯅꯕꯤꯗꯨꯅꯥ ꯑꯦꯗꯃꯤꯅꯤꯁ꯭ꯠꯔꯦꯇꯔꯒꯥ ꯄꯥꯎ ꯐꯥꯑꯣꯅꯕꯤꯌꯨ꯫",
    "功能不存在，请联系管理员" => "ꯐꯉ꯭ꯀꯁꯟ ꯂꯩꯇꯦ, ꯆꯥꯅꯕꯤꯗꯨꯅꯥ ꯑꯦꯗꯃꯤꯅꯤꯁ꯭ꯠꯔꯦꯇꯔꯒꯥ ꯄꯥꯎ ꯐꯥꯑꯣꯅꯕꯤꯌꯨ꯫",
    "功能被禁用，请联系管理员" => "ꯐꯉ꯭ꯀꯁꯟ ꯑꯁꯤ ꯗꯤꯁꯦꯕꯜ ꯇꯧꯔꯦ, ꯆꯥꯅꯕꯤꯗꯨꯅꯥ ꯑꯦꯗꯃꯤꯅꯤꯁ꯭ꯠꯔꯦꯇꯔꯒꯥ ꯄꯥꯎ ꯐꯥꯑꯣꯅꯕꯤꯌꯨ꯫",
    "真的彻底删除选中的行吗？" => "ꯃꯁꯤꯅꯥ ꯇꯁꯦꯡꯅꯥ ꯈꯅꯒꯠꯂꯕꯥ ꯔꯣ ꯑꯗꯨ ꯃꯄꯨꯡ ꯐꯥꯅꯥ ꯃꯨꯠꯊꯠꯀꯗ꯭ꯔꯥ?",
    "真的审核通过选中的行吗？" => "ꯈꯅꯒꯠꯂꯕꯥ ꯔꯣꯁꯤꯡ ꯑꯁꯤ ꯇꯁꯦꯡꯅꯥ ꯑꯌꯥꯕꯥ ꯄꯤꯕꯔꯥ?",
    "目标语言与源语言不能相同" => "ꯇꯥꯔꯒꯦꯠ ꯂꯣꯜ ꯑꯁꯤ ꯁꯣꯔꯁ ꯂꯣꯂꯒꯥ ꯃꯥꯟꯅꯕꯥ ꯑꯣꯏꯕꯥ ꯌꯥꯔꯣꯏ꯫",
    "提取语言包与源语言不一致" => "ꯑꯦꯛꯁꯠꯔꯥꯛꯇꯦꯗ ꯂꯦꯉ꯭ꯒꯨꯌꯦꯖ ꯄꯦꯀꯦꯖ ꯑꯁꯤ ꯁꯣꯔꯁ ꯂꯦꯉ꯭ꯒꯨꯌꯦꯖꯒꯥ ꯃꯥꯟꯅꯗꯦ꯫",
    "文件有在使用，删除失败！" => "ꯐꯥꯏꯜ ꯑꯗꯨ ꯁꯤꯖꯤꯟꯅꯔꯤ ꯑꯃꯁꯨꯡ ꯃꯨꯠꯊꯠꯄꯥ ꯉꯃꯗꯦ!",
    "没有权限，请联系管理员" => "ꯑꯌꯥꯕꯥ ꯑꯃꯠꯇꯥ ꯂꯩꯇꯦ, ꯆꯥꯅꯕꯤꯗꯨꯅꯥ ꯑꯦꯗꯃꯤꯅꯤꯁ꯭ꯠꯔꯦꯇꯔꯒꯥ ꯄꯥꯎ ꯐꯥꯑꯣꯅꯕꯤꯌꯨ꯫",
    "真的待审核选中的行吗？" => "ꯃꯁꯤ ꯇꯁꯦꯡꯅꯥ ꯔꯤꯚꯤꯌꯨ ꯇꯧꯕꯥ ꯑꯃꯁꯨꯡ ꯈꯅꯒꯠꯄꯥ ꯃꯊꯧ ꯇꯥꯕꯥ ꯂꯥꯏꯟ ꯑꯃꯅꯤ ꯍꯥꯌꯕꯔꯥ?",
    "目标语言包有空行，行数" => "ꯇꯥꯔꯒꯦꯠ ꯂꯦꯉ꯭ꯒꯨꯌꯦꯖ ꯄꯦꯀꯦꯖ ꯑꯁꯤꯗꯥ ꯕ꯭ꯂꯦꯉ꯭ꯛ ꯂꯥꯏꯅꯁꯤꯡ ꯂꯩ, ꯂꯥꯏꯅꯁꯤꯡꯒꯤ ꯃꯁꯤꯡ꯫",
    "此操作恢复到出厂设置？" => "ꯃꯁꯤꯒꯤ ꯑꯣꯄꯔꯦꯁꯟ ꯑꯁꯤꯅꯥ ꯐꯦꯛꯇꯔꯤꯒꯤ ꯁꯦꯇꯤꯡꯁꯤꯡꯗꯥ ꯑꯃꯨꯛ ꯍꯟꯅꯥ ꯁꯦꯃꯒꯠꯂꯤꯕꯔꯥ?",
    "朗读错误，请联网后再试" => "ꯄꯥꯕꯒꯤ ꯑꯁꯣꯌꯕꯥ ꯂꯩꯔꯦ, ꯆꯥꯅꯕꯤꯗꯨꯅꯥ ꯏꯟꯇꯔꯅꯦꯇꯇꯥ ꯀꯅꯦꯛꯇ ꯇꯧꯕꯤꯌꯨ ꯑꯃꯁꯨꯡ ꯑꯃꯨꯛ ꯍꯟꯅꯥ ꯍꯣꯠꯅꯕꯤꯌꯨ꯫",
    "还没有添加分类描述信息" => "ꯀꯦꯇꯦꯒꯣꯔꯤꯒꯤ ꯗꯤꯁ꯭ꯛꯔꯤꯄꯁꯅꯒꯤ ꯏꯅꯐꯣꯔꯃꯦꯁꯟ ꯑꯃꯠꯇꯥ ꯍꯧꯖꯤꯛ ꯐꯥꯎꯕꯥ ꯍꯥꯄꯆꯤꯅꯗ꯭ꯔꯤ꯫",
    "本地媒体已失效或不存在" => "ꯂꯣꯀꯦꯜ ꯃꯤꯗꯤꯌꯥ ꯑꯁꯤ ꯆꯠꯅꯗꯦ ꯅꯠꯔꯒꯥ ꯂꯩꯇꯦ꯫",
    "库尔德语（库尔曼吉语）" => "ꯀꯨꯔꯃꯥꯟꯖꯤ (ꯀꯨꯔꯃꯥꯟꯖꯤ)",
    "作者名未注册或被禁用" => "ꯑꯣꯊꯣꯔꯒꯤ ꯃꯃꯤꯡ ꯔꯦꯖꯤꯁ꯭ꯇꯔ ꯇꯧꯔꯕꯥ ꯅꯠꯔꯒꯥ ꯗꯤꯁꯦꯕꯜ ꯇꯧꯗ꯭ꯔꯤ꯫",
    "真的删除选中的行吗？" => "ꯅꯍꯥꯛꯅꯥ ꯈꯅꯒꯠꯂꯕꯥ ꯔꯣ ꯑꯗꯨ ꯇꯁꯦꯡꯅꯥ ꯃꯨꯠꯊꯠꯂꯤꯕ꯭ꯔꯥ?",
    "真的还原选中的行吗？" => "ꯅꯍꯥꯛꯅꯥ ꯈꯅꯒꯠꯂꯕꯥ ꯔꯣꯁꯤꯡ ꯑꯗꯨ ꯇꯁꯦꯡꯅꯥ ꯔꯤꯁ꯭ꯇꯣꯔ ꯇꯧꯕ꯭ꯔꯥ?",
    "真的删除行或子行么？" => "ꯑꯗꯣꯝꯅꯥ ꯇꯁꯦꯡꯅꯥ ꯔꯣ ꯅꯠꯔꯒꯥ ꯁꯕꯔꯣ ꯃꯨꯠꯊꯠꯄꯥ ꯄꯥꯝꯕꯤꯕꯔꯥ?",
    "目标语言包未发现空行" => "ꯇꯥꯔꯒꯦꯠ ꯂꯦꯉ꯭ꯒꯨꯌꯦꯖ ꯄꯦꯀꯇꯥ ꯏꯝꯄꯣꯔꯠ ꯂꯥꯏꯟ ꯐꯪꯗꯦ꯫",
    "该语言不支持语音朗读" => "ꯂꯣꯜ ꯑꯁꯤꯅꯥ ꯚꯣꯏꯁ ꯔꯤꯗꯤꯡꯗꯥ ꯃꯇꯦꯡ ꯄꯥꯡꯗꯦ꯫",
    "语言包数据未发生改变" => "ꯂꯦꯉ꯭ꯒꯨꯌꯦꯖ ꯄꯦꯛ ꯗꯦꯇꯥꯗꯥ ꯑꯍꯣꯡꯕꯥ ꯑꯃꯠꯇꯥ ꯂꯩꯇꯦ꯫",
    "欢迎使用后台管理系统" => "ꯕꯦꯀꯑꯦꯟꯗ ꯃꯦꯅꯦꯖꯃꯦꯟꯇ ꯁꯤꯁ꯭ꯇꯦꯃꯗꯥ ꯇꯔꯥꯝꯅꯥ ꯑꯣꯀꯆꯔꯤ꯫",
    "文件有在使用或已失效" => "ꯐꯥꯏꯜ ꯑꯗꯨ ꯁꯤꯖꯤꯟꯅꯔꯤ ꯅꯠꯠꯔꯒꯥ ꯃꯇꯝ ꯂꯣꯏꯔꯦ꯫",
    "合成失败，请稍后再试" => "ꯁꯤꯟꯊꯦꯁꯤꯁ ꯇꯧꯕꯥ ꯉꯃꯗꯦ, ꯆꯥꯅꯕꯤꯗꯨꯅꯥ ꯃꯇꯨꯡꯗꯥ ꯑꯃꯨꯛ ꯍꯟꯅꯥ ꯍꯣꯠꯅꯕꯤꯌꯨ꯫",
    "&copy; 2021-2023 Company, Inc." => "© ꯲꯰꯲꯱-꯲꯰꯲꯳ ꯀꯝꯄꯦꯅꯤ, ꯏꯅꯛ.",
    "梅泰语（曼尼普尔语）" => "ꯃꯦꯏꯇꯦꯏ (ꯃꯅꯤꯄꯨꯔꯤ) ꯴.",
    "两次密码输入不一致" => "ꯄꯥꯁꯋꯥꯔꯗ ꯏꯅꯄꯨꯠ ꯑꯅꯤ ꯑꯁꯤ ꯌꯥꯝꯅꯥ ꯃꯥꯟꯅꯗꯦ꯫",
    "该用户密码不可修改" => "ꯌꯨꯖꯔ ꯄꯥꯁꯋꯥꯔꯗ ꯑꯁꯤ ꯃꯣꯗꯤꯐꯥꯏ ꯇꯧꯕꯥ ꯌꯥꯗꯦ꯫",
    "添加文章时创建标签" => "ꯑꯥꯔꯇꯤꯀꯂꯁꯤꯡ ꯍꯥꯄꯆꯤꯅꯕꯥ ꯃꯇꯃꯗꯥ ꯇꯦꯒꯁꯤꯡ ꯁꯦꯝꯃꯨ꯫",
    "删除文章时删除评论" => "ꯑꯥꯔꯇꯤꯀꯜ ꯑꯃꯥ ꯃꯨꯠꯊꯠꯄꯥ ꯃꯇꯃꯗꯥ ꯀꯃꯦꯟꯇꯁꯤꯡ ꯃꯨꯠꯊꯠꯄꯥ꯫",
    "控制器与方法已存在" => "ꯀꯟꯠꯔꯣꯂꯔ ꯑꯃꯁꯨꯡ ꯃꯦꯊꯗ ꯑꯁꯤ ꯍꯥꯟꯅꯅꯥ ꯂꯩꯔꯦ꯫",
    "标题或名称不能为空" => "ꯇꯥꯏꯇꯜ ꯅꯠꯔꯒꯥ ꯃꯤꯡ ꯑꯁꯤ ꯏꯝꯄꯣꯔꯠ ꯇꯧꯕꯥ ꯌꯥꯔꯣꯏ꯫",
    "请选择一种语言朗读" => "ꯆꯥꯅꯕꯤꯗꯨꯅꯥ ꯈꯣꯟꯖꯦꯜ ꯊꯣꯛꯅꯥ ꯄꯥꯕꯥ ꯂꯣꯜ ꯑꯃꯥ ꯈꯅꯕꯤꯌꯨ꯫",
    "分类有文章不能删除" => "ꯀꯦꯇꯦꯒꯣꯔꯤ ꯑꯁꯤꯗꯥ ꯃꯨꯠꯊꯠꯄꯥ ꯌꯥꯗꯕꯥ ꯑꯥꯔꯇꯤꯀꯂꯁꯤꯡ ꯂꯩꯔꯤ꯫",
    "审核为垃圾评论成功" => "ꯁ꯭ꯄꯦꯝ ꯑꯣꯏꯅꯥ ꯃꯥꯌ ꯄꯥꯛꯅꯥ ꯔꯤꯚꯤꯌꯨ ꯇꯧꯈꯤ꯫",
    "审核为垃圾评论失败" => "ꯁ꯭ꯄꯦꯝ ꯀꯃꯦꯟꯇꯀꯤꯗꯃꯛ ꯔꯤꯚꯤꯌꯨ ꯇꯧꯕꯥ ꯉꯃꯈꯤꯗꯦ꯫",
    "确定要登出站点吗？" => "ꯅꯍꯥꯛꯅꯥ ꯁꯥꯏꯠ ꯑꯁꯤꯗꯒꯤ ꯂꯣꯒ ꯑꯥꯎꯠ ꯇꯧꯕꯥ ꯄꯥꯝꯕꯤꯕ꯭ꯔꯥ?",
    "网站名称或地址为空" => "ꯋꯦꯕꯁꯥꯏꯇꯀꯤ ꯃꯃꯤꯡ ꯅꯠꯠꯔꯒꯥ ꯑꯦꯗ꯭ꯔꯦꯁ ꯑꯗꯨ ꯑꯍꯥꯡꯕꯥ ꯑꯣꯏꯔꯦ꯫",
    "网站名称或地址重名" => "ꯋꯦꯕꯁꯥꯏꯇꯀꯤ ꯃꯃꯤꯡ ꯅꯠꯠꯔꯒꯥ ꯑꯦꯗ꯭ꯔꯦꯁ ꯑꯗꯨ ꯑꯅꯤꯔꯛ ꯁꯨꯕꯥ꯫",
    "允许上传的文件类型" => "ꯑꯄꯂꯣꯗ ꯇꯧꯕꯥ ꯌꯥꯕꯥ ꯐꯥꯏꯜ ꯃꯈꯂꯁꯤꯡ꯫",
    "标签有文章不能删除" => "ꯇꯦꯒ ꯌꯥꯑꯣꯕꯥ ꯑꯥꯔꯇꯤꯀꯂꯁꯤꯡ ꯃꯨꯠꯊꯠꯄꯥ ꯌꯥꯔꯣꯏ꯫",
    "人觉得这篇文章很赞" => "ꯃꯤꯌꯥꯝꯅꯥ ꯋꯥꯐꯝ ꯑꯁꯤ ꯑꯆꯧꯕꯥ ꯑꯃꯅꯤ ꯍꯥꯌꯅꯥ ꯈꯜꯂꯤ꯫",
    "还没有页面描述信息" => "ꯍꯧꯖꯤꯛ ꯐꯥꯑꯣꯕꯥ ꯄꯦꯖ ꯗꯤꯁ꯭ꯛꯔꯤꯄꯁꯅꯒꯤ ꯏꯅꯐꯣꯔꯃꯦꯁꯟ ꯑꯃꯠꯇꯥ ꯂꯩꯇꯦ꯫",
    "回复与评论内容重复" => "ꯔꯤꯞꯂꯥꯏ ꯑꯃꯁꯨꯡ ꯀꯃꯦꯟꯇꯀꯤ ꯀꯟꯇꯦꯟꯇ ꯑꯅꯤꯔꯛ ꯊꯣꯀꯄꯥ꯫",
    "失败！请稍后再试。" => "ꯃꯥꯏꯊꯤꯕ! ꯆꯥꯅꯕꯤꯗꯨꯅꯥ ꯇꯨꯡꯗꯥ ꯑꯃꯨꯛ ꯍꯟꯅꯥ ꯍꯣꯠꯅꯕꯤꯌꯨ꯫",
    "主耶稣基督里的教会" => "ꯏꯕꯨꯡꯉꯣ ꯌꯤꯁꯨ ꯈ꯭ꯔ꯭ꯏꯁ꯭ꯇꯗꯥ ꯂꯩꯕꯥ ꯁꯤꯡꯂꯨꯞ꯫",
    "库尔德语（索拉尼）" => "ꯀꯨꯔꯗꯤꯁ (ꯁꯣꯔꯥꯅꯤ)",
    "布尔语(南非荷兰语)" => "ꯕꯨꯂꯦꯇꯤꯟ (ꯑꯐ꯭ꯔꯤꯀꯥꯒꯤ ꯃꯤꯑꯣꯏꯁꯤꯡ)",
    "用户名或密码错误" => "ꯑꯔꯥꯅꯕꯥ ꯌꯨꯖꯔ ꯅꯦꯝ ꯅꯠꯠꯔꯒꯥ ꯄꯥꯁꯋꯥꯔꯗ꯫",
    "源语言必须是中文" => "ꯁꯣꯔꯁ ꯂꯣꯜ ꯑꯗꯨ ꯆꯥꯏꯅꯥꯒꯤ ꯑꯣꯏꯒꯗꯕꯅꯤ꯫",
    "目标语言别名为空" => "ꯇꯥꯔꯒꯦꯠ ꯂꯦꯉ꯭ꯒꯨꯌꯦꯖ ꯑꯦꯂꯤꯌꯥꯁ ꯑꯁꯤ ꯏꯪꯅꯥ ꯂꯩ꯫",
    "国际化文章时标签" => "ꯏꯟꯇꯔꯅꯦꯁ꯭ꯅꯦꯂꯥꯏꯖ ꯇꯧꯔꯕꯥ ꯑꯥꯔꯇꯤꯀꯜ ꯇꯦꯒꯁꯤꯡ꯫",
    "确定清除缓存吗？" => "ꯅꯍꯥꯛꯅꯥ ꯀꯦꯁ ꯀ꯭ꯂꯤꯌꯔ ꯇꯧꯕꯥ ꯄꯥꯝꯂꯤꯕ꯭ꯔꯥ?",
    "超过的单文件大小" => "ꯁꯤꯉ꯭ꯒꯜ ꯐꯥꯏꯜ ꯁꯥꯏꯖ ꯍꯦꯅꯒꯠꯂꯦ꯫",
    "超过前端表单限制" => "ꯐ꯭ꯔꯟꯇꯑꯦꯟꯗ ꯐꯣꯔꯝ ꯂꯤꯃꯤꯠ ꯍꯦꯅꯒꯠꯂꯦ꯫",
    "目标没有写入权限" => "ꯇꯥꯔꯒꯦꯠꯇꯥ ꯏꯕꯒꯤ ꯑꯌꯥꯕꯥ ꯂꯩꯇꯦ꯫",
    "不允许的上传类型" => "ꯑꯌꯥꯕꯥ ꯄꯤꯗꯕꯥ ꯑꯄꯂꯣꯗ ꯃꯈꯂꯁꯤꯡ꯫",
    "文件大小不能超过" => "ꯐꯥꯏꯂꯒꯤ ꯁꯥꯏꯖ ꯑꯁꯤꯗꯒꯤ ꯍꯦꯅꯕꯥ ꯌꯥꯔꯣꯏ꯫",
    "保存基础设置成功" => "ꯕꯦꯁꯤꯛ ꯁꯦꯇꯤꯡꯁꯤꯡ ꯃꯥꯌꯄꯥꯛꯅꯥ ꯁꯦꯚ ꯇꯧꯈꯤ꯫",
    "首次基础设置成功" => "ꯑꯍꯥꯅꯕꯥ ꯕꯦꯁꯤꯛ ꯁꯦꯇꯑꯞ ꯃꯥꯌ ꯄꯥꯛꯂꯦ꯫",
    "正在合成，请稍后" => "ꯁꯤꯟꯊꯦꯁꯤꯁ ꯇꯧꯔꯤ, ꯆꯥꯅꯕꯤꯗꯨꯅꯥ ꯉꯥꯏꯕꯤꯌꯨ꯫",
    "不合理的请求方法" => "ꯃꯔꯝ ꯆꯥꯗꯕꯥ ꯔꯤꯛꯕꯦꯁ꯭ꯠ ꯇꯧꯕꯒꯤ ꯃꯑꯣꯡ꯫",
    "Session无效或过期" => "ꯁꯦꯁꯟ ꯑꯁꯤ ꯑꯁꯣꯌꯕꯥ ꯅꯠꯔꯒꯥ ꯃꯇꯝ ꯂꯣꯏꯔꯦ꯫",
    "手机号码不正确" => "ꯐꯣꯟ ꯅꯝꯕꯔ ꯑꯗꯨ ꯑꯔꯥꯅꯕꯥ ꯑꯣꯏꯔꯦ꯫",
    "手机号码已存在" => "ꯃꯣꯕꯥꯏꯜ ꯅꯝꯕꯔ ꯍꯥꯟꯅꯅꯥ ꯂꯩꯔꯦ꯫",
    "标题或内容为空" => "ꯇꯥꯏꯇꯜ ꯅꯠꯔꯒꯥ ꯀꯟꯇꯦꯟꯇ ꯑꯁꯤ ꯏꯝꯄꯣꯔꯠ ꯇꯧꯏ꯫",
    "创建文章时标签" => "ꯑꯥꯔꯇꯤꯀꯜ ꯑꯃꯥ ꯁꯦꯝꯕꯥ ꯃꯇꯃꯗꯥ ꯇꯦꯒ ꯇꯧꯕꯥ꯫",
    "编辑文章时标签" => "ꯑꯥꯔꯇꯤꯀꯜ ꯑꯃꯥ ꯑꯦꯗꯤꯠ ꯇꯧꯕꯥ ꯃꯇꯃꯗꯥ ꯇꯦꯒ ꯇꯧꯕꯥ꯫",
    "真的删除行么？" => "ꯑꯗꯣꯝꯅꯥ ꯇꯁꯦꯡꯅꯥ ꯂꯥꯏꯟ ꯑꯗꯨ ꯃꯨꯠꯊꯠꯄꯥ ꯄꯥꯝꯕꯤꯕ꯭ꯔꯥ?",
    "请输入菜单名称" => "ꯆꯥꯅꯕꯤꯗꯨꯅꯥ ꯃꯦꯅꯨꯒꯤ ꯃꯃꯤꯡ ꯑꯃꯥ ꯑꯦꯟꯇꯔ ꯇꯧꯕꯤꯌꯨ꯫",
    "请先删除子菜单" => "ꯆꯥꯅꯕꯤꯗꯨꯅꯥ ꯑꯍꯥꯅꯕꯗꯥ ꯁꯕꯃꯦꯅꯨ ꯑꯗꯨ ꯃꯨꯠꯊꯠꯄꯤꯌꯨ",
    "未登录访问后台" => "ꯂꯣꯒ ꯏꯟ ꯇꯧꯗꯅꯥ ꯕꯦꯀꯑꯦꯟꯗ ꯑꯦꯛꯁꯦꯁ ꯇꯧꯕꯥ꯫",
    "Cookie无效或过期" => "ꯀꯨꯀꯤ ꯑꯁꯤ ꯑꯁꯣꯌꯕꯥ ꯅꯠꯔꯒꯥ ꯃꯇꯝ ꯂꯣꯏꯔꯕꯥ ꯌꯥꯏ꯫",
    "真的还原行么？" => "ꯃꯁꯤ ꯇꯁꯦꯡꯅꯥ ꯑꯃꯨꯛ ꯍꯟꯅꯥ ꯁꯦꯃꯒꯠꯄꯥ ꯉꯃꯒꯗ꯭ꯔꯥ?",
    "编辑器内容为空" => "ꯑꯦꯗꯤꯇꯔ ꯀꯟꯇꯦꯟꯇ ꯑꯁꯤ ꯏꯝꯄꯣꯔꯠ ꯇꯧꯏ꯫",
    "未选择整行文本" => "ꯇꯦꯛꯁꯇꯀꯤ ꯂꯥꯏꯟ ꯄꯨꯝꯅꯃꯛ ꯈꯅꯒꯠꯄꯥ ꯂꯩꯇꯦ꯫",
    "划词选择行错误" => "ꯋꯥꯍꯩ ꯈꯅꯕꯒꯤ ꯂꯥꯏꯅꯥꯒꯤ ꯑꯁꯣꯌꯕꯥ꯫",
    "数据源发生改变" => "ꯗꯦꯇꯥ ꯁꯣꯔꯁ ꯍꯣꯡꯂꯀꯄꯥ꯫",
    "填充成功，行号" => "ꯃꯥꯌ ꯄꯥꯛꯅꯥ ꯂꯣꯏꯁꯤꯅꯈ꯭ꯔꯦ, ꯂꯥꯏꯟ ꯅꯝꯕꯔ꯫",
    "请输入分类名称" => "ꯆꯥꯅꯕꯤꯗꯨꯅꯥ ꯀꯦꯇꯦꯒꯣꯔꯤꯒꯤ ꯃꯃꯤꯡ ꯑꯗꯨ ꯑꯦꯟꯇꯔ ꯇꯧꯕꯤꯌꯨ꯫",
    "分类名不能为空" => "ꯀꯦꯇꯦꯒꯣꯔꯤꯒꯤ ꯃꯃꯤꯡ ꯑꯁꯤ ꯏꯝꯄꯣꯔꯠ ꯇꯧꯕꯥ ꯌꯥꯔꯣꯏ꯫",
    "排序只能是数字" => "ꯁꯣꯔꯇꯤꯡ ꯑꯁꯤ ꯅ꯭ꯌꯨꯃꯔꯤꯛ ꯈꯛꯇꯃꯛ ꯑꯣꯏꯕꯥ ꯌꯥꯏ꯫",
    "请先删除子分类" => "ꯆꯥꯅꯕꯤꯗꯨꯅꯥ ꯑꯍꯥꯅꯕꯗꯥ ꯁꯕꯀꯦꯇꯦꯒꯣꯔꯤꯁꯤꯡ ꯃꯨꯠꯊꯠꯄꯤꯌꯨ꯫",
    "请输入评论作者" => "ꯀꯃꯦꯟꯇ ꯑꯣꯊꯣꯔ ꯑꯗꯨ ꯑꯦꯟꯇꯔ ꯇꯧꯕꯤꯌꯨ꯫",
    "请选择目标语言" => "ꯆꯥꯅꯕꯤꯗꯨꯅꯥ ꯇꯥꯔꯒꯦꯠ ꯂꯣꯜ ꯈꯜꯂꯨ꯫",
    "语言包生成成功" => "ꯂꯦꯉ꯭ꯒꯨꯌꯦꯖ ꯄꯦꯛ ꯃꯥꯌ ꯄꯥꯛꯅꯥ ꯖꯦꯅꯦꯔꯦꯠ ꯇꯧꯈꯤ꯫",
    "语言包生成失败" => "ꯂꯦꯉ꯭ꯒꯨꯌꯦꯖ ꯄꯦꯛ ꯖꯦꯅꯦꯔꯦꯁꯟ ꯇꯧꯕꯥ ꯉꯃꯈꯤꯗꯦ꯫",
    "国际化分类成功" => "ꯏꯟꯇꯔꯅꯦꯁ꯭ꯅꯦꯜ ꯀ꯭ꯂꯥꯁꯤꯐꯤꯀꯦꯁꯟ ꯃꯥꯌ ꯄꯥꯛꯂꯦ꯫",
    "国际化分类失败" => "ꯏꯟꯇꯔꯅꯦꯁ꯭ꯅꯦꯜ ꯀ꯭ꯂꯥꯁꯤꯐꯤꯀꯦꯁꯟ ꯑꯁꯤ ꯃꯥꯌ ꯄꯥꯀꯈꯤꯗꯦ꯫",
    "请输入标签名称" => "ꯆꯥꯅꯕꯤꯗꯨꯅꯥ ꯂꯦꯕꯦꯂꯒꯤ ꯃꯃꯤꯡ ꯑꯃꯥ ꯑꯦꯟꯇꯔ ꯇꯧꯕꯤꯌꯨ꯫",
    "国际化标签成功" => "ꯏꯟꯇꯔꯅꯦꯁ꯭ꯅꯦꯜ ꯂꯦꯕꯦꯂꯒꯤ ꯃꯥꯏꯄꯥꯀꯄꯥ꯫",
    "国际化标签失败" => "ꯏꯟꯇꯔꯅꯦꯁ꯭ꯅꯦꯂꯥꯏꯖꯦꯁꯟ ꯇꯦꯒ ꯑꯗꯨ ꯃꯥꯌ ꯄꯥꯀꯈꯤꯗꯦ꯫",
    "国际化文章成功" => "ꯏꯟꯇꯔꯅꯦꯁ꯭ꯅꯦꯜ ꯑꯥꯔꯇꯤꯀꯜ ꯃꯥꯌ ꯄꯥꯀꯄꯥ꯫",
    "国际化文章失败" => "ꯏꯟꯇꯔꯅꯦꯁ꯭ꯅꯦꯜ ꯑꯥꯔꯇꯤꯀꯜ ꯑꯁꯤ ꯃꯥꯌ ꯄꯥꯀꯈꯤꯗꯦ꯫",
    "请输入页面标题" => "ꯆꯥꯅꯕꯤꯗꯨꯅꯥ ꯄꯦꯖꯒꯤ ꯃꯃꯤꯡ ꯑꯦꯟꯇꯔ ꯇꯧꯕꯤꯌꯨ꯫",
    "国际化页面成功" => "ꯏꯟꯇꯔꯅꯦꯁ꯭ꯅꯦꯂꯥꯏꯖꯦꯁꯅꯒꯤ ꯄꯦꯖ ꯑꯁꯤ ꯃꯥꯌ ꯄꯥꯛꯂꯦ꯫",
    "国际化页面失败" => "ꯏꯟꯇꯔꯅꯦꯁ꯭ꯅꯦꯂꯥꯏꯖꯦꯁꯅꯒꯤ ꯄꯦꯖ ꯑꯁꯤ ꯐꯜ ꯂꯩꯈꯤꯗꯦ꯫",
    "作者：葡萄枝子" => "ꯂꯤꯈꯨꯟ: ꯑꯉ꯭ꯒꯨꯔ ꯁꯥꯈꯥ꯫",
    "请输入网站名称" => "ꯆꯥꯅꯕꯤꯗꯨꯅꯥ ꯋꯦꯕꯁꯥꯏꯇꯀꯤ ꯃꯃꯤꯡ ꯑꯦꯟꯇꯔ ꯇꯧꯕꯤꯌꯨ꯫",
    "请输入网站地址" => "ꯆꯥꯅꯕꯤꯗꯨꯅꯥ ꯋꯦꯕꯁꯥꯏꯇꯀꯤ ꯑꯦꯗ꯭ꯔꯦꯁ ꯑꯗꯨ ꯑꯦꯟꯇꯔ ꯇꯧꯕꯤꯌꯨ꯫",
    "链接名不能为空" => "ꯂꯤꯉ꯭ꯀꯀꯤ ꯃꯃꯤꯡ ꯑꯁꯤ ꯏꯝꯄꯣꯔꯠ ꯇꯧꯕꯥ ꯌꯥꯔꯣꯏ꯫",
    "上传文件不完整" => "ꯑꯄꯂꯣꯗ ꯇꯧꯔꯕꯥ ꯐꯥꯏꯜ ꯑꯗꯨ ꯃꯄꯨꯡ ꯐꯥꯗꯦ꯫",
    "没有文件被上传" => "ꯐꯥꯏꯜ ꯑꯃꯠꯇꯥ ꯑꯄꯂꯣꯗ ꯇꯧꯈꯤꯗꯦ꯫",
    "找不到临时目录" => "ꯇꯦꯝꯄ ꯗꯥꯏꯔꯦꯛꯇꯣꯔꯤ ꯐꯪꯗ꯭ꯔꯤ꯫",
    "未知的文件类型" => "ꯃꯁꯛ ꯈꯪꯗꯕꯥ ꯐꯥꯏꯜ ꯃꯈꯜ꯫",
    "文件名不能为空" => "ꯐꯥꯏꯂꯒꯤ ꯃꯃꯤꯡ ꯑꯁꯤ ꯏꯝꯄꯣꯔꯠ ꯇꯧꯕꯥ ꯌꯥꯔꯣꯏ꯫",
    "个文件有在使用" => "ꯐꯥꯏꯂꯁꯤꯡ ꯁꯤꯖꯤꯟꯅꯔꯤ꯫",
    "请先删除子页面" => "ꯆꯥꯅꯕꯤꯗꯨꯅꯥ ꯑꯍꯥꯅꯕꯗꯥ ꯁꯕꯄꯦꯖ ꯑꯗꯨ ꯃꯨꯠꯊꯠꯄꯤꯌꯨ꯫",
    "请输入角色名称" => "ꯆꯥꯅꯕꯤꯗꯨꯅꯥ ꯔꯣꯜ ꯅꯦꯝ ꯑꯃꯥ ꯑꯦꯟꯇꯔ ꯇꯧꯕꯤꯌꯨ꯫",
    "管理员不可禁用" => "ꯑꯦꯗꯃꯤꯅꯤꯁ꯭ꯠꯔꯦꯇꯔꯁꯤꯡꯅꯥ ꯗꯤꯁꯦꯕꯜ ꯇꯧꯕꯥ ꯌꯥꯗꯦ꯫",
    "管理员不可删除" => "ꯑꯦꯗꯃꯤꯅꯤꯁ꯭ꯠꯔꯦꯇꯔꯁꯤꯡꯅꯥ ꯗꯤꯂꯤꯠ ꯇꯧꯕꯥ ꯌꯥꯗꯦ꯫",
    "请输入限制大小" => "ꯆꯥꯅꯕꯤꯗꯨꯅꯥ ꯂꯤꯃꯤꯠ ꯁꯥꯏꯖ ꯑꯦꯟꯇꯔ ꯇꯧꯕꯤꯌꯨ꯫",
    "请输入版权信息" => "ꯆꯥꯅꯕꯤꯗꯨꯅꯥ ꯀꯣꯄꯤꯔꯥꯏꯠ ꯏꯅꯐꯣꯔꯃꯦꯁꯟ ꯑꯦꯟꯇꯔ ꯇꯧꯕꯤꯌꯨ꯫",
    "恢复出厂成功！" => "ꯐꯦꯛꯇꯔꯤ ꯔꯤꯁꯦꯠ ꯇꯧꯕꯥ ꯃꯥꯌ ꯄꯥꯛꯂꯦ!",
    "恢复出厂失败！" => "ꯐꯦꯛꯇꯔꯤ ꯔꯤꯁꯦꯠ ꯇꯧꯕꯥ ꯉꯃꯗꯦ!",
    "标签名不能为空" => "ꯇꯦꯒ ꯅꯦꯝ ꯑꯁꯤ ꯏꯝꯄꯣꯔꯠ ꯇꯧꯕꯥ ꯌꯥꯔꯣꯏ꯫",
    "还没有内容信息" => "ꯍꯧꯖꯤꯛ ꯐꯥꯑꯣꯕꯥ ꯀꯟꯇꯦꯟꯇ ꯏꯅꯐꯣꯔꯃꯦꯁꯟ ꯑꯃꯠꯇꯥ ꯂꯩꯇꯦ꯫",
    "这篇文章很有用" => "ꯋꯥꯔꯣꯜ ꯑꯁꯤ ꯌꯥꯝꯅꯥ ꯀꯥꯟꯅꯕꯅꯤ꯫",
    "保存分类国际化" => "ꯀꯦꯇꯦꯒꯣꯔꯤ ꯏꯟꯇꯔꯅꯦꯁ꯭ꯅꯦꯂꯥꯏꯖꯦꯁꯟ ꯁꯦꯚ ꯇꯧꯕꯥ꯫",
    "分类国际化帮助" => "ꯀ꯭ꯂꯥꯁꯤꯐꯤꯀꯦꯁꯟ ꯏꯟꯇꯔꯅꯦꯁ꯭ꯅꯦꯂꯥꯏꯖꯦꯁꯅꯒꯤ ꯃꯇꯦꯡ ꯄꯥꯡꯏ꯫",
    "保存标签国际化" => "ꯂꯦꯕꯦꯜ ꯏꯟꯇꯔꯅꯦꯁ꯭ꯅꯦꯂꯥꯏꯖꯦꯁꯟ ꯁꯦꯚ ꯇꯧꯕꯥ꯫",
    "标签国际化帮助" => "ꯏꯟꯇꯔꯅꯦꯁ꯭ꯅꯦꯂꯥꯏꯖꯦꯁꯅꯒꯤ ꯃꯇꯦꯡ ꯇꯦꯒ ꯇꯧꯕꯥ꯫",
    "保存文章国际化" => "ꯑꯥꯔꯇꯤꯀꯜ ꯏꯟꯇꯔꯅꯦꯁ꯭ꯅꯦꯂꯥꯏꯖꯦꯁꯟ ꯁꯦꯚ ꯇꯧꯕꯥ꯫",
    "文章国际化帮助" => "ꯑꯥꯔꯇꯤꯀꯜ ꯏꯟꯇꯔꯅꯦꯁ꯭ꯅꯦꯂꯥꯏꯖꯦꯁꯅꯒꯤ ꯃꯇꯦꯡ ꯄꯥꯡꯏ꯫",
    "保存页面国际化" => "ꯄꯦꯖ ꯏꯟꯇꯔꯅꯦꯁ꯭ꯅꯦꯂꯥꯏꯖꯦꯁꯟ ꯁꯦꯚ ꯇꯧꯕꯥ꯫",
    "页面国际化帮助" => "ꯄꯦꯖ ꯏꯟꯇꯔꯅꯦꯁ꯭ꯅꯦꯂꯥꯏꯖꯦꯁꯅꯒꯤ ꯃꯇꯦꯡ ꯄꯥꯡꯏ꯫",
    "海地克里奥尔语" => "ꯍꯥꯏꯇꯤꯒꯤ ꯀ꯭ꯔꯦꯑꯣꯜ꯫",
    "非法的ajax请求" => "ꯑꯥꯏꯟꯅꯥ ꯌꯥꯗꯕꯥ ꯑꯖꯥꯛꯁ ꯔꯤꯛꯕꯦꯁ꯭ꯠ ꯇꯧꯕꯥ꯫",
    "密码至少位数" => "ꯄꯥꯁꯋꯥꯔꯗ ꯑꯗꯨꯗꯥ ꯌꯥꯃꯗ꯭ꯔꯕꯗꯥ ꯗꯤꯖꯤꯠ ꯂꯩꯒꯗꯕꯅꯤ꯫",
    "验证码不正确" => "ꯑꯔꯥꯅꯕꯥ ꯚꯦꯔꯤꯐꯤꯀꯦꯁꯟ ꯀꯣꯗ꯫",
    "包含非法参数" => "ꯑꯥꯏꯟꯅꯥ ꯌꯥꯗꯕꯥ ꯄꯦꯔꯥꯃꯤꯇꯔꯁꯤꯡ ꯌꯥꯑꯣꯏ꯫",
    "请输入用户名" => "ꯆꯥꯅꯕꯤꯗꯨꯅꯥ ꯌꯨꯖꯔ ꯅꯦꯝ ꯑꯦꯟꯇꯔ ꯇꯧꯕꯤꯌꯨ꯫",
    "用户名已存在" => "ꯌꯨꯖꯔꯅꯦꯝ ꯍꯥꯟꯅꯅꯥ ꯂꯩꯔꯦ꯫",
    "请重输入密码" => "ꯆꯥꯅꯕꯤꯗꯨꯅꯥ ꯅꯍꯥꯛꯀꯤ ꯄꯥꯁꯋꯥꯔꯗ ꯑꯗꯨ ꯑꯃꯨꯛ ꯍꯟꯅꯥ ꯑꯦꯟꯇꯔ ꯇꯧꯕꯤꯌꯨ꯫",
    "图片格式错误" => "ꯏꯃꯦꯖ ꯐꯣꯔꯃꯦꯠꯀꯤ ꯑꯁꯣꯌꯕꯥ ꯂꯩ꯫",
    "修改资料成功" => "ꯗꯦꯇꯥ ꯃꯥꯌ ꯄꯥꯛꯅꯥ ꯃꯣꯗꯤꯐꯥꯏ ꯇꯧꯈꯤ꯫",
    "没有改变信息" => "ꯏꯅꯐꯣꯔꯃꯦꯁꯟ ꯑꯃꯠꯇꯥ ꯍꯣꯡꯈꯤꯗꯦ꯫",
    "请输入浏览量" => "ꯆꯥꯅꯕꯤꯗꯨꯅꯥ ꯌꯦꯡꯕꯒꯤ ꯃꯁꯤꯡ ꯑꯗꯨ ꯑꯦꯟꯇꯔ ꯇꯧꯕꯤꯌꯨ꯫",
    "请输入点赞数" => "ꯆꯥꯅꯕꯤꯗꯨꯅꯥ ꯂꯥꯏꯀꯀꯤ ꯃꯁꯤꯡ ꯑꯗꯨ ꯑꯦꯟꯇꯔ ꯇꯧꯕꯤꯌꯨ꯫",
    "请选择子分类" => "ꯆꯥꯅꯕꯤꯗꯨꯅꯥ ꯁꯕꯀꯦꯇꯦꯒꯣꯔꯤ ꯑꯃꯥ ꯈꯅꯕꯤꯌꯨ꯫",
    "创建文章成功" => "ꯑꯥꯔꯇꯤꯀꯜ ꯃꯥꯌ ꯄꯥꯛꯅꯥ ꯁꯦꯃꯈꯤ꯫",
    "创建文章失败" => "ꯑꯥꯔꯇꯤꯀꯜ ꯁꯦꯝꯕꯥ ꯉꯃꯈꯤꯗꯦ꯫",
    "编辑文章成功" => "ꯑꯥꯔꯇꯤꯀꯜ ꯑꯗꯨ ꯃꯥꯌ ꯄꯥꯛꯅꯥ ꯑꯦꯗꯤꯠ ꯇꯧꯕꯥ꯫",
    "标题不能为空" => "ꯇꯥꯏꯇꯜ ꯑꯁꯤ ꯕ꯭ꯂꯦꯉ꯭ꯛ ꯑꯣꯏꯕꯥ ꯌꯥꯔꯣꯏ꯫",
    "菜单名称重复" => "ꯃꯦꯅꯨꯒꯤ ꯃꯃꯤꯡ ꯑꯅꯤ ꯊꯣꯀꯄꯥ꯫",
    "创建菜单成功" => "ꯃꯦꯅꯨ ꯃꯥꯌ ꯄꯥꯛꯅꯥ ꯁꯦꯃꯈꯤ꯫",
    "创建菜单失败" => "ꯃꯦꯅꯨ ꯁꯦꯝꯕꯥ ꯉꯃꯗꯦ꯫",
    "编辑菜单成功" => "ꯑꯦꯗꯤꯠ ꯃꯦꯅꯨ ꯃꯥꯌ ꯄꯥꯛꯂꯦ꯫",
    "请选择行数据" => "ꯆꯥꯅꯕꯤꯗꯨꯅꯥ ꯔꯣ ꯗꯦꯇꯥ ꯈꯅꯕꯤꯌꯨ꯫",
    "计算孤立文件" => "ꯑꯉꯥꯡꯁꯤꯡꯒꯤ ꯐꯥꯏꯂꯁꯤꯡ ꯀꯥꯎꯟꯇ ꯇꯧꯕꯥ꯫",
    "划词选择错误" => "ꯋꯥꯍꯩꯁꯤꯡꯒꯤ ꯑꯔꯥꯅꯕꯥ ꯈꯅꯕꯒꯤ ꯃꯑꯣꯡ꯫",
    "请提取语言包" => "ꯆꯥꯅꯕꯤꯗꯨꯅꯥ ꯂꯣꯜ ꯄꯦꯛ ꯑꯁꯤ ꯂꯧꯊꯣꯀꯄꯤꯌꯨ꯫",
    "没有语音文字" => "ꯚꯣꯏꯁ ꯇꯦꯛꯁ ꯑꯃꯠꯇꯥ ꯂꯩꯇꯦ꯫",
    "语音朗读完成" => "ꯚꯣꯏꯁ ꯔꯤꯗꯤꯡ ꯇꯧꯕꯥ ꯂꯣꯏꯔꯦ꯫",
    "分类名称为空" => "ꯀꯦꯇꯦꯒꯣꯔꯤꯒꯤ ꯃꯃꯤꯡ ꯑꯁꯤ ꯏꯪꯅꯥ ꯂꯩ꯫",
    "创建分类成功" => "ꯀ꯭ꯂꯥꯁꯤꯐꯤꯀꯦꯁꯟ ꯃꯥꯌ ꯄꯥꯛꯅꯥ ꯁꯦꯃꯈꯤ꯫",
    "创建分类失败" => "ꯀꯦꯇꯦꯒꯣꯔꯤ ꯁꯦꯝꯕꯥ ꯉꯃꯈꯤꯗꯦ꯫",
    "编辑分类成功" => "ꯀꯦꯇꯦꯒꯣꯔꯤ ꯑꯁꯤ ꯃꯥꯌ ꯄꯥꯛꯅꯥ ꯑꯦꯗꯤꯠ ꯇꯧꯕꯥ꯫",
    "回复评论为空" => "ꯀꯃꯦꯟꯇꯀꯤ ꯄꯥꯎꯈꯨꯝ ꯑꯗꯨ ꯏꯝꯄꯣꯔꯠ ꯇꯧꯔꯦ꯫",
    "回复评论成功" => "ꯃꯥꯌ ꯄꯥꯛꯅꯥ ꯀꯃꯦꯟꯇ ꯇꯧꯅꯕꯥ ꯄꯥꯎꯈꯨꯝ ꯄꯤꯌꯨ꯫",
    "回复评论失败" => "ꯀꯃꯦꯟꯇꯀꯤ ꯄꯥꯎꯈꯨꯝ ꯄꯤꯕꯥ ꯉꯃꯈꯤꯗꯦ꯫",
    "评论内容为空" => "ꯀꯃꯦꯟꯇ ꯀꯟꯇꯦꯟꯇ ꯑꯁꯤ ꯏꯝꯄꯣꯔꯠ ꯇꯧꯏ꯫",
    "编辑评论成功" => "ꯀꯃꯦꯟꯇ ꯑꯗꯨ ꯃꯥꯌ ꯄꯥꯛꯅꯥ ꯑꯦꯗꯤꯠ ꯇꯧꯕꯥ꯫",
    "待审评论成功" => "ꯀꯃꯦꯟꯇ ꯄꯦꯟꯗꯤꯡ ꯔꯤꯚꯤꯌꯨ ꯃꯥꯌ ꯄꯥꯛꯂꯦ꯫",
    "待审评论失败" => "ꯔꯤꯚꯤꯌꯨ ꯇꯧꯗ꯭ꯔꯤꯉꯩꯗꯥ ꯀꯃꯦꯟꯇ ꯇꯧꯕꯥ ꯉꯃꯈꯤꯗꯦ꯫",
    "审核通过评论" => "ꯑꯦꯞꯔꯨꯕ ꯇꯧꯔꯕꯥ ꯀꯃꯦꯟꯇꯁꯤꯡ꯫",
    "审核评论成功" => "ꯔꯤꯚꯤꯌꯨ ꯔꯤꯚꯤꯌꯨ ꯃꯥꯌ ꯄꯥꯛꯂꯦ꯫",
    "审核评论失败" => "ꯀꯃꯦꯟꯇ ꯔꯤꯚꯤꯌꯨ ꯇꯧꯕꯥ ꯉꯃꯈꯤꯗꯦ꯫",
    "删除评论失败" => "ꯀꯃꯦꯟꯇ ꯃꯨꯠꯊꯠꯄꯥ ꯉꯃꯈꯤꯗꯦ꯫",
    "请选择源语言" => "ꯆꯥꯅꯕꯤꯗꯨꯅꯥ ꯁꯣꯔꯁ ꯂꯣꯜ ꯈꯜꯂꯨ꯫",
    "别名不可更改" => "ꯑꯦꯂꯤꯌꯥꯁꯁꯤꯡ ꯍꯣꯡꯗꯣꯀꯄꯥ ꯌꯥꯔꯣꯏ꯫",
    "标签名称为空" => "ꯇꯦꯒꯀꯤ ꯃꯃꯤꯡ ꯑꯁꯤ ꯏꯝꯄꯣꯔꯠ ꯇꯧꯔꯦ꯫",
    "创建链接成功" => "ꯂꯤꯉ꯭ꯛ ꯑꯁꯤ ꯃꯥꯌ ꯄꯥꯛꯅꯥ ꯁꯦꯃꯈꯤ꯫",
    "创建链接失败" => "ꯂꯤꯉ꯭ꯛ ꯁꯦꯝꯕꯥ ꯉꯃꯗꯦ꯫",
    "编辑链接成功" => "ꯂꯤꯉ꯭ꯛ ꯑꯁꯤ ꯃꯥꯌ ꯄꯥꯛꯅꯥ ꯑꯦꯗꯤꯠ ꯇꯧꯕꯥ꯫",
    "网站名称重名" => "ꯋꯦꯕꯁꯥꯏꯇꯀꯤ ꯃꯃꯤꯡ ꯑꯅꯤ ꯊꯣꯀꯄꯥ꯫",
    "网站地址重复" => "ꯋꯦꯕꯁꯥꯏꯇꯀꯤ ꯑꯦꯗ꯭ꯔꯦꯁ ꯑꯗꯨ ꯗꯨꯞꯂꯤꯀꯦꯠ ꯇꯧꯕꯥ꯫",
    "图片压缩失败" => "ꯏꯃꯦꯖ ꯀꯝꯞꯔꯦꯁꯟ ꯇꯧꯕꯥ ꯉꯃꯈꯤꯗꯦ꯫",
    "移动文件失败" => "ꯐꯥꯏꯜ ꯂꯧꯊꯣꯀꯄꯥ ꯉꯃꯗꯦ꯫",
    "上传文件成功" => "ꯐꯥꯏꯜ ꯃꯥꯌ ꯄꯥꯛꯅꯥ ꯑꯄꯂꯣꯗ ꯇꯧꯈ꯭ꯔꯦ꯫",
    "上传文件失败" => "ꯐꯥꯏꯜ ꯑꯄꯂꯣꯗ ꯇꯧꯕꯥ ꯉꯃꯈꯤꯗꯦ꯫",
    "共找到文件：" => "ꯑꯄꯨꯅꯕꯥ ꯐꯥꯏꯂꯁꯤꯡ ꯐꯪꯈꯤꯕꯥ:",
    "创建页面成功" => "ꯄꯦꯖ ꯑꯁꯤ ꯃꯥꯌ ꯄꯥꯛꯅꯥ ꯁꯦꯃꯈꯤ꯫",
    "创建页面失败" => "ꯄꯦꯖ ꯁꯦꯝꯕꯥ ꯉꯃꯈꯤꯗꯦ꯫",
    "编辑页面成功" => "ꯄꯦꯖ ꯑꯁꯤ ꯃꯥꯌ ꯄꯥꯛꯅꯥ ꯑꯦꯗꯤꯠ ꯇꯧꯕꯥ꯫",
    "权限数据错误" => "ꯑꯌꯥꯕꯥ ꯄꯤꯕꯥ ꯗꯦꯇꯥꯒꯤ ꯑꯁꯣꯌꯕꯥ꯫",
    "创建角色成功" => "ꯊꯧꯗꯥꯡ ꯃꯥꯌ ꯄꯥꯛꯅꯥ ꯁꯦꯃꯈꯤ꯫",
    "创建角色失败" => "ꯊꯧꯗꯥꯡ ꯁꯦꯝꯕꯥ ꯉꯃꯈꯤꯗꯦ꯫",
    "编辑角色成功" => "ꯃꯥꯌ ꯄꯥꯛꯅꯥ ꯑꯦꯗꯤꯠ ꯇꯧꯕꯥ ꯊꯧꯗꯥꯡ꯫",
    "游客不可删除" => "ꯚꯤꯖꯤꯇꯔꯁꯤꯡꯅꯥ ꯗꯤꯂꯤꯠ ꯇꯧꯕꯥ ꯌꯥꯔꯣꯏ꯫",
    "创建标签成功" => "ꯇꯦꯒ ꯃꯥꯌ ꯄꯥꯛꯅꯥ ꯁꯦꯃꯈꯤ꯫",
    "创建标签失败" => "ꯂꯦꯕꯦꯜ ꯁꯦꯝꯕꯥ ꯉꯃꯗꯦ꯫",
    "编辑标签成功" => "ꯇꯦꯒ ꯑꯗꯨ ꯃꯥꯌ ꯄꯥꯛꯅꯥ ꯑꯦꯗꯤꯠ ꯇꯧꯕꯥ꯫",
    "角色数据错误" => "ꯀꯦꯔꯦꯛꯇꯔ ꯗꯦꯇꯥꯒꯤ ꯑꯁꯣꯌꯕꯥ ꯂꯩ꯫",
    "语言数据错误" => "ꯂꯣꯂꯒꯤ ꯗꯦꯇꯥꯒꯤ ꯑꯁꯣꯌꯕꯥ꯫",
    "状态数据错误" => "ꯁ꯭ꯇꯦꯇꯁ ꯗꯦꯇꯥꯒꯤ ꯑꯁꯣꯌꯕꯥ ꯂꯩ꯫",
    "创建用户成功" => "ꯌꯨꯖꯔꯅꯥ ꯃꯥꯌ ꯄꯥꯛꯅꯥ ꯁꯦꯃꯈꯤ꯫",
    "创建用户失败" => "ꯌꯨꯖꯔ ꯁꯦꯝꯕꯥ ꯉꯃꯗꯦ꯫",
    "编辑用户成功" => "ꯌꯨꯖꯔ ꯃꯥꯌ ꯄꯥꯛꯅꯥ ꯑꯦꯗꯤꯠ ꯇꯧꯕꯥ꯫",
    "本文博客网址" => "ꯑꯥꯔꯇꯤꯀꯜ ꯑꯁꯤꯒꯤ ꯕ꯭ꯂꯣꯒ URL꯫",
    "评论内容重复" => "ꯀꯃꯦꯟꯇ ꯀꯟꯇꯦꯟꯇ ꯑꯅꯤ ꯊꯣꯀꯄꯥ꯫",
    "回复内容重复" => "ꯔꯤꯞꯂꯥꯏ ꯀꯟꯇꯦꯟꯇ ꯑꯅꯤ ꯊꯣꯀꯄꯥ꯫",
    "评论发表成功" => "ꯀꯃꯦꯟꯇ ꯑꯁꯤ ꯃꯥꯌ ꯄꯥꯛꯅꯥ ꯄꯣꯁ꯭ꯠ ꯇꯧꯈꯤ꯫",
    "发表评论失败" => "ꯀꯃꯦꯟꯇ ꯄꯣꯁ꯭ꯠ ꯇꯧꯕꯥ ꯉꯃꯈꯤꯗꯦ꯫",
    "前端删除评论" => "ꯐ꯭ꯔꯟꯇ ꯑꯦꯟꯗꯗꯥ ꯀꯃꯦꯟꯇꯁꯤꯡ ꯃꯨꯠꯊꯠꯄꯥ꯫",
    "中文（简体）" => "ꯆꯥꯏꯅꯥꯒꯤ (ꯁꯤꯝꯄꯂꯐꯥꯏꯗ) ꯴.",
    "加泰罗尼亚语" => "ꯀꯦꯇꯂꯅꯒꯤ ꯂꯃꯗꯥ꯫",
    "苏格兰盖尔语" => "ꯁ꯭ꯀꯠ ꯒꯦꯂꯤꯛ꯫",
    "中文（繁体）" => "ꯇ꯭ꯔꯦꯗꯤꯁ꯭ꯅꯦꯜ ꯆꯥꯏꯅꯥꯒꯤ)",
    "马拉雅拉姆语" => "ꯃꯂꯌꯥꯂꯃꯗꯥ ꯂꯩꯕꯥ꯫",
    "斯洛文尼亚语" => "ꯁ꯭ꯂꯣꯚꯦꯅꯤꯌꯥꯟ ꯑꯣꯏꯕꯥ꯫",
    "阿尔巴尼亚语" => "ꯑꯂꯕꯥꯅꯤꯌꯥꯟ ꯑꯣꯏꯕꯥ꯫",
    "密码至少5位" => "ꯄꯥꯁꯋꯥꯔꯗ ꯑꯗꯨ ꯌꯥꯃꯗ꯭ꯔꯕꯗꯥ ꯆꯦꯛꯔꯦꯇꯔ 5 ꯑꯣꯏꯒꯗꯕꯅꯤ꯫",
    "你已经登录" => "ꯅꯍꯥꯛꯅꯥ ꯍꯥꯟꯅꯅꯥ ꯂꯣꯒ ꯏꯟ ꯇꯧꯈ꯭ꯔꯦ꯫",
    "账号被禁用" => "ꯑꯦꯀꯥꯎꯟꯇ ꯑꯁꯤ ꯗꯤꯁꯦꯕꯜ ꯇꯧꯔꯦ꯫",
    "请输入密码" => "ꯆꯥꯅꯕꯤꯗꯨꯅꯥ ꯄꯥꯁꯋꯥꯔꯗ ꯑꯦꯟꯇꯔ ꯇꯧꯕꯤꯌꯨ꯫",
    "留空不修改" => "ꯕ꯭ꯂꯦꯉ꯭ꯛ ꯑꯣꯏꯅꯥ ꯊꯝꯃꯨ ꯑꯃꯁꯨꯡ ꯃꯣꯗꯤꯐꯥꯏ ꯇꯧꯔꯣꯏꯗꯕꯅꯤ꯫",
    "请输入标题" => "ꯆꯥꯅꯕꯤꯗꯨꯅꯥ ꯇꯥꯏꯇꯜ ꯑꯃꯥ ꯑꯦꯟꯇꯔ ꯇꯧꯕꯤꯌꯨ꯫",
    "请输入内容" => "ꯆꯥꯅꯕꯤꯗꯨꯅꯥ ꯀꯟꯇꯦꯟꯇ ꯑꯦꯟꯇꯔ ꯇꯧꯕꯤꯌꯨ꯫",
    "多标签半角" => "ꯃꯜꯇꯤ-ꯂꯦꯕꯦꯜ ꯍꯥꯐ-ꯋꯥꯏꯗ ꯑꯣꯏꯕꯥ꯫",
    "关键词建议" => "ꯀꯤ-ꯋꯥꯔꯗ ꯁꯖꯦꯁꯅꯁꯤꯡ꯫",
    "请输入作者" => "ꯆꯥꯅꯕꯤꯗꯨꯅꯥ ꯑꯏꯕꯥ ꯑꯗꯨ ꯑꯦꯟꯇꯔ ꯇꯧꯕꯤꯌꯨ꯫",
    "请选择数据" => "ꯆꯥꯅꯕꯤꯗꯨꯅꯥ ꯗꯦꯇꯥ ꯈꯅꯕꯤꯌꯨ꯫",
    "软删除文章" => "ꯁꯣꯐ꯭ꯠ ꯗꯤꯂꯤꯠ ꯑꯥꯔꯇꯤꯀꯜ꯫",
    "软删除成功" => "ꯁꯣꯐ꯭ꯠ ꯗꯤꯂꯤꯠ ꯃꯥꯌ ꯄꯥꯛꯂꯦ꯫",
    "软删除失败" => "ꯁꯣꯐ꯭ꯠ ꯗꯤꯂꯤꯠ ꯇꯧꯕꯥ ꯉꯃꯈꯤꯗꯦ꯫",
    "角色不存在" => "ꯊꯧꯗꯥꯡ ꯑꯁꯤ ꯂꯩꯇꯦ꯫",
    "角色被禁用" => "ꯊꯧꯗꯥꯡ ꯑꯁꯤ ꯗꯤꯁꯑꯦꯕꯜ ꯑꯣꯏꯔꯦ꯫",
    "功能不存在" => "ꯐꯉ꯭ꯀꯁꯟ ꯂꯩꯇꯦ꯫",
    "功能被禁用" => "ꯐꯉ꯭ꯀꯁꯟ ꯑꯁꯤ ꯗꯤꯁꯦꯕꯜ ꯇꯧꯔꯦ꯫",
    "国际化帮助" => "ꯏꯟꯇꯔꯅꯦꯁ꯭ꯅꯦꯂꯥꯏꯖꯦꯁꯅꯒꯤ ꯃꯇꯦꯡ ꯄꯥꯡꯏ꯫",
    "未选择文本" => "ꯇꯦꯛꯁ ꯑꯃꯠꯇꯥ ꯈꯅꯈꯤꯗꯦ꯫",
    "请选择语言" => "ꯆꯥꯅꯕꯤꯗꯨꯅꯥ ꯂꯣꯜ ꯈꯅꯕꯤꯌꯨ꯫",
    "请输入排序" => "ꯆꯥꯅꯕꯤꯗꯨꯅꯥ ꯁꯣꯔꯇꯤꯡ ꯑꯦꯟꯇꯔ ꯇꯧꯕꯤꯌꯨ꯫",
    "未修改属性" => "ꯄ꯭ꯔꯣꯄꯔꯇꯤꯁꯤꯡ ꯃꯣꯗꯤꯐꯥꯏ ꯇꯧꯗꯕꯥ꯫",
    "机器人评论" => "ꯔꯣꯕꯣꯠ ꯔꯤꯚꯤꯌꯨ ꯇꯧꯕꯥ꯫",
    "生成语言包" => "ꯂꯣꯜ ꯄꯦꯛ ꯖꯦꯅꯦꯔꯦꯠ ꯇꯧꯕꯥ꯫",
    "国际化分类" => "ꯏꯟꯇꯔꯅꯦꯁ꯭ꯅꯦꯜ ꯀ꯭ꯂꯥꯁꯤꯐꯤꯀꯦꯁꯟ꯫",
    "国际化标签" => "ꯏꯟꯇꯔꯅꯦꯁ꯭ꯅꯦꯂꯥꯏꯖꯦꯁꯅꯒꯤ ꯇꯦꯒ꯫",
    "国际化文章" => "ꯏꯟꯇꯔꯅꯦꯁ꯭ꯅꯦꯜ ꯑꯥꯔꯇꯤꯀꯂꯁꯤꯡ꯫",
    "国际化页面" => "ꯏꯟꯇꯔꯅꯦꯁ꯭ꯅꯦꯜ ꯄꯦꯖ꯫",
    "服务器环境" => "ꯁꯔꯚꯔꯒꯤ ꯑꯀꯣꯌꯔꯣꯟ꯫",
    "数据库信息" => "ꯗꯦꯇꯥꯕꯦꯖꯒꯤ ꯏꯅꯐꯣꯔꯃꯦꯁꯟ꯫",
    "服务器时间" => "ꯁꯔꯚꯔꯒꯤ ꯃꯇꯝ꯫",
    "还没有评论" => "ꯍꯧꯖꯤꯛ ꯐꯥꯑꯣꯕꯥ ꯀꯃꯦꯟꯇ ꯑꯃꯠꯇꯥ ꯂꯩꯇꯦ꯫",
    "还没有数据" => "ꯍꯧꯖꯤꯛ ꯐꯥꯑꯣꯕꯥ ꯗꯦꯇꯥ ꯑꯃꯠꯇꯥ ꯂꯩꯇꯦ꯫",
    "网址不合法" => "URL ꯑꯁꯤ ꯑꯥꯏꯟꯅꯥ ꯌꯥꯗꯕꯥ ꯑꯃꯅꯤ꯫",
    "选择多文件" => "ꯃꯁꯤꯡ ꯌꯥꯝꯂꯕꯥ ꯐꯥꯏꯂꯁꯤꯡ ꯈꯅꯕꯤꯌꯨ꯫",
    "个未使用，" => "ꯁꯤꯖꯤꯟꯅꯗꯕꯥ, ꯴.",
    "文件不存在" => "ꯐꯥꯏꯜ ꯂꯩꯇꯦ꯫",
    "重命名失败" => "ꯔꯤꯅꯦꯝ ꯇꯧꯕꯥ ꯉꯃꯈꯤꯗꯦ꯫",
    "重命名成功" => "ꯃꯥꯌ ꯄꯥꯛꯂꯦ ꯍꯥꯌꯅꯥ ꯃꯃꯤꯡ ꯍꯣꯡꯗꯣꯀꯄꯥ꯫",
    "角色已存在" => "ꯊꯧꯗꯥꯡ ꯍꯥꯟꯅꯅꯥ ꯂꯩꯔꯦ꯫",
    "蜘蛛不索引" => "ꯁ꯭ꯄꯥꯏꯗꯥꯔ ꯏꯟꯗꯦꯛꯁ ꯇꯧꯗꯕꯥ꯫",
    "请输入数量" => "ꯆꯥꯅꯕꯤꯗꯨꯅꯥ ꯀ꯭ꯕꯥꯟꯇꯤꯇꯤ ꯑꯦꯟꯇꯔ ꯇꯧꯕꯤꯌꯨ꯫",
    "昵称已存在" => "ꯅꯤꯀꯅꯦꯝ ꯍꯥꯟꯅꯅꯥ ꯂꯩꯔꯦ꯫",
    "页面没找到" => "ꯄꯦꯖ ꯑꯁꯤ ꯐꯪꯗ꯭ꯔꯤ꯫",
    "还没有文章" => "ꯍꯧꯖꯤꯛ ꯐꯥꯑꯣꯕꯥ ꯑꯥꯔꯇꯤꯀꯜ ꯑꯃꯠꯇꯥ ꯌꯥꯑꯣꯗ꯭ꯔꯤ꯫",
    "还没有页面" => "ꯍꯧꯖꯤꯛ ꯐꯥꯑꯣꯕꯥ ꯄꯦꯖ ꯑꯃꯠꯇꯥ ꯂꯩꯇꯦ꯫",
    "还没有分类" => "ꯍꯧꯖꯤꯛ ꯐꯥꯑꯣꯕꯥ ꯀ꯭ꯂꯥꯁꯤꯐꯤꯀꯦꯁꯟ ꯑꯃꯠꯇꯥ ꯂꯩꯇꯦ꯫",
    "还没有标签" => "ꯍꯧꯖꯤꯛ ꯐꯥꯑꯣꯕꯥ ꯇꯦꯒ ꯑꯃꯠꯇꯥ ꯂꯩꯇꯦ꯫",
    "还没有热门" => "ꯍꯧꯖꯤꯛ ꯐꯥꯑꯣꯕꯥ ꯃꯤꯌꯥꯝꯅꯥ ꯄꯥꯝꯅꯕꯥ ꯑꯣꯏꯗ꯭ꯔꯤ꯫",
    "还没有更新" => "ꯍꯧꯖꯤꯛ ꯐꯥꯑꯣꯕꯥ ꯑꯄꯗꯦꯠ ꯇꯧꯗ꯭ꯔꯤ꯫",
    "还没有网址" => "ꯍꯧꯖꯤꯛ ꯐꯥꯑꯣꯕꯥ URL ꯑꯃꯠꯇꯥ ꯂꯩꯇꯦ꯫",
    "请写点评论" => "ꯆꯥꯅꯕꯤꯗꯨꯅꯥ ꯀꯃꯦꯟꯇ ꯑꯃꯥ ꯏꯕꯤꯌꯨ꯫",
    "，等待审核" => ",ꯃꯣꯗꯦꯔꯦꯇꯦꯗ ꯑꯣꯏꯕꯥ꯫",
    "你已经注册" => "ꯅꯍꯥꯛꯅꯥ ꯍꯥꯟꯅꯅꯥ ꯔꯦꯖꯤꯁ꯭ꯇꯔ ꯇꯧꯈ꯭ꯔꯦ꯫",
    "提取语言包" => "ꯂꯣꯜ ꯄꯦꯛ ꯂꯧꯊꯣꯀꯄꯥ꯫",
    "分类国际化" => "ꯀ꯭ꯂꯥꯁꯤꯐꯤꯀꯦꯁꯟ ꯏꯟꯇꯔꯅꯦꯁ꯭ꯅꯦꯂꯥꯏꯖꯦꯁꯟ꯫",
    "标签国际化" => "ꯂꯦꯕꯦꯜ ꯏꯟꯇꯔꯅꯦꯁ꯭ꯅꯦꯂꯥꯏꯖꯦꯁꯟ꯫",
    "文章国际化" => "ꯑꯥꯔꯇꯤꯀꯜ ꯏꯟꯇꯔꯅꯦꯁ꯭ꯅꯦꯂꯥꯏꯖꯦꯁꯟ꯫",
    "页面国际化" => "ꯄꯦꯖ ꯏꯟꯇꯔꯅꯦꯁ꯭ꯅꯦꯂꯥꯏꯖꯦꯁꯟ ꯇꯧꯕꯥ꯫",
    "格鲁吉亚语" => "ꯖꯔꯖꯤꯌꯥꯟ ꯑꯣꯏꯕꯥ꯫",
    "博杰普尔语" => "ꯚꯣꯖꯄꯨꯔꯤ",
    "白俄罗斯语" => "ꯕꯦꯂꯥꯔꯨꯁꯀꯤ ꯑꯣꯏꯕꯥ꯫",
    "斯瓦希里语" => "ꯁ꯭ꯕꯥꯍꯤꯂꯤ꯫",
    "古吉拉特语" => "ꯒꯨꯖꯔꯥꯇꯤ꯫",
    "斯洛伐克语" => "ꯁ꯭ꯂꯣꯚꯥꯛ ꯑꯃꯁꯨꯡ ꯑꯦꯝ",
    "阿塞拜疆语" => "ꯑꯖꯔꯕꯇꯥ ꯂꯩꯕꯥ꯫",
    "印尼巽他语" => "ꯏꯟꯗꯣꯅꯦꯁꯤꯌꯥꯒꯤ ꯁꯨꯟꯗꯥꯅꯤꯖ꯫",
    "加利西亚语" => "ꯒ꯭ꯌꯥꯂꯤꯁꯤꯌꯅꯒꯤ ꯑꯣꯏꯕꯥ꯫",
    "拉脱维亚语" => "ꯂꯥꯇꯚꯤꯌꯥꯒꯤ ꯃꯤꯑꯣꯏꯁꯤꯡ꯫",
    "罗马尼亚语" => "ꯔꯣꯃꯥꯅꯤꯌꯥꯒꯤ ꯑꯣꯏꯕꯥ꯫",
    "亚美尼亚语" => "ꯑꯥꯔꯃꯦꯅꯤꯌꯥꯒꯤ ꯑꯣꯏꯕꯥ꯫",
    "保加利亚语" => "ꯕꯨꯂꯒꯦꯔꯤꯌꯥꯒꯤ ꯑꯣꯏꯕꯥ꯫",
    "爱沙尼亚语" => "ꯏꯁ꯭ꯇꯣꯅꯤꯌꯥꯅꯒꯤ ꯑꯣꯏꯕꯥ꯫",
    "阿姆哈拉语" => "ꯑꯝꯍꯥꯔꯤꯛꯀꯤ ꯑꯣꯏꯕꯥ꯫",
    "吉尔吉斯语" => "ꯀꯤꯔꯒꯤꯖꯗꯥ ꯂꯩꯕꯥ ꯌꯨ.ꯑꯦꯁ",
    "克罗地亚语" => "ꯀ꯭ꯔꯣꯌꯦꯁꯤꯌꯥꯒꯤ꯫",
    "波斯尼亚语" => "ꯕꯁꯅꯤꯌꯥꯅꯒꯤ ꯑꯣꯏꯕꯥ꯫",
    "蒂格尼亚语" => "ꯇꯤꯒꯅꯥꯟ꯫",
    "克里奥尔语" => "ꯀ꯭ꯔꯦꯑꯣꯜ ꯑꯣꯏꯕꯥ꯫",
    "马尔加什语" => "ꯃꯥꯂꯥꯒꯥꯁꯤ꯫",
    "南非科萨语" => "ꯈꯥ ꯑꯐ꯭ꯔꯤꯀꯥꯒꯤ ꯖꯣꯁꯥ꯫",
    "南非祖鲁语" => "ꯖꯨꯂꯨ, ꯈꯥ ꯑꯐ꯭ꯔꯤꯀꯥꯗꯥ ꯂꯩꯕꯥ ꯌꯨ.ꯑꯦꯁ",
    "塞尔维亚语" => "ꯁꯥꯔꯕꯤꯌꯥꯟ ꯑꯣꯏꯕꯥ꯫",
    "乌兹别克语" => "ꯎꯖꯕꯦꯀꯇꯥ ꯂꯩꯕꯥ ꯌꯨ.ꯑꯦꯁ",
    "伊洛卡诺语" => "ꯂꯣꯀꯥꯅꯣ",
    "印尼爪哇语" => "ꯖꯥꯚꯥꯅꯤꯖ ꯏꯟꯗꯣꯅꯦꯁꯤꯌꯥꯟ꯫",
    "回复ID错误" => "ꯔꯤꯞꯂꯥꯏ ꯑꯥꯏ.ꯗꯤ.ꯒꯤ ꯑꯁꯣꯌꯕꯥ ꯂꯩ꯫",
    "非法文章ID" => "ꯑꯥꯏꯟꯅꯥ ꯌꯥꯗꯕꯥ ꯑꯥꯔꯇꯤꯀꯜ ꯑꯥꯏ.ꯗꯤ",
    "管理后台" => "ꯃꯦꯅꯦꯖꯃꯦꯟꯇꯀꯤ ꯃꯇꯨꯡ ꯏꯟꯅꯥ ꯆꯠꯄꯥ꯫",
    "管理登录" => "ꯑꯦꯗꯃꯤꯟ ꯂꯣꯒꯏꯟ ꯇꯧꯕꯥ꯫",
    "登录成功" => "ꯂꯣꯒꯏꯟ ꯇꯧꯕꯥ ꯃꯥꯌ ꯄꯥꯛꯂꯦ꯫",
    "切换注册" => "ꯔꯦꯖꯤꯁ꯭ꯠꯔꯦꯁꯟ ꯍꯣꯡꯗꯣꯀꯄꯥ꯫",
    "你已登出" => "ꯅꯍꯥꯛ ꯂꯣꯒ ꯑꯥꯎꯠ ꯇꯧꯔꯦ꯫",
    "登出成功" => "ꯂꯣꯒꯑꯥꯎꯠ ꯃꯥꯌ ꯄꯥꯛꯂꯦ꯫",
    "用户注册" => "ꯌꯨꯖꯔ ꯔꯦꯖꯤꯁ꯭ꯠꯔꯦꯁꯟ ꯇꯧꯕꯥ꯫",
    "注册成功" => "ꯔꯦꯖꯤꯁ꯭ꯠꯔꯦꯁꯟ ꯃꯥꯏꯄꯥꯀꯄꯥ꯫",
    "注册失败" => "ꯔꯦꯖꯤꯁ꯭ꯠꯔꯦꯁꯟ ꯇꯧꯕꯥ ꯉꯃꯈꯤꯗꯦ꯫",
    "重复密码" => "ꯄꯥꯁꯋꯥꯔꯗ ꯑꯗꯨ ꯍꯟꯖꯤꯟ ꯍꯟꯖꯤꯟ ꯇꯧ꯫",
    "切换登录" => "ꯂꯣꯒꯏꯟ ꯍꯣꯡꯗꯣꯀꯄꯥ꯫",
    "请先登录" => "ꯆꯥꯅꯕꯤꯗꯨꯅꯥ ꯑꯍꯥꯅꯕꯗꯥ ꯂꯣꯒ ꯏꯟ ꯇꯧꯕꯤꯌꯨ꯫",
    "修改资料" => "ꯏꯅꯐꯣꯔꯃꯦꯁꯟ ꯃꯣꯗꯤꯐꯥꯏ ꯇꯧꯕꯥ꯫",
    "没有改变" => "ꯑꯍꯣꯡꯕꯥ ꯑꯃꯠꯇꯥ ꯂꯩꯇꯦ꯫",
    "不可修改" => "ꯍꯣꯡꯗꯣꯀꯄꯥ ꯉꯃꯗꯕꯥ꯫",
    "上传失败" => "ꯑꯄꯂꯣꯗ ꯇꯧꯕꯥ ꯉꯃꯈꯤꯗꯦ꯫",
    "清除成功" => "ꯃꯥꯌ ꯄꯥꯛꯅꯥ ꯀ꯭ꯂꯤꯌꯥꯔ ꯇꯧꯕꯥ꯫",
    "暂无记录" => "ꯔꯦꯀꯣꯔꯗ ꯑꯃꯠꯇꯥ ꯂꯩꯇꯦ꯫",
    "批量删除" => "ꯕꯦꯆ ꯃꯨꯠꯊꯠꯄꯥ꯫",
    "文章列表" => "ꯑꯥꯔꯇꯤꯀꯜ ꯂꯤꯁ꯭ꯠ꯫",
    "发布时间" => "ꯔꯤꯂꯤꯖ ꯇꯧꯕꯒꯤ ꯃꯇꯝ꯫",
    "修改时间" => "ꯃꯇꯝ ꯍꯣꯡꯗꯣꯀꯎ꯫",
    "文章标题" => "ꯑꯥꯔꯇꯤꯀꯂꯒꯤ ꯃꯃꯤꯡ꯫",
    "标题重名" => "ꯇꯥꯏꯇꯜ ꯑꯅꯤ ꯊꯣꯀꯄꯥ꯫",
    "别名重复" => "ꯑꯦꯂꯤꯌꯥꯁ ꯑꯅꯤ ꯊꯣꯀꯄꯥ꯫",
    "创建文章" => "ꯑꯥꯔꯇꯤꯀꯜ ꯁꯦꯝꯃꯨ꯫",
    "编辑文章" => "ꯑꯥꯔꯇꯤꯀꯜ ꯑꯦꯗꯤꯠ ꯇꯧꯕꯥ꯫",
    "非法字段" => "ꯑꯥꯏꯟꯅꯥ ꯌꯥꯗꯕꯥ ꯂꯝ꯫",
    "填写整数" => "ꯏꯟꯇꯤꯖꯔ ꯑꯗꯨ ꯃꯄꯨꯡ ꯐꯥꯍꯅꯒꯗꯕꯅꯤ꯫",
    "修改属性" => "ꯄ꯭ꯔꯣꯄꯔꯇꯤꯁꯤꯡ ꯃꯣꯗꯤꯐꯥꯏ ꯇꯧꯕꯥ꯫",
    "修改成功" => "ꯃꯥꯌ ꯄꯥꯛꯅꯥ ꯃꯣꯗꯤꯐꯥꯏ ꯇꯧꯈꯤ꯫",
    "修改失败" => "ꯑꯦꯗꯤꯠ ꯇꯧꯕꯥ ꯉꯃꯗꯦ꯫",
    "批量还原" => "ꯕꯦꯆ ꯔꯤꯁ꯭ꯇꯣꯔ ꯇꯧꯕꯥ꯫",
    "还原文章" => "ꯑꯥꯔꯇꯤꯀꯦꯜ ꯑꯗꯨ ꯔꯤꯁ꯭ꯇꯣꯔ ꯇꯧꯕꯥ꯫",
    "还原成功" => "ꯃꯥꯌ ꯄꯥꯛꯅꯥ ꯔꯤꯁ꯭ꯇꯣꯔ ꯇꯧꯕꯥ꯫",
    "还原失败" => "ꯔꯤꯁ꯭ꯇꯣꯔ ꯇꯧꯕꯥ ꯉꯃꯈꯤꯗꯦ꯫",
    "删除文章" => "ꯑꯥꯔꯇꯤꯀꯦꯜ ꯑꯗꯨ ꯃꯨꯠꯊꯠꯂꯨ꯫",
    "删除成功" => "ꯃꯥꯌ ꯄꯥꯛꯅꯥ ꯃꯨꯠꯊꯠꯈ꯭ꯔꯦ꯫",
    "删除失败" => "ꯃꯨꯠꯊꯠꯄꯥ ꯉꯃꯈꯤꯗꯦ꯫",
    "全部展开" => "ꯄꯨꯝꯅꯃꯛ ꯄꯥꯀꯊꯣꯛ ꯆꯥꯎꯊꯣꯀꯍꯅꯕꯥ꯫",
    "全部折叠" => "ꯄꯨꯝꯅꯃꯛ ꯀꯣꯂꯥꯞ ꯇꯧꯕꯥ꯫",
    "添加下级" => "ꯁꯕꯑꯔꯕꯥꯟ ꯑꯃꯥ ꯍꯥꯄꯆꯤꯅꯕꯥ꯫",
    "编辑菜单" => "ꯃꯦꯅꯨ ꯑꯦꯗꯤꯠ ꯇꯧꯕꯥ꯫",
    "菜单名称" => "ꯃꯦꯅꯨꯒꯤ ꯃꯃꯤꯡ꯫",
    "父级菜单" => "ꯃꯃꯥ-ꯃꯄꯥꯒꯤ ꯃꯦꯅꯨ꯫",
    "顶级菜单" => "ꯃꯊꯛꯀꯤ ꯃꯦꯅꯨ꯫",
    "创建菜单" => "ꯃꯦꯅꯨ ꯁꯦꯝꯕꯥ꯫",
    "删除菜单" => "ꯃꯦꯅꯨ ꯃꯨꯠꯊꯠꯄꯥ꯫",
    "非法访问" => "ꯑꯌꯥꯕꯥ ꯄꯤꯗꯕꯥ ꯑꯦꯛꯁꯦꯁ ꯂꯩꯕꯥ꯫",
    "拒绝访问" => "ꯑꯦꯛꯁꯦꯁ ꯇꯧꯕꯥ ꯌꯥꯍꯟꯗꯦ꯫",
    "没有权限" => "ꯑꯌꯥꯕꯥ ꯄꯤꯈꯤꯗꯦ꯫",
    "彻底删除" => "ꯃꯄꯨꯡꯐꯥꯅꯥ ꯂꯧꯊꯣꯀꯎ꯫",
    "批量待审" => "ꯕꯦꯆ ꯑꯗꯨ ꯔꯤꯚꯤꯌꯨ ꯇꯧꯅꯕꯥ ꯂꯦꯞꯂꯤ꯫",
    "批量审核" => "ꯕꯦꯆ ꯔꯤꯚꯤꯌꯨ ꯇꯧꯕꯥ꯫",
    "垃圾评论" => "ꯁ꯭ꯄꯦꯝ ꯀꯃꯦꯟꯇꯁꯤꯡ꯫",
    "分类名称" => "ꯀꯦꯇꯦꯒꯣꯔꯤꯒꯤ ꯃꯃꯤꯡ꯫",
    "分类列表" => "ꯀꯦꯇꯦꯒꯣꯔꯤꯒꯤ ꯂꯤꯁ꯭ꯠ꯫",
    "父级分类" => "ꯃꯃꯥ-ꯃꯄꯥꯒꯤ ꯀꯦꯇꯦꯒꯣꯔꯤ꯫",
    "顶级分类" => "ꯃꯀꯣꯛ ꯊꯣꯡꯕꯥ ꯀ꯭ꯂꯥꯁꯤꯐꯤꯀꯦꯁꯟ꯫",
    "分类重名" => "ꯃꯤꯡ ꯑꯅꯤ ꯊꯣꯀꯄꯥ ꯀꯦꯇꯦꯒꯣꯔꯤꯁꯤꯡ꯫",
    "创建分类" => "ꯀꯦꯇꯦꯒꯣꯔꯤꯁꯤꯡ ꯁꯦꯝꯕꯥ꯫",
    "编辑分类" => "ꯀꯦꯇꯦꯒꯣꯔꯤ ꯑꯦꯗꯤꯠ ꯇꯧꯕꯥ꯫",
    "删除分类" => "ꯀꯦꯇꯦꯒꯣꯔꯤ ꯃꯨꯠꯊꯠꯄꯥ꯫",
    "评论作者" => "ꯔꯤꯚꯤꯌꯨ ꯇꯧꯕꯥ ꯑꯣꯊꯣꯔ꯫",
    "评论内容" => "ꯀꯃꯦꯟꯇꯁꯤꯡ ꯄꯤꯈꯤ꯫",
    "评论列表" => "ꯀꯃꯦꯟꯇ ꯂꯤꯁ꯭ꯠ ꯇꯧꯕꯥ꯫",
    "评论文章" => "ꯑꯥꯔꯇꯤꯀꯂꯁꯤꯡ ꯌꯦꯡꯁꯤꯅꯕꯥ꯫",
    "回复内容" => "ꯀꯟꯇꯦꯟꯇ ꯔꯤꯞꯂꯥꯏ ꯇꯧꯕꯥ꯫",
    "回复评论" => "ꯀꯃꯦꯟꯇꯀꯤ ꯄꯥꯎꯈꯨꯝ ꯄꯤꯌꯨ꯫",
    "编辑评论" => "ꯑꯦꯗꯤꯇꯣꯔꯤꯌꯜ ꯀꯃꯦꯟꯇꯁꯤꯡ꯫",
    "待审评论" => "ꯀꯃꯦꯟꯇꯁꯤꯡ ꯂꯦꯞꯂꯤ꯫",
    "待审成功" => "ꯃꯥꯌ ꯄꯥꯛꯅꯥ ꯇ꯭ꯔꯥꯏꯑꯦꯜ ꯇꯧꯕꯥ ꯂꯦꯞꯂꯤ꯫",
    "待审失败" => "ꯇ꯭ꯔꯥꯏꯑꯦꯜ ꯇꯧꯗ꯭ꯔꯤꯉꯩꯗꯥ ꯃꯥꯌ ꯄꯥꯀꯄꯥ꯫",
    "审核成功" => "ꯔꯤꯚꯤꯌꯨ ꯃꯥꯌ ꯄꯥꯛꯂꯦ꯫",
    "审核失败" => "ꯑꯣꯗꯤꯠ ꯇꯧꯕꯥ ꯉꯃꯗꯕꯥ꯫",
    "删除评论" => "ꯀꯃꯦꯟꯇ ꯑꯗꯨ ꯃꯨꯠꯊꯠꯂꯨ꯫",
    "谷歌翻译" => "ꯒꯨꯒꯜ ꯇ꯭ꯔꯥꯟꯁꯂꯦꯠ",
    "检测语言" => "ꯂꯣꯜ ꯈꯉꯗꯣꯀꯄꯥ꯫",
    "别名错误" => "ꯑꯦꯂꯤꯌꯥꯁ ꯑꯦꯔꯥꯔ ꯇꯧꯕꯥ꯫",
    "语言错误" => "ꯂꯣꯂꯒꯤ ꯑꯁꯣꯌꯕꯥ ꯂꯩꯕꯥ꯫",
    "标签名称" => "ꯇꯦꯒꯀꯤ ꯃꯃꯤꯡ꯫",
    "标签列表" => "ꯇꯦꯒ ꯂꯤꯁ꯭ꯠ ꯇꯧꯕꯥ꯫",
    "标签重名" => "ꯃꯤꯡ ꯑꯅꯤ ꯊꯣꯀꯄꯥ ꯇꯦꯒꯁꯤꯡ꯫",
    "页面列表" => "ꯄꯦꯖ ꯂꯤꯁ꯭ꯠ꯫",
    "页面标题" => "ꯄꯦꯖꯒꯤ ꯃꯤꯡꯊꯣꯜ꯫",
    "父级页面" => "ꯃꯃꯥ-ꯃꯄꯥꯒꯤ ꯄꯦꯖ꯫",
    "顶级页面" => "ꯃꯊꯛꯀꯤ ꯄꯦꯖꯗꯥ ꯂꯩ꯫",
    "清除缓存" => "ꯀꯦꯁ ꯀ꯭ꯂꯤꯌꯔ ꯇꯧꯕꯥ꯫",
    "当前版本" => "ꯍꯧꯖꯤꯛ ꯂꯩꯔꯤꯕꯥ ꯚꯔꯖꯟ ꯑꯁꯤꯅꯤ꯫",
    "重置系统" => "ꯁꯤꯁ꯭ꯇꯦꯝ ꯔꯤꯁꯦꯠ ꯇꯧꯕꯥ꯫",
    "最新评论" => "ꯈ꯭ꯕꯥꯏꯗꯒꯤ ꯅꯧꯕꯥ ꯀꯃꯦꯟꯇ꯫",
    "联系我们" => "ꯑꯩꯈꯣꯌꯒꯥ ꯄꯥꯎ ꯐꯥꯑꯣꯅꯕꯤꯌꯨ꯫",
    "数据统计" => "ꯁ꯭ꯇꯦꯇꯤꯁ꯭ꯇꯤꯛꯁ꯫",
    "网站名称" => "ꯋꯦꯕꯁꯥꯏꯇꯀꯤ ꯃꯃꯤꯡ꯫",
    "网站地址" => "ꯋꯦꯕꯁꯥꯏꯇꯀꯤ ꯑꯦꯗ꯭ꯔꯦꯁ꯫",
    "链接列表" => "ꯂꯤꯉ꯭ꯛ ꯇꯧꯔꯕꯥ ꯂꯤꯁ꯭ꯠ꯫",
    "创建链接" => "ꯂꯤꯉ꯭ꯛ ꯁꯦꯝꯃꯨ꯫",
    "编辑链接" => "ꯂꯤꯉ꯭ꯛ ꯑꯦꯗꯤꯠ ꯇꯧꯕꯥ꯫",
    "删除链接" => "ꯂꯤꯉ꯭ꯛ ꯑꯗꯨ ꯂꯧꯊꯣꯀꯎ꯫",
    "日志标题" => "ꯂꯣꯒ ꯇꯥꯏꯇꯜ꯫",
    "消息内容" => "ꯃꯦꯁꯦꯖ ꯀꯟꯇꯦꯟꯇ꯫",
    "请求方法" => "ꯔꯤꯛꯕꯦꯁ꯭ꯠ ꯇꯧꯕꯒꯤ ꯃꯑꯣꯡ꯫",
    "请求路径" => "ꯔꯤꯛꯕꯦꯁ꯭ꯠ ꯇꯧꯕꯒꯤ ꯂꯝꯕꯤ꯫",
    "日志列表" => "ꯂꯣꯒ ꯂꯤꯁ꯭ꯠ꯫",
    "上传成功" => "ꯑꯄꯂꯣꯗ ꯇꯧꯕꯥ ꯃꯥꯌ ꯄꯥꯛꯂꯦ꯫",
    "媒体列表" => "ꯃꯤꯗꯤꯌꯥ ꯂꯤꯁ꯭ꯠ꯫",
    "开始上传" => "ꯑꯄꯂꯣꯗ ꯇꯧꯕꯥ ꯍꯧꯒꯗꯕꯅꯤ꯫",
    "等待上传" => "ꯑꯄꯂꯣꯗ ꯇꯧꯕꯥ ꯉꯥꯏꯔꯤ꯫",
    "重新上传" => "ꯑꯃꯨꯛ ꯍꯟꯅꯥ ꯑꯄꯂꯣꯗ ꯇꯧꯕꯥ꯫",
    "上传完成" => "ꯑꯄꯂꯣꯗ ꯇꯧꯕꯥ ꯂꯣꯏꯔꯦ꯫",
    "系统保留" => "ꯁꯤꯁ꯭ꯇꯦꯝ ꯔꯤꯖꯔꯚ ꯇꯧꯈ꯭ꯔꯦ꯫",
    "发现重复" => "ꯗꯨꯞꯂꯤꯀꯦꯠ ꯐꯪꯂꯦ꯫",
    "上传文件" => "ꯐꯥꯏꯂꯁꯤꯡ ꯑꯄꯂꯣꯗ ꯇꯧꯕꯥ꯫",
    "个在用，" => "ꯑꯃꯅꯥ ꯁꯤꯖꯤꯟꯅꯔꯤ, [...]",
    "文件重名" => "ꯐꯥꯏꯂꯒꯤ ꯃꯃꯤꯡ ꯑꯅꯤ ꯊꯣꯀꯄꯥ꯫",
    "删除文件" => "ꯐꯥꯏꯂꯁꯤꯡ ꯃꯨꯠꯊꯠꯄꯥ꯫",
    "创建页面" => "ꯄꯦꯖ ꯑꯃꯥ ꯁꯦꯝꯃꯨ꯫",
    "编辑页面" => "ꯄꯦꯖ ꯑꯦꯗꯤꯠ ꯇꯧꯕꯥ꯫",
    "删除页面" => "ꯄꯦꯖ ꯃꯨꯠꯊꯠꯄꯥ꯫",
    "角色列表" => "ꯔꯣꯜ ꯂꯤꯁ꯭ꯠ ꯇꯧꯕꯥ꯫",
    "角色名称" => "ꯊꯧꯗꯥꯡꯒꯤ ꯃꯃꯤꯡ꯫",
    "权限分配" => "ꯑꯌꯥꯕꯥ ꯄꯤꯕꯥ꯫",
    "创建角色" => "ꯊꯧꯗꯥꯡ ꯑꯃꯥ ꯁꯦꯝꯕꯥ꯫",
    "编辑角色" => "ꯑꯦꯗꯤꯠ ꯇꯧꯕꯒꯤ ꯊꯧꯗꯥꯡ꯫",
    "删除角色" => "ꯔꯣꯜ ꯃꯨꯠꯊꯠꯄꯥ꯫",
    "基础设置" => "ꯕꯦꯁꯤꯛ ꯁꯦꯇꯤꯡꯁꯤꯡ꯫",
    "高级设置" => "ꯑꯦꯗꯚꯥꯟꯁ ꯁꯦꯇꯤꯡꯁꯤꯡ꯫",
    "其他设置" => "ꯑꯇꯣꯞꯄꯥ ꯁꯦꯇꯤꯡꯁꯤꯡ꯫",
    "上传类型" => "ꯑꯄꯂꯣꯗ ꯇꯧꯕꯒꯤ ꯃꯈꯜ꯫",
    "限制大小" => "ꯂꯤꯃꯤꯠ ꯁꯥꯏꯖ ꯇꯧꯕꯥ꯫",
    "默认最大" => "ꯗꯤꯐꯣꯜꯇ ꯃꯦꯛꯁꯤꯃꯝ ꯑꯣꯏꯕꯥ꯫",
    "单点登录" => "ꯁꯥꯏꯟ ꯏꯟ ꯇꯧꯕꯥ꯫",
    "版权信息" => "ꯀꯣꯄꯤꯔꯥꯏꯠ ꯏꯅꯐꯣꯔꯃꯦꯁꯟ꯫",
    "链接地址" => "ꯂꯤꯉ꯭ꯛ ꯑꯦꯗ꯭ꯔꯦꯁ ꯇꯧꯕꯥ꯫",
    "热门文章" => "ꯃꯤꯌꯥꯝꯅꯥ ꯄꯥꯝꯅꯕꯥ ꯋꯥꯔꯣꯂꯁꯤꯡ꯫",
    "首页评论" => "ꯍꯣꯝ ꯄꯦꯖꯒꯤ ꯀꯃꯦꯟꯇꯁꯤꯡ꯫",
    "内容相关" => "ꯀꯟꯇꯦꯟꯇꯀꯥ ꯃꯔꯤ ꯂꯩꯅꯕꯥ꯫",
    "图片分页" => "ꯄꯤꯛꯆꯔ ꯄꯦꯖꯤꯡ ꯇꯧꯕꯥ꯫",
    "友情链接" => "ꯂꯤꯉ꯭ꯀꯁꯤꯡ꯫",
    "语音朗读" => "ꯚꯣꯏꯁ ꯔꯤꯗꯤꯡ ꯇꯧꯕꯥ꯫",
    "评论登录" => "ꯀꯃꯦꯟꯇ ꯂꯣꯒꯏꯟ ꯇꯧꯕꯥ꯫",
    "评论待审" => "ꯀꯃꯦꯟꯇ ꯑꯁꯤ ꯂꯦꯞꯂꯤ꯫",
    "保存成功" => "ꯃꯥꯌ ꯄꯥꯛꯅꯥ ꯁꯦꯚ ꯇꯧꯈꯤ꯫",
    "创建标签" => "ꯇꯦꯒꯁꯤꯡ ꯁꯦꯝꯕꯥ꯫",
    "编辑标签" => "ꯇꯦꯒ ꯑꯦꯗꯤꯠ ꯇꯧꯕꯥ꯫",
    "删除标签" => "ꯇꯦꯒ ꯃꯨꯠꯊꯠꯄꯥ꯫",
    "用户列表" => "ꯌꯨꯖꯔ ꯂꯤꯁ꯭ꯠ꯫",
    "注册时间" => "ꯔꯦꯖꯤꯁ꯭ꯠꯔꯦꯁꯅꯒꯤ ꯃꯇꯝ꯫",
    "首次登陆" => "ꯑꯍꯥꯅꯕꯥ ꯑꯣꯏꯅꯥ ꯂꯣꯒꯏꯟ ꯇꯧꯕꯥ꯫",
    "最后登陆" => "ꯑꯔꯣꯏꯕꯥ ꯂꯣꯒꯏꯟ ꯇꯧꯕꯥ꯫",
    "语言权限" => "ꯂꯣꯂꯒꯤ ꯑꯌꯥꯕꯥ ꯄꯤꯕꯥ꯫",
    "创建用户" => "ꯌꯨꯖꯔ ꯁꯦꯝꯕꯥ꯫",
    "不能修改" => "ꯃꯣꯗꯤꯐꯥꯏ ꯇꯧꯕꯥ ꯌꯥꯔꯣꯏ꯫",
    "编辑用户" => "ꯌꯨꯖꯔ ꯑꯦꯗꯤꯠ ꯇꯧꯕꯥ꯫",
    "删除用户" => "ꯌꯨꯖꯔꯁꯤꯡ ꯃꯨꯠꯊꯠꯄꯥ꯫",
    "最新文章" => "ꯈ꯭ꯕꯥꯏꯗꯒꯤ ꯅꯧꯕꯥ ꯋꯥꯔꯣꯂꯁꯤꯡ꯫",
    "搜索结果" => "ꯁꯥꯔꯆ ꯔꯤꯖꯜꯇꯁꯤꯡ꯫",
    "选择语言" => "ꯂꯣꯜ ꯑꯃꯥ ꯈꯅꯕꯤꯌꯨ꯫",
    "分类为空" => "ꯀꯦꯇꯦꯒꯣꯔꯤ ꯑꯁꯤ ꯏꯪꯅꯥ ꯂꯩ꯫",
    "收藏文章" => "ꯑꯥꯔꯇꯤꯀꯂꯁꯤꯡ ꯈꯣꯃꯖꯤꯅꯕꯥ꯫",
    "收藏成功" => "ꯀꯂꯦꯛꯁꯟ ꯃꯥꯌ ꯄꯥꯛꯂꯦ꯫",
    "取消收藏" => "ꯐꯦꯚꯥꯔꯤꯠꯁꯤꯡ ꯀꯦꯟꯁꯦꯜ ꯇꯧꯕꯥ꯫",
    "取消回复" => "ꯄꯥꯎꯈꯨꯝ ꯀꯦꯟꯁꯦꯜ ꯇꯧꯕꯥ꯫",
    "发表评论" => "ꯋꯥꯐꯝ ꯊꯝꯕ",
    "语音播放" => "ꯚꯣꯏꯁ ꯄ꯭ꯂꯦꯕꯦꯛ ꯇꯧꯕꯥ꯫",
    "回到顶部" => "ꯃꯊꯛꯇꯥ ꯍꯜꯂꯀꯎ꯫",
    "分类目录" => "ꯀꯦꯇꯦꯒꯣꯔꯤꯁꯤꯡ꯫",
    "下载内容" => "ꯀꯟꯇꯦꯟꯇ ꯗꯥꯎꯅꯂꯣꯗ ꯇꯧꯕꯥ꯫",
    "相关文章" => "ꯃꯔꯤ ꯂꯩꯅꯕꯥ ꯋꯥꯔꯣꯂꯁꯤꯡ꯫",
    "媒体合成" => "ꯃꯤꯗꯤꯌꯥ ꯁꯤꯟꯊꯦꯁꯤꯁ ꯇꯧꯕꯥ꯫",
    "语音合成" => "ꯁ꯭ꯄꯤꯆ ꯁꯤꯟꯊꯦꯁꯤꯁ ꯇꯧꯕꯥ꯫",
    "前端登录" => "ꯐ꯭ꯔꯟꯇ-ꯑꯦꯟꯗ ꯂꯣꯒꯏꯟ ꯇꯧꯕꯥ꯫",
    "前端注册" => "ꯐ꯭ꯔꯟꯇ-ꯑꯦꯟꯗ ꯔꯦꯖꯤꯁ꯭ꯠꯔꯦꯁꯟ ꯇꯧꯕꯥ꯫",
    "前端登出" => "ꯐ꯭ꯔꯟꯇꯑꯦꯟꯗ ꯂꯣꯒꯑꯥꯎꯠ ꯇꯧꯕꯥ꯫",
    "后台首页" => "ꯕꯦꯀꯒ꯭ꯔꯥꯎꯟꯗ ꯍꯣꯝ꯫",
    "欢迎页面" => "ꯇꯔꯥꯝꯅꯥ ꯑꯣꯀꯄꯒꯤ ꯄꯦꯖ꯫",
    "权限管理" => "ꯑꯣꯊꯣꯔꯤꯇꯤ ꯃꯦꯅꯦꯖꯃꯦꯟꯇ ꯇꯧꯕꯥ꯫",
    "用户管理" => "ꯌꯨꯖꯔ ꯃꯦꯅꯦꯖꯃꯦꯟꯇ꯫",
    "角色管理" => "ꯔꯣꯜ ꯃꯦꯅꯦꯖꯃꯦꯟꯇ ꯇꯧꯕꯥ꯫",
    "权限菜单" => "ꯑꯌꯥꯕꯥ ꯄꯤꯕꯒꯤ ꯃꯦꯅꯨ꯫",
    "分类管理" => "ꯀ꯭ꯂꯥꯁꯤꯐꯤꯀꯦꯁꯟ ꯃꯦꯅꯦꯖꯃꯦꯟꯇ ꯇꯧꯕꯥ꯫",
    "标签管理" => "ꯇꯦꯒ ꯃꯦꯅꯦꯖꯃꯦꯟꯇ ꯇꯧꯕꯥ꯫",
    "文章管理" => "ꯑꯥꯔꯇꯤꯀꯜ ꯃꯦꯅꯦꯖꯃꯦꯟꯇ꯫",
    "页面管理" => "ꯄꯦꯖ ꯃꯦꯅꯦꯖꯃꯦꯟꯇ ꯇꯧꯕꯥ꯫",
    "媒体管理" => "ꯃꯤꯗꯤꯌꯥ ꯃꯦꯅꯦꯖꯃꯦꯟꯇ ꯇꯧꯕꯥ꯫",
    "上传接口" => "ꯏꯟꯇꯔꯐꯦꯁ ꯑꯄꯂꯣꯗ ꯇꯧꯕꯥ꯫",
    "链接管理" => "ꯂꯤꯉ꯭ꯛ ꯃꯦꯅꯦꯖꯃꯦꯟꯇ꯫",
    "评论管理" => "ꯀꯃꯦꯟꯇ ꯃꯦꯅꯦꯖꯃꯦꯟꯇ ꯇꯧꯕꯥ꯫",
    "审核通过" => "ꯄꯔꯤꯈꯥ ꯄꯥꯁ ꯇꯧꯈꯤ꯫",
    "机器评论" => "ꯃꯦꯁꯤꯟ ꯔꯤꯚꯤꯌꯨ ꯇꯧꯕꯥ꯫",
    "系统管理" => "ꯁꯤꯁ꯭ꯇꯦꯝ ꯃꯦꯅꯦꯖꯃꯦꯟꯇ꯫",
    "系统设置" => "ꯁꯤꯁ꯭ꯇꯦꯝ ꯁꯦꯇꯤꯡꯁꯤꯡ꯫",
    "保存设置" => "ꯁꯦꯇꯤꯡꯁꯤꯡ ꯁꯦꯚ ꯇꯧꯕꯥ꯫",
    "恢复出厂" => "ꯐꯦꯛꯇꯔꯤ ꯔꯤꯁ꯭ꯇꯣꯔ ꯇꯧꯕꯥ꯫",
    "操作日志" => "ꯑꯣꯄꯔꯦꯁꯟ ꯂꯣꯒ ꯇꯧꯕꯥ꯫",
    "删除日志" => "ꯂꯣꯒ ꯃꯨꯠꯊꯠꯄꯥ꯫",
    "科西嘉语" => "ꯀꯣꯔꯁꯤꯀꯥꯟ ꯑꯣꯏꯕꯥ꯫",
    "瓜拉尼语" => "ꯒꯨꯋꯥꯔꯥꯅꯤ",
    "卢旺达语" => "ꯀꯤꯅꯤꯌꯥꯔꯋꯥꯟꯗꯥ꯫",
    "约鲁巴语" => "ꯌꯣꯔꯨꯕꯥ꯫",
    "尼泊尔语" => "ꯅꯦꯄꯥꯂꯤꯒꯤ꯫",
    "夏威夷语" => "ꯍꯋꯥꯏꯒꯤ꯫",
    "意第绪语" => "ꯌꯤꯗ꯭ꯗꯤꯁ꯫",
    "爱尔兰语" => "ꯑꯥꯏꯔꯤꯁ ꯑꯣꯏꯕꯥ꯫",
    "希伯来语" => "ꯍꯤꯕ꯭ꯔꯨ ꯂꯣꯟꯗꯥ꯫",
    "卡纳达语" => "ꯀꯟꯅꯥꯗꯥ꯫",
    "匈牙利语" => "ꯍꯨꯉ꯭ꯒꯥꯔꯩꯒꯤ ꯑꯣꯏꯕꯥ꯫",
    "泰米尔语" => "ꯇꯥꯃꯤꯜ",
    "阿拉伯语" => "ꯑꯔꯕꯤꯛ ꯂꯣꯟꯗꯥ꯫",
    "孟加拉语" => "ꯕꯦꯉ꯭ꯒꯂꯤꯗꯥ ꯂꯩꯕꯥ꯫",
    "萨摩亚语" => "ꯁꯥꯃꯣꯌꯥꯟ꯫",
    "班巴拉语" => "ꯕꯝꯕꯥꯔꯥ",
    "立陶宛语" => "ꯂꯤꯊꯨꯅꯤꯌꯥꯒꯤ ꯑꯣꯏꯕꯥ꯫",
    "马耳他语" => "ꯃꯥꯂꯇꯤꯖ ꯑꯣꯏꯕꯥ꯫",
    "土库曼语" => "ꯇꯨꯔꯀꯃꯦꯟꯗꯥ ꯂꯩꯕꯥ ꯌꯨ.ꯑꯦꯁ",
    "阿萨姆语" => "ꯑꯁꯥꯃꯤꯁ",
    "僧伽罗语" => "ꯁꯤꯅꯍꯥꯂꯥ꯫",
    "乌克兰语" => "ꯌꯨꯛꯔꯦꯅꯒꯤ꯫",
    "威尔士语" => "ꯋꯦꯂꯁꯇꯥ ꯂꯩꯕꯥ ꯌꯨ.ꯑꯦꯁ",
    "菲律宾语" => "ꯐꯤꯂꯤꯄꯤꯟꯁꯀꯤ ꯃꯤꯑꯣꯏꯅꯤ꯫",
    "艾马拉语" => "ꯑꯥꯏꯃꯥꯔꯥ",
    "泰卢固语" => "ꯇꯦꯂꯨꯒꯨ꯫",
    "多格来语" => "ꯗꯣꯒꯂꯥꯏ꯫",
    "迈蒂利语" => "ꯃꯥꯏꯊꯤꯂꯤ",
    "普什图语" => "ꯄꯁ꯭ꯇꯣ꯫",
    "迪维希语" => "ꯙꯤꯕꯦꯍꯤ",
    "卢森堡语" => "ꯂꯨꯛꯁꯦꯃꯕꯥꯔꯖꯤꯗꯥ ꯂꯩꯕꯥ ꯌꯨ.ꯑꯦꯁ",
    "土耳其语" => "ꯇꯨꯔꯀꯤ ꯑꯣꯏꯕꯥ꯫",
    "马其顿语" => "ꯃꯦꯁꯤꯗꯣꯅꯤꯌꯥꯟ ꯑꯣꯏꯕꯥ꯫",
    "卢干达语" => "ꯂꯨꯒꯥꯟꯗꯥ",
    "马拉地语" => "ꯃꯔꯥꯊꯤ꯫",
    "乌尔都语" => "ꯎꯔꯗꯨꯗꯥ ꯂꯩꯕꯥ꯫",
    "葡萄牙语" => "ꯄꯣꯔꯇꯨꯒꯤꯖ ꯂꯣꯟꯗꯥ꯫",
    "奥罗莫语" => "ꯑꯣꯔꯣꯃꯣ",
    "西班牙语" => "ꯁꯄꯦꯟꯒꯤ ꯂꯣꯜ",
    "弗里西语" => "ꯐ꯭ꯔꯤꯁꯤꯌꯥꯟ ꯑꯣꯏꯕꯥ꯫",
    "索马里语" => "ꯁꯣꯃꯥꯂꯤꯗꯥ ꯂꯩꯕꯥ ꯌꯨ.ꯑꯦꯁ",
    "齐切瓦语" => "ꯆꯤꯆꯦꯋꯥ꯫",
    "旁遮普语" => "ꯄꯟꯖꯥꯕꯤ꯫",
    "巴斯克语" => "ꯕꯥꯁ꯭ꯀꯦꯠ ꯇꯧꯕꯥ꯫",
    "意大利语" => "ꯏꯇꯥꯂꯤꯒꯤ ꯑꯣꯏꯕꯥ꯫",
    "塔吉克语" => "ꯇꯥꯖꯤꯛ ꯑꯣꯏꯕꯥ꯫",
    "克丘亚语" => "ꯀ꯭ꯋꯦꯆꯥ",
    "奥利亚语" => "ꯑꯣꯔꯤꯌꯥ꯫",
    "哈萨克语" => "ꯀꯖꯥꯈꯗꯥ ꯂꯩꯕꯥ ꯌꯨ.ꯑꯦꯁ",
    "林格拉语" => "ꯂꯤꯡꯒꯥꯂꯥ",
    "塞佩蒂语" => "ꯁꯦꯄꯦꯇꯤ꯫",
    "塞索托语" => "ꯁꯦꯁꯣꯊꯣꯗꯥ ꯂꯩꯕꯥ꯫",
    "维吾尔语" => "ꯎꯏꯘꯨꯔ꯫",
    "ICP No.001" => "ꯑꯥꯏ.ꯁꯤ.ꯄꯤ.ꯅꯪ.꯰꯰꯱",
    "用户名" => "ꯌꯨꯖꯔꯅꯦꯝ꯫",
    "验证码" => "ꯚꯦꯔꯤꯐꯤꯀꯦꯁꯟ ꯀꯣꯗ꯫",
    "记住我" => "ꯑꯩꯕꯨ ꯅꯤꯡꯁꯤꯡꯕꯤꯌꯨ꯫",
    "手机号" => "ꯐꯣꯟ ꯅꯝꯕꯔ꯫",
    "请输入" => "ꯆꯥꯅꯕꯤꯗꯨꯅꯥ ꯑꯦꯟꯇꯔ ꯇꯧꯕꯤꯌꯨ꯫",
    "请选择" => "ꯆꯥꯅꯕꯤꯗꯨꯅꯥ ꯈꯅꯕꯤꯌꯨ꯫",
    "浏览量" => "ꯚꯤꯎꯁꯤꯡ꯫",
    "回收站" => "ꯔꯤꯁꯥꯏꯀꯜ ꯇꯧꯕꯥ ꯕꯤꯟ꯫",
    "菜单树" => "ꯃꯦꯅꯨ ꯇ꯭ꯔꯤ꯫",
    "控制器" => "ꯀꯟꯠꯔꯣꯂꯔ ꯑꯣꯏꯅꯥ ꯊꯕꯛ ꯇꯧꯔꯤ꯫",
    "第一页" => "ꯑꯍꯥꯅꯕꯥ ꯄꯦꯖ꯫",
    "最后页" => "ꯑꯔꯣꯏꯕꯥ ꯄꯦꯖꯗꯥ꯫",
    "筛选列" => "ꯀꯣꯂꯃꯁꯤꯡ ꯐꯤꯜꯇꯔ ꯇꯧꯕꯥ꯫",
    "删除行" => "ꯔꯣ ꯃꯨꯠꯊꯠꯄꯥ꯫",
    "还原行" => "ꯑꯟꯗꯣ ꯔꯣ꯫",
    "重命名" => "ꯃꯃꯤꯡ ꯍꯣꯡꯗꯣꯀꯎ꯫",
    "国际化" => "ꯒ꯭ꯂꯣꯕꯦꯂꯥꯏꯖꯦꯁꯟ꯫",
    "文章数" => "ꯑꯥꯔꯇꯤꯀꯂꯁꯤꯡꯒꯤ ꯃꯁꯤꯡ꯫",
    "机器人" => "ꯔꯣꯕꯣꯠ ꯑꯁꯤꯅꯤ꯫",
    "星期日" => "ꯅꯣꯡꯃꯥꯏꯖꯤꯡ",
    "星期一" => "ꯅꯤꯡꯊꯧꯀꯥꯕ",
    "星期二" => "ꯂꯩꯕꯥꯛꯄꯣꯛꯄ",
    "星期三" => "ꯌꯨꯝꯁꯀꯩꯁ",
    "星期四" => "ꯁꯒꯣꯜꯁꯦꯜ",
    "星期五" => "ꯏꯔꯥꯏ",
    "星期六" => "ꯊꯥꯡꯖ",
    "会员数" => "ꯃꯦꯝꯕꯔ ꯃꯁꯤꯡ꯫",
    "评论数" => "ꯀꯃꯦꯟꯇꯁꯤꯡꯒꯤ ꯃꯁꯤꯡ꯫",
    "文件名" => "ꯐꯥꯏꯂꯒꯤ ꯃꯃꯤꯡ꯫",
    "音视频" => "ꯑꯣꯗꯤꯑꯣ ꯑꯃꯁꯨꯡ ꯚꯤꯗꯤꯑꯣ꯫",
    "扩展名" => "ꯑꯦꯛꯁꯇꯦꯟꯁꯅꯒꯤ ꯃꯃꯤꯡ꯫",
    "未使用" => "ꯁꯤꯖꯤꯟꯅꯗꯕꯥ꯫",
    "个无效" => "ꯆꯠꯅꯗ꯭ꯔꯕ",
    "共删除" => "ꯑꯄꯨꯅꯕꯥ ꯃꯨꯠꯊꯠꯈ꯭ꯔꯦ꯫",
    "个文件" => "ꯐꯥꯏꯂꯁꯤꯡ ꯌꯥꯑꯣꯔꯤ꯫",
    "备案号" => "ꯀꯦꯁ ꯅꯝꯕꯔ꯫",
    "轮播图" => "ꯀꯦꯔꯣꯁꯤꯜ ꯇꯧꯕꯥ꯫",
    "条显示" => "ꯕꯥꯔ ꯗꯤꯁꯞꯂꯦ ꯇꯧꯕꯥ꯫",
    "上一页" => "ꯃꯃꯥꯡꯒꯤ ꯄꯦꯖꯗꯥ꯫",
    "下一页" => "ꯃꯊꯪꯒꯤ ꯄꯦꯖꯗꯥ꯫",
    "未分类" => "ꯑꯅꯀꯦꯇꯦꯒꯣꯔꯤꯗꯥ ꯌꯨꯝꯐꯝ ꯑꯣꯏꯕꯥ꯫",
    "空标签" => "ꯏꯝꯄꯣꯔꯠ ꯂꯦꯕꯦꯜ꯫",
    "无标题" => "ꯇꯥꯏꯇꯜ ꯂꯩꯇꯕꯥ꯫",
    "控制台" => "ꯀꯟꯁꯣꯜ ꯇꯧꯕꯥ꯫",
    "登录中" => "ꯂꯣꯒ ꯏꯟ ꯇꯧꯕꯥ꯫",
    "注册中" => "ꯔꯦꯖꯤꯁ꯭ꯇꯔ ꯇꯧꯕꯥ꯫",
    "跳转中" => "ꯔꯤꯗꯔꯥꯏꯖ ꯇꯧꯕꯥ꯫",
    "提交中" => "ꯁꯕꯃꯤꯠ ꯇꯧꯕꯥ꯫",
    "审核中" => "ꯔꯤꯚꯤꯌꯨꯒꯤ ꯃꯈꯥꯗꯥ ꯂꯩꯔꯤ꯫",
    "请稍后" => "ꯆꯥꯅꯕꯤꯗꯨꯅꯥ ꯉꯥꯏꯕꯤꯌꯨ꯫",
    "篇文章" => "ꯄꯣꯠꯂꯝ",
    "客户端" => "ꯀ꯭ꯂꯥꯏꯟꯠ",
    "没有了" => "ꯂꯦꯃꯍꯧꯔꯤꯕꯥ ꯑꯃꯠꯇꯥ ꯂꯩꯇꯦ꯫",
    "后评论" => "ꯀꯃꯦꯟꯇꯁꯤꯡ ꯄꯣꯁ꯭ꯠ ꯇꯧꯕꯤꯌꯨ꯫",
    "未命名" => "ꯃꯃꯤꯡ ꯂꯩꯔꯕꯥ꯫",
    "软删除" => "ꯁꯣꯐ꯭ꯠ ꯗꯤꯂꯤꯠ ꯇꯧꯕꯥ꯫",
    "语言包" => "ꯂꯣꯂꯒꯤ ꯄꯦꯛ꯫",
    "豪萨语" => "ꯍꯧꯁꯥ꯫",
    "挪威语" => "ꯅꯣꯔꯋꯦꯒꯤ ꯃꯤꯑꯣꯏꯅꯤ꯫",
    "贡根语" => "ꯒꯣꯡꯒꯥꯟ ꯂꯣꯜ꯫",
    "拉丁语" => "ꯂꯦꯇꯤꯟ ꯑꯣꯏꯕꯥ꯫",
    "捷克语" => "ꯆꯦꯛꯀꯤ ꯂꯃꯗꯥ ꯑꯦꯝ",
    "波斯语" => "ꯄꯥꯔꯁꯤꯌꯥꯟ ꯑꯣꯏꯕꯥ꯫",
    "印地语" => "ꯍꯤꯟꯗꯤ꯫",
    "冰岛语" => "ꯑꯥꯏꯁꯂꯦꯟꯗꯒꯤ ꯑꯣꯏꯕꯥ꯫",
    "契维语" => "ꯇ꯭ꯋꯤ",
    "高棉语" => "ꯀꯝꯕꯣꯗꯤꯌꯥꯒꯤ ꯑꯣꯏꯕꯥ꯫",
    "丹麦语" => "ꯗꯥꯅꯤꯁ ꯑꯣꯏꯕꯥ꯫",
    "修纳语" => "ꯁꯣꯅꯥ꯫",
    "越南语" => "ꯚꯤꯌꯦꯠꯅꯥꯃꯒꯤ ꯃꯤꯑꯣꯏꯁꯤꯡ꯫",
    "宿务语" => "ꯁꯦꯕꯨꯌꯥꯅꯣ꯫",
    "波兰语" => "ꯄꯣꯂꯤꯁ ꯇꯧꯕꯥ꯫",
    "鞑靼语" => "ꯇꯥꯇꯔ꯫",
    "老挝语" => "ꯂꯥꯑꯣꯗꯥ ꯂꯩꯕꯥ ꯌꯨ.ꯑꯦꯁ",
    "瑞典语" => "ꯁ꯭ꯕꯤꯗꯦꯅꯒꯤ ꯑꯣꯏꯕꯥ꯫",
    "缅甸语" => "ꯕꯔꯃꯤꯖ ꯑꯣꯏꯕꯥ꯫",
    "信德语" => "ꯁꯤꯟꯙꯤ꯫",
    "马来语" => "ꯃꯥꯂꯦꯃꯗꯥ ꯂꯩꯕꯥ꯫",
    "伊博语" => "ꯏꯒꯕꯣ꯫",
    "希腊语" => "ꯒ꯭ꯔꯤꯛ ꯑꯣꯏꯕꯥ꯫",
    "芬兰语" => "ꯐꯤꯅꯤꯁꯀꯤ ꯋꯥꯐꯝ꯫",
    "埃维语" => "ꯏꯋꯦ",
    "毛利语" => "ꯃꯥꯑꯣꯔꯤꯒꯤ꯫",
    "荷兰语" => "ꯗꯆ꯭ꯁꯇꯥ ꯂꯩꯕꯥ ꯌꯨ.ꯑꯦꯁ",
    "蒙古语" => "ꯃꯣꯉ꯭ꯒꯣꯂꯤꯌꯅꯒꯤ ꯑꯣꯏꯕꯥ꯫",
    "米佐语" => "ꯝꯤꯖꯣ",
    "世界语" => "ꯑꯦꯁ꯭ꯄꯦꯔꯥꯟꯇꯣꯗꯥ ꯂꯩꯕꯥ꯫",
    "印尼语" => "ꯏꯟꯗꯣꯅꯦꯁꯤꯌꯥꯒꯤ...",
    "宗加语" => "ꯖꯣꯉ꯭ꯒꯥ꯫",
    "密码" => "ꯑꯔꯣꯟꯕꯁꯣ",
    "登录" => "ꯂꯣꯒ ꯏꯟ ꯇꯧꯕꯤꯌꯨ꯫",
    "重置" => "ꯔꯤꯁꯦꯠ ꯇꯧꯕꯥ꯫",
    "信息" => "ꯏꯄꯥꯎ",
    "成功" => "ꯃꯥꯏ ꯄꯥꯛꯄ",
    "失败" => "ꯃꯥꯏꯊꯤꯕ",
    "昵称" => "ꯅꯤꯛ ꯅꯦꯝ꯫",
    "选填" => "ꯈꯟꯕ ꯌꯥꯕ",
    "注册" => "ꯃꯤꯡ ꯆꯟꯕ",
    "错误" => "ꯑꯁꯣꯏꯕ",
    "头像" => "ꯑꯋꯇꯥꯔꯗꯥ ꯂꯩꯕꯥ꯫",
    "上传" => "ꯑꯄꯂꯣꯗ ꯇꯧꯕꯥ꯫",
    "保存" => "ꯀꯟꯕ",
    "标题" => "ꯃꯤꯡꯊꯣꯜ",
    "别名" => "ꯑꯦꯂꯤꯌꯥꯁ ꯍꯥꯌꯅꯥ ꯈꯉꯅꯕꯥ꯫",
    "描述" => "ꯁꯟꯗꯣꯛꯅ ꯇꯥꯛꯄ",
    "封面" => "ꯃꯥꯏꯈꯨꯝ",
    "分类" => "ꯀ꯭ꯂꯥꯁꯤꯐꯤꯀꯦꯁꯟ ꯇꯧꯕꯥ꯫",
    "状态" => "ꯑꯣꯏꯔꯤꯕ ꯐꯤꯚꯝ",
    "正常" => "ꯃꯍꯧꯁꯥꯒꯨꯝꯕ",
    "禁用" => "ꯌꯥꯍꯟꯗꯕ",
    "作者" => "ꯑꯏꯕ",
    "日期" => "ꯇꯥꯡ",
    "添加" => "ꯍꯥꯄꯆꯤꯜꯂꯨ꯫",
    "刷新" => "ꯔꯤꯐ꯭ꯔꯦꯁ ꯇꯧꯕꯥ꯫",
    "标签" => "ꯃꯃꯤꯡ ꯊꯥꯟꯕ",
    "浏览" => "ꯕ꯭ꯔꯣꯎꯖ ꯇꯧꯕꯥ꯫",
    "点赞" => "ꯄꯥꯝꯕ",
    "评论" => "ꯋꯥꯐꯝ ꯊꯝꯕ",
    "操作" => "ꯑꯣꯄꯔꯦꯠ ꯇꯧꯕꯥ꯫",
    "阅读" => "ꯄꯥꯕ",
    "编辑" => "ꯁꯦꯃꯗꯣꯛꯄ",
    "删除" => "ꯃꯨꯊꯠꯄ",
    "翻译" => "ꯋꯥꯍꯟꯊꯣꯛ ꯍꯟꯕ",
    "内容" => "ꯑꯌꯥꯎꯕ",
    "分割" => "ꯁꯦꯒꯃꯦꯟꯇꯦꯁꯟ ꯇꯧꯕꯥ꯫",
    "建议" => "ꯄꯥꯎꯇꯥꯈ",
    "非法" => "ꯑꯏꯟꯅ ꯌꯥꯗꯕ",
    "还原" => "ꯍꯟꯊꯍꯅꯕꯥ꯫",
    "方法" => "ꯊꯧꯑꯪꯡ",
    "排序" => "ꯈꯟꯗꯣꯛꯄ",
    "图标" => "ꯑꯥꯏꯀꯟ꯫",
    "菜单" => "ꯃꯦꯅꯨꯗꯥ ꯌꯥꯑꯣꯔꯤ꯫",
    "时间" => "ꯃꯇꯝ",
    "匿名" => "ꯃꯁꯛ ꯈꯪꯗꯕ",
    "确定" => "ꯁꯣꯏꯗꯕ",
    "取消" => "ꯀꯛꯄ",
    "跳转" => "ꯆꯣꯡꯕ",
    "页码" => "ꯄꯦꯖ ꯅꯝꯕꯔ꯫",
    "共计" => "ꯑꯄꯨꯟꯕ",
    "每页" => "ꯄꯦꯖ ꯑꯃꯗꯥ ꯄꯤꯔꯤ꯫",
    "导出" => "ꯃꯤꯔꯝꯗ ꯡꯥꯗꯣꯡꯄ",
    "打印" => "ꯄ꯭ꯔꯤꯟꯇ ꯇꯧꯕꯥ꯫",
    "回复" => "ꯄꯥꯎꯈꯨꯝ ꯄꯤꯕ",
    "路径" => "ꯂꯝꯕꯤ",
    "预览" => "ꯄ꯭ꯔꯤꯚꯤꯎ ꯇꯧꯕꯥ꯫",
    "名称" => "ꯃꯤꯡ",
    "行数" => "ꯔꯣꯁꯤꯡ꯫",
    "相册" => "ꯐꯣꯇꯣ ꯑꯦꯂꯕꯝ꯫",
    "文章" => "ꯄꯣꯠꯂꯝ",
    "待审" => "ꯄꯟꯗꯨꯅ ꯂꯩꯕ",
    "通过" => "ꯂꯥꯟꯅꯕ",
    "垃圾" => "ꯀꯥꯟꯅꯗꯕ",
    "提取" => "ꯆꯤꯡꯊꯣꯛꯄ",
    "生成" => "ꯄꯨꯊꯣꯛꯄ",
    "定位" => "ꯊꯥꯛ",
    "填充" => "ꯐꯤꯜꯂꯤꯡ ꯇꯧꯕꯥ꯫",
    "帮助" => "ꯇꯦꯡꯕꯥꯡꯕ",
    "返回" => "ꯍꯜꯂꯛꯄ",
    "语言" => "ꯂꯣꯜ",
    "宽屏" => "ꯋꯥꯏꯗꯁ꯭ꯛꯔꯤꯟ ꯇꯧꯕꯥ꯫",
    "普通" => "ꯏꯆꯝ ꯆꯝꯕ",
    "后台" => "ꯕꯦꯀꯁ꯭ꯇꯦꯖꯗꯥ ꯂꯩꯕꯥ꯫",
    "前台" => "ꯐ꯭ꯔꯟꯇ ꯗꯦꯁ꯭ꯛ ꯂꯩꯕꯥ꯫",
    "登出" => "ꯁꯥꯏꯟ ꯑꯥꯎꯠ ꯇꯧꯕꯥ꯫",
    "版本" => "ꯃꯈꯜ",
    "关于" => "ꯃꯔꯝꯗꯤ",
    "微信" => "ꯋꯤꯆꯦꯠ ꯇꯧꯕꯥ꯫",
    "会员" => "ꯃꯦꯝꯕꯔ ꯑꯣꯏꯔꯤ꯫",
    "地址" => "ꯂꯩꯐꯝ",
    "类型" => "ꯃꯈꯜ",
    "图片" => "ꯃꯃꯤ",
    "文件" => "ꯆꯦ ꯆꯥꯡ",
    "失效" => "ꯆꯠꯅꯗ꯭ꯔꯕ",
    "大小" => "ꯆꯥꯎꯕꯒꯤ ꯆꯥꯡ",
    "下载" => "ꯂꯧꯊꯕ",
    "导航" => "ꯅꯦꯚꯤꯒꯦꯁꯟ ꯇꯧꯕꯥ꯫",
    "站标" => "ꯁꯥꯏꯠ ꯂꯣꯒꯣ꯫",
    "单位" => "ꯄꯊꯥꯞ",
    "加速" => "ꯈꯣꯡꯖꯦꯜ ꯌꯥꯡꯈꯠꯍꯅꯕꯥ꯫",
    "角色" => "ꯊꯧꯗꯥꯡ",
    "首页" => "ꯐ꯭ꯔꯟꯇ ꯄꯦꯖꯗꯥ ꯌꯥꯑꯣꯔꯤ꯫",
    "个数" => "ꯃꯁꯤꯡ",
    "我的" => "ꯑꯩꯍꯥꯛꯀꯤ",
    "说道" => "ꯍꯥꯏꯈꯤꯕ",
    "手机" => "ꯁꯦꯜ ꯐꯣꯟ ꯇꯧꯕꯥ꯫",
    "可选" => "ꯈꯟꯕ ꯌꯥꯕ",
    "搜索" => "ꯊꯤꯕ",
    "页面" => "ꯄꯦꯖꯗꯥ ꯌꯥꯑꯣꯔꯤ꯫",
    "查看" => "ꯌꯦꯡꯁꯤꯟꯕ",
    "创建" => "ꯁꯦꯝꯕ",
    "更新" => "ꯔꯤꯟꯌꯨ ꯇꯧꯕꯥ꯫",
    "英语" => "ꯍꯪꯂꯤꯁ",
    "法语" => "ꯐ꯭ꯔꯦꯟꯆ",
    "俄语" => "ꯔꯁꯤꯌꯥꯒꯤ꯫",
    "梵语" => "ꯁꯪꯁꯀ꯭ꯔꯤꯠ",
    "日语" => "ꯖꯄꯥꯅꯤꯖ ꯑꯣꯏꯕꯥ꯫",
    "泰语" => "ꯊꯥꯏꯂꯦꯟꯗꯒꯤ꯫",
    "苗语" => "ꯍ꯭ꯃꯣꯡ꯫",
    "德语" => "ꯖꯔꯃꯅꯤꯒꯤ ꯑꯣꯏꯕꯥ꯫",
    "韩语" => "ꯀꯣꯔꯤꯌꯥꯒꯤ ꯑꯣꯏꯕꯥ꯫",
    "请" => "ꯆꯥꯟꯕꯤꯗꯨꯅꯥ",
];
