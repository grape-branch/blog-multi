<?php
/**
 * @author admin
 * @date 2024-06-06 05:02:03
 * @desc 马尔加什语语言包
 */
return [
    "迷你mvc小框架，一套用于开发各类管理系统的通用型后台管理系统模板，基于开源免费的 layui 的制作，内置各类实际业务场景下相对丰富的示例。" => "Mini MVC dia rafitra kely, fitambarana modely rafitra fitantanana backend ho an\'ny fampandrosoana rafitra fitantanana isan-karazany.",
    "除非特别说明，本博客均为原创，如需转载，请以链接形式标明来源出处。" => "Raha tsy misy filazana hafa, ity blôgy ity dia tany am-boalohany Raha mila averina atao pirinty ianao, azafady mba asehoy amin\'ny endrika rohy ny loharano.",
    "重置系统，将删除用户所有数据和附件，恢复到出厂设置。" => "Ny famerenan\'ny rafitra dia hamafa ny angon\'ny mpampiasa sy ny fametahana rehetra ary hamerina azy amin\'ny toeran\'ny orinasa.",
    "用户名只能英文字母数字下划线或中划线，位数" => "Ny solonanarana dia tsy afaka mirakitra afa-tsy litera amin\'ny teny anglisy, isa, tsipitsipika na tsipitsipika, ary ny isan\'ny isa",
    "昵称只能中英文字母数字下划线中划线，位数" => "Ny anaram-bosotra dia tsy mety ahitana litera sinoa sy anglisy, isa, tsipika, tsipitsipika, ary isa ihany.",
    "有道接口不支持，需科学上网使用谷歌接口。" => "Tsy tohanana ny interface Youdao.",
    "别名只能英文字母数字下划线中划线，位数" => "Ny alia dia tsy misy afa-tsy litera amin\'ny teny anglisy, isa, tsipika, tsipitsipika, ary isa.",
    "计算孤立文件需要较长时间，确定继续吗？" => "Mitaky fotoana maharitra ny kajy ny rakitra kamboty.",
    "语音不支持，需科学上网使用谷歌接口。" => "Tsy tohanana ny Voice, ka mila mampiasa ny interface Google ianao raha te hidirana amin\'ny Internet amin\'ny lafiny siantifika.",
    "您的账号已在别处登录，请重新登录！" => "Niditra tany an-kafa ny kaontinao, midira indray azafady!",
    "你的账号已被禁用，请联系管理员" => "Nofoanana ny kaontinao, mifandraisa amin\'ny mpitantana",
    "文件有在使用中，请重新计算状态" => "Ampiasaina ny rakitra, azafady avereno kajy ny sata",
    "目标语言权限不够，请联系管理员" => "Tsy ampy ny fahazoan-dàlana amin\'ny fiteny kendrena, mifandraisa amin\'ny mpitantana",
    "真的标记垃圾评论选中的行吗？" => "Tena marihinao ho spam ve ny andalana voafantina?",
    "源语言别名为空，请联系源作者" => "Foana ny solon\'ny teny loharano, mifandraisa amin\'ny mpanoratra loharano",
    "点击上传，或将文件拖拽到此处" => "Tsindrio raha hampiakatra, na hisintona ny rakitra eto",
    "合成成功！再次点击按钮下载。" => "Nahomby ny synthesis! Tsindrio indray ny bokotra raha hisintona.",
    "没有语言权限，请联系管理员" => "Tsy mahazo alalana amin\'ny fiteny, mifandraisa amin\'ny mpitantana",
    "源语言别名为空，不能国际化" => "Foana ny solon-teny loharano ary tsy azo atao iraisam-pirenena.",
    "源语言与目标语言都不能为空" => "Na ny fiteny loharano na ny fiteny kendrena dia tsy mety ho foana.",
    "源语言与目标语言数量不一致" => "Tsy mifanaraka ny isan\'ny fiteny loharano sy kendrena",
    "删除文件失败，文件有在使用" => "Tsy voafafa ny rakitra, ampiasaina ny rakitra",
    "角色不存在，请联系管理员" => "Tsy misy ny andraikitra, mifandraisa amin\'ny mpitantana",
    "角色被禁用，请联系管理员" => "Tsy afaka ny anjara, mifandraisa amin\'ny mpitantana",
    "功能不存在，请联系管理员" => "Tsy misy ny fiasa, mifandraisa amin\'ny mpitantana",
    "功能被禁用，请联系管理员" => "Tsy miasa ny fiasa, mifandraisa amin\'ny mpitantana",
    "真的彻底删除选中的行吗？" => "Tena mamafa tanteraka ny laharana voafantina ve izany?",
    "真的审核通过选中的行吗？" => "Tena ekena tokoa ve ireo andalana voafantina?",
    "目标语言与源语言不能相同" => "Ny fiteny kendrena dia tsy mety mitovy amin\'ny fiteny loharano",
    "提取语言包与源语言不一致" => "Ny fonosan\'ny fiteny nalaina dia tsy mifanaraka amin\'ny fiteny loharano",
    "文件有在使用，删除失败！" => "Ny rakitra dia ampiasaina ary tsy nahomby ny famafana!",
    "没有权限，请联系管理员" => "Tsy nahazo alalana, mifandraisa amin\'ny mpitantana",
    "真的待审核选中的行吗？" => "Tena tsipika mila jerena sy mifantina tokoa ve izany?",
    "目标语言包有空行，行数" => "Ny fonosan\'ny fiteny kendrena dia manana tsipika tsy misy dikany, ny isan\'ny andalana",
    "此操作恢复到出厂设置？" => "Miverina amin\'ny firafitry ny orinasa ity hetsika ity?",
    "朗读错误，请联网后再试" => "Fahadisoana amin\'ny famakiana, midira amin\'ny Internet azafady ary andramo indray",
    "还没有添加分类描述信息" => "Tsy mbola nampiana fampahalalana momba ny sokajy",
    "本地媒体已失效或不存在" => "Ny haino aman-jery ao an-toerana dia tsy manan-kery na tsy misy",
    "库尔德语（库尔曼吉语）" => "Kiorda (Kurmanji)",
    "作者名未注册或被禁用" => "Tsy voasoratra anarana na kilemaina ny anaran\'ny mpanoratra",
    "真的删除选中的行吗？" => "Fafainao tokoa ve ny andalana voafantina?",
    "真的还原选中的行吗？" => "Tena mamerina ny andalana voafantina ve ianao?",
    "真的删除行或子行么？" => "Tena te-hamafa andalana na zana-tsipika ve ianao?",
    "目标语言包未发现空行" => "Tsy hita ao amin\'ny fonosan\'ny fiteny kendrena ny tsipika banga",
    "该语言不支持语音朗读" => "Ity fiteny ity dia tsy manohana ny famakiana feo",
    "语言包数据未发生改变" => "Tsy niova ny angona fonosan\'ny fiteny",
    "欢迎使用后台管理系统" => "Tongasoa eto amin\'ny rafitra fitantanana backend",
    "文件有在使用或已失效" => "Ny rakitra dia ampiasaina na lany daty",
    "合成失败，请稍后再试" => "Tsy nahomby ny synthesis, andramo indray rehefa afaka kelikely",
    "&copy; 2021-2023 Company, Inc." => "© 2021-2023 Company, Inc.",
    "梅泰语（曼尼普尔语）" => "Meitei (Manipuri)",
    "两次密码输入不一致" => "Ny tenimiafina roa dia tsy mifanaraka",
    "该用户密码不可修改" => "Tsy azo ovaina ity tenimiafina mpampiasa ity",
    "添加文章时创建标签" => "Mamorona marika rehefa manampy lahatsoratra",
    "删除文章时删除评论" => "Fafao ny fanehoan-kevitra rehefa mamafa lahatsoratra",
    "控制器与方法已存在" => "Ny fanaraha-maso sy ny fomba dia efa misy",
    "标题或名称不能为空" => "Tsy azo foanana ny lohateny na anarana",
    "请选择一种语言朗读" => "Misafidiana fiteny hovakiana mafy azafady",
    "分类有文章不能删除" => "Misy lahatsoratra ao amin\'ny sokajy tsy azo fafana",
    "审核为垃圾评论成功" => "Nodinihina soa aman-tsara ho spam",
    "审核为垃圾评论失败" => "Tsy nahomby tamin\'ny fanehoan-kevitra momba ny spam",
    "确定要登出站点吗？" => "Tena te-hivoaka amin\'ny tranokala ve ianao?",
    "网站名称或地址为空" => "Foana ny anaran\'ny tranokala na ny adiresiny",
    "网站名称或地址重名" => "Anarana na adiresin\'ny tranokala kopia",
    "允许上传的文件类型" => "Karazana rakitra navela hampidirina",
    "标签有文章不能删除" => "Tsy azo fafana ny lahatsoratra misy marika",
    "人觉得这篇文章很赞" => "Mihevitra ny olona fa tsara ity lahatsoratra ity",
    "还没有页面描述信息" => "Tsy mbola misy fampahalalana momba ny pejy",
    "回复与评论内容重复" => "Adikao ny votoatin\'ny valiny sy fanehoan-kevitra",
    "失败！请稍后再试。" => "tsy mahomby! Andramo indray rahatrizay.",
    "主耶稣基督里的教会" => "ny fiangonana ao amin’i Jesosy Kristy Tompo",
    "库尔德语（索拉尼）" => "Kiorda (Sorani)",
    "布尔语(南非荷兰语)" => "Booleana (Afrikaans)",
    "用户名或密码错误" => "diso anarana mpampiasa na tenimiafina",
    "源语言必须是中文" => "Ny fiteny loharano dia tokony ho Sinoa",
    "目标语言别名为空" => "Foana ny alias amin\'ny fiteny kendrena",
    "国际化文章时标签" => "Tags lahatsoratra iraisam-pirenena",
    "确定清除缓存吗？" => "Tena te hanala cache ve ianao?",
    "超过的单文件大小" => "Nihoatra ny haben\'ny rakitra tokana",
    "超过前端表单限制" => "Nihoatra ny fetran\'ny endriny ivelany",
    "目标没有写入权限" => "Tsy manana alalana hanoratra ny tanjona",
    "不允许的上传类型" => "Karazana fampiakarana tsy azo atao",
    "文件大小不能超过" => "Tsy azo hihoatra ny haben\'ny rakitra",
    "保存基础设置成功" => "Voatahiry soa aman-tsara ny kira fototra",
    "首次基础设置成功" => "Tafita ny fanamboarana fototra voalohany",
    "正在合成，请稍后" => "Mamorona, andraso azafady",
    "不合理的请求方法" => "Fomba fangatahana tsy mitombina",
    "Session无效或过期" => "Tsy manan-kery na lany daty ny fivoriana",
    "手机号码不正确" => "diso ny laharan-telefaona",
    "手机号码已存在" => "Efa misy ny laharana finday",
    "标题或内容为空" => "Foana ny lohateny na atiny",
    "创建文章时标签" => "Tags rehefa mamorona lahatsoratra",
    "编辑文章时标签" => "Tags rehefa manova lahatsoratra",
    "真的删除行么？" => "Te-hamafa ny tsipika ve ianao?",
    "请输入菜单名称" => "Ampidiro anarana sakafo azafady",
    "请先删除子菜单" => "Fafao aloha ny submenu azafady",
    "未登录访问后台" => "Midira ao amin\'ny backend nefa tsy miditra",
    "Cookie无效或过期" => "Tsy mety na lany daty ny cookie",
    "真的还原行么？" => "Tena azo atao ve ny mamerina azy io?",
    "编辑器内容为空" => "Foana ny votoatin\'ny mpamoaka lahatsoratra",
    "未选择整行文本" => "Tsy voafantina ny andalana manontolo amin\'ny lahatsoratra",
    "划词选择行错误" => "Fahadisoana andalana fifantenana teny",
    "数据源发生改变" => "Fiovan\'ny loharanom-baovao",
    "填充成功，行号" => "Nofenoina soa aman-tsara, laharan-tsipika",
    "请输入分类名称" => "Ampidiro azafady ny anaran\'ny sokajy",
    "分类名不能为空" => "Tsy azo foanana ny anaran\'ny sokajy",
    "排序只能是数字" => "Ny fanasokajiana dia mety amin\'ny isa ihany",
    "请先删除子分类" => "Fafao aloha ny zana-tsokajy",
    "请输入评论作者" => "Ampidiro azafady ny mpanoratra hevitra",
    "请选择目标语言" => "Mifidiana fiteny kendrena azafady",
    "语言包生成成功" => "Vita soa aman-tsara ny fonosan\'ny fiteny",
    "语言包生成失败" => "Tsy nahomby ny famoronana fonon-teny",
    "国际化分类成功" => "Tafita ny fanasokajiana iraisam-pirenena",
    "国际化分类失败" => "Tsy nahomby ny fanasokajiana iraisam-pirenena",
    "请输入标签名称" => "Ampidiro anarana etikety azafady",
    "国际化标签成功" => "Fahombiazan\'ny marika iraisam-pirenena",
    "国际化标签失败" => "Tsy nahomby ny tenifototra iraisam-pirenena",
    "国际化文章成功" => "Fahombiazana lahatsoratra iraisam-pirenena",
    "国际化文章失败" => "Tsy nahomby ny lahatsoratra iraisam-pirenena",
    "请输入页面标题" => "Ampidiro azafady ny lohatenin\'ny pejy",
    "国际化页面成功" => "Tafita ny pejy iraisam-pirenena",
    "国际化页面失败" => "Tsy nahomby ny pejy iraisam-pirenena",
    "作者：葡萄枝子" => "Mpanoratra: Sampana voaloboka",
    "请输入网站名称" => "Ampidiro azafady ny anaran\'ny tranokala",
    "请输入网站地址" => "Ampidiro azafady ny adiresy tranokala",
    "链接名不能为空" => "Tsy azo foanana ny anaran\'ny rohy",
    "上传文件不完整" => "Tsy feno ny rakitra nampidirina",
    "没有文件被上传" => "Tsy nisy rakitra nampidirina",
    "找不到临时目录" => "Tsy hita ny lahatahiry temp",
    "未知的文件类型" => "Karazana rakitra tsy fantatra",
    "文件名不能为空" => "Tsy azo foanana ny anaran\'ny rakitra",
    "个文件有在使用" => "ampiasaina ny rakitra",
    "请先删除子页面" => "Fafao aloha ny zanapejy",
    "请输入角色名称" => "Ampidiro anarana anjara azafady",
    "管理员不可禁用" => "Tsy afaka manafoana ny mpitantana",
    "管理员不可删除" => "Tsy afaka mamafa ny mpitantana",
    "请输入限制大小" => "Ampidiro azafady ny haben\'ny fetra",
    "请输入版权信息" => "Ampidiro azafady ny fampahalalana momba ny zon\'ny mpamorona",
    "恢复出厂成功！" => "Tafita ny famerenan\'ny orinasa!",
    "恢复出厂失败！" => "Tsy nahomby ny famerenana ny orinasa!",
    "标签名不能为空" => "Tsy azo foanana ny anaran\'ny tag",
    "还没有内容信息" => "Tsy mbola misy fampahalalana momba ny atiny",
    "这篇文章很有用" => "Tena ilaina ity lahatsoratra ity",
    "保存分类国际化" => "Save category internationalization",
    "分类国际化帮助" => "Fanasokajiana iraisam-pirenena fanampiana",
    "保存标签国际化" => "Save label internationalization",
    "标签国际化帮助" => "Tag fanampiana iraisam-pirenena",
    "保存文章国际化" => "Save article internationalization",
    "文章国际化帮助" => "Fanampiana iraisam-pirenena lahatsoratra",
    "保存页面国际化" => "Save page internationalization",
    "页面国际化帮助" => "Fanampiana iraisam-pirenena momba ny pejy",
    "海地克里奥尔语" => "Kreole Haitiana",
    "非法的ajax请求" => "Fangatahana ajax tsy ara-dalàna",
    "密码至少位数" => "Ny tenimiafina dia tsy maintsy manana isa fara-fahakeliny",
    "验证码不正确" => "Kaody fanamarinana diso",
    "包含非法参数" => "Misy masontsivana tsy ara-dalàna",
    "请输入用户名" => "ampidiro azafady ny anaran\'ny mpampiasa",
    "用户名已存在" => "Anarana mpampiasa efa misy",
    "请重输入密码" => "Ampidiro indray azafady ny tenimiafinao",
    "图片格式错误" => "Hadisoana endrika sary",
    "修改资料成功" => "Nahomby ny angona",
    "没有改变信息" => "Tsy nisy vaovao niova",
    "请输入浏览量" => "Ampidiro azafady ny isan\'ny fijerena",
    "请输入点赞数" => "Ampidiro azafady ny isan\'ny tia",
    "请选择子分类" => "Misafidiana zana-tsokajy azafady",
    "创建文章成功" => "Vita soa aman-tsara ny lahatsoratra",
    "创建文章失败" => "Tsy nahavita namorona lahatsoratra",
    "编辑文章成功" => "Ahitsio soa aman-tsara ny lahatsoratra",
    "标题不能为空" => "Tsy azo atao banga ny lohateny",
    "菜单名称重复" => "Anaran\'ny menio dika mitovy",
    "创建菜单成功" => "Vita soa aman-tsara ny menu",
    "创建菜单失败" => "Tsy nahavita namorona sakafo",
    "编辑菜单成功" => "Nahomby ny fanitsiana ny sakafo",
    "请选择行数据" => "Mifidiana angona andalana azafady",
    "计算孤立文件" => "Manisa rakitra kamboty",
    "划词选择错误" => "Diso safidy ny teny",
    "请提取语言包" => "Esory azafady ny fonosana fiteny",
    "没有语音文字" => "Tsy misy sora-peo",
    "语音朗读完成" => "Vita ny famakiana feo",
    "分类名称为空" => "Foana ny anaran\'ny sokajy",
    "创建分类成功" => "Vita soa aman-tsara ny fanasokajiana",
    "创建分类失败" => "Tsy nahavita namorona sokajy",
    "编辑分类成功" => "Ahitsio soa aman-tsara ny sokajy",
    "回复评论为空" => "Foana ny valin-teny",
    "回复评论成功" => "Valio soa aman-tsara ny fanehoan-kevitra",
    "回复评论失败" => "Tsy nahomby ny valin-teny",
    "评论内容为空" => "Foana ny votoatin\'ny fanehoan-kevitra",
    "编辑评论成功" => "Nahomby ny fanitsiana tsetsatsetsa",
    "待审评论成功" => "Tafita ny fanehoan-kevitra miandry",
    "待审评论失败" => "Tsy nahomby ny fanehoan-kevitra miandry",
    "审核通过评论" => "Hevitra nekena",
    "审核评论成功" => "Tafita ny famerenana famerenana",
    "审核评论失败" => "Tsy nahomby ny famerenana tsetsatsetsa",
    "删除评论失败" => "Tsy voafafa tsetsatsetsa",
    "请选择源语言" => "Mifidiana fiteny loharano azafady",
    "别名不可更改" => "Tsy azo ovaina ny anarana",
    "标签名称为空" => "Foana ny anaran\'ny tag",
    "创建链接成功" => "Nahomby ny rohy",
    "创建链接失败" => "Tsy nahavita namorona rohy",
    "编辑链接成功" => "Ahitsio soa aman-tsara ny rohy",
    "网站名称重名" => "Anaran-tranonkala dika mitovy",
    "网站地址重复" => "Adiresy tranokala kopia",
    "图片压缩失败" => "Tsy nahomby ny famoretana sary",
    "移动文件失败" => "Tsy nahomby ny famindrana rakitra",
    "上传文件成功" => "Tafiditra soa aman-tsara ny rakitra",
    "上传文件失败" => "Tsy nahomby ny fampiakarana rakitra",
    "共找到文件：" => "Total rakitra hita:",
    "创建页面成功" => "Vita soa aman-tsara ny pejy",
    "创建页面失败" => "Tsy nahomby ny famoronana pejy",
    "编辑页面成功" => "Ahitsio soa aman-tsara ny pejy",
    "权限数据错误" => "Fahadisoana angon-drakitra fahazoan-dàlana",
    "创建角色成功" => "Nahomby ny andraikitra",
    "创建角色失败" => "Tsy nahavita namorona anjara",
    "编辑角色成功" => "Ahitsio soa aman-tsara",
    "游客不可删除" => "Tsy afaka mamafa ny mpitsidika",
    "创建标签成功" => "Nahomby ny tenifototra",
    "创建标签失败" => "Tsy nahavita namorona etikety",
    "编辑标签成功" => "Hanova soa aman-tsara ny marika",
    "角色数据错误" => "Fahadisoana momba ny angon-drakitra",
    "语言数据错误" => "Hadisoana angona momba ny fiteny",
    "状态数据错误" => "Fahadisoana angon-drakitra",
    "创建用户成功" => "Noforonina soa aman-tsara ny mpampiasa",
    "创建用户失败" => "Tsy nahavita namorona mpampiasa",
    "编辑用户成功" => "Ahitsio soa aman-tsara ny mpampiasa",
    "本文博客网址" => "URL bilaogy an\'ity lahatsoratra ity",
    "评论内容重复" => "Votoatin\'ny fanehoan-kevitra dika mitovy",
    "回复内容重复" => "Votoatin\'ny valiny dika mitovy",
    "评论发表成功" => "Nahomby ny fanehoan-kevitra",
    "发表评论失败" => "Tsy nahavita nandefa fanehoan-kevitra",
    "前端删除评论" => "Famafana ny fanehoan-kevitra eo amin\'ny farany",
    "中文（简体）" => "Sinoa (tsotsotra)",
    "加泰罗尼亚语" => "Katalana",
    "苏格兰盖尔语" => "Gaelika Scottish",
    "中文（繁体）" => "Sinoa nentim-paharazana)",
    "马拉雅拉姆语" => "Malayalam",
    "斯洛文尼亚语" => "Slovenianina",
    "阿尔巴尼亚语" => "albaney",
    "密码至少5位" => "Ny tenimiafina dia tokony ho 5 farafahakeliny",
    "你已经登录" => "Efa tafiditra ianao",
    "账号被禁用" => "Tsy mandeha ny kaonty",
    "请输入密码" => "Ampidiro azafady ny tenimiafina",
    "留空不修改" => "Avelao ho banga ary aza ovaina",
    "请输入标题" => "Ampidiro lohateny azafady",
    "请输入内容" => "Ampidiro azafady ny atiny",
    "多标签半角" => "Multi-label antsasaky ny sakany",
    "关键词建议" => "Soso-kevitra amin\'ny teny fototra",
    "请输入作者" => "Ampidiro azafady ny mpanoratra",
    "请选择数据" => "Mifidiana angona azafady",
    "软删除文章" => "lahatsoratra famafana malefaka",
    "软删除成功" => "Tafita ny famafana malefaka",
    "软删除失败" => "Tsy nahomby ny famafana malefaka",
    "角色不存在" => "tsy misy ny anjara",
    "角色被禁用" => "Ny anjara asa dia kilemaina",
    "功能不存在" => "Tsy misy ny fiasa",
    "功能被禁用" => "Tsy miasa ny fiasa",
    "国际化帮助" => "Fanampiana iraisam-pirenena",
    "未选择文本" => "Tsy misy lahatsoratra voafantina",
    "请选择语言" => "Mifidiana fiteny azafady",
    "请输入排序" => "Ampidiro misoroka azafady",
    "未修改属性" => "fananana tsy ovaina",
    "机器人评论" => "Robot Reviews",
    "生成语言包" => "Mamorona fonosana fiteny",
    "国际化分类" => "Fanasokajiana iraisam-pirenena",
    "国际化标签" => "tag iraisam-pirenena",
    "国际化文章" => "Lahatsoratra iraisam-pirenena",
    "国际化页面" => "Pejy iraisam-pirenena",
    "服务器环境" => "Tontolo iainana mpizara",
    "数据库信息" => "Fampahafantarana angona",
    "服务器时间" => "fotoanan\'ny mpizara",
    "还没有评论" => "Tsy mbola misy fanehoan-kevitra",
    "还没有数据" => "Tsy mbola misy angona",
    "网址不合法" => "Tsy ara-dalàna ny URL",
    "选择多文件" => "Mifidiana rakitra maromaro",
    "个未使用，" => "tsy ampiasaina,",
    "文件不存在" => "tsy misy ny rakitra",
    "重命名失败" => "Tsy nahomby ny fanovana anarana",
    "重命名成功" => "Tafita ny fanovana anarana",
    "角色已存在" => "Efa misy ny andraikitra",
    "蜘蛛不索引" => "hala tsy indexing",
    "请输入数量" => "Ampidiro azafady ny habetsahana",
    "昵称已存在" => "Anarana efa misy",
    "页面没找到" => "Pejy tsy hita",
    "还没有文章" => "Mbola tsy misy lahatsoratra",
    "还没有页面" => "Tsy mbola misy pejy",
    "还没有分类" => "Tsy mbola misy fanasokajiana",
    "还没有标签" => "Tsy mbola misy marika",
    "还没有热门" => "Tsy mbola malaza",
    "还没有更新" => "Tsy mbola nohavaozina",
    "还没有网址" => "Tsy mbola misy URL",
    "请写点评论" => "Manorata fanehoan-kevitra azafady",
    "，等待审核" => ",Moderate",
    "你已经注册" => "efa nisoratra anarana ianao",
    "提取语言包" => "Esory ny fonosana fiteny",
    "分类国际化" => "Fanasokajiana iraisam-pirenena",
    "标签国际化" => "Label internationalization",
    "文章国际化" => "Internationalization lahatsoratra",
    "页面国际化" => "Pejy iraisam-pirenena",
    "格鲁吉亚语" => "Zeorziana",
    "博杰普尔语" => "Bhojpuri",
    "白俄罗斯语" => "Belarusian",
    "斯瓦希里语" => "swahili",
    "古吉拉特语" => "gujarati",
    "斯洛伐克语" => "silaovaka",
    "阿塞拜疆语" => "Azerbaijani",
    "印尼巽他语" => "Indoneziana Sundanese",
    "加利西亚语" => "galisiana",
    "拉脱维亚语" => "Zavatra tsy",
    "罗马尼亚语" => "Malagasy Romanian",
    "亚美尼亚语" => "Armeniana",
    "保加利亚语" => "biolgara",
    "爱沙尼亚语" => "Estoniana",
    "阿姆哈拉语" => "amharic",
    "吉尔吉斯语" => "Kyrgyz",
    "克罗地亚语" => "Kroaty",
    "波斯尼亚语" => "Bosniaka",
    "蒂格尼亚语" => "Tignan",
    "克里奥尔语" => "kreôla",
    "马尔加什语" => "Malagasy",
    "南非科萨语" => "Afrika Atsimo Xhosa",
    "南非祖鲁语" => "Zulu, Afrika Atsimo",
    "塞尔维亚语" => "serbianina",
    "乌兹别克语" => "Uzbek",
    "伊洛卡诺语" => "Ilokano",
    "印尼爪哇语" => "Indoneziana Javanese",
    "回复ID错误" => "Valiny ID fahadisoana",
    "非法文章ID" => "ID lahatsoratra tsy ara-dalàna",
    "管理后台" => "Fitantanana background",
    "管理登录" => "Login admin",
    "登录成功" => "nahomby ny fidirana",
    "切换注册" => "Miova fisoratana anarana",
    "你已登出" => "Efa nivoaka ianao",
    "登出成功" => "Tafita ny fivoahana",
    "用户注册" => "fisoratana anarana mpampiasa",
    "注册成功" => "fahombiazana fisoratana anarana",
    "注册失败" => "tsy nahomby ny fisoratana anarana",
    "重复密码" => "Avereno ny tenimiafina",
    "切换登录" => "Hanova ny fidirana",
    "请先登录" => "midira aloha azafady",
    "修改资料" => "Manova vaovao",
    "没有改变" => "Tsy misy fiovana",
    "不可修改" => "tsy miova",
    "上传失败" => "tsy nahomby ny fampiakarana",
    "清除成功" => "Fafao soa aman-tsara",
    "暂无记录" => "Tsy misy firaketana",
    "批量删除" => "famafana batch",
    "文章列表" => "Lisitry ny lahatsoratra",
    "发布时间" => "fotoana famoahana",
    "修改时间" => "Ovay ny fotoana",
    "文章标题" => "Lohatenin\'ny lahatsoratra",
    "标题重名" => "Lohateny dika mitovy",
    "别名重复" => "Adika duplicate",
    "创建文章" => "Mamorona lahatsoratra",
    "编辑文章" => "Ahitsio lahatsoratra",
    "非法字段" => "saha tsy ara-dalàna",
    "填写整数" => "Fenoy ny integer",
    "修改属性" => "Ovao ny fananana",
    "修改成功" => "Vita soa aman-tsara",
    "修改失败" => "tsy mahavita manitsy",
    "批量还原" => "Batch famerenana",
    "还原文章" => "Avereno lahatsoratra",
    "还原成功" => "Tafita ny famerenana",
    "还原失败" => "Tsy nahomby ny famerenana",
    "删除文章" => "Fafao ny lahatsoratra",
    "删除成功" => "voafafa soa aman-tsara",
    "删除失败" => "tsy voafafa",
    "全部展开" => "Ampitomboy daholo",
    "全部折叠" => "Arovy daholo",
    "添加下级" => "Ampio ny ambany",
    "编辑菜单" => "Ahitsio ny menu",
    "菜单名称" => "Anaran\'ny menu",
    "父级菜单" => "Menu ho an\'ny ray aman-dreny",
    "顶级菜单" => "menu ambony",
    "创建菜单" => "Mamorona sakafo",
    "删除菜单" => "fafao ny sakafo",
    "非法访问" => "Fidirana tsy nahazoana alalana",
    "拒绝访问" => "tsy nahazo alalàna",
    "没有权限" => "tsy nahazo fahazoan-dàlana",
    "彻底删除" => "Esory tanteraka",
    "批量待审" => "Andrasana ny famerenana",
    "批量审核" => "Famerenana andiany",
    "垃圾评论" => "Spam fanehoan-kevitra",
    "分类名称" => "Anaran\'ny sokajy",
    "分类列表" => "Lisitry ny sokajy",
    "父级分类" => "sokajy ray aman-dreny",
    "顶级分类" => "Fanasokajiana ambony",
    "分类重名" => "Sokajy misy anarana mitovy",
    "创建分类" => "Mamorona sokajy",
    "编辑分类" => "Ahitsio sokajy",
    "删除分类" => "Fafao sokajy",
    "评论作者" => "Famerenana ny mpanoratra",
    "评论内容" => "fanehoan-kevitra",
    "评论列表" => "lisitry ny fanehoan-kevitra",
    "评论文章" => "Famerenana lahatsoratra",
    "回复内容" => "Valio votoaty",
    "回复评论" => "Valio ny fanehoan-kevitra",
    "编辑评论" => "Hevitra tonian-dahatsoratra",
    "待审评论" => "Hevitra miandry",
    "待审成功" => "Nahomby ny fitsarana miandry",
    "待审失败" => "Tsy fahombiazana miandry fitsarana",
    "审核成功" => "Tafita ny famerenana",
    "审核失败" => "Tsy fahombiazana ny fanaraha-maso",
    "删除评论" => "Fafao ny fanehoan-kevitra",
    "谷歌翻译" => "Dikanteny Google",
    "检测语言" => "Fantaro ny fiteny",
    "别名错误" => "Error alias",
    "语言错误" => "Fiteny diso",
    "标签名称" => "Anaran\'ny marika",
    "标签列表" => "lisitry ny tag",
    "标签重名" => "Tags misy anarana mitovy",
    "页面列表" => "Lisitra pejy",
    "页面标题" => "lohatenin\'ny pejy",
    "父级页面" => "pejy ray aman-dreny",
    "顶级页面" => "pejy ambony",
    "清除缓存" => "madio cache",
    "当前版本" => "dikan-teny ankehitriny",
    "重置系统" => "Reset ny rafitra",
    "最新评论" => "fanehoan-kevitra farany",
    "联系我们" => "Mifandraisa aminay",
    "数据统计" => "antontan\'isa",
    "网站名称" => "Anaran\'ny tranokala",
    "网站地址" => "adiresy tranonkala",
    "链接列表" => "Lisitra mifandray",
    "创建链接" => "Mamorona rohy",
    "编辑链接" => "Ahitsio rohy",
    "删除链接" => "Esory ny rohy",
    "日志标题" => "Lohatenin\'ny log",
    "消息内容" => "Ny votoatin\'ny hafatra",
    "请求方法" => "Fomba fangatahana",
    "请求路径" => "Mangataka lalana",
    "日志列表" => "lisitry ny log",
    "上传成功" => "Tafita ny fandefasana",
    "媒体列表" => "lisitry ny media",
    "开始上传" => "Atombohy ny fampiakarana",
    "等待上传" => "Miandry ny fampiakarana",
    "重新上传" => "mamerina indray",
    "上传完成" => "vita ny fampiakarana",
    "系统保留" => "System voatokana",
    "发现重复" => "Dika mitovy hita",
    "上传文件" => "mampakatra rakitra",
    "个在用，" => "Ny iray dia ampiasaina,",
    "文件重名" => "Anaran\'ny rakitra duplicate",
    "删除文件" => "Fafao ny rakitra",
    "创建页面" => "Mamorona pejy",
    "编辑页面" => "Ahitsio pejy",
    "删除页面" => "mamafa pejy",
    "角色列表" => "lisitry ny andraikitra",
    "角色名称" => "Anaran\'ny andraikitra",
    "权限分配" => "Fanomezana alalana",
    "创建角色" => "Mamorona anjara",
    "编辑角色" => "Ahitsio anjara",
    "删除角色" => "Fafao ny anjara",
    "基础设置" => "Fikirana fototra",
    "高级设置" => "fanovana mandroso",
    "其他设置" => "toe-javatra hafa",
    "上传类型" => "Karazana fampiakarana",
    "限制大小" => "fetra habe",
    "默认最大" => "Default maximum",
    "单点登录" => "Hiditra",
    "版权信息" => "Fampahalalana momba ny zon\'ny mpamorona",
    "链接地址" => "adiresy rohy",
    "热门文章" => "lahatsoratra malaza",
    "首页评论" => "Hevitra momba ny pejy fandraisana",
    "内容相关" => "Mifandray amin\'ny atiny",
    "图片分页" => "Pejy sary",
    "友情链接" => "rohy",
    "语音朗读" => "Famakiana feo",
    "评论登录" => "Fidirana fanehoan-kevitra",
    "评论待审" => "Hevitra miandry",
    "保存成功" => "Voavonjy soa aman-tsara",
    "创建标签" => "Mamorona marika",
    "编辑标签" => "Ahitsio tag",
    "删除标签" => "Fafao ny marika",
    "用户列表" => "lisitry ny mpampiasa",
    "注册时间" => "Fotoana fisoratana anarana",
    "首次登陆" => "Fidirana voalohany",
    "最后登陆" => "fidirana farany",
    "语言权限" => "Fahazoan-dalana amin\'ny fiteny",
    "创建用户" => "Mamorona mpampiasa",
    "不能修改" => "Tsy azo ovaina",
    "编辑用户" => "Ahitsio mpampiasa",
    "删除用户" => "mamafa mpampiasa",
    "最新文章" => "lahatsoratra farany",
    "搜索结果" => "valin\'ny fikarohana",
    "选择语言" => "Misafidiana fiteny iray",
    "分类为空" => "Foana ny sokajy",
    "收藏文章" => "Manangona lahatsoratra",
    "收藏成功" => "Tafita ny fanangonana",
    "取消收藏" => "Foana ny ankafiziny",
    "取消回复" => "Fanafoanana ny valiny",
    "发表评论" => "fanehoan-kevitra",
    "语音播放" => "Famerenana feo",
    "回到顶部" => "miverina any ambony",
    "分类目录" => "Sokajy",
    "下载内容" => "Misintona votoaty",
    "相关文章" => "lahatsoratra mifandraika",
    "媒体合成" => "synthesis media",
    "语音合成" => "fitenenana synthesis",
    "前端登录" => "Fidirana eo anoloana",
    "前端注册" => "Fisoratana anarana aloha",
    "前端登出" => "Fivoahan\'ny frontend",
    "后台首页" => "Trano fonenana",
    "欢迎页面" => "pejy tongasoa",
    "权限管理" => "fitantanana fahefana",
    "用户管理" => "Fitantanana mpampiasa",
    "角色管理" => "fitantanana andraikitra",
    "权限菜单" => "Menu fahazoan-dàlana",
    "分类管理" => "Fitantanana fanasokajiana",
    "标签管理" => "fitantanana tag",
    "文章管理" => "Fitantanana lahatsoratra",
    "页面管理" => "Fitantanana pejy",
    "媒体管理" => "fitantanana ny haino aman-jery",
    "上传接口" => "Ampidiro ny interface",
    "链接管理" => "Fitantanana rohy",
    "评论管理" => "Fitantanana fanehoan-kevitra",
    "审核通过" => "lany ny fanadinana",
    "机器评论" => "famerenana milina",
    "系统管理" => "System Management",
    "系统设置" => "Fikirana rafitra",
    "保存设置" => "Save Settings",
    "恢复出厂" => "Avereno ny Factory",
    "操作日志" => "Loharanon\'ny asa",
    "删除日志" => "Fafao ny log",
    "科西嘉语" => "Korsika",
    "瓜拉尼语" => "Guaraní",
    "卢旺达语" => "Kinyarwanda",
    "约鲁巴语" => "yoruba",
    "尼泊尔语" => "Nepali",
    "夏威夷语" => "Hawaii",
    "意第绪语" => "Yiddish",
    "爱尔兰语" => "TENY IRLANDEY",
    "希伯来语" => "Hebreo",
    "卡纳达语" => "kannada",
    "匈牙利语" => "hongariana",
    "泰米尔语" => "tamil",
    "阿拉伯语" => "Arabo",
    "孟加拉语" => "Bengali",
    "萨摩亚语" => "samoanina",
    "班巴拉语" => "Bambara",
    "立陶宛语" => "litoanianina",
    "马耳他语" => "Maltais",
    "土库曼语" => "Turkmen",
    "阿萨姆语" => "Assamese",
    "僧伽罗语" => "sinhala",
    "乌克兰语" => "OKRAINIANA",
    "威尔士语" => "valesa",
    "菲律宾语" => "Filipiana",
    "艾马拉语" => "Aymara",
    "泰卢固语" => "telugu",
    "多格来语" => "dogglai",
    "迈蒂利语" => "Maithili",
    "普什图语" => "Pashto",
    "迪维希语" => "Dhivehi",
    "卢森堡语" => "Luxembourgish",
    "土耳其语" => "Tiorka",
    "马其顿语" => "Masedoniana",
    "卢干达语" => "Luganda",
    "马拉地语" => "marathi",
    "乌尔都语" => "Urdu",
    "葡萄牙语" => "portogey",
    "奥罗莫语" => "Oromo",
    "西班牙语" => "ESPANIOLA",
    "弗里西语" => "Frisian",
    "索马里语" => "Somali",
    "齐切瓦语" => "Chichewa",
    "旁遮普语" => "punjabi",
    "巴斯克语" => "Baska",
    "意大利语" => "ITALIANINA",
    "塔吉克语" => "Tajik",
    "克丘亚语" => "Quechua",
    "奥利亚语" => "Oriya",
    "哈萨克语" => "Kazakh",
    "林格拉语" => "Lingala",
    "塞佩蒂语" => "Sepeti",
    "塞索托语" => "Sesotho",
    "维吾尔语" => "Uighur",
    "ICP No.001" => "ICP No.001",
    "用户名" => "Anaran\'ny mpampiasa",
    "验证码" => "Kaody fanamarinana",
    "记住我" => "Tsarovy aho",
    "手机号" => "Nomeraon-telefaona",
    "请输入" => "midira azafady",
    "请选择" => "safidio azafady",
    "浏览量" => "hevitra",
    "回收站" => "dabam-bary",
    "菜单树" => "Menu hazo",
    "控制器" => "-maso",
    "第一页" => "Pejy voalohany",
    "最后页" => "pejy farany",
    "筛选列" => "Andry sivana",
    "删除行" => "Fafao ny laharana",
    "还原行" => "Esory ny laharana",
    "重命名" => "Hanova anarana",
    "国际化" => "fanatontoloana",
    "文章数" => "Isan\'ny lahatsoratra",
    "机器人" => "robot",
    "星期日" => "ALAHADY",
    "星期一" => "ALATSINAINY",
    "星期二" => "TALATA",
    "星期三" => "ALAROBIA",
    "星期四" => "ALAKAMISY",
    "星期五" => "ZOMA",
    "星期六" => "ASABOTSY",
    "会员数" => "Isan\'ny mpikambana",
    "评论数" => "Isan\'ny fanehoan-kevitra",
    "文件名" => "anaran\'ny fisie",
    "音视频" => "Audio sy video",
    "扩展名" => "anarana fanitarana",
    "未使用" => "miasa",
    "个无效" => "tsy mety",
    "共删除" => "Voafafa tanteraka",
    "个文件" => "rakitra",
    "备案号" => "laharana tranga",
    "轮播图" => "carousel",
    "条显示" => "fampisehoana bar",
    "上一页" => "Pejy teo aloha",
    "下一页" => "Pejy manaraka",
    "未分类" => "tsy voasokajy",
    "空标签" => "etikety foana",
    "无标题" => "Tsy misy lohateny",
    "控制台" => "hampionona",
    "登录中" => "miditra",
    "注册中" => "ny fisoratam",
    "跳转中" => "Redirecting",
    "提交中" => "manaiky",
    "审核中" => "eo am-pandinihana",
    "请稍后" => "andraso azafady",
    "篇文章" => "Article",
    "客户端" => "mpanjifa",
    "没有了" => "tsy misy tavela",
    "后评论" => "mandefa fanehoan-kevitra",
    "未命名" => "tsy voalaza anarana",
    "软删除" => "famafana malefaka",
    "语言包" => "Fonosana fiteny",
    "豪萨语" => "Hausa",
    "挪威语" => "norvejiana",
    "贡根语" => "fiteny gonggan",
    "拉丁语" => "LATININA",
    "捷克语" => "TSEKY",
    "波斯语" => "PERSANINA",
    "印地语" => "hindi",
    "冰岛语" => "Anarana",
    "契维语" => "Twi",
    "高棉语" => "Kambojiana",
    "丹麦语" => "Danoà",
    "修纳语" => "Shona",
    "越南语" => "vietnamiana",
    "宿务语" => "Cebuano",
    "波兰语" => "poloney",
    "鞑靼语" => "Tatar",
    "老挝语" => "Lao",
    "瑞典语" => "Anarana",
    "缅甸语" => "Birmana",
    "信德语" => "sindhi",
    "马来语" => "Malay",
    "伊博语" => "Igbo",
    "希腊语" => "GRIKA",
    "芬兰语" => "Anarana",
    "埃维语" => "Ewe",
    "毛利语" => "Maori",
    "荷兰语" => "Anarana iombonana",
    "蒙古语" => "Mongoliana",
    "米佐语" => "Mizo",
    "世界语" => "esperanto",
    "印尼语" => "indonezianina",
    "宗加语" => "Dzonga",
    "密码" => "Password",
    "登录" => "Hiditra",
    "重置" => "mamerina",
    "信息" => "Information",
    "成功" => "FETY",
    "失败" => "tsy",
    "昵称" => "Anarana anarana",
    "选填" => "tsy voatery",
    "注册" => "hisoratra anarana",
    "错误" => "fahadisoana",
    "头像" => "avatar",
    "上传" => "Zavatra tsy",
    "保存" => "afa-tsy",
    "标题" => "Lohateny",
    "别名" => "antsoina",
    "描述" => "Farito",
    "封面" => "MATOAN-DAHATSORATRA",
    "分类" => "Fisokajiana",
    "状态" => "FANJAKANA",
    "正常" => "ara-dalàna",
    "禁用" => "Atsaharo",
    "作者" => "mpanoratra",
    "日期" => "daty",
    "添加" => "hatovana ny",
    "刷新" => "mamelombelona",
    "标签" => "Label",
    "浏览" => "Mikaroka",
    "点赞" => "TOY",
    "评论" => "fanehoan-kevitra",
    "操作" => "hiasa",
    "阅读" => "HAMAKY",
    "编辑" => "Ovay",
    "删除" => "Fafao",
    "翻译" => "translate",
    "内容" => "votoaty",
    "分割" => "fizarana",
    "建议" => "SOSO-KEVITRA",
    "非法" => "Tsy Ara-dalàna",
    "还原" => "fampihenana",
    "方法" => "FOMBA",
    "排序" => "sort",
    "图标" => "matoanteny",
    "菜单" => "sakafo",
    "时间" => "Time",
    "匿名" => "Anonymous",
    "确定" => "Azo antoka",
    "取消" => "hanafoana",
    "跳转" => "Hanketo",
    "页码" => "laharan\'ny pejy",
    "共计" => "sokajy",
    "每页" => "isaky ny pejy",
    "导出" => "fanondranana",
    "打印" => "PIRINTY",
    "回复" => "navalin\'i",
    "路径" => "LALANA",
    "预览" => "TOPI-MASO",
    "名称" => "Anarana",
    "行数" => "andalana",
    "相册" => "raki-tsary",
    "文章" => "lahatsoratra",
    "待审" => "mbola miandry",
    "通过" => "nitranga",
    "垃圾" => "fako",
    "提取" => "hanesorana",
    "生成" => "hiteraka",
    "定位" => "toerana",
    "填充" => "famenoana",
    "帮助" => "Vonjeo",
    "返回" => "FIVERENANA",
    "语言" => "fiteny",
    "宽屏" => "widescreen",
    "普通" => "tsotra",
    "后台" => "Backstage",
    "前台" => "birao fidirana",
    "登出" => "hivoaka",
    "版本" => "Malagasy Bible",
    "关于" => "About",
    "微信" => "WeChat",
    "会员" => "FIANGONANA",
    "地址" => "adiresy",
    "类型" => "karazana",
    "图片" => "sary",
    "文件" => "tahirin-kevitra",
    "失效" => "Tsy mety",
    "大小" => "Size",
    "下载" => "DOWNLOAD",
    "导航" => "Fikarohana",
    "站标" => "Logo tranokala",
    "单位" => "vondrona",
    "加速" => "haingana",
    "角色" => "anjara asa",
    "首页" => "pejy voalohany",
    "个数" => "isa",
    "我的" => "pitrandrahana",
    "说道" => "nanao hoe:",
    "手机" => "finday",
    "可选" => "tsy voatery",
    "搜索" => "karohy",
    "页面" => "pejy",
    "查看" => "Taratasim-bola",
    "创建" => "MANANGANA",
    "更新" => "manavao",
    "英语" => "anglisy",
    "法语" => "FRANTSAY",
    "俄语" => "ROSIANINA",
    "梵语" => "Sanskrit",
    "日语" => "Anarana",
    "泰语" => "Thai",
    "苗语" => "Hmong",
    "德语" => "Anarana",
    "韩语" => "Koreana",
    "请" => "Mba miangavy re",
];
