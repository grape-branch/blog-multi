<?php
// 支持的语言
return [
    [
        "id" => "corsican",
        "name" => "Corsu",
        "title" => "科西嘉语",
        "slug" => "co",
        "voice" => ""
    ],
    [
        "id" => "guarani",
        "name" => "guarani",
        "title" => "瓜拉尼语",
        "slug" => "gn",
        "voice" => "",
    ],
    [
        "id" => "kinyarwanda",
        "name" => "Kinyarwanda",
        "title" => "卢旺达语",
        "slug" => "rw",
        "voice" => ""
    ],
    [
        "id" => "hausa",
        "name" => "Hausa",
        "title" => "豪萨语",
        "slug" => "ha",
        "voice" => ""
    ],
    [
        "id" => "norwegian",
        "name" => "Norge",
        "title" => "挪威语",
        "slug" => "no",
        "voice" => "nb-NO"
    ],
    [
        "id" => "afrikaans",
        "name" => "Booleaanse (Afrikaans)",
        "title" => "布尔语(南非荷兰语)",
        "slug" => "af",
        "voice" => "af-ZA"
    ],
    [
        "id" => "yoruba",
        "name" => "Yoruba",
        "title" => "约鲁巴语",
        "slug" => "yo",
        "voice" => ""
    ],
    [
        "id" => "english",
        "name" => "English",
        "title" => "英语",
        "slug" => "en",
        "voice" => "en-US"
    ],
    [
        "id" => "gongen",
        "name" => "गोंगेन हें नांव",
        "title" => "贡根语",
        "slug" => "gom",
        "voice" => ""
    ],
    [
        "id" => "latin",
        "name" => "Latina",
        "title" => "拉丁语",
        "slug" => "la",
        "voice" => ""
    ],
    [
        "id" => "nepali",
        "name" => "नेपालीName",
        "title" => "尼泊尔语",
        "slug" => "ne",
        "voice" => "ne-NP"
    ],
    [
        "id" => "french",
        "name" => "Français",
        "title" => "法语",
        "slug" => "fr",
        "voice" => "fr-FR"
    ],
    [
        "id" => "czech",
        "name" => "čeština",
        "title" => "捷克语",
        "slug" => "cs",
        "voice" => "cs-CZ"
    ],
    [
        "id" => "hawaiian",
        "name" => "ʻŌlelo Hawaiʻi",
        "title" => "夏威夷语",
        "slug" => "haw",
        "voice" => ""
    ],
    [
        "id" => "georgian",
        "name" => "ჯორჯიანიName",
        "title" => "格鲁吉亚语",
        "slug" => "ka",
        "voice" => ""
    ],
    [
        "id" => "russian",
        "name" => "Русский язык",
        "title" => "俄语",
        "slug" => "ru",
        "voice" => "ru-RU"
    ],
    [
        "id" => "chinese_simplified",
        "name" => "简体中文",
        "title" => "中文（简体）",
        "slug" => "zh-CN",
        "voice" => "cmn-Hans-CN"
    ],
    [
        "id" => "persian",
        "name" => "فارسی",
        "title" => "波斯语",
        "slug" => "fa",
        "voice" => ""
    ],
    [
        "id" => "bhojpuri",
        "name" => "भोजपुरी",
        "title" => "博杰普尔语",
        "slug" => "bho",
        "voice" => ""
    ],
    [
        "id" => "hindi",
        "name" => "हिंदी",
        "title" => "印地语",
        "slug" => "hi",
        "voice" => "hi-IN"
    ],
    [
        "id" => "belarusian",
        "name" => "беларускі",
        "title" => "白俄罗斯语",
        "slug" => "be",
        "voice" => ""
    ],
    [
        "id" => "swahili",
        "name" => "Kiswahili",
        "title" => "斯瓦希里语",
        "slug" => "sw",
        "voice" => "sw"
    ],
    [
        "id" => "icelandic",
        "name" => "ÍslandName",
        "title" => "冰岛语",
        "slug" => "is",
        "voice" => "is-IS"
    ],
    [
        "id" => "yiddish",
        "name" => "ייַדיש",
        "title" => "意第绪语",
        "slug" => "yi",
        "voice" => ""
    ],
    [
        "id" => "twi",
        "name" => "tur",
        "title" => "契维语",
        "slug" => "ak",
        "voice" => ""
    ],
    [
        "id" => "irish",
        "name" => "Gaeilge",
        "title" => "爱尔兰语",
        "slug" => "ga",
        "voice" => ""
    ],
    [
        "id" => "gujarati",
        "name" => "ગુજરાતી",
        "title" => "古吉拉特语",
        "slug" => "gu",
        "voice" => "gu-IN"
    ],
    [
        "id" => "khmer",
        "name" => "កម្ពុជា។",
        "title" => "高棉语",
        "slug" => "km",
        "voice" => "km-KH"
    ],
    [
        "id" => "slovak",
        "name" => "Slovenská",
        "title" => "斯洛伐克语",
        "slug" => "sk",
        "voice" => "sk-SK"
    ],
    [
        "id" => "hebrew",
        "name" => "היברית",
        "title" => "希伯来语",
        "slug" => "iw",
        "voice" => "he-IL"
    ],
    [
        "id" => "kannada",
        "name" => "ಕನ್ನಡ್Name",
        "title" => "卡纳达语",
        "slug" => "kn",
        "voice" => "kn-IN"
    ],
    [
        "id" => "hungarian",
        "name" => "Magyar",
        "title" => "匈牙利语",
        "slug" => "hu",
        "voice" => "hu-HU"
    ],
    [
        "id" => "tamil",
        "name" => "தாமில்",
        "title" => "泰米尔语",
        "slug" => "ta",
        "voice" => "ta-IN"
    ],
    [
        "id" => "arabic",
        "name" => "بالعربية",
        "title" => "阿拉伯语",
        "slug" => "ar",
        "voice" => "ar-EG"
    ],
    [
        "id" => "bengali",
        "name" => "বাংলা",
        "title" => "孟加拉语",
        "slug" => "bn",
        "voice" => "bn-BD"
    ],
    [
        "id" => "azerbaijani",
        "name" => "Azərbaycan",
        "title" => "阿塞拜疆语",
        "slug" => "az",
        "voice" => ""
    ],
    [
        "id" => "samoan",
        "name" => "lifiava",
        "title" => "萨摩亚语",
        "slug" => "sm",
        "voice" => ""
    ],
    [
        "id" => "indonesian",
        "name" => "IndonesiaName",
        "title" => "印尼巽他语",
        "slug" => "su",
        "voice" => "su-ID"
    ],
    [
        "id" => "danish",
        "name" => "dansk",
        "title" => "丹麦语",
        "slug" => "da",
        "voice" => "da-DK"
    ],
    [
        "id" => "shona",
        "name" => "Shona",
        "title" => "修纳语",
        "slug" => "sn",
        "voice" => ""
    ],
    [
        "id" => "bambara",
        "name" => "Bamanankan",
        "title" => "班巴拉语",
        "slug" => "bm",
        "voice" => ""
    ],
    [
        "id" => "lithuanian",
        "name" => "Lietuva",
        "title" => "立陶宛语",
        "slug" => "lt",
        "voice" => "lt-LT"
    ],
    [
        "id" => "vietnamese",
        "name" => "Tiếng Việt",
        "title" => "越南语",
        "slug" => "vi",
        "voice" => "vi-VN"
    ],
    [
        "id" => "maltese",
        "name" => "Malti",
        "title" => "马耳他语",
        "slug" => "mt",
        "voice" => ""
    ],
    [
        "id" => "turkmen",
        "name" => "Türkmençe",
        "title" => "土库曼语",
        "slug" => "tk",
        "voice" => ""
    ],
    [
        "id" => "assamese",
        "name" => "অসমীয়া",
        "title" => "阿萨姆语",
        "slug" => "as",
        "voice" => ""
    ],
    [
        "id" => "catalan",
        "name" => "català",
        "title" => "加泰罗尼亚语",
        "slug" => "ca",
        "voice" => "ca-ES"
    ],
    [
        "id" => "singapore",
        "name" => "සිංගාපුර්",
        "title" => "僧伽罗语",
        "slug" => "si",
        "voice" => "si-LK"
    ],
    [
        "id" => "cebuano",
        "name" => "Cebuano",
        "title" => "宿务语",
        "slug" => "ceb",
        "voice" => ""
    ],
    [
        "id" => "scottish-gaelic",
        "name" => "Gàidhlig na h-Alba",
        "title" => "苏格兰盖尔语",
        "slug" => "gd",
        "voice" => ""
    ],
    [
        "id" => "sanskrit",
        "name" => "Sanskrit",
        "title" => "梵语",
        "slug" => "sa",
        "voice" => ""
    ],
    [
        "id" => "polish",
        "name" => "Polski",
        "title" => "波兰语",
        "slug" => "pl",
        "voice" => "pl-PL"
    ],
    [
        "id" => "galician",
        "name" => "galego",
        "title" => "加利西亚语",
        "slug" => "gl",
        "voice" => "gl-ES"
    ],
    [
        "id" => "latvian",
        "name" => "latviešu",
        "title" => "拉脱维亚语",
        "slug" => "lv",
        "voice" => "lv-LV"
    ],
    [
        "id" => "ukrainian",
        "name" => "УкраїнськаName",
        "title" => "乌克兰语",
        "slug" => "uk",
        "voice" => "uk-UA"
    ],
    [
        "id" => "tatar",
        "name" => "Татар",
        "title" => "鞑靼语",
        "slug" => "tt",
        "voice" => ""
    ],
    [
        "id" => "welsh",
        "name" => "Cymraeg",
        "title" => "威尔士语",
        "slug" => "cy",
        "voice" => ""
    ],
    [
        "id" => "japanese",
        "name" => "日本語",
        "title" => "日语",
        "slug" => "ja",
        "voice" => "ja-JP"
    ],
    [
        "id" => "filipino",
        "name" => "Pilipino",
        "title" => "菲律宾语",
        "slug" => "tl",
        "voice" => "fil-PH"
    ],
    [
        "id" => "aymara",
        "name" => "Aymara",
        "title" => "艾马拉语",
        "slug" => "ay",
        "voice" => ""
    ],
    [
        "id" => "lao",
        "name" => "ກະຣຸນາ",
        "title" => "老挝语",
        "slug" => "lo",
        "voice" => ""
    ],
    [
        "id" => "telugu",
        "name" => "తెలుగు",
        "title" => "泰卢固语",
        "slug" => "te",
        "voice" => "te-IN"
    ],
    [
        "id" => "romanian",
        "name" => "Română",
        "title" => "罗马尼亚语",
        "slug" => "ro",
        "voice" => "ro-RO"
    ],
    [
        "id" => "creole",
        "name" => "a n:n",
        "title" => "海地克里奥尔语",
        "slug" => "ht",
        "voice" => ""
    ],
    [
        "id" => "dogrid",
        "name" => "डोग्रिड ने दी",
        "title" => "多格来语",
        "slug" => "doi",
        "voice" => ""
    ],
    [
        "id" => "swedish",
        "name" => "Svenska",
        "title" => "瑞典语",
        "slug" => "sv",
        "voice" => "sv-SE"
    ],
    [
        "id" => "maithili",
        "name" => "मरातिलीName",
        "title" => "迈蒂利语",
        "slug" => "mai",
        "voice" => ""
    ],
    [
        "id" => "thai",
        "name" => "ภาษาไทย",
        "title" => "泰语",
        "slug" => "th",
        "voice" => "th-TH"
    ],
    [
        "id" => "armenian",
        "name" => "հայերեն",
        "title" => "亚美尼亚语",
        "slug" => "hy",
        "voice" => ""
    ],
    [
        "id" => "burmese",
        "name" => "ဗာရမ်",
        "title" => "缅甸语",
        "slug" => "my",
        "voice" => "my-MM"
    ],
    [
        "id" => "pashto",
        "name" => "پښتوName",
        "title" => "普什图语",
        "slug" => "ps",
        "voice" => ""
    ],
    [
        "id" => "hmong",
        "name" => "Hmoob",
        "title" => "苗语",
        "slug" => "hmn",
        "voice" => ""
    ],
    [
        "id" => "dhivehi",
        "name" => "ދިވެހި",
        "title" => "迪维希语",
        "slug" => "dv",
        "voice" => ""
    ],
    [
        "id" => "chinese_traditional",
        "name" => "繁體中文",
        "title" => "中文（繁体）",
        "slug" => "zh-TW",
        "voice" => "cmn-Hant-TW"
    ],
    [
        "id" => "luxembourgish",
        "name" => "LëtzebuergeschName",
        "title" => "卢森堡语",
        "slug" => "lb",
        "voice" => ""
    ],
    [
        "id" => "sindhi",
        "name" => "سنڌي",
        "title" => "信德语",
        "slug" => "sd",
        "voice" => ""
    ],
    [
        "id" => "kurdish",
        "name" => "Kurdî",
        "title" => "库尔德语（库尔曼吉语）",
        "slug" => "ku",
        "voice" => ""
    ],
    [
        "id" => "turkish",
        "name" => "Türkçe",
        "title" => "土耳其语",
        "slug" => "tr",
        "voice" => "tr-TR"
    ],
    [
        "id" => "macedonian",
        "name" => "Македонски",
        "title" => "马其顿语",
        "slug" => "mk",
        "voice" => ""
    ],
    [
        "id" => "bulgarian",
        "name" => "български",
        "title" => "保加利亚语",
        "slug" => "bg",
        "voice" => "bg-BG"
    ],
    [
        "id" => "malay",
        "name" => "Malay",
        "title" => "马来语",
        "slug" => "ms",
        "voice" => "ms-MY"
    ],
    [
        "id" => "luganda",
        "name" => "luganda",
        "title" => "卢干达语",
        "slug" => "lg",
        "voice" => ""
    ],
    [
        "id" => "marathi",
        "name" => "मराठीName",
        "title" => "马拉地语",
        "slug" => "mr",
        "voice" => "mr-IN"
    ],
    [
        "id" => "estonian",
        "name" => "eesti keel",
        "title" => "爱沙尼亚语",
        "slug" => "et",
        "voice" => "et-EE"
    ],
    [
        "id" => "malayalam",
        "name" => "മലമാലം",
        "title" => "马拉雅拉姆语",
        "slug" => "ml",
        "voice" => "ml-IN"
    ],
    [
        "id" => "deutsch",
        "name" => "Deutsch",
        "title" => "德语",
        "slug" => "de",
        "voice" => "de-DE"
    ],
    [
        "id" => "slovene",
        "name" => "slovenščina",
        "title" => "斯洛文尼亚语",
        "slug" => "sl",
        "voice" => ""
    ],
    [
        "id" => "urdu",
        "name" => "اوردو",
        "title" => "乌尔都语",
        "slug" => "ur",
        "voice" => "ur-PK"
    ],
    [
        "id" => "portuguese",
        "name" => "Português",
        "title" => "葡萄牙语",
        "slug" => "pt",
        "voice" => "pt-BR"
    ],
    [
        "id" => "igbo",
        "name" => "igbo",
        "title" => "伊博语",
        "slug" => "ig",
        "voice" => ""
    ],
    [
        "id" => "kurdish_sorani",
        "name" => "کوردی-سۆرانی",
        "title" => "库尔德语（索拉尼）",
        "slug" => "ckb",
        "voice" => ""
    ],
    [
        "id" => "oromo",
        "name" => "adeta",
        "title" => "奥罗莫语",
        "slug" => "om",
        "voice" => ""
    ],
    [
        "id" => "greek",
        "name" => "Ελληνικά",
        "title" => "希腊语",
        "slug" => "el",
        "voice" => "el-GR"
    ],
    [
        "id" => "spanish",
        "name" => "español",
        "title" => "西班牙语",
        "slug" => "es",
        "voice" => "es-US"
    ],
    [
        "id" => "frisian",
        "name" => "Frysk",
        "title" => "弗里西语",
        "slug" => "fy",
        "voice" => ""
    ],
    [
        "id" => "somali",
        "name" => "Soomaali",
        "title" => "索马里语",
        "slug" => "so",
        "voice" => ""
    ],
    [
        "id" => "amharic",
        "name" => "አማርኛ",
        "title" => "阿姆哈拉语",
        "slug" => "am",
        "voice" => "am-ET"
    ],
    [
        "id" => "nyanja",
        "name" => "potakuyan",
        "title" => "齐切瓦语",
        "slug" => "ny",
        "voice" => ""
    ],
    [
        "id" => "punjabi",
        "name" => "ਪੰਜਾਬੀName",
        "title" => "旁遮普语",
        "slug" => "pa",
        "voice" => "pa-Guru-IN"
    ],
    [
        "id" => "basque",
        "name" => "euskara",
        "title" => "巴斯克语",
        "slug" => "eu",
        "voice" => "eu-ES"
    ],
    [
        "id" => "italian",
        "name" => "Italiano",
        "title" => "意大利语",
        "slug" => "it",
        "voice" => "it-IT"
    ],
    [
        "id" => "albanian",
        "name" => "albanian",
        "title" => "阿尔巴尼亚语",
        "slug" => "sq",
        "voice" => "sq-AL"
    ],
    [
        "id" => "korean",
        "name" => "한어",
        "title" => "韩语",
        "slug" => "ko",
        "voice" => "ko-KR"
    ],
    [
        "id" => "tajik",
        "name" => "ТаjikӣName",
        "title" => "塔吉克语",
        "slug" => "tg",
        "voice" => ""
    ],
    [
        "id" => "finnish",
        "name" => "Suomalainen",
        "title" => "芬兰语",
        "slug" => "fi",
        "voice" => "fi-FI"
    ],
    [
        "id" => "kyrgyz",
        "name" => "Кыргыз тили",
        "title" => "吉尔吉斯语",
        "slug" => "ky",
        "voice" => ""
    ],
    [
        "id" => "ewe",
        "name" => "Eʋegbe",
        "title" => "埃维语",
        "slug" => "ee",
        "voice" => ""
    ],
    [
        "id" => "croatian",
        "name" => "Hrvatski",
        "title" => "克罗地亚语",
        "slug" => "hr",
        "voice" => "hr-HR"
    ],
    [
        "id" => "quechua",
        "name" => "Quechua",
        "title" => "克丘亚语",
        "slug" => "qu",
        "voice" => ""
    ],
    [
        "id" => "bosnian",
        "name" => "bosanski",
        "title" => "波斯尼亚语",
        "slug" => "bs",
        "voice" => ""
    ],
    [
        "id" => "maori",
        "name" => "Maori",
        "title" => "毛利语",
        "slug" => "mi",
        "voice" => ""
    ],
    [
        "id" => "",
        "name" => "ଓଡିଆ",
        "title" => "奥利亚语",
        "slug" => "or",
        "voice" => ""
    ],
    [
        "id" => "",
        "name" => "ትግናን",
        "title" => "蒂格尼亚语",
        "slug" => "ti",
        "voice" => ""
    ],
    [
        "id" => "",
        "name" => "қазақ",
        "title" => "哈萨克语",
        "slug" => "kk",
        "voice" => ""
    ],
    [
        "id" => "",
        "name" => "Nederlands",
        "title" => "荷兰语",
        "slug" => "nl",
        "voice" => "nl-NL"
    ],
    [
        "id" => "",
        "name" => "Krio we dɛn kɔl Krio",
        "title" => "克里奥尔语",
        "slug" => "kri",
        "voice" => ""
    ],
    [
        "id" => "",
        "name" => "Lingala",
        "title" => "林格拉语",
        "slug" => "ln",
        "voice" => ""
    ],
    [
        "id" => "",
        "name" => "Malagasy",
        "title" => "马尔加什语",
        "slug" => "mg",
        "voice" => ""
    ],
    [
        "id" => "",
        "name" => "Монгол",
        "title" => "蒙古语",
        "slug" => "mn",
        "voice" => ""
    ],
    [
        "id" => "",
        "name" => "Mizo tawng",
        "title" => "米佐语",
        "slug" => "lus",
        "voice" => ""
    ],
    [
        "id" => "",
        "name" => "IsiXhosa saseMzantsi Afrika",
        "title" => "南非科萨语",
        "slug" => "xh",
        "voice" => ""
    ],
    [
        "id" => "",
        "name" => "Zulu, eNingizimu Afrika",
        "title" => "南非祖鲁语",
        "slug" => "zu",
        "voice" => ""
    ],
    [
        "id" => "",
        "name" => "Српски",
        "title" => "塞尔维亚语",
        "slug" => "sr",
        "voice" => "sr-RS"
    ],
    [
        "id" => "",
        "name" => "Sepeti",
        "title" => "塞佩蒂语",
        "slug" => "nso",
        "voice" => ""
    ],
    [
        "id" => "",
        "name" => "Sesotho",
        "title" => "塞索托语",
        "slug" => "st",
        "voice" => ""
    ],
    [
        "id" => "",
        "name" => "Esperanto",
        "title" => "世界语",
        "slug" => "eo",
        "voice" => ""
    ],
    [
        "id" => "",
        "name" => "แบบไทย",
        "title" => "梅泰语（曼尼普尔语）",
        "slug" => "mni-Mtei",
        "voice" => ""
    ],
    [
        "id" => "",
        "name" => "ئۇيغۇر",
        "title" => "维吾尔语",
        "slug" => "ug",
        "voice" => ""
    ],
    [
        "id" => "",
        "name" => "o'zbek",
        "title" => "乌兹别克语",
        "slug" => "uz",
        "voice" => ""
    ],
    [
        "id" => "",
        "name" => "Ilocano",
        "title" => "伊洛卡诺语",
        "slug" => "ilo",
        "voice" => ""
    ],
    [
        "id" => "",
        "name" => "bahasa Indonesia",
        "title" => "印尼语",
        "slug" => "id",
        "voice" => "id-ID"
    ],
    [
        "id" => "",
        "name" => "Basa Jawa Indonesia",
        "title" => "印尼爪哇语",
        "slug" => "jw",
        "voice" => "jv-ID"
    ],
    [
        "id" => "",
        "name" => "Xitsonga",
        "title" => "宗加语",
        "slug" => "ts",
        "voice" => ""
    ]
];
