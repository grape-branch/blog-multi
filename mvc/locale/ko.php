<?php
/**
 * @author admin
 * @date 2024-06-05 13:00:42
 * @desc 韩语语言包
 */
return [
    "迷你mvc小框架，一套用于开发各类管理系统的通用型后台管理系统模板，基于开源免费的 layui 的制作，内置各类实际业务场景下相对丰富的示例。" => "Mini MVC는 다양한 관리 시스템을 개발하기 위한 범용 백엔드 관리 시스템 템플릿 세트인 작은 프레임워크로, 오픈 소스 및 무료 Layui를 기반으로 하며 다양한 실제 비즈니스 시나리오에서 상대적으로 풍부한 사례 세트를 보유하고 있습니다.",
    "除非特别说明，本博客均为原创，如需转载，请以链接形式标明来源出处。" => "별도의 언급이 없는 한 이 블로그는 원본입니다. 재인쇄가 필요한 경우 링크 형식으로 출처를 표시해 주세요.",
    "重置系统，将删除用户所有数据和附件，恢复到出厂设置。" => "시스템을 재설정하면 모든 사용자 데이터와 첨부 파일이 삭제되고 공장 설정으로 복원됩니다.",
    "用户名只能英文字母数字下划线或中划线，位数" => "사용자 이름에는 영문자, 숫자, 밑줄 또는 밑줄, 자릿수만 포함할 수 있습니다.",
    "昵称只能中英文字母数字下划线中划线，位数" => "닉네임에는 중국어, 영문자, 숫자, 밑줄, 밑줄, 숫자만 포함할 수 있습니다.",
    "有道接口不支持，需科学上网使用谷歌接口。" => "Youdao 인터페이스는 지원되지 않습니다. 과학적으로 인터넷에 액세스하려면 Google 인터페이스를 사용해야 합니다.",
    "别名只能英文字母数字下划线中划线，位数" => "별칭에는 영문자, 숫자, 밑줄, 밑줄, 숫자만 포함할 수 있습니다.",
    "计算孤立文件需要较长时间，确定继续吗？" => "분리된 파일을 계산하는 데 시간이 오래 걸립니다. 계속하시겠습니까?",
    "语音不支持，需科学上网使用谷歌接口。" => "음성은 지원되지 않으므로 과학적으로 인터넷에 접속하려면 구글 인터페이스를 사용해야 합니다.",
    "您的账号已在别处登录，请重新登录！" => "귀하의 계정은 다른 곳에 로그인되어 있습니다. 다시 로그인하십시오!",
    "你的账号已被禁用，请联系管理员" => "귀하의 계정이 비활성화되었습니다. 관리자에게 문의하세요",
    "文件有在使用中，请重新计算状态" => "파일이 사용 중입니다. 상태를 다시 계산하세요.",
    "目标语言权限不够，请联系管理员" => "대상 언어 권한이 부족합니다. 관리자에게 문의하세요.",
    "真的标记垃圾评论选中的行吗？" => "선택한 줄을 스팸으로 표시하시겠습니까?",
    "源语言别名为空，请联系源作者" => "소스 언어 별칭이 비어 있습니다. 소스 작성자에게 문의하세요.",
    "点击上传，或将文件拖拽到此处" => "업로드하려면 클릭하거나 여기로 파일을 드래그하세요.",
    "合成成功！再次点击按钮下载。" => "합성 성공! 다운로드하려면 버튼을 다시 클릭하세요.",
    "没有语言权限，请联系管理员" => "언어 권한이 없습니다. 관리자에게 문의하세요.",
    "源语言别名为空，不能国际化" => "소스 언어 별칭이 비어 있어 국제화할 수 없습니다.",
    "源语言与目标语言都不能为空" => "출발어와 도착어를 모두 비워둘 수 없습니다.",
    "源语言与目标语言数量不一致" => "출발어와 도착어의 개수가 일치하지 않습니다.",
    "删除文件失败，文件有在使用" => "파일을 삭제하지 못했습니다. 파일이 사용 중입니다.",
    "角色不存在，请联系管理员" => "해당 역할이 존재하지 않습니다. 관리자에게 문의하세요.",
    "角色被禁用，请联系管理员" => "역할이 비활성화되었습니다. 관리자에게 문의하세요.",
    "功能不存在，请联系管理员" => "기능이 존재하지 않습니다. 관리자에게 문의하세요.",
    "功能被禁用，请联系管理员" => "기능이 비활성화되었습니다. 관리자에게 문의하세요.",
    "真的彻底删除选中的行吗？" => "선택한 행이 실제로 완전히 삭제됩니까?",
    "真的审核通过选中的行吗？" => "선택한 행이 실제로 승인되었나요?",
    "目标语言与源语言不能相同" => "도착어는 출발어와 동일할 수 없습니다.",
    "提取语言包与源语言不一致" => "추출된 언어 패키지가 소스 언어와 일치하지 않습니다.",
    "文件有在使用，删除失败！" => "파일이 사용 중이며 삭제에 실패했습니다!",
    "没有权限，请联系管理员" => "권한이 없습니다. 관리자에게 문의하세요.",
    "真的待审核选中的行吗？" => "정말 검토하고 선택해야 하는 라인인가요?",
    "目标语言包有空行，行数" => "대상 언어 패키지에 빈 줄이 있습니다. 줄 수는",
    "此操作恢复到出厂设置？" => "이 작업을 하면 공장 설정으로 복원됩니까?",
    "朗读错误，请联网后再试" => "읽는 중 오류가 발생했습니다. 인터넷에 연결한 후 다시 시도해 주세요.",
    "还没有添加分类描述信息" => "아직 카테고리 설명 정보가 추가되지 않았습니다.",
    "本地媒体已失效或不存在" => "로컬 미디어가 잘못되었거나 존재하지 않습니다.",
    "库尔德语（库尔曼吉语）" => "쿠르드어(쿠르만지어)",
    "作者名未注册或被禁用" => "작성자 이름이 등록되지 않았거나 비활성화되었습니다.",
    "真的删除选中的行吗？" => "선택한 행을 정말로 삭제하시겠습니까?",
    "真的还原选中的行吗？" => "선택한 행을 실제로 복원합니까?",
    "真的删除行或子行么？" => "행이나 하위 행을 정말로 삭제하시겠습니까?",
    "目标语言包未发现空行" => "대상 언어 팩에서 빈 줄을 찾을 수 없습니다.",
    "该语言不支持语音朗读" => "이 언어는 음성 읽기를 지원하지 않습니다.",
    "语言包数据未发生改变" => "언어 팩 데이터가 변경되지 않았습니다.",
    "欢迎使用后台管理系统" => "백엔드 관리 시스템에 오신 것을 환영합니다.",
    "文件有在使用或已失效" => "파일이 사용 중이거나 만료되었습니다.",
    "合成失败，请稍后再试" => "합성에 실패했습니다. 나중에 다시 시도해 주세요.",
    "&copy; 2021-2023 Company, Inc." => "© 2021-2023 회사, Inc.",
    "梅泰语（曼尼普尔语）" => "메이테이(마니푸리)",
    "两次密码输入不一致" => "두 개의 비밀번호 입력이 일치하지 않습니다",
    "该用户密码不可修改" => "이 사용자 비밀번호는 수정할 수 없습니다.",
    "添加文章时创建标签" => "기사 추가 시 태그 만들기",
    "删除文章时删除评论" => "기사 삭제 시 댓글 삭제",
    "控制器与方法已存在" => "컨트롤러와 메서드가 이미 존재합니다.",
    "标题或名称不能为空" => "직함이나 이름은 비워둘 수 없습니다.",
    "请选择一种语言朗读" => "소리내어 읽을 언어를 선택하세요.",
    "分类有文章不能删除" => "해당 카테고리에 삭제할 수 없는 글이 있습니다",
    "审核为垃圾评论成功" => "스팸으로 검토되었습니다.",
    "审核为垃圾评论失败" => "스팸 댓글로 인해 검토가 실패했습니다.",
    "确定要登出站点吗？" => "정말로 사이트에서 로그아웃하시겠습니까?",
    "网站名称或地址为空" => "웹사이트 이름 또는 주소가 비어 있습니다.",
    "网站名称或地址重名" => "중복된 웹사이트 이름 또는 주소",
    "允许上传的文件类型" => "업로드가 허용된 파일 형식",
    "标签有文章不能删除" => "태그가 있는 글은 삭제할 수 없습니다.",
    "人觉得这篇文章很赞" => "사람들은 이 기사가 훌륭하다고 생각합니다.",
    "还没有页面描述信息" => "아직 페이지 설명 정보가 없습니다.",
    "回复与评论内容重复" => "답글과 댓글의 내용이 중복되었습니다.",
    "失败！请稍后再试。" => "실패하다! 나중에 다시 시도 해주십시오.",
    "主耶稣基督里的教会" => "주 예수 그리스도 안에 있는 교회",
    "库尔德语（索拉尼）" => "쿠르드어(소라니어)",
    "布尔语(南非荷兰语)" => "부울(아프리칸스어)",
    "用户名或密码错误" => "잘못된 사용자 이름 또는 비밀번호",
    "源语言必须是中文" => "소스 언어는 중국어여야 합니다.",
    "目标语言别名为空" => "대상 언어 별칭이 비어 있습니다.",
    "国际化文章时标签" => "국제화된 기사 태그",
    "确定清除缓存吗？" => "캐시를 지우시겠습니까?",
    "超过的单文件大小" => "단일 파일 크기가 초과되었습니다.",
    "超过前端表单限制" => "프런트엔드 양식 한도를 초과했습니다.",
    "目标没有写入权限" => "대상에 쓰기 권한이 없습니다",
    "不允许的上传类型" => "허용되지 않는 업로드 유형",
    "文件大小不能超过" => "파일 크기는 다음을 초과할 수 없습니다.",
    "保存基础设置成功" => "기본 설정이 성공적으로 저장되었습니다",
    "首次基础设置成功" => "첫 번째 기본 설정 성공",
    "正在合成，请稍后" => "합성 중입니다. 잠시 기다려 주세요.",
    "不合理的请求方法" => "부당한 요구방식",
    "Session无效或过期" => "세션이 유효하지 않거나 만료되었습니다.",
    "手机号码不正确" => "전화번호가 정확하지 않습니다",
    "手机号码已存在" => "휴대폰 번호가 이미 존재합니다.",
    "标题或内容为空" => "제목이나 내용이 비어 있습니다.",
    "创建文章时标签" => "기사 작성 시 태그",
    "编辑文章时标签" => "기사 편집 시 태그",
    "真的删除行么？" => "정말로 해당 줄을 삭제하시겠습니까?",
    "请输入菜单名称" => "메뉴 이름을 입력하세요.",
    "请先删除子菜单" => "먼저 하위 메뉴를 삭제해 주세요",
    "未登录访问后台" => "로그인하지 않고 백엔드에 액세스",
    "Cookie无效或过期" => "쿠키가 유효하지 않거나 만료되었습니다.",
    "真的还原行么？" => "정말 복원이 가능한가요?",
    "编辑器内容为空" => "편집기 내용이 비어 있습니다.",
    "未选择整行文本" => "전체 텍스트 줄이 선택되지 않았습니다.",
    "划词选择行错误" => "단어 선택 줄 오류",
    "数据源发生改变" => "데이터 소스 변경",
    "填充成功，行号" => "성공적으로 채워졌습니다. 줄 번호",
    "请输入分类名称" => "카테고리 이름을 입력해주세요",
    "分类名不能为空" => "카테고리 이름은 비워둘 수 없습니다.",
    "排序只能是数字" => "정렬은 숫자로만 가능합니다.",
    "请先删除子分类" => "먼저 하위 카테고리를 삭제하세요.",
    "请输入评论作者" => "댓글 작성자를 입력하세요.",
    "请选择目标语言" => "타겟 언어를 선택하세요",
    "语言包生成成功" => "언어 팩이 성공적으로 생성되었습니다.",
    "语言包生成失败" => "언어 팩 생성 실패",
    "国际化分类成功" => "국제 분류 성공",
    "国际化分类失败" => "국제 분류 실패",
    "请输入标签名称" => "라벨 이름을 입력하세요.",
    "国际化标签成功" => "국제 레이블 성공",
    "国际化标签失败" => "국제화 태그 실패",
    "国际化文章成功" => "국제 기사 성공",
    "国际化文章失败" => "국제 기사가 실패했습니다",
    "请输入页面标题" => "페이지 제목을 입력하세요",
    "国际化页面成功" => "국제화 페이지 성공",
    "国际化页面失败" => "국제화 페이지 실패",
    "作者：葡萄枝子" => "저자 : 포도 가지",
    "请输入网站名称" => "웹사이트 이름을 입력하세요.",
    "请输入网站地址" => "웹사이트 주소를 입력해주세요",
    "链接名不能为空" => "링크 이름은 비워둘 수 없습니다.",
    "上传文件不完整" => "업로드된 파일이 불완전합니다.",
    "没有文件被上传" => "업로드된 파일이 없습니다.",
    "找不到临时目录" => "임시 디렉터리를 찾을 수 없습니다.",
    "未知的文件类型" => "알 수 없는 파일 형식",
    "文件名不能为空" => "파일 이름은 비워둘 수 없습니다.",
    "个文件有在使用" => "파일이 사용 중입니다",
    "请先删除子页面" => "먼저 하위 페이지를 삭제하세요.",
    "请输入角色名称" => "역할 이름을 입력하세요.",
    "管理员不可禁用" => "관리자는 비활성화할 수 없습니다.",
    "管理员不可删除" => "관리자는 삭제할 수 없습니다.",
    "请输入限制大小" => "제한 크기를 입력하세요.",
    "请输入版权信息" => "저작권 정보를 입력해주세요",
    "恢复出厂成功！" => "공장 초기화에 성공했습니다!",
    "恢复出厂失败！" => "공장 초기화에 실패했습니다!",
    "标签名不能为空" => "태그 이름은 비워둘 수 없습니다.",
    "还没有内容信息" => "아직 콘텐츠 정보가 없습니다.",
    "这篇文章很有用" => "이 기사는 매우 유용합니다.",
    "保存分类国际化" => "카테고리 국제화 저장",
    "分类国际化帮助" => "분류 국제화 도움말",
    "保存标签国际化" => "라벨 국제화 저장",
    "标签国际化帮助" => "태그 국제화 도움말",
    "保存文章国际化" => "기사 국제화 저장",
    "文章国际化帮助" => "기사 국제화 도움말",
    "保存页面国际化" => "페이지 국제화 저장",
    "页面国际化帮助" => "페이지 국제화 도움말",
    "海地克里奥尔语" => "아이티 크리올어",
    "非法的ajax请求" => "불법적인 아약스 요청",
    "密码至少位数" => "비밀번호는 숫자 이상이어야 합니다.",
    "验证码不正确" => "잘못된 인증 코드",
    "包含非法参数" => "잘못된 매개변수가 포함되어 있습니다.",
    "请输入用户名" => "사용자 이름을 입력하세요",
    "用户名已存在" => "사용자 이름이 이미 존재합니다",
    "请重输入密码" => "비밀번호를 다시 입력해주세요",
    "图片格式错误" => "이미지 형식 오류",
    "修改资料成功" => "데이터가 수정되었습니다.",
    "没有改变信息" => "변경된 정보 없음",
    "请输入浏览量" => "조회수를 입력하세요.",
    "请输入点赞数" => "좋아요 수를 입력해주세요",
    "请选择子分类" => "하위 카테고리를 선택하세요.",
    "创建文章成功" => "기사가 성공적으로 생성되었습니다.",
    "创建文章失败" => "기사를 작성하지 못했습니다.",
    "编辑文章成功" => "기사를 수정했습니다.",
    "标题不能为空" => "제목은 비워둘 수 없습니다.",
    "菜单名称重复" => "메뉴 이름이 중복되었습니다.",
    "创建菜单成功" => "메뉴가 성공적으로 생성되었습니다",
    "创建菜单失败" => "메뉴를 생성하지 못했습니다.",
    "编辑菜单成功" => "메뉴 수정 성공",
    "请选择行数据" => "행 데이터를 선택하세요.",
    "计算孤立文件" => "고아 파일 수 계산",
    "划词选择错误" => "잘못된 단어 선택",
    "请提取语言包" => "언어팩을 추출해주세요",
    "没有语音文字" => "음성 텍스트 없음",
    "语音朗读完成" => "음성읽기 완료",
    "分类名称为空" => "카테고리 이름이 비어 있습니다.",
    "创建分类成功" => "분류가 성공적으로 생성되었습니다.",
    "创建分类失败" => "카테고리를 생성하지 못했습니다.",
    "编辑分类成功" => "카테고리를 수정했습니다.",
    "回复评论为空" => "댓글에 대한 답글이 비어 있습니다.",
    "回复评论成功" => "댓글에 성공적으로 응답",
    "回复评论失败" => "댓글에 답글을 달지 못했습니다.",
    "评论内容为空" => "댓글 내용이 비어 있습니다.",
    "编辑评论成功" => "댓글을 수정했습니다.",
    "待审评论成功" => "검토 대기 중인 댓글이 성공했습니다.",
    "待审评论失败" => "검토 대기 중인 댓글이 실패했습니다.",
    "审核通过评论" => "승인된 댓글",
    "审核评论成功" => "리뷰 리뷰 성공",
    "审核评论失败" => "댓글 검토 실패",
    "删除评论失败" => "댓글을 삭제하지 못했습니다.",
    "请选择源语言" => "소스 언어를 선택하세요.",
    "别名不可更改" => "별칭은 변경할 수 없습니다.",
    "标签名称为空" => "태그 이름이 비어 있습니다.",
    "创建链接成功" => "링크가 성공적으로 생성되었습니다.",
    "创建链接失败" => "링크를 생성하지 못했습니다.",
    "编辑链接成功" => "링크를 수정했습니다.",
    "网站名称重名" => "중복된 웹사이트 이름",
    "网站地址重复" => "중복된 웹사이트 주소",
    "图片压缩失败" => "이미지 압축 실패",
    "移动文件失败" => "파일을 이동하지 못했습니다.",
    "上传文件成功" => "파일이 성공적으로 업로드되었습니다",
    "上传文件失败" => "파일 업로드 실패",
    "共找到文件：" => "발견된 총 파일 수:",
    "创建页面成功" => "페이지가 성공적으로 생성되었습니다",
    "创建页面失败" => "페이지 생성 실패",
    "编辑页面成功" => "페이지 편집이 완료되었습니다.",
    "权限数据错误" => "권한 데이터 오류",
    "创建角色成功" => "역할이 성공적으로 생성되었습니다.",
    "创建角色失败" => "역할을 생성하지 못했습니다.",
    "编辑角色成功" => "역할을 수정했습니다.",
    "游客不可删除" => "방문자는 삭제할 수 없습니다.",
    "创建标签成功" => "태그가 성공적으로 생성되었습니다.",
    "创建标签失败" => "라벨을 생성하지 못했습니다.",
    "编辑标签成功" => "태그를 수정했습니다.",
    "角色数据错误" => "문자 데이터 오류",
    "语言数据错误" => "언어 데이터 오류",
    "状态数据错误" => "상태 데이터 오류",
    "创建用户成功" => "사용자가 성공적으로 생성되었습니다.",
    "创建用户失败" => "사용자를 생성하지 못했습니다.",
    "编辑用户成功" => "사용자를 편집했습니다.",
    "本文博客网址" => "이 기사의 블로그 URL",
    "评论内容重复" => "댓글 내용이 중복되었습니다.",
    "回复内容重复" => "답글 내용이 중복되었습니다.",
    "评论发表成功" => "댓글이 성공적으로 게시되었습니다.",
    "发表评论失败" => "댓글을 게시하지 못했습니다.",
    "前端删除评论" => "프런트 엔드에서 댓글 삭제하기",
    "中文（简体）" => "중국어 (간체)",
    "加泰罗尼亚语" => "카탈로니아 사람",
    "苏格兰盖尔语" => "스코틀랜드 게일어",
    "中文（繁体）" => "중국어 번체)",
    "马拉雅拉姆语" => "말라얄람어",
    "斯洛文尼亚语" => "슬로베니아",
    "阿尔巴尼亚语" => "알바니아",
    "密码至少5位" => "비밀번호는 5자 이상이어야 합니다.",
    "你已经登录" => "귀하는 이미 로그인하셨습니다",
    "账号被禁用" => "계정이 비활성화되었습니다",
    "请输入密码" => "비밀번호를 입력 해주세요",
    "留空不修改" => "비워두고 수정하지 마세요.",
    "请输入标题" => "제목을 입력하세요",
    "请输入内容" => "내용을 입력해주세요",
    "多标签半角" => "다중 라벨 반폭",
    "关键词建议" => "키워드 제안",
    "请输入作者" => "작성자를 입력해주세요.",
    "请选择数据" => "데이터를 선택하세요",
    "软删除文章" => "기사 일시 삭제",
    "软删除成功" => "일시 삭제 성공",
    "软删除失败" => "일시 삭제 실패",
    "角色不存在" => "역할이 존재하지 않습니다",
    "角色被禁用" => "역할이 비활성화되었습니다.",
    "功能不存在" => "기능이 존재하지 않습니다",
    "功能被禁用" => "기능이 비활성화되었습니다.",
    "国际化帮助" => "국제화 도움말",
    "未选择文本" => "선택한 텍스트가 없습니다.",
    "请选择语言" => "언어를 선택하세요",
    "请输入排序" => "정렬을 입력하세요",
    "未修改属性" => "수정되지 않은 속성",
    "机器人评论" => "로봇 리뷰",
    "生成语言包" => "언어 팩 생성",
    "国际化分类" => "국제분류",
    "国际化标签" => "국제화 태그",
    "国际化文章" => "국제 기사",
    "国际化页面" => "국제 페이지",
    "服务器环境" => "서버 환경",
    "数据库信息" => "데이터베이스 정보",
    "服务器时间" => "서버 시간",
    "还没有评论" => "아직 댓글이 없습니다",
    "还没有数据" => "아직 데이터가 없습니다",
    "网址不合法" => "URL이 불법입니다",
    "选择多文件" => "여러 파일 선택",
    "个未使用，" => "미사용,",
    "文件不存在" => "파일이 없습니다",
    "重命名失败" => "이름 바꾸기 실패",
    "重命名成功" => "이름 변경 성공",
    "角色已存在" => "역할이 이미 존재합니다.",
    "蜘蛛不索引" => "스파이더가 색인을 생성하지 않음",
    "请输入数量" => "수량을 입력해주세요",
    "昵称已存在" => "닉네임이 이미 존재합니다.",
    "页面没找到" => "페이지를 찾을 수 없습니다",
    "还没有文章" => "아직 기사가 없습니다",
    "还没有页面" => "아직 페이지가 없습니다",
    "还没有分类" => "아직 분류가 없습니다.",
    "还没有标签" => "아직 태그가 없습니다.",
    "还没有热门" => "아직 인기가 없음",
    "还没有更新" => "아직 업데이트되지 않았습니다.",
    "还没有网址" => "아직 URL이 없습니다",
    "请写点评论" => "댓글을 작성해주세요",
    "，等待审核" => ",조정됨",
    "你已经注册" => "당신은 이미 등록했습니다",
    "提取语言包" => "언어 팩 추출",
    "分类国际化" => "분류 국제화",
    "标签国际化" => "라벨 국제화",
    "文章国际化" => "기사 국제화",
    "页面国际化" => "페이지 국제화",
    "格鲁吉亚语" => "그루지야 사람",
    "博杰普尔语" => "보지푸리",
    "白俄罗斯语" => "벨로루시어",
    "斯瓦希里语" => "스와힐리어",
    "古吉拉特语" => "구자라트어",
    "斯洛伐克语" => "슬로바키아 사람",
    "阿塞拜疆语" => "아제르바이잔",
    "印尼巽他语" => "인도네시아 순다어",
    "加利西亚语" => "갈리시아어",
    "拉脱维亚语" => "라트비아 사람",
    "罗马尼亚语" => "루마니아 사람",
    "亚美尼亚语" => "아르메니아 사람",
    "保加利亚语" => "불가리아 사람",
    "爱沙尼亚语" => "에스토니아 사람",
    "阿姆哈拉语" => "암하라어",
    "吉尔吉斯语" => "키르기스어",
    "克罗地亚语" => "크로아티아어",
    "波斯尼亚语" => "보스니아어",
    "蒂格尼亚语" => "티냥",
    "克里奥尔语" => "크리올 사람",
    "马尔加什语" => "마다가스카르 사람",
    "南非科萨语" => "남아프리카 코사어",
    "南非祖鲁语" => "남아프리카공화국 줄루",
    "塞尔维亚语" => "세르비아 사람",
    "乌兹别克语" => "우즈벡어",
    "伊洛卡诺语" => "일로카노어",
    "印尼爪哇语" => "자바어 인도네시아어",
    "回复ID错误" => "답장 ID 오류",
    "非法文章ID" => "불법 기사 ID",
    "管理后台" => "경영배경",
    "管理登录" => "관리자 로그인",
    "登录成功" => "성공적 로그인",
    "切换注册" => "스위치 등록",
    "你已登出" => "로그아웃되었습니다",
    "登出成功" => "로그아웃 성공",
    "用户注册" => "사용자 등록",
    "注册成功" => "등록 성공",
    "注册失败" => "등록 실패",
    "重复密码" => "비밀번호를 반복하세요",
    "切换登录" => "로그인 전환",
    "请先登录" => "먼저 로그인을 해주세요",
    "修改资料" => "정보 수정",
    "没有改变" => "변경 없음",
    "不可修改" => "불변",
    "上传失败" => "업로드 실패",
    "清除成功" => "성공적으로 삭제됨",
    "暂无记录" => "기록 없음",
    "批量删除" => "일괄 삭제",
    "文章列表" => "기사 목록",
    "发布时间" => "출시 시간",
    "修改时间" => "시간 변경",
    "文章标题" => "기사 제목",
    "标题重名" => "중복된 제목",
    "别名重复" => "중복된 별칭",
    "创建文章" => "기사 작성",
    "编辑文章" => "기사 편집",
    "非法字段" => "불법적인 필드",
    "填写整数" => "정수를 채우세요",
    "修改属性" => "속성 수정",
    "修改成功" => "수정되었습니다.",
    "修改失败" => "편집에 실패하다",
    "批量还原" => "일괄 복원",
    "还原文章" => "기사 복원",
    "还原成功" => "복원 성공",
    "还原失败" => "복원 실패",
    "删除文章" => "기사 삭제",
    "删除成功" => "성공적으로 삭제되었습니다",
    "删除失败" => "삭제하지 못했습니다.",
    "全部展开" => "모두 펼치기",
    "全部折叠" => "모든 축소",
    "添加下级" => "부하 직원 추가",
    "编辑菜单" => "편집 메뉴",
    "菜单名称" => "메뉴명",
    "父级菜单" => "부모 메뉴",
    "顶级菜单" => "상위 메뉴",
    "创建菜单" => "메뉴 생성",
    "删除菜单" => "메뉴 삭제",
    "非法访问" => "승인되지 않은 접근",
    "拒绝访问" => "접근 불가",
    "没有权限" => "허가가 거부되었습니다",
    "彻底删除" => "완전히 제거",
    "批量待审" => "일괄 검토 대기 중",
    "批量审核" => "일괄 검토",
    "垃圾评论" => "스팸댓글",
    "分类名称" => "카테고리 이름",
    "分类列表" => "카테고리 목록",
    "父级分类" => "상위 카테고리",
    "顶级分类" => "상위 분류",
    "分类重名" => "이름이 중복된 카테고리",
    "创建分类" => "카테고리 생성",
    "编辑分类" => "카테고리 수정",
    "删除分类" => "카테고리 삭제",
    "评论作者" => "리뷰 작성자",
    "评论内容" => "코멘트",
    "评论列表" => "댓글 목록",
    "评论文章" => "기사 검토",
    "回复内容" => "답글 내용",
    "回复评论" => "댓글에 답장하기",
    "编辑评论" => "편집자 의견",
    "待审评论" => "대기 중인 댓글",
    "待审成功" => "재판 대기 중",
    "待审失败" => "실패 재판 보류 중",
    "审核成功" => "검토가 완료되었습니다.",
    "审核失败" => "감사 실패",
    "删除评论" => "댓글 삭제",
    "谷歌翻译" => "구글 번역",
    "检测语言" => "언어를 감지",
    "别名错误" => "별칭 오류",
    "语言错误" => "언어 오류",
    "标签名称" => "태그 이름",
    "标签列表" => "태그 목록",
    "标签重名" => "이름이 중복된 태그",
    "页面列表" => "페이지 목록",
    "页面标题" => "페이지 제목",
    "父级页面" => "상위 페이지",
    "顶级页面" => "톱 페이지",
    "清除缓存" => "캐시 지우기",
    "当前版本" => "현재 버전",
    "重置系统" => "시스템 재설정",
    "最新评论" => "최신 댓글",
    "联系我们" => "문의하기",
    "数据统计" => "통계",
    "网站名称" => "웹사이트 이름",
    "网站地址" => "웹 사이트 주소",
    "链接列表" => "연결리스트",
    "创建链接" => "링크 만들기",
    "编辑链接" => "링크 수정",
    "删除链接" => "링크 삭제",
    "日志标题" => "로그 제목",
    "消息内容" => "메시지 내용",
    "请求方法" => "요청 방법",
    "请求路径" => "요청 경로",
    "日志列表" => "로그 목록",
    "上传成功" => "업로드 성공",
    "媒体列表" => "미디어 목록",
    "开始上传" => "업로드 시작",
    "等待上传" => "업로드 대기 중",
    "重新上传" => "다시 업로드",
    "上传完成" => "업로드 완료",
    "系统保留" => "시스템 예약",
    "发现重复" => "중복된 항목이 발견되었습니다.",
    "上传文件" => "파일 업로드하다",
    "个在用，" => "하나는 사용중이고,",
    "文件重名" => "중복된 파일 이름",
    "删除文件" => "파일 삭제",
    "创建页面" => "페이지 만들기",
    "编辑页面" => "페이지 편집",
    "删除页面" => "페이지 삭제",
    "角色列表" => "역할 목록",
    "角色名称" => "역할 이름",
    "权限分配" => "권한 할당",
    "创建角色" => "역할 생성",
    "编辑角色" => "역할 수정",
    "删除角色" => "역할 삭제",
    "基础设置" => "기본 설정",
    "高级设置" => "고급 설정",
    "其他设置" => "다른 설정",
    "上传类型" => "업로드 유형",
    "限制大小" => "크기 제한",
    "默认最大" => "기본 최대값",
    "单点登录" => "로그인",
    "版权信息" => "저작권 정보",
    "链接地址" => "링크 주소",
    "热门文章" => "인기 기사",
    "首页评论" => "홈페이지 댓글",
    "内容相关" => "콘텐츠 관련",
    "图片分页" => "그림 페이징",
    "友情链接" => "연결",
    "语音朗读" => "음성읽기",
    "评论登录" => "댓글 로그인",
    "评论待审" => "댓글 대기 중",
    "保存成功" => "성공적으로 저장 되었음",
    "创建标签" => "태그 만들기",
    "编辑标签" => "태그 수정",
    "删除标签" => "태그 삭제",
    "用户列表" => "사용자 목록",
    "注册时间" => "등록 시간",
    "首次登陆" => "처음 로그인",
    "最后登陆" => "마지막 로그인",
    "语言权限" => "언어 권한",
    "创建用户" => "사용자 생성",
    "不能修改" => "수정할 수 없습니다.",
    "编辑用户" => "사용자 편집",
    "删除用户" => "사용자 삭제",
    "最新文章" => "최신 기사",
    "搜索结果" => "검색 결과",
    "选择语言" => "언어를 선택하세요",
    "分类为空" => "카테고리가 비어 있습니다.",
    "收藏文章" => "기사 수집",
    "收藏成功" => "수집 성공",
    "取消收藏" => "즐겨찾기 취소",
    "取消回复" => "답장 취소",
    "发表评论" => "논평",
    "语音播放" => "음성 재생",
    "回到顶部" => "맨 위로 돌아가기",
    "分类目录" => "카테고리",
    "下载内容" => "콘텐츠 다운로드",
    "相关文章" => "관련 기사",
    "媒体合成" => "미디어 합성",
    "语音合成" => "음성 합성",
    "前端登录" => "프런트엔드 로그인",
    "前端注册" => "프런트엔드 등록",
    "前端登出" => "프런트엔드 로그아웃",
    "后台首页" => "배경 홈",
    "欢迎页面" => "환영 페이지",
    "权限管理" => "권한 관리",
    "用户管理" => "사용자 관리",
    "角色管理" => "역할 관리",
    "权限菜单" => "권한 메뉴",
    "分类管理" => "분류관리",
    "标签管理" => "태그 관리",
    "文章管理" => "기사 관리",
    "页面管理" => "페이지 관리",
    "媒体管理" => "미디어 관리",
    "上传接口" => "업로드 인터페이스",
    "链接管理" => "링크 관리",
    "评论管理" => "댓글 관리",
    "审核通过" => "시험 합격",
    "机器评论" => "기계 검토",
    "系统管理" => "시스템 관리",
    "系统设置" => "환경 설정",
    "保存设置" => "설정 저장",
    "恢复出厂" => "공장 복원",
    "操作日志" => "작업 로그",
    "删除日志" => "로그 삭제",
    "科西嘉语" => "코르시카어",
    "瓜拉尼语" => "과라니",
    "卢旺达语" => "키냐르완다어",
    "约鲁巴语" => "요루바",
    "尼泊尔语" => "네팔어",
    "夏威夷语" => "하와이안",
    "意第绪语" => "이디시어",
    "爱尔兰语" => "아일랜드의",
    "希伯来语" => "헤브라이 사람",
    "卡纳达语" => "칸나다어",
    "匈牙利语" => "헝가리 인",
    "泰米尔语" => "타밀 사람",
    "阿拉伯语" => "아라비아 말",
    "孟加拉语" => "벵골 사람",
    "萨摩亚语" => "사모아어",
    "班巴拉语" => "밤바라",
    "立陶宛语" => "리투아니아 사람",
    "马耳他语" => "몰티즈",
    "土库曼语" => "투르크멘 말",
    "阿萨姆语" => "아삼어",
    "僧伽罗语" => "신할라어",
    "乌克兰语" => "우크라이나 인",
    "威尔士语" => "웨일스 말",
    "菲律宾语" => "필리핀 사람",
    "艾马拉语" => "아이마라어",
    "泰卢固语" => "텔루구어",
    "多格来语" => "도글라이",
    "迈蒂利语" => "마이틸리",
    "普什图语" => "파슈토어",
    "迪维希语" => "디베히어",
    "卢森堡语" => "룩셈부르크어",
    "土耳其语" => "터키 사람",
    "马其顿语" => "마케도니아 어",
    "卢干达语" => "루간다어",
    "马拉地语" => "마라티어",
    "乌尔都语" => "우르두어",
    "葡萄牙语" => "포르투갈 인",
    "奥罗莫语" => "오로모어",
    "西班牙语" => "스페인의",
    "弗里西语" => "프리지아어",
    "索马里语" => "소말리아어",
    "齐切瓦语" => "치체와어",
    "旁遮普语" => "펀자브어",
    "巴斯克语" => "바스크 사람",
    "意大利语" => "이탈리아 사람",
    "塔吉克语" => "타직어",
    "克丘亚语" => "케추아어",
    "奥利亚语" => "오리야어",
    "哈萨克语" => "카자흐어",
    "林格拉语" => "링갈라",
    "塞佩蒂语" => "세페티",
    "塞索托语" => "세소토어",
    "维吾尔语" => "위구르어",
    "ICP No.001" => "ICP No.001",
    "用户名" => "사용자 이름",
    "验证码" => "확인 코드",
    "记住我" => "날 기억해",
    "手机号" => "전화 번호",
    "请输入" => "들어 오세요",
    "请选择" => "선택해주세요",
    "浏览量" => "견해",
    "回收站" => "쓰레기통",
    "菜单树" => "메뉴 트리",
    "控制器" => "제어 장치",
    "第一页" => "첫 페이지",
    "最后页" => "마지막 페이지",
    "筛选列" => "열 필터링",
    "删除行" => "행 삭제",
    "还原行" => "행 실행 취소",
    "重命名" => "이름 바꾸기",
    "国际化" => "세계화",
    "文章数" => "기사 수",
    "机器人" => "로봇",
    "星期日" => "일요일",
    "星期一" => "월요일",
    "星期二" => "화요일",
    "星期三" => "수요일",
    "星期四" => "목요일",
    "星期五" => "금요일",
    "星期六" => "토요일",
    "会员数" => "회원 수",
    "评论数" => "댓글 수",
    "文件名" => "파일 이름",
    "音视频" => "오디오 및 비디오",
    "扩展名" => "확장자 이름",
    "未使用" => "미사용",
    "个无效" => "유효하지 않은",
    "共删除" => "삭제된 총계",
    "个文件" => "파일",
    "备案号" => "사건 번호",
    "轮播图" => "회전목마",
    "条显示" => "바 디스플레이",
    "上一页" => "이전 페이지",
    "下一页" => "다음 페이지",
    "未分类" => "분류되지 않은",
    "空标签" => "빈 라벨",
    "无标题" => "제목 없음",
    "控制台" => "콘솔",
    "登录中" => "로그인",
    "注册中" => "등록 중",
    "跳转中" => "리디렉션 중",
    "提交中" => "제출 중",
    "审核中" => "검토중",
    "请稍后" => "기다리세요.",
    "篇文章" => "기사",
    "客户端" => "고객",
    "没有了" => "아무것도 남지 않았어",
    "后评论" => "댓글을 게시하다",
    "未命名" => "이름이 없는",
    "软删除" => "일시 삭제",
    "语言包" => "언어 팩",
    "豪萨语" => "하우사어",
    "挪威语" => "노르웨이 인",
    "贡根语" => "공간어",
    "拉丁语" => "라틴어",
    "捷克语" => "체코 사람",
    "波斯语" => "페르시아 인",
    "印地语" => "힌디 어",
    "冰岛语" => "아이슬란드어",
    "契维语" => "트위어",
    "高棉语" => "캄보디아어",
    "丹麦语" => "덴마크 말",
    "修纳语" => "쇼나",
    "越南语" => "베트남어",
    "宿务语" => "세부아노어",
    "波兰语" => "광택",
    "鞑靼语" => "타타르어",
    "老挝语" => "라오스",
    "瑞典语" => "스웨덴어",
    "缅甸语" => "버마 사람",
    "信德语" => "신디",
    "马来语" => "말레이 사람",
    "伊博语" => "이보어",
    "希腊语" => "그리스 어",
    "芬兰语" => "핀란드어",
    "埃维语" => "암양",
    "毛利语" => "마오리족",
    "荷兰语" => "네덜란드 사람",
    "蒙古语" => "몽고 어",
    "米佐语" => "미조",
    "世界语" => "에스페란토 말",
    "印尼语" => "인도네시아 인",
    "宗加语" => "종가",
    "密码" => "비밀번호",
    "登录" => "로그인",
    "重置" => "초기화",
    "信息" => "정보",
    "成功" => "성공",
    "失败" => "실패하다",
    "昵称" => "닉네임",
    "选填" => "선택 과목",
    "注册" => "등록하다",
    "错误" => "실수",
    "头像" => "화신",
    "上传" => "업로드",
    "保存" => "구하다",
    "标题" => "제목",
    "别名" => "별명",
    "描述" => "설명하다",
    "封面" => "씌우다",
    "分类" => "분류",
    "状态" => "상태",
    "正常" => "정상",
    "禁用" => "장애를 입히다",
    "作者" => "작가",
    "日期" => "날짜",
    "添加" => "다음에 추가",
    "刷新" => "새로 고치다",
    "标签" => "상표",
    "浏览" => "검색",
    "点赞" => "좋다",
    "评论" => "논평",
    "操作" => "작동하다",
    "阅读" => "읽다",
    "编辑" => "편집하다",
    "删除" => "삭제",
    "翻译" => "번역하다",
    "内容" => "콘텐츠",
    "分割" => "분할",
    "建议" => "제안",
    "非法" => "불법적인",
    "还原" => "절감",
    "方法" => "방법",
    "排序" => "종류",
    "图标" => "상",
    "菜单" => "메뉴",
    "时间" => "시간",
    "匿名" => "익명의",
    "确定" => "확신하는",
    "取消" => "취소",
    "跳转" => "도약",
    "页码" => "페이지 번호",
    "共计" => "총",
    "每页" => "페이지 당",
    "导出" => "내보내다",
    "打印" => "인쇄",
    "回复" => "회신하다",
    "路径" => "길",
    "预览" => "시사",
    "名称" => "이름",
    "行数" => "행",
    "相册" => "사진 앨범",
    "文章" => "기사",
    "待审" => "보류 중",
    "通过" => "통과하다",
    "垃圾" => "쓰레기",
    "提取" => "발췌",
    "生成" => "생성하다",
    "定位" => "위치",
    "填充" => "충전재",
    "帮助" => "돕다",
    "返回" => "반품",
    "语言" => "언어",
    "宽屏" => "와이드스크린",
    "普通" => "평범한",
    "后台" => "무대 뒤에서",
    "前台" => "프런트 데스크",
    "登出" => "로그아웃",
    "版本" => "버전",
    "关于" => "~에 대한",
    "微信" => "위챗",
    "会员" => "회원",
    "地址" => "주소",
    "类型" => "유형",
    "图片" => "그림",
    "文件" => "문서",
    "失效" => "유효하지 않은",
    "大小" => "크기",
    "下载" => "다운로드",
    "导航" => "항해",
    "站标" => "사이트 로고",
    "单位" => "단위",
    "加速" => "가속하다",
    "角色" => "역할",
    "首页" => "첫 장",
    "个数" => "숫자",
    "我的" => "내 거",
    "说道" => "말했다",
    "手机" => "휴대폰",
    "可选" => "선택 과목",
    "搜索" => "찾다",
    "页面" => "페이지",
    "查看" => "확인하다",
    "创建" => "만들다",
    "更新" => "고쳐 쓰다",
    "英语" => "영어",
    "法语" => "프랑스 국민",
    "俄语" => "러시아인",
    "梵语" => "산스크리트",
    "日语" => "일본어",
    "泰语" => "태국어",
    "苗语" => "몽족",
    "德语" => "독일 사람",
    "韩语" => "한국인",
    "请" => "제발",
];
