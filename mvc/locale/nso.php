<?php
/**
 * @author admin
 * @date 2024-06-06 05:03:31
 * @desc 塞佩蒂语语言包
 */
return [
    "迷你mvc小框架，一套用于开发各类管理系统的通用型后台管理系统模板，基于开源免费的 layui 的制作，内置各类实际业务场景下相对丰富的示例。" => "Mini MVC ke tlhako ye nnyane, sete ya dithempleite tša tshepedišo ya taolo ya morago ga bokahohleng ya go hlabolla ditshepedišo tša taolo tše di fapanego E theilwe godimo ga mohlodi wo o bulegilego le layui ya mahala gomme e na le sete ye e humilego kudu ya mehlala maemong a go fapafapana a kgwebo ao a šomago.",
    "除非特别说明，本博客均为原创，如需转载，请以链接形式标明来源出处。" => "Ntle le ge go boletšwe ka tsela ye nngwe, blog ye ke ya mathomo Ge o hloka go gatiša gape, hle bontšha mothopo ka mokgwa wa kgokagano.",
    "重置系统，将删除用户所有数据和附件，恢复到出厂设置。" => "Go seta tshepedišo gape go tla phumola datha ka moka ya mosediriši le dihlomathišo gomme gwa e bušetša go dipeakanyo tša feketori.",
    "用户名只能英文字母数字下划线或中划线，位数" => "Leina la mosediriši le ka ba le fela ditlhaka tša Seisemane, dinomoro, ditlhaka tša ka fase goba tša ka fase, le palo ya dinomoro",
    "昵称只能中英文字母数字下划线中划线，位数" => "Leina la sereto le ka ba le fela ditlhaka tša Setšhaena le tša Seisemane, dinomoro, ditlhaka tša ka fase, ditlhaka tša ka fase le dinomoro.",
    "有道接口不支持，需科学上网使用谷歌接口。" => "Segokaganyi sa Youdao ga se thekgwe O swanetše go šomiša segokanyimmediamentsi sa sebolokigolo sa Google go fihlelela Inthanete ka mahlale.",
    "别名只能英文字母数字下划线中划线，位数" => "Mantšu a maina a ka ba le ditlhaka tša Seisemane fela, dinomoro, ditlhaka tša ka fase, ditlhalošo tša ka fase, le dinomoro.",
    "计算孤立文件需要较长时间，确定继续吗？" => "Go tšea nako ye telele go bala difaele tša ditšhiwana Na o na le bonnete bja gore o nyaka go tšwela pele?",
    "语音不支持，需科学上网使用谷歌接口。" => "Lentšu ga le thekgwe, ka fao o swanetše go šomiša segokanyimmediamentsi sa sebolokigolo sa Google go fihlelela Inthanete ka mahlale.",
    "您的账号已在别处登录，请重新登录！" => "Akhaonto ya gago e tsene felotsoko, hle tsena gape!",
    "你的账号已被禁用，请联系管理员" => "Akhaonto ya gago e šitišitšwe, hle ikopanye le molaodi",
    "文件有在使用中，请重新计算状态" => "Faele e a šomišwa, hle bala gape maemo",
    "目标语言权限不够，请联系管理员" => "Ditumelelo tša polelo ya nepišo tše di sa lekanego, hle ikopanye le molaodi",
    "真的标记垃圾评论选中的行吗？" => "Na o tloga o swaya mela ye e kgethilwego bjalo ka spam?",
    "源语言别名为空，请联系源作者" => "Leina la polelo ya mothopo ga le na selo, hle ikopanye le mongwadi wa mothopo",
    "点击上传，或将文件拖拽到此处" => "Tobetsa go tsenya, goba goga faele mo",
    "合成成功！再次点击按钮下载。" => "Synthesis e atlegile! Tobetsa konopo gape go taonelouta.",
    "没有语言权限，请联系管理员" => "Ga go na tumelelo ya polelo, hle ikopanye le molaodi",
    "源语言别名为空，不能国际化" => "Leina la leina la polelo ya mohlodi ga le na selo gomme le ka se be la boditšhabatšhaba.",
    "源语言与目标语言都不能为空" => "Leleme la mothopo goba leleme leo le lebantšwego di ka se be le selo.",
    "源语言与目标语言数量不一致" => "Palo ya maleme a mothopo le a nepišo ga e sepelelane",
    "删除文件失败，文件有在使用" => "E paletšwe ke go phumola faele, faele e a šomišwa",
    "角色不存在，请联系管理员" => "Karolo ga e gona, hle ikopanye le molaodi",
    "角色被禁用，请联系管理员" => "Karolo e šitišitšwe, hle ikopanye le molaodi",
    "功能不存在，请联系管理员" => "Mošomo ga o gona, hle ikopanye le molaodi",
    "功能被禁用，请联系管理员" => "Mošomo o šitišitšwe, hle ikopanye le molaodi",
    "真的彻底删除选中的行吗？" => "Na e tloga e phumola mothalo wo o kgethilwego ka mo go feletšego?",
    "真的审核通过选中的行吗？" => "Na methaladi ye e kgethilwego e tloga e dumeletšwe?",
    "目标语言与源语言不能相同" => "Polelo ye e nepišitšwego e ka se swane le polelo ya mothopo",
    "提取语言包与源语言不一致" => "Sephuthelwana sa polelo ye e ntšhitšwego ga se sepelelane le polelo ya mothopo",
    "文件有在使用，删除失败！" => "Faele e a šomišwa gomme go phumolwa go paletšwe!",
    "没有权限，请联系管理员" => "Ga go na tumelelo, hle ikopanye le molaodi",
    "真的待审核选中的行吗？" => "Na ruri ke mola wo o swanetšego go hlahlobja le go kgethwa?",
    "目标语言包有空行，行数" => "Sephuthelwana sa polelo ya nepišo se na le mela ye e se nago selo, palo ya mela",
    "此操作恢复到出厂设置？" => "Tshebetso ena khutlisa ho di-setting fektheri?",
    "朗读错误，请联网后再试" => "Phošo ya go bala, hle kgokagana le Inthanete gomme o leke gape",
    "还没有添加分类描述信息" => "Ga go na tshedimošo ya tlhalošo ya legoro yeo e okeditšwego go fihla ga bjale",
    "本地媒体已失效或不存在" => "Methopo ya ditaba ya lefelong leo ga e šome goba ga e gona",
    "库尔德语（库尔曼吉语）" => "Sekurd (Kurmanji) .",
    "作者名未注册或被禁用" => "Leina la mongwadi ga se la ngwadišwa goba la šitišwa",
    "真的删除选中的行吗？" => "Na o tloga o phumola mothalo wo o kgethilwego?",
    "真的还原选中的行吗？" => "Na o tloga o bušetša methaladi yeo e kgethilwego?",
    "真的删除行或子行么？" => "Na o tloga o nyaka go phumola methaladi goba methaladi ye mennyane?",
    "目标语言包未发现空行" => "Mola wo o se nago selo ga o hwetšwe ka gare ga sephuthelwana sa polelo ye e nepišitšwego",
    "该语言不支持语音朗读" => "Polelo ye ga e thekge go bala ka lentšu",
    "语言包数据未发生改变" => "Datha ya sephuthelwana sa polelo ga se ya fetoga",
    "欢迎使用后台管理系统" => "Re a le amogela mo tshepedišong ya taolo ya backend",
    "文件有在使用或已失效" => "Faele e a šomišwa goba e fedile nako",
    "合成失败，请稍后再试" => "Tlhagišo e paletšwe, hle leka gape ka morago",
    "&copy; 2021-2023 Company, Inc." => "© 2021-2023 Khamphani, Inc.",
    "梅泰语（曼尼普尔语）" => "Meitei (Manipuri) .",
    "两次密码输入不一致" => "Ditshenyegelo tše pedi tša phasewete ga di dumelelane",
    "该用户密码不可修改" => "Phasewete ye ya mosediriši e ka se fetošwe",
    "添加文章时创建标签" => "Hlama dithegi ge o oketša diathikele",
    "删除文章时删除评论" => "Phimola ditshwayotshwayo ge o phumola sehlogo",
    "控制器与方法已存在" => "Molaodi le mokgwa di šetše di le gona",
    "标题或名称不能为空" => "Thaetlele goba leina e ka se be le selo",
    "请选择一种语言朗读" => "Hle kgetha leleme leo o tlago go le bala ka go hlaboša lentšu",
    "分类有文章不能删除" => "Go na le diathikele ka legoro tšeo di ka se phumolwego",
    "审核为垃圾评论成功" => "E hlahlobilwe bjalo ka spam ka katlego",
    "审核为垃圾评论失败" => "Tlhahlobo e paletšwe bakeng sa tshwaelo ya spam",
    "确定要登出站点吗？" => "Na o na le bonnete bja gore o nyaka go tšwa saeteng?",
    "网站名称或地址为空" => "Leina la wepesaete goba aterese ga e na selo",
    "网站名称或地址重名" => "Leina la weposaete goba aterese ya go pheta",
    "允许上传的文件类型" => "Mehuta ya difaele e dumeletšwe go tsenywa",
    "标签有文章不能删除" => "Dihlogo tšeo di nago le dithegi di ka se phumolwe",
    "人觉得这篇文章很赞" => "Batho ba nagana gore sehlogo se ke se segolo",
    "还没有页面描述信息" => "Ga go na tshedimošo ya tlhalošo ya letlakala go fihla ga bjale",
    "回复与评论内容重复" => "Phetoletsa diteng tsa karabo le tshwaelo",
    "失败！请稍后再试。" => "palelwa! Hle leka gape ka morago.",
    "主耶稣基督里的教会" => "kereke ka morena jesu kriste",
    "库尔德语（索拉尼）" => "Se-Kurd (Sorani) .",
    "布尔语(南非荷兰语)" => "Boolean (Maafrika) .",
    "用户名或密码错误" => "leina la mosebedisi le fosahetseng kapa phasewete",
    "源语言必须是中文" => "Polelo ya mothopo e swanetše go ba Setšhaena",
    "目标语言别名为空" => "Leina la leina la polelo ya nepišo ga le na selo",
    "国际化文章时标签" => "Dithegi tša sehlogo sa boditšhabatšhaba",
    "确定清除缓存吗？" => "Na o na le bonnete bja gore o nyaka go hlatswa cache?",
    "超过的单文件大小" => "Bogolo bja faele e tee bo fetile",
    "超过前端表单限制" => "Frontend foromo moedi fetile",
    "目标没有写入权限" => "Target ga e na tumelelo ya go ngwala",
    "不允许的上传类型" => "Mehuta ya go tsenya yeo e sa dumelelwago",
    "文件大小不能超过" => "Bogolo bja faele bo ka se fete",
    "保存基础设置成功" => "Dipeakanyo tša motheo di bolokilwe ka katlego",
    "首次基础设置成功" => "Pele motheo setup atlehile",
    "正在合成，请稍后" => "Synthesizing, hle ema",
    "不合理的请求方法" => "Mokgwa wa kgopelo wo o sa kwagalego",
    "Session无效或过期" => "Session ga e šome goba e fedile",
    "手机号码不正确" => "nomoro ya mogala ga se ya nepagala",
    "手机号码已存在" => "Nomoro ya sellathekeng e šetše e le gona",
    "标题或内容为空" => "Thaetlele goba diteng ga di na selo",
    "创建文章时标签" => "Dithegi ge o hlama sengwalwa",
    "编辑文章时标签" => "Dithegi ge o rulaganya sengwalwa",
    "真的删除行么？" => "Na o tloga o nyaka go phumola mola?",
    "请输入菜单名称" => "Hle tsenya leina la thepo",
    "请先删除子菜单" => "Hle phumola thepo ya ka fasana pele",
    "未登录访问后台" => "Fihlelela backend ntle le go tsena",
    "Cookie无效或过期" => "Kuki ga e šome goba e fedile nako",
    "真的还原行么？" => "Na ruri go a kgonega go e tsošološa?",
    "编辑器内容为空" => "Diteng tša morulaganyi ga di na selo",
    "未选择整行文本" => "Mothaladi ka moka wa sengwalwa ga se wa kgethwa",
    "划词选择行错误" => "Phošo ya mola wa kgetho ya mantšu",
    "数据源发生改变" => "Mohlodi wa datha o a fetoga",
    "填充成功，行号" => "E tladitšwe ka katlego, nomoro ya mola",
    "请输入分类名称" => "Hle tsenya leina la legoro",
    "分类名不能为空" => "Leina la legoro le ka se be le lefeela",
    "排序只能是数字" => "Go hlopha e ka ba feela ka dipalo",
    "请先删除子分类" => "Hle phumola magoro a manyenyane pele",
    "请输入评论作者" => "Ke kgopela o tsenye mongwadi wa ditshwayotshwayo",
    "请选择目标语言" => "Hle kgetha polelo yeo e nepišitšwego",
    "语言包生成成功" => "Polelo sephuthelwana generated ka katleho",
    "语言包生成失败" => "Moloko wa sephuthelwana sa polelo o paletšwe",
    "国际化分类成功" => "Go hlopha ga boditšhabatšhaba go atlegile",
    "国际化分类失败" => "Go hlopha ga boditšhabatšhaba go ile gwa palelwa",
    "请输入标签名称" => "Hle tsenya leina la leina",
    "国际化标签成功" => "Katlego ya leina la boditšhabatšhaba",
    "国际化标签失败" => "Thegi ya boditšhabatšhaba e paletšwe",
    "国际化文章成功" => "Katlego ya sehlogo sa boditšhabatšhaba",
    "国际化文章失败" => "Sehlogo sa ditšhaba-tšhaba se ile sa palelwa",
    "请输入页面标题" => "Hle tsenya sehlogo sa letlakala",
    "国际化页面成功" => "Leqephe la boditšhabatšhaba le atlegile",
    "国际化页面失败" => "Letlakala la boditšhabatšhaba le paletšwe",
    "作者：葡萄枝子" => "Mongwadi: Lekala la Morara",
    "请输入网站名称" => "Hle tsenya leina la weposaete",
    "请输入网站地址" => "Hle tsenya aterese ya weposaete",
    "链接名不能为空" => "Leina la kgokagano le ka se be le lefeela",
    "上传文件不完整" => "Faele yeo e tsentšwego ga se ya felela",
    "没有文件被上传" => "Ga go na difaele tšeo di ilego tša tsenywa",
    "找不到临时目录" => "Temp directory ga se ya hwetšwa",
    "未知的文件类型" => "Mohuta wa faele wo o sa tsebjego",
    "文件名不能为空" => "Leina la faele le ka se be le lefeela",
    "个文件有在使用" => "difaele di a sebediswa",
    "请先删除子页面" => "Hle phumola letlakala la ka fasana pele",
    "请输入角色名称" => "Hle tsenya leina la tema",
    "管理员不可禁用" => "Balaodi ba ka se kgone go šitiša",
    "管理员不可删除" => "Balaodi ba ka se kgone go phumola",
    "请输入限制大小" => "Hle tsenya bogolo bja moedi",
    "请输入版权信息" => "Hle tsenya tshedimošo ya tokelo ya ngwalollo",
    "恢复出厂成功！" => "Factory reset e atlegile!",
    "恢复出厂失败！" => "Factory reset e paletšwe!",
    "标签名不能为空" => "Leina la tag le ka se be le lefeela",
    "还没有内容信息" => "Ga go na tshedimošo ya diteng go fihla ga bjale",
    "这篇文章很有用" => "Sehlogo se se na le mohola kudu",
    "保存分类国际化" => "Boloka sehlopha sa boditšhabatšhaba",
    "分类国际化帮助" => "Classification boditšhabatšhaba thušo",
    "保存标签国际化" => "Boloka leina la boditšhabatšhaba",
    "标签国际化帮助" => "Tag boditšhabatšhaba thušo",
    "保存文章国际化" => "Boloka sehlogo sa boditšhabatšhaba",
    "文章国际化帮助" => "Sehlogo sa boditšhabatšhaba thušo",
    "保存页面国际化" => "Boloka letlakala la boditšhabatšhaba",
    "页面国际化帮助" => "Page internationalization thuso",
    "海地克里奥尔语" => "Secreole sa Sehaiti",
    "非法的ajax请求" => "Kgopelo ya ajax yeo e sego molaong",
    "密码至少位数" => "Phasewete e swanetše go ba le bonyane dinomoro",
    "验证码不正确" => "Khoutu ya netefatšo ye e fošagetšego",
    "包含非法参数" => "E na le ditekanyetšo tšeo di sego molaong",
    "请输入用户名" => "hle tsenya leina la mosediriši",
    "用户名已存在" => "Leina la mosediriši le šetše le le gona",
    "请重输入密码" => "Hle tsenya phasewete ya gago gape",
    "图片格式错误" => "Phošo ya sebopego sa seswantšho",
    "修改资料成功" => "Data fetotšwe ka katlego",
    "没有改变信息" => "Ga go na tshedimošo yeo e fetogilego",
    "请输入浏览量" => "Hle tsenya palo ya dipono",
    "请输入点赞数" => "Ke kgopela o tsenye palo ya di like",
    "请选择子分类" => "Hle kgetha legoro le lenyenyane",
    "创建文章成功" => "Sehlogo se hlotšwe ka katlego",
    "创建文章失败" => "E paletšwe ke go hlama sehlogo",
    "编辑文章成功" => "Edita sehlogo ka katlego",
    "标题不能为空" => "Thaetlele e ka se be lefeela",
    "菜单名称重复" => "Lebitso la menu ya pheta-pheta",
    "创建菜单成功" => "Menu bōpiloe ka katleho",
    "创建菜单失败" => "E paletšwe ke go hlama thepo",
    "编辑菜单成功" => "Edita menu ya atlehile",
    "请选择行数据" => "Hle kgetha datha ya mothaladi",
    "计算孤立文件" => "Bala difaele ditšhiwana",
    "划词选择错误" => "Kgetho ye e fošagetšego ya mantšu",
    "请提取语言包" => "Hle ntšha sephuthelwana sa polelo",
    "没有语音文字" => "Ga go na sengwalwa sa lentšu",
    "语音朗读完成" => "Go bala ka lentšu go phethilwe",
    "分类名称为空" => "Lebitso la legoro ga le na selo",
    "创建分类成功" => "Classification bōpiloe ka katleho",
    "创建分类失败" => "E paletšwe ke go hlama legoro",
    "编辑分类成功" => "Edita legoro ka katlego",
    "回复评论为空" => "Karabo ya comment ga e na selo",
    "回复评论成功" => "Araba go tshwaela ka katlego",
    "回复评论失败" => "Karabo ya tshwaelo e paletšwe",
    "评论内容为空" => "Diteng tša ditshwayotshwayo ga di na selo",
    "编辑评论成功" => "Edita tshwaelo ka katlego",
    "待审评论成功" => "Tlhaloso e sa emetše tshekatsheko e atlegile",
    "待审评论失败" => "Tlhaloso yeo e sa emetšego tshekatsheko e paletšwe",
    "审核通过评论" => "Ditshwayotshwayo tše di dumeletšwego",
    "审核评论成功" => "Tlhahlobo tshekatsheko e atlegile",
    "审核评论失败" => "Tekolo ya ditshwayotshwayo e paletšwe",
    "删除评论失败" => "E paletšwe ke go phumola tshwaelo",
    "请选择源语言" => "Hle kgetha polelo ya mothopo",
    "别名不可更改" => "Di-aliases di ka se fetošwe",
    "标签名称为空" => "Lebitso la tag ga le na selo",
    "创建链接成功" => "Link bōpiloe ka katleho",
    "创建链接失败" => "E paletšwe ke go hlama kgokagano",
    "编辑链接成功" => "Edita kgokagano ka katlego",
    "网站名称重名" => "Maina a weposaete a pheta-pheta",
    "网站地址重复" => "Aterese ya weposaete e pheta-pheta",
    "图片压缩失败" => "Go gatelela seswantšho go paletšwe",
    "移动文件失败" => "E paletšwe ke go šuthiša faele",
    "上传文件成功" => "Faele uploaded ka katleho",
    "上传文件失败" => "Go tsenya faele go paletšwe",
    "共找到文件：" => "Palomoka ya difaele tše di hweditšwego:",
    "创建页面成功" => "Page bōpiloe ka katleho",
    "创建页面失败" => "Tlholo ya letlakala e paletšwe",
    "编辑页面成功" => "Edita letlakala ka katlego",
    "权限数据错误" => "Phošo ya data ya tumelelo",
    "创建角色成功" => "Karolo e hlotšwego ka katlego",
    "创建角色失败" => "E paletšwe ke go hlola tema",
    "编辑角色成功" => "Edita karolo ka katleho",
    "游客不可删除" => "Baeti ba ka se kgone go phumola",
    "创建标签成功" => "Tag bōpiloe ka katleho",
    "创建标签失败" => "E paletšwe ke go hlama leina",
    "编辑标签成功" => "Edita tag ka katlego",
    "角色数据错误" => "Phošo ya datha ya semelo",
    "语言数据错误" => "Phošo ya datha ya polelo",
    "状态数据错误" => "Phošo ya datha ya maemo",
    "创建用户成功" => "Modiriši o hlotšwe ka katlego",
    "创建用户失败" => "E paletšwe ke go hlama mosediriši",
    "编辑用户成功" => "Edita mosebedisi ka katleho",
    "本文博客网址" => "URL ya blog ya sehlogo se",
    "评论内容重复" => "Diteng tša ditshwayotshwayo tše di ipoeletšago",
    "回复内容重复" => "Diteng tša karabo tše di ipoeletšago",
    "评论发表成功" => "Comment e rometšwe ka katlego",
    "发表评论失败" => "E paletšwe ke go romela tshwaelo",
    "前端删除评论" => "Go phumola ditshwayotshwayo ka pele",
    "中文（简体）" => "Setšhaena (Se Nolofaditšwego) .",
    "加泰罗尼亚语" => "Secatalan",
    "苏格兰盖尔语" => "Segaelic sa Scotland",
    "中文（繁体）" => "Setšhaena sa setšo) .",
    "马拉雅拉姆语" => "Semalayalam",
    "斯洛文尼亚语" => "se-slovenia sa se-slovenia",
    "阿尔巴尼亚语" => "sealbania",
    "密码至少5位" => "Phasewete e swanetše go ba bonyane ditlhaka tše 5",
    "你已经登录" => "O šetše o tsene",
    "账号被禁用" => "Akhaonto e šitišitšwe",
    "请输入密码" => "Hle tsenya phasewete",
    "留空不修改" => "Tlogela lefeela gomme o se ke wa fetoša",
    "请输入标题" => "Hle tsenya sehlogo",
    "请输入内容" => "Hle tsenya diteng",
    "多标签半角" => "Multi-leibole halofo-bophara",
    "关键词建议" => "Ditšhišinyo tša bohlokwa",
    "请输入作者" => "Hle tsenya mongwadi",
    "请选择数据" => "Hle kgetha ya data",
    "软删除文章" => "bonolo hlakola sehlogo",
    "软删除成功" => "Soft hlakola atlehile",
    "软删除失败" => "Go phumola ka boleta go paletšwe",
    "角色不存在" => "tema ga e gona",
    "角色被禁用" => "Karolo e golofetše",
    "功能不存在" => "Mošomo ga o gona",
    "功能被禁用" => "Mošomo o šitišitšwe",
    "国际化帮助" => "Boditšhabatšhaba thušo",
    "未选择文本" => "Ga go sengwalwa seo se kgethilwego",
    "请选择语言" => "Hle kgetha polelo",
    "请输入排序" => "Hle tsenya go hlopha",
    "未修改属性" => "dithoto tšeo di sa fetošwago",
    "机器人评论" => "Ditlhahlobo tša Roboto",
    "生成语言包" => "Hlagiša sephuthelwana sa polelo",
    "国际化分类" => "Go hlopha ga boditšhabatšhaba",
    "国际化标签" => "tag ya boditšhabatšhaba",
    "国际化文章" => "Dihlogo tša boditšhabatšhaba",
    "国际化页面" => "Letlakala la boditšhabatšhaba",
    "服务器环境" => "Tikologo ya seva",
    "数据库信息" => "Tshedimošo ya polokelo ya tshedimošo",
    "服务器时间" => "nako ya seva",
    "还没有评论" => "Ga go na ditshwayotshwayo go fihla ga bjale",
    "还没有数据" => "Ga go na ya data ga bjale",
    "网址不合法" => "URL ga e molaong",
    "选择多文件" => "Kgetha difaele tse ngata",
    "个未使用，" => "e sa dirišwego, .",
    "文件不存在" => "faele ga e gona",
    "重命名失败" => "Rename e paletšwe",
    "重命名成功" => "Rename e atlegile",
    "角色已存在" => "Karolo e šetše e le gona",
    "蜘蛛不索引" => "segokgo sa indexing",
    "请输入数量" => "Hle tsenya bontši",
    "昵称已存在" => "Leina la sereto le šetše le le gona",
    "页面没找到" => "Letlakala ga se la hwetšwa",
    "还没有文章" => "Ga go na dihlogo go fihla ga bjale",
    "还没有页面" => "Ga go na letlakala go fihla ga bjale",
    "还没有分类" => "Ga go na go hlopha ga bjale",
    "还没有标签" => "Ga go na dithegi ga bjale",
    "还没有热门" => "Ga se ya tuma go fihla ga bjale",
    "还没有更新" => "Ga se ya mpshafatšwa ga bjale",
    "还没有网址" => "Ga go na URL ga bjale",
    "请写点评论" => "Ke kgopela le ngwale tshwaelo",
    "，等待审核" => ",E lekanetšwa",
    "你已经注册" => "o šetše o ingwadišitše",
    "提取语言包" => "Ntšha sephuthelwana sa polelo",
    "分类国际化" => "Go hlopha Boditšhabatšhaba",
    "标签国际化" => "Label boditšhabatšhaba",
    "文章国际化" => "Athikele ya boditšhabatšhaba",
    "页面国际化" => "Page boditšhabatšhaba",
    "格鲁吉亚语" => "Segeorgia",
    "博杰普尔语" => "Bhojpuri",
    "白俄罗斯语" => "Se-Belarus sa se-Belarus",
    "斯瓦希里语" => "Seswahili",
    "古吉拉特语" => "segujarati",
    "斯洛伐克语" => "Seslovakia",
    "阿塞拜疆语" => "Se-Azerbaijan, Se-Azerbaijan",
    "印尼巽他语" => "Se-Sundane sa Indonesia",
    "加利西亚语" => "Segalicia",
    "拉脱维亚语" => "Selatvia sa Selatvia",
    "罗马尼亚语" => "Seromania",
    "亚美尼亚语" => "Searmenia sa Searmenia",
    "保加利亚语" => "Sebulgaria",
    "爱沙尼亚语" => "Seestonia sa Seestonia",
    "阿姆哈拉语" => "Seamharic",
    "吉尔吉斯语" => "SeKyrgyz",
    "克罗地亚语" => "Secroatia",
    "波斯尼亚语" => "Sebosnia sa Bosnia",
    "蒂格尼亚语" => "Tignan e le",
    "克里奥尔语" => "Secreole sa se-Creole",
    "马尔加什语" => "Semalagasy",
    "南非科萨语" => "Se-Xhosa sa Afrika Borwa",
    "南非祖鲁语" => "Sezulu, Afrika Borwa",
    "塞尔维亚语" => "Seserbia",
    "乌兹别克语" => "Seuzbek",
    "伊洛卡诺语" => "Ilocano",
    "印尼爪哇语" => "Se-Java Seindonesia",
    "回复ID错误" => "Phošo ya ID ya Karabo",
    "非法文章ID" => "ID ya sehlogo seo se sego molaong",
    "管理后台" => "Taolo ya morago",
    "管理登录" => "Admin kena",
    "登录成功" => "go tsena go atlegile",
    "切换注册" => "Switch ngodiso",
    "你已登出" => "O tšwile ka ntle",
    "登出成功" => "Logout e atlegile",
    "用户注册" => "Boingwadišo bja mosediriši",
    "注册成功" => "katlego ya ngwadišo",
    "注册失败" => "ngwadišo e paletšwe",
    "重复密码" => "Pheta phasewete",
    "切换登录" => "Switch go tsena",
    "请先登录" => "hle tsena pele",
    "修改资料" => "Fetoša tshedimošo",
    "没有改变" => "Ga go na phetogo",
    "不可修改" => "E sa fetogego",
    "上传失败" => "go tsenya go paletšwe",
    "清除成功" => "Hlakola ka katlego",
    "暂无记录" => "Ga go na direkoto",
    "批量删除" => "go phumolwa ga sehlopha",
    "文章列表" => "Lenaneo la sehlooho",
    "发布时间" => "nako ya go lokollwa",
    "修改时间" => "Fetola nako",
    "文章标题" => "Thaetlele ya sengwalwa",
    "标题重名" => "Thaetlele e pheta-pheta",
    "别名重复" => "Phetolelo ya leina la leina",
    "创建文章" => "Bopa sehlogo",
    "编辑文章" => "Edita sehlogo",
    "非法字段" => "Tšhemo yeo e sego molaong",
    "填写整数" => "Tlatša palomoka",
    "修改属性" => "Fetoša dithoto",
    "修改成功" => "E fetotšwe ka katlego",
    "修改失败" => "palelwa ke go rulaganya",
    "批量还原" => "Batch tsosolosa",
    "还原文章" => "Tsošološa sehlogo",
    "还原成功" => "Tsošološa e atlegile",
    "还原失败" => "Tsošološa e paletšwe",
    "删除文章" => "Phimola sehlogo",
    "删除成功" => "e phumotšwe ka katlego",
    "删除失败" => "e paletšwe ke go phumola",
    "全部展开" => "Katološa ka moka",
    "全部折叠" => "Phuhla ka moka",
    "添加下级" => "Oketša ya ka tlase",
    "编辑菜单" => "Edita menu ya",
    "菜单名称" => "Lebitso la menu",
    "父级菜单" => "Menu ya motswadi",
    "顶级菜单" => "menu ya godimo",
    "创建菜单" => "Theha menu ya",
    "删除菜单" => "hlakola menu ya",
    "非法访问" => "Phihlelelo ye e sa dumelelwago",
    "拒绝访问" => "phihlelelo e gannwe",
    "没有权限" => "tumelelo e gannwe",
    "彻底删除" => "Tlosa ka botlalo",
    "批量待审" => "Batch e sa emetše tshekatsheko",
    "批量审核" => "Tlhahlobo ya sehlopha",
    "垃圾评论" => "Ditshwayotshwayo tša spam",
    "分类名称" => "Leina la Legoro",
    "分类列表" => "Lenaneo la Legoro",
    "父级分类" => "Legoro la motswadi",
    "顶级分类" => "Go hlopha ga godimo",
    "分类重名" => "Dihlopha tšeo di nago le maina a go ipoeletša",
    "创建分类" => "Hlama magoro",
    "编辑分类" => "Edita legoro",
    "删除分类" => "Phimola legoro",
    "评论作者" => "Mongwadi wa tshekatsheko",
    "评论内容" => "ditshwayotshwayo",
    "评论列表" => "lenaneo la ditshwayotshwayo",
    "评论文章" => "Boeletša dihlogo",
    "回复内容" => "Diteng tša go araba",
    "回复评论" => "Araba go tshwaela",
    "编辑评论" => "Ditshwayotshwayo tša barulaganyi",
    "待审评论" => "Go sa emetše ditshwayotshwayo",
    "待审成功" => "Go sa emetše tsheko ka katlego",
    "待审失败" => "Go palelwa go sa emetše tsheko",
    "审核成功" => "Tlhahlobo e atlegile",
    "审核失败" => "Go palelwa ga tlhahlobo",
    "删除评论" => "Phimola tshwaelo",
    "谷歌翻译" => "Google Translate",
    "检测语言" => "Lemoga polelo",
    "别名错误" => "Phošo ya leina le lefsa",
    "语言错误" => "Phošo ya polelo",
    "标签名称" => "Lebitso la tag",
    "标签列表" => "lenaneo la tag",
    "标签重名" => "Dithegi tšeo di nago le maina a go ipoeletša",
    "页面列表" => "Lenaneo la matlakala",
    "页面标题" => "sehlogo sa letlakala",
    "父级页面" => "letlakala la motswadi",
    "顶级页面" => "letlakala la godimo",
    "清除缓存" => "hlakola cache",
    "当前版本" => "phetolelo ya bjale",
    "重置系统" => "Reset tsamaiso",
    "最新评论" => "tshwaelo ya moragorago",
    "联系我们" => "ikopanye le rena",
    "数据统计" => "Dipalopalo",
    "网站名称" => "Leina la wepesaete",
    "网站地址" => "aterese ya weposaete",
    "链接列表" => "Lenaneo le kgokagantšwego",
    "创建链接" => "Bopa kgokagano",
    "编辑链接" => "Edita kgokagano",
    "删除链接" => "Tlosa kgokagano",
    "日志标题" => "Thaetlele ya log",
    "消息内容" => "Diteng tša molaetša",
    "请求方法" => "Mokgwa wa kgopelo",
    "请求路径" => "Tsela ya kgopelo",
    "日志列表" => "Lenaneo la dilog",
    "上传成功" => "Upload e atlegile",
    "媒体列表" => "lenaneo la boraditaba",
    "开始上传" => "Thoma go tsenya",
    "等待上传" => "E emetše go tsenywa",
    "重新上传" => "go tsenya gape",
    "上传完成" => "upload e phethilwe",
    "系统保留" => "Tshepedišo e bolokilwe",
    "发现重复" => "Duplicate e hweditšwe",
    "上传文件" => "upload difaele",
    "个在用，" => "E nngwe e a dirišwa, .",
    "文件重名" => "Maina a faele a pheta-pheta",
    "删除文件" => "Phimola Difaele",
    "创建页面" => "Hlama letlakala",
    "编辑页面" => "Edita letlakala",
    "删除页面" => "phumola letlakala",
    "角色列表" => "lenaneo la dikarolo",
    "角色名称" => "Leina la Karolo",
    "权限分配" => "Kabelo ya tumelelo",
    "创建角色" => "Go Hlama Karolo",
    "编辑角色" => "Edita karolo",
    "删除角色" => "Phimola karolo",
    "基础设置" => "Dipeakanyo tša Motheo",
    "高级设置" => "di-setting e tsoetseng pele",
    "其他设置" => "dipeakanyo tše dingwe",
    "上传类型" => "Mohuta wa go tsenya",
    "限制大小" => "moedi boholo",
    "默认最大" => "Default palo e kahodimodimo",
    "单点登录" => "saena ka gare",
    "版权信息" => "Tshedimošo ya Tokelo ya ngwalollo",
    "链接地址" => "aterese ya kgokagano",
    "热门文章" => "dihlogo tše di tumilego",
    "首页评论" => "Ditshwayotshwayo tša letlakala la gae",
    "内容相关" => "Diteng tše di amanago",
    "图片分页" => "Go beakanya diswantšho",
    "友情链接" => "Dikgokagano",
    "语音朗读" => "Go bala ka lentšu",
    "评论登录" => "Go tsena ka tshwaelo",
    "评论待审" => "Tlhaloso e sa emetše",
    "保存成功" => "E bolokilwe ka katlego",
    "创建标签" => "Hlama dithegi",
    "编辑标签" => "Edita tag",
    "删除标签" => "Phimola tag",
    "用户列表" => "lenaneo la mosebedisi",
    "注册时间" => "Nako ya go ingwadiša",
    "首次登陆" => "Lekga la mathomo go tsena",
    "最后登陆" => "go tsena ga mafelelo",
    "语言权限" => "Ditumelelo tša polelo",
    "创建用户" => "Bopa mosebedisi",
    "不能修改" => "E ka se fetošwe",
    "编辑用户" => "Edita mosebedisi",
    "删除用户" => "hlakola basebedisi ba",
    "最新文章" => "dihlogo tša moragorago",
    "搜索结果" => "dipoelo tša nyakišišo",
    "选择语言" => "Kgetha leleme",
    "分类为空" => "Legoro le se na selo",
    "收藏文章" => "Kgoboketša dihlogo",
    "收藏成功" => "Kgoboketšo e atlegile",
    "取消收藏" => "Khansela dilo tšeo o di ratago",
    "取消回复" => "Khansela karabo",
    "发表评论" => "Swayaswaya",
    "语音播放" => "Go bapala ka lentšu",
    "回到顶部" => "morago go ya godimo",
    "分类目录" => "Dihlopha",
    "下载内容" => "Download dikagare",
    "相关文章" => "dihlogo tše di tswalanago le tšona",
    "媒体合成" => "tswako ya methopo ya ditaba",
    "语音合成" => "tswako ya polelo",
    "前端登录" => "Go tsena ka pele",
    "前端注册" => "Boingwadišo bja ka pele",
    "前端登出" => "Frontend go tšwa",
    "后台首页" => "Legae la Lemorago",
    "欢迎页面" => "letlakala la kamogelo",
    "权限管理" => "taolo ya bolaodi",
    "用户管理" => "Taolo ya Modiriši",
    "角色管理" => "taolo ya tema",
    "权限菜单" => "Thepo ya ditumello",
    "分类管理" => "Taolo ya go hlopha",
    "标签管理" => "taolo ya tag",
    "文章管理" => "Taolo ya sengwalwa",
    "页面管理" => "Taolo ya letlakala",
    "媒体管理" => "taolo ya boraditaba",
    "上传接口" => "Upload segokanyimmediamentsi sa sebolokigolo",
    "链接管理" => "Taolo ya kgokagano",
    "评论管理" => "Taolo ya ditshwayotshwayo",
    "审核通过" => "tlhahlobo e ile ya feta",
    "机器评论" => "motšhene tlhahlobo",
    "系统管理" => "Taolo ya Tshepedišo",
    "系统设置" => "Dipeakanyo tša tshepedišo",
    "保存设置" => "Boloka Dipeakanyo",
    "恢复出厂" => "Tsošološa Feme",
    "操作日志" => "Tshepedišo ya log",
    "删除日志" => "Phimola log",
    "科西嘉语" => "Secorsica",
    "瓜拉尼语" => "Guarani",
    "卢旺达语" => "Sekinyarwanda",
    "约鲁巴语" => "se-yoruba sa se-yoruba",
    "尼泊尔语" => "Senepali",
    "夏威夷语" => "Se-Hawaii",
    "意第绪语" => "Seyiddish sa Seyiddish",
    "爱尔兰语" => "irish ya se-irish",
    "希伯来语" => "seheberu",
    "卡纳达语" => "sekannada",
    "匈牙利语" => "se-hungary sa se-hungary",
    "泰米尔语" => "setamil",
    "阿拉伯语" => "Searabia",
    "孟加拉语" => "Sebengali",
    "萨摩亚语" => "Mosamoa",
    "班巴拉语" => "Bambara",
    "立陶宛语" => "Selithuania sa Selithuania",
    "马耳他语" => "Se-Malta",
    "土库曼语" => "Turkmemen",
    "阿萨姆语" => "Assamese",
    "僧伽罗语" => "sinhala ya go swana le",
    "乌克兰语" => "Seukraine",
    "威尔士语" => "Sewalese sa Sewalese",
    "菲律宾语" => "Mofilipino",
    "艾马拉语" => "Aymara",
    "泰卢固语" => "telugu",
    "多格来语" => "dogglai ya dimpša",
    "迈蒂利语" => "Maithili",
    "普什图语" => "Sepashto sa Sepashto",
    "迪维希语" => "Dhivehi",
    "卢森堡语" => "Se-Luxembourg sa se-Luxembourg",
    "土耳其语" => "seturkey sa seturkey",
    "马其顿语" => "Momatsedonia",
    "卢干达语" => "Luganda",
    "马拉地语" => "marathi",
    "乌尔都语" => "Seurdu sa se-Urdu",
    "葡萄牙语" => "Sepotokisi",
    "奥罗莫语" => "Oromo",
    "西班牙语" => "Sepeniši",
    "弗里西语" => "Sefrisian",
    "索马里语" => "Sesomalia",
    "齐切瓦语" => "Mochichewa",
    "旁遮普语" => "sepunjabi",
    "巴斯克语" => "basque basque",
    "意大利语" => "Setaliana",
    "塔吉克语" => "Setajik",
    "克丘亚语" => "Quechua",
    "奥利亚语" => "Oriya",
    "哈萨克语" => "Sekazakh sa Sekazakh",
    "林格拉语" => "Lingala",
    "塞佩蒂语" => "Sepeti",
    "塞索托语" => "Sesotho sa Sesotho",
    "维吾尔语" => "Se-Uighur sa se-Uighur",
    "ICP No.001" => "ICP No.001",
    "用户名" => "leina la mosebedisi",
    "验证码" => "Khoutu ya netefatšo",
    "记住我" => "nkgopole",
    "手机号" => "Nomoro ya mogala",
    "请输入" => "hle tsena",
    "请选择" => "hle kgetha",
    "浏览量" => "Dipono",
    "回收站" => "lešoba la go diriša gape",
    "菜单树" => "Sefate sa menu",
    "控制器" => "molaodi wa molao",
    "第一页" => "Letlakala la mathomo",
    "最后页" => "letlakala la mafelelo",
    "筛选列" => "Sefa dikholomo",
    "删除行" => "Phimola mola",
    "还原行" => "Bušetša morago mothaladi",
    "重命名" => "Rena leina lefsa",
    "国际化" => "go ikopanya ga lefase ka bophara",
    "文章数" => "Palo ya dihlogo",
    "机器人" => "roboto",
    "星期日" => "Sontaga",
    "星期一" => "Mošupologo",
    "星期二" => "Labobedi",
    "星期三" => "Laboraro",
    "星期四" => "Labone",
    "星期五" => "Labohlano",
    "星期六" => "Mokibelo",
    "会员数" => "Palo ya maloko",
    "评论数" => "Palo ya ditshwayotshwayo",
    "文件名" => "leina la faele",
    "音视频" => "Mamelwang le bidio",
    "扩展名" => "leina la katoloso",
    "未使用" => "Ga se ya šomišwa",
    "个无效" => "sego ya makgonthe",
    "共删除" => "Palomoka e phumotšwe",
    "个文件" => "difaele",
    "备案号" => "nomoro ya molato",
    "轮播图" => "carousel ya go swara",
    "条显示" => "pontšo bareng",
    "上一页" => "Letlakala la go feta",
    "下一页" => "Letlakala le le latelago",
    "未分类" => "e sa arolwago ka magoro",
    "空标签" => "leina le le se nago selo",
    "无标题" => "E se na sehlogo",
    "控制台" => "khomotšo",
    "登录中" => "go tsena ka gare",
    "注册中" => "Go ingwadiša",
    "跳转中" => "Go lebiša lefsa",
    "提交中" => "go romela",
    "审核中" => "ka tlase ga tshekatsheko",
    "请稍后" => "hle ema",
    "篇文章" => "Selo",
    "客户端" => "klaente",
    "没有了" => "ga go na yo a šetšego",
    "后评论" => "phatlalatša ditshwayotshwayo",
    "未命名" => "e sa bolelwago ka leina",
    "软删除" => "bonolo phumola",
    "语言包" => "Sephuthelwana sa polelo",
    "豪萨语" => "Se-Hausa",
    "挪威语" => "se-norway",
    "贡根语" => "polelo ya gonggan",
    "拉丁语" => "selatine sa selatine",
    "捷克语" => "Se-Czech sa se-Czech",
    "波斯语" => "Seperesia",
    "印地语" => "Sehindi sa Sehindi",
    "冰岛语" => "Se-Iceland sa se-Iceland",
    "契维语" => "Twi",
    "高棉语" => "Se-Cambodia",
    "丹麦语" => "sedanishe",
    "修纳语" => "Shona",
    "越南语" => "Sevietnam",
    "宿务语" => "Mo-Cebuano oa",
    "波兰语" => "Sepoliši sa Sepoliši",
    "鞑靼语" => "Tatar",
    "老挝语" => "Lao",
    "瑞典语" => "Seswedishe",
    "缅甸语" => "Seburu",
    "信德语" => "sindhi",
    "马来语" => "Semalay",
    "伊博语" => "Se-Igbo",
    "希腊语" => "Segerika",
    "芬兰语" => "Sefinnishe",
    "埃维语" => "Ewe",
    "毛利语" => "Semaori",
    "荷兰语" => "Sedache sa Madache",
    "蒙古语" => "Semongolia",
    "米佐语" => "Mizo",
    "世界语" => "Se-Esperanto sa se-Esperanto",
    "印尼语" => "Se-Indonesia",
    "宗加语" => "Dzonga",
    "密码" => "phasewete",
    "登录" => "Tsena ka gare",
    "重置" => "seta gape",
    "信息" => "tshedimošo",
    "成功" => "katlego",
    "失败" => "palelwa",
    "昵称" => "Lebitso la Nick",
    "选填" => "Boikgethelo",
    "注册" => "ingwadiša",
    "错误" => "phošo",
    "头像" => "avatar",
    "上传" => "go tsenya",
    "保存" => "boloka",
    "标题" => "thaetlele",
    "别名" => "Leina la leina le lefsa",
    "描述" => "hlaloša",
    "封面" => "šireletša",
    "分类" => "Go hlopha",
    "状态" => "mmušo",
    "正常" => "tlwaelo",
    "禁用" => "Šitiša",
    "作者" => "mongwadi",
    "日期" => "letšatšikgwedi",
    "添加" => "Oketša go",
    "刷新" => "hlabolla",
    "标签" => "Leibole",
    "浏览" => "Browse",
    "点赞" => "rata",
    "评论" => "Swayaswaya",
    "操作" => "sebetsa",
    "阅读" => "bala",
    "编辑" => "rulaganya",
    "删除" => "phumola",
    "翻译" => "fetolela",
    "内容" => "diteng",
    "分割" => "karoganyo ya karoganyo",
    "建议" => "tšhišinyo",
    "非法" => "sego molaong",
    "还原" => "phokotšo",
    "方法" => "mokgwa",
    "排序" => "hlopha",
    "图标" => "letšoao",
    "菜单" => "menu ya",
    "时间" => "nako",
    "匿名" => "sa tsebjego leina",
    "确定" => "Kgonthiša",
    "取消" => "Khansela",
    "跳转" => "Tshela",
    "页码" => "nomoro ya letlakala",
    "共计" => "palomoka",
    "每页" => "ka letlakala",
    "导出" => "Romela ntle",
    "打印" => "Kgatišo",
    "回复" => "fetola",
    "路径" => "tsela",
    "预览" => "Tebelelopele ya pele",
    "名称" => "leina",
    "行数" => "Methaladi",
    "相册" => "alebamo ya dinepe",
    "文章" => "selo",
    "待审" => "Letetšwe",
    "通过" => "feta",
    "垃圾" => "Ditlakala",
    "提取" => "kgola",
    "生成" => "tšweletša",
    "定位" => "maemo",
    "填充" => "go tlatša",
    "帮助" => "thušo",
    "返回" => "boa",
    "语言" => "polelo",
    "宽屏" => "skrine se sephara",
    "普通" => "tlwaelo",
    "后台" => "Ka morago ga sefala",
    "前台" => "tafoleng ya ka pele",
    "登出" => "Saena go tšwa",
    "版本" => "Bešene",
    "关于" => "mabapi le",
    "微信" => "ReBoledišana",
    "会员" => "leloko",
    "地址" => "aterese",
    "类型" => "mohuta",
    "图片" => "seswantšho",
    "文件" => "tokumente",
    "失效" => "Sego ya makgonthe",
    "大小" => "bogolo",
    "下载" => "taonelouta",
    "导航" => "go sepelasepela",
    "站标" => "Letshwao la sebaka",
    "单位" => "yuniti",
    "加速" => "potlakisa",
    "角色" => "Tema",
    "首页" => "letlakala la pele",
    "个数" => "nomoro",
    "我的" => "moepo",
    "说道" => "boletše",
    "手机" => "mogalathekeng",
    "可选" => "Boikgethelo",
    "搜索" => "nyaka",
    "页面" => "letlakala",
    "查看" => "Lekola",
    "创建" => "hlama",
    "更新" => "mpshafatša",
    "英语" => "Seisimane",
    "法语" => "Sefora",
    "俄语" => "Se-Russia",
    "梵语" => "Sanskrit",
    "日语" => "Sejapane",
    "泰语" => "Sethai",
    "苗语" => "Se-Hmong",
    "德语" => "Sejeremane",
    "韩语" => "Sekorea",
    "请" => "hle",
];
