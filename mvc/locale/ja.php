<?php
/**
 * @author admin
 * @date 2024-06-05 12:51:24
 * @desc 日语语言包
 */
return [
    "迷你mvc小框架，一套用于开发各类管理系统的通用型后台管理系统模板，基于开源免费的 layui 的制作，内置各类实际业务场景下相对丰富的示例。" => "Mini MVC は、さまざまな管理システムを開発するための汎用バックエンド管理システム テンプレートのセットである小さなフレームワークであり、オープン ソースおよび無料の Lauiui に基づいており、さまざまな実際のビジネス シナリオにおけるサンプルのセットが比較的豊富です。",
    "除非特别说明，本博客均为原创，如需转载，请以链接形式标明来源出处。" => "特に明記されていない限り、このブログはオリジナルです。転載する必要がある場合は、リンクの形式で出典を示してください。",
    "重置系统，将删除用户所有数据和附件，恢复到出厂设置。" => "システムをリセットすると、すべてのユーザー データと添付ファイルが削除され、工場出荷時の設定に復元されます。",
    "用户名只能英文字母数字下划线或中划线，位数" => "ユーザー名には、英語の文字、数字、アンダースコア、および桁数のみを含めることができます",
    "昵称只能中英文字母数字下划线中划线，位数" => "ニックネームには、中国語と英語の文字、数字、アンダースコア、アンダースコア、数字のみを含めることができます。",
    "有道接口不支持，需科学上网使用谷歌接口。" => "Youdao インターフェースはサポートされていません。科学的にインターネットにアクセスするには、Google インターフェースを使用する必要があります。",
    "别名只能英文字母数字下划线中划线，位数" => "エイリアスには、英語の文字、数字、アンダースコア、アンダースコア、数字のみを含めることができます。",
    "计算孤立文件需要较长时间，确定继续吗？" => "孤立したファイルの計算には時間がかかります。続行してもよろしいですか?",
    "语音不支持，需科学上网使用谷歌接口。" => "音声はサポートされていないため、科学的にインターネットにアクセスするには、Google インターフェイスを使用する必要があります。",
    "您的账号已在别处登录，请重新登录！" => "あなたのアカウントは別の場所でログインしています。もう一度ログインしてください。",
    "你的账号已被禁用，请联系管理员" => "あなたのアカウントは無効になっています。管理者に連絡してください",
    "文件有在使用中，请重新计算状态" => "ファイルは使用中です。ステータスを再計算してください",
    "目标语言权限不够，请联系管理员" => "ターゲット言語の権限が不十分です。管理者に問い合わせてください。",
    "真的标记垃圾评论选中的行吗？" => "選択した行を本当にスパムとしてマークしますか?",
    "源语言别名为空，请联系源作者" => "ソース言語のエイリアスが空です。ソースの作成者に問い合わせてください。",
    "点击上传，或将文件拖拽到此处" => "クリックしてアップロードするか、ここにファイルをドラッグします",
    "合成成功！再次点击按钮下载。" => "合成成功！もう一度ボタンをクリックしてダウンロードします。",
    "没有语言权限，请联系管理员" => "言語の許可がありません。管理者に連絡してください",
    "源语言别名为空，不能国际化" => "ソース言語のエイリアスが空であるため、国際化できません。",
    "源语言与目标语言都不能为空" => "ソース言語もターゲット言語も空にすることはできません。",
    "源语言与目标语言数量不一致" => "ソース言語とターゲット言語の数が一致しない",
    "删除文件失败，文件有在使用" => "ファイルの削除に失敗しました。ファイルは使用中です",
    "角色不存在，请联系管理员" => "ロールが存在しません。管理者に問い合わせてください。",
    "角色被禁用，请联系管理员" => "役割が無効になっています。管理者に連絡してください",
    "功能不存在，请联系管理员" => "機能が存在しません。管理者に連絡してください",
    "功能被禁用，请联系管理员" => "機能が無効になっています。管理者に連絡してください",
    "真的彻底删除选中的行吗？" => "本当に選択した行を完全に削除するのでしょうか？",
    "真的审核通过选中的行吗？" => "選択した行は本当に承認されていますか?",
    "目标语言与源语言不能相同" => "ターゲット言語をソース言語と同じにすることはできません",
    "提取语言包与源语言不一致" => "抽出された言語パッケージはソース言語と一致しません",
    "文件有在使用，删除失败！" => "ファイルは使用中のため削除できませんでした。",
    "没有权限，请联系管理员" => "許可がありません。管理者に連絡してください",
    "真的待审核选中的行吗？" => "本当に見直して選択すべきラインなのか？",
    "目标语言包有空行，行数" => "ターゲット言語パッケージに空白行があり、その行数が",
    "此操作恢复到出厂设置？" => "この操作により工場出荷時の設定に戻りますか?",
    "朗读错误，请联网后再试" => "読み取りエラー。インターネットに接続してもう一度お試しください。",
    "还没有添加分类描述信息" => "カテゴリ説明情報はまだ追加されていません",
    "本地媒体已失效或不存在" => "ローカルメディアが無効か存在しません",
    "库尔德语（库尔曼吉语）" => "クルド語 (クルマンジ)",
    "作者名未注册或被禁用" => "著者名が登録されていないか無効になっています",
    "真的删除选中的行吗？" => "本当に選択した行を削除しますか?",
    "真的还原选中的行吗？" => "選択した行を本当に復元しますか?",
    "真的删除行或子行么？" => "本当に行または下位行を削除しますか?",
    "目标语言包未发现空行" => "ターゲット言語パックに空行が見つかりません",
    "该语言不支持语音朗读" => "この言語は音声読み上げをサポートしていません",
    "语言包数据未发生改变" => "言語パックのデータは変更されていません",
    "欢迎使用后台管理系统" => "バックエンド管理システムへようこそ",
    "文件有在使用或已失效" => "ファイルは使用中か期限切れです",
    "合成失败，请稍后再试" => "合成に失敗しました。後でもう一度お試しください",
    "&copy; 2021-2023 Company, Inc." => "© 2021-2023 株式会社カンパニー",
    "梅泰语（曼尼普尔语）" => "迷亭（マニプリ）",
    "两次密码输入不一致" => "2 つのパスワード入力が矛盾しています",
    "该用户密码不可修改" => "このユーザーのパスワードは変更できません",
    "添加文章时创建标签" => "記事追加時にタグを作成する",
    "删除文章时删除评论" => "記事削除時にコメントも削除",
    "控制器与方法已存在" => "コントローラーとメソッドはすでに存在します",
    "标题或名称不能为空" => "タイトルまたは名前を空にすることはできません",
    "请选择一种语言朗读" => "読み上げる言語を選択してください",
    "分类有文章不能删除" => "カテゴリ内に削除できない記事があります",
    "审核为垃圾评论成功" => "スパムとして審査されました",
    "审核为垃圾评论失败" => "スパムコメントのためレビューに失敗しました",
    "确定要登出站点吗？" => "サイトからログアウトしてもよろしいですか?",
    "网站名称或地址为空" => "ウェブサイト名またはアドレスが空です",
    "网站名称或地址重名" => "ウェブサイト名またはアドレスが重複しています",
    "允许上传的文件类型" => "アップロードできるファイルの種類",
    "标签有文章不能删除" => "タグ付き記事は削除できません",
    "人觉得这篇文章很赞" => "人々はこの記事が素晴らしいと考えています",
    "还没有页面描述信息" => "ページ説明情報はまだありません",
    "回复与评论内容重复" => "返信とコメントの内容が重複しています",
    "失败！请稍后再试。" => "失敗！後でもう一度試してください。",
    "主耶稣基督里的教会" => "主イエス・キリストにある教会",
    "库尔德语（索拉尼）" => "クルド語 (ソラニ)",
    "布尔语(南非荷兰语)" => "ブール値 (アフリカーンス語)",
    "用户名或密码错误" => "間違ったユーザー名またはパスワード",
    "源语言必须是中文" => "ソース言語は中国語である必要があります",
    "目标语言别名为空" => "ターゲット言語のエイリアスが空です",
    "国际化文章时标签" => "国際化された記事タグ",
    "确定清除缓存吗？" => "キャッシュをクリアしてもよろしいですか?",
    "超过的单文件大小" => "単一ファイルのサイズを超えました",
    "超过前端表单限制" => "フロントエンドフォームの制限を超えました",
    "目标没有写入权限" => "ターゲットには書き込み権限がありません",
    "不允许的上传类型" => "許可されていないアップロードの種類",
    "文件大小不能超过" => "ファイルサイズは超えることはできません",
    "保存基础设置成功" => "基本設定が正常に保存されました",
    "首次基础设置成功" => "最初の基本セットアップが成功しました",
    "正在合成，请稍后" => "合成中です、お待ちください",
    "不合理的请求方法" => "不当な要求方法",
    "Session无效或过期" => "セッションが無効か期限切れです",
    "手机号码不正确" => "電話番号が間違っています",
    "手机号码已存在" => "携帯電話番号はすでに存在します",
    "标题或内容为空" => "タイトルまたはコンテンツが空です",
    "创建文章时标签" => "記事作成時のタグ",
    "编辑文章时标签" => "記事編集時のタグ",
    "真的删除行么？" => "本当にその行を削除しますか?",
    "请输入菜单名称" => "メニュー名を入力してください",
    "请先删除子菜单" => "まずサブメニューを削除してください",
    "未登录访问后台" => "ログインせずにバックエンドにアクセスする",
    "Cookie无效或过期" => "Cookie が無効か期限切れです",
    "真的还原行么？" => "本当に復元できるのでしょうか？",
    "编辑器内容为空" => "エディターのコンテンツが空です",
    "未选择整行文本" => "テキスト行全体が選択されていません",
    "划词选择行错误" => "単語選択行エラー",
    "数据源发生改变" => "データソースの変更",
    "填充成功，行号" => "正常に入力されました、行番号",
    "请输入分类名称" => "カテゴリ名を入力してください",
    "分类名不能为空" => "カテゴリ名を空にすることはできません",
    "排序只能是数字" => "並べ替えは数値のみで行うことができます",
    "请先删除子分类" => "まずサブカテゴリを削除してください",
    "请输入评论作者" => "コメントの作成者を入力してください",
    "请选择目标语言" => "ターゲット言語を選択してください",
    "语言包生成成功" => "言語パックが正常に生成されました",
    "语言包生成失败" => "言語パックの生成に失敗しました",
    "国际化分类成功" => "国際分類に成功しました",
    "国际化分类失败" => "国際分類に失敗しました",
    "请输入标签名称" => "ラベル名を入力してください",
    "国际化标签成功" => "国際的なレーベルの成功",
    "国际化标签失败" => "国際化タグが失敗しました",
    "国际化文章成功" => "国際的な記事の成功",
    "国际化文章失败" => "国際記事が失敗しました",
    "请输入页面标题" => "ページタイトルを入力してください",
    "国际化页面成功" => "ページの国際化が成功しました",
    "国际化页面失败" => "国際化ページが失敗しました",
    "作者：葡萄枝子" => "著者: ぶどうの枝",
    "请输入网站名称" => "ウェブサイト名を入力してください",
    "请输入网站地址" => "ウェブサイトのアドレスを入力してください",
    "链接名不能为空" => "リンク名を空にすることはできません",
    "上传文件不完整" => "アップロードされたファイルは不完全です",
    "没有文件被上传" => "ファイルはアップロードされませんでした",
    "找不到临时目录" => "一時ディレクトリが見つかりません",
    "未知的文件类型" => "不明なファイルタイプ",
    "文件名不能为空" => "ファイル名を空にすることはできません",
    "个文件有在使用" => "ファイルは使用中です",
    "请先删除子页面" => "まずサブページを削除してください",
    "请输入角色名称" => "ロール名を入力してください",
    "管理员不可禁用" => "管理者は無効にできません",
    "管理员不可删除" => "管理者は削除できません",
    "请输入限制大小" => "制限サイズを入力してください",
    "请输入版权信息" => "著作権情報を入力してください",
    "恢复出厂成功！" => "工場出荷時設定へのリセットが成功しました。",
    "恢复出厂失败！" => "工場出荷時設定へのリセットに失敗しました!",
    "标签名不能为空" => "タグ名を空にすることはできません",
    "还没有内容信息" => "コンテンツ情報はまだありません",
    "这篇文章很有用" => "この記事はとても役に立ちます",
    "保存分类国际化" => "保存カテゴリの国際化",
    "分类国际化帮助" => "分類の国際化に関するヘルプ",
    "保存标签国际化" => "ラベルの国際化を保存",
    "标签国际化帮助" => "タグの国際化に関するヘルプ",
    "保存文章国际化" => "記事の国際化を保存",
    "文章国际化帮助" => "記事の国際化に関するヘルプ",
    "保存页面国际化" => "保存ページの国際化",
    "页面国际化帮助" => "ページの国際化に関するヘルプ",
    "海地克里奥尔语" => "ハイチ クレオール",
    "非法的ajax请求" => "不正な ajax リクエスト",
    "密码至少位数" => "パスワードには少なくとも数字が必要です",
    "验证码不正确" => "間違った確認コード",
    "包含非法参数" => "不正なパラメータが含まれています",
    "请输入用户名" => "ユーザー名を入力してください",
    "用户名已存在" => "ユーザー名は既に存在します",
    "请重输入密码" => "パスワードを再入力してください",
    "图片格式错误" => "画像フォーマットエラー",
    "修改资料成功" => "データは正常に変更されました",
    "没有改变信息" => "情報の変更はありません",
    "请输入浏览量" => "閲覧数を入力してください",
    "请输入点赞数" => "「いいね！」の数を入力してください",
    "请选择子分类" => "サブカテゴリを選択してください",
    "创建文章成功" => "記事が正常に作成されました",
    "创建文章失败" => "記事の作成に失敗しました",
    "编辑文章成功" => "記事が正常に編集されました",
    "标题不能为空" => "タイトルを空白にすることはできません",
    "菜单名称重复" => "メニュー名が重複しています",
    "创建菜单成功" => "メニューが正常に作成されました",
    "创建菜单失败" => "メニューの作成に失敗しました",
    "编辑菜单成功" => "メニューの編集が成功しました",
    "请选择行数据" => "行データを選択してください",
    "计算孤立文件" => "孤立したファイルを数える",
    "划词选择错误" => "言葉の選択が間違っている",
    "请提取语言包" => "言語パックを抽出してください",
    "没有语音文字" => "音声テキストなし",
    "语音朗读完成" => "音声読み上げ完了",
    "分类名称为空" => "カテゴリ名が空です",
    "创建分类成功" => "分類が正常に作成されました",
    "创建分类失败" => "カテゴリの作成に失敗しました",
    "编辑分类成功" => "カテゴリを正常に編集しました",
    "回复评论为空" => "コメントへの返信が空です",
    "回复评论成功" => "コメントに正常に返信しました",
    "回复评论失败" => "コメントへの返信に失敗しました",
    "评论内容为空" => "コメント内容が空です",
    "编辑评论成功" => "コメントが正常に編集されました",
    "待审评论成功" => "コメント保留中のレビューが成功しました",
    "待审评论失败" => "コメント保留中のレビューが失敗しました",
    "审核通过评论" => "承認されたコメント",
    "审核评论成功" => "レビューレビューが成功しました",
    "审核评论失败" => "コメントのレビューに失敗しました",
    "删除评论失败" => "コメントの削除に失敗しました",
    "请选择源语言" => "ソース言語を選択してください",
    "别名不可更改" => "エイリアスは変更できません",
    "标签名称为空" => "タグ名が空です",
    "创建链接成功" => "リンクが正常に作成されました",
    "创建链接失败" => "リンクの作成に失敗しました",
    "编辑链接成功" => "リンクが正常に編集されました",
    "网站名称重名" => "重複した Web サイト名",
    "网站地址重复" => "ウェブサイトのアドレスが重複しています",
    "图片压缩失败" => "画像圧縮に失敗しました",
    "移动文件失败" => "ファイルの移動に失敗しました",
    "上传文件成功" => "ファイルが正常にアップロードされました",
    "上传文件失败" => "ファイルのアップロードに失敗しました",
    "共找到文件：" => "見つかったファイルの合計:",
    "创建页面成功" => "ページが正常に作成されました",
    "创建页面失败" => "ページの作成に失敗しました",
    "编辑页面成功" => "ページが正常に編集されました",
    "权限数据错误" => "許可データエラー",
    "创建角色成功" => "ロールが正常に作成されました",
    "创建角色失败" => "ロールの作成に失敗しました",
    "编辑角色成功" => "役割を正常に編集しました",
    "游客不可删除" => "訪問者は削除できません",
    "创建标签成功" => "タグが正常に作成されました",
    "创建标签失败" => "ラベルの作成に失敗しました",
    "编辑标签成功" => "タグが正常に編集されました",
    "角色数据错误" => "文字データエラー",
    "语言数据错误" => "言語データエラー",
    "状态数据错误" => "ステータスデータエラー",
    "创建用户成功" => "ユーザーが正常に作成されました",
    "创建用户失败" => "ユーザーの作成に失敗しました",
    "编辑用户成功" => "ユーザーを正常に編集しました",
    "本文博客网址" => "この記事のブログURL",
    "评论内容重复" => "コメント内容が重複しています",
    "回复内容重复" => "返信内容が重複しています",
    "评论发表成功" => "コメントが正常に投稿されました",
    "发表评论失败" => "コメントの投稿に失敗しました",
    "前端删除评论" => "フロントエンドのコメントを削除する",
    "中文（简体）" => "中国語（簡体字）",
    "加泰罗尼亚语" => "カタルーニャ語",
    "苏格兰盖尔语" => "スコットランド・ゲール語",
    "中文（繁体）" => "繁体字中国語）",
    "马拉雅拉姆语" => "マラヤーラム語",
    "斯洛文尼亚语" => "スロベニア語",
    "阿尔巴尼亚语" => "アルバニア人",
    "密码至少5位" => "パスワードは5文字以上である必要があります",
    "你已经登录" => "あなたは既にログインしています",
    "账号被禁用" => "アカウントが無効になっています",
    "请输入密码" => "パスワードを入力してください",
    "留空不修改" => "空白のままにして変更しないでください",
    "请输入标题" => "タイトルを入力してください",
    "请输入内容" => "内容を入力してください",
    "多标签半角" => "マルチラベル半角",
    "关键词建议" => "キーワードの提案",
    "请输入作者" => "著者を入力してください",
    "请选择数据" => "データを選択してください",
    "软删除文章" => "記事のソフト削除",
    "软删除成功" => "論理的な削除が成功しました",
    "软删除失败" => "論理的な削除に失敗しました",
    "角色不存在" => "役割が存在しません",
    "角色被禁用" => "役割が無効になっています",
    "功能不存在" => "機能が存在しません",
    "功能被禁用" => "機能が無効になっています",
    "国际化帮助" => "国際化のサポート",
    "未选择文本" => "テキストが選択されていません",
    "请选择语言" => "言語を選択してください",
    "请输入排序" => "並べ替えを入力してください",
    "未修改属性" => "プロパティは変更されていません",
    "机器人评论" => "ロボットのレビュー",
    "生成语言包" => "言語パックを生成する",
    "国际化分类" => "国際分類",
    "国际化标签" => "国際化タグ",
    "国际化文章" => "国際記事",
    "国际化页面" => "インターナショナルページ",
    "服务器环境" => "サーバー環境",
    "数据库信息" => "データベース情報",
    "服务器时间" => "サーバー時間",
    "还没有评论" => "コメントはまだありません",
    "还没有数据" => "まだデータがありません",
    "网址不合法" => "URLが違法です",
    "选择多文件" => "複数のファイルを選択する",
    "个未使用，" => "未使用、",
    "文件不存在" => "ファイルが存在しません",
    "重命名失败" => "名前の変更に失敗しました",
    "重命名成功" => "名前変更が成功しました",
    "角色已存在" => "ロールはすでに存在します",
    "蜘蛛不索引" => "スパイダーがインデックスを作成しない",
    "请输入数量" => "数量を入力してください",
    "昵称已存在" => "ニックネームはすでに存在します",
    "页面没找到" => "ページが見つかりません",
    "还没有文章" => "まだ記事はありません",
    "还没有页面" => "まだページがありません",
    "还没有分类" => "まだ分​​類されていません",
    "还没有标签" => "まだタグがありません",
    "还没有热门" => "まだ人気がありません",
    "还没有更新" => "まだ更新されていません",
    "还没有网址" => "URLはまだありません",
    "请写点评论" => "コメントを書いてください",
    "，等待审核" => ",モデレート",
    "你已经注册" => "あなたはすでに登録しています",
    "提取语言包" => "言語パックを抽出する",
    "分类国际化" => "分類の国際化",
    "标签国际化" => "ラベルの国際化",
    "文章国际化" => "記事の国際化",
    "页面国际化" => "ページの国際化",
    "格鲁吉亚语" => "ジョージア語",
    "博杰普尔语" => "ボージプリ",
    "白俄罗斯语" => "ベラルーシ語",
    "斯瓦希里语" => "スワヒリ語",
    "古吉拉特语" => "グジャラート語",
    "斯洛伐克语" => "スロバキア語",
    "阿塞拜疆语" => "アゼルバイジャン語",
    "印尼巽他语" => "インドネシア料理 スンダ料理",
    "加利西亚语" => "ガリシア語",
    "拉脱维亚语" => "ラトビア語",
    "罗马尼亚语" => "ルーマニア語",
    "亚美尼亚语" => "アルメニア語",
    "保加利亚语" => "ブルガリア語",
    "爱沙尼亚语" => "エストニア語",
    "阿姆哈拉语" => "アムハラ語",
    "吉尔吉斯语" => "キルギス",
    "克罗地亚语" => "クロアチア語",
    "波斯尼亚语" => "ボスニア語",
    "蒂格尼亚语" => "ティニャン",
    "克里奥尔语" => "クレオール",
    "马尔加什语" => "マダガスカル",
    "南非科萨语" => "南アフリカのコーサ語",
    "南非祖鲁语" => "ズールー族、南アフリカ",
    "塞尔维亚语" => "セルビア語",
    "乌兹别克语" => "ウズベク語",
    "伊洛卡诺语" => "イロカノ",
    "印尼爪哇语" => "ジャワ語 インドネシア語",
    "回复ID错误" => "返信IDエラー",
    "非法文章ID" => "不正な記事ID",
    "管理后台" => "経営背景",
    "管理登录" => "管理者ログイン",
    "登录成功" => "ログイン成功",
    "切换注册" => "スイッチ登録",
    "你已登出" => "ログアウトしています",
    "登出成功" => "ログアウト成功",
    "用户注册" => "ユーザー登録",
    "注册成功" => "登録完了",
    "注册失败" => "登録に失敗しました",
    "重复密码" => "パスワードを再度入力してください。",
    "切换登录" => "ログインを切り替える",
    "请先登录" => "まずログインしてください",
    "修改资料" => "情報の変更",
    "没有改变" => "変化なし",
    "不可修改" => "不変",
    "上传失败" => "アップロードに失敗しました",
    "清除成功" => "無事クリア",
    "暂无记录" => "記録なし",
    "批量删除" => "一括削除",
    "文章列表" => "記事一覧",
    "发布时间" => "リリースタイム",
    "修改时间" => "時間を変更してください",
    "文章标题" => "記事タイトル",
    "标题重名" => "タイトルが重複しています",
    "别名重复" => "重複したエイリアス",
    "创建文章" => "記事を作成する",
    "编辑文章" => "記事を編集する",
    "非法字段" => "不正なフィールド",
    "填写整数" => "整数を入力してください",
    "修改属性" => "プロパティの変更",
    "修改成功" => "正常に変更されました",
    "修改失败" => "編集に失敗する",
    "批量还原" => "バッチ復元",
    "还原文章" => "記事を復元する",
    "还原成功" => "復元に成功しました",
    "还原失败" => "復元に失敗しました",
    "删除文章" => "記事の削除",
    "删除成功" => "正常に削除されました",
    "删除失败" => "削除に失敗しました",
    "全部展开" => "すべて展開",
    "全部折叠" => "すべて折りたたむ",
    "添加下级" => "部下を追加する",
    "编辑菜单" => "編集メニュー",
    "菜单名称" => "メニュー名",
    "父级菜单" => "親メニュー",
    "顶级菜单" => "トップメニュー",
    "创建菜单" => "メニューの作成",
    "删除菜单" => "メニューの削除",
    "非法访问" => "不正アクセス",
    "拒绝访问" => "アクセスが拒否されました",
    "没有权限" => "許可が拒否されました",
    "彻底删除" => "完全に取り除きます",
    "批量待审" => "レビュー保留中のバッチ",
    "批量审核" => "一括レビュー",
    "垃圾评论" => "スパムコメント",
    "分类名称" => "種別名",
    "分类列表" => "カテゴリ一覧",
    "父级分类" => "親カテゴリ",
    "顶级分类" => "上位分類",
    "分类重名" => "重複した名前を持つカテゴリ",
    "创建分类" => "カテゴリの作成",
    "编辑分类" => "カテゴリを編集する",
    "删除分类" => "カテゴリの削除",
    "评论作者" => "レビュー著者",
    "评论内容" => "コメント",
    "评论列表" => "コメント一覧",
    "评论文章" => "レビュー記事",
    "回复内容" => "返信内容",
    "回复评论" => "コメントに返信する",
    "编辑评论" => "編集者のコメント",
    "待审评论" => "保留中のコメント",
    "待审成功" => "保留中のトライアルが正常に完了しました",
    "待审失败" => "失敗は裁判保留中",
    "审核成功" => "レビューは成功しました",
    "审核失败" => "監査の失敗",
    "删除评论" => "コメントの削除",
    "谷歌翻译" => "グーグル翻訳",
    "检测语言" => "言語を検出",
    "别名错误" => "エイリアスエラー",
    "语言错误" => "言語エラー",
    "标签名称" => "タグ名",
    "标签列表" => "タグリスト",
    "标签重名" => "重複した名前のタグ",
    "页面列表" => "ページ一覧",
    "页面标题" => "ページタイトル",
    "父级页面" => "親ページ",
    "顶级页面" => "トップページ",
    "清除缓存" => "キャッシュの消去",
    "当前版本" => "現行版",
    "重置系统" => "リセットシステム",
    "最新评论" => "最新のコメント",
    "联系我们" => "お問い合わせ",
    "数据统计" => "統計",
    "网站名称" => "ウェブサイト名",
    "网站地址" => "ウェブサイトアドレス",
    "链接列表" => "リンクされたリスト",
    "创建链接" => "リンクの作成",
    "编辑链接" => "リンクを編集",
    "删除链接" => "リンクを削除する",
    "日志标题" => "ログのタイトル",
    "消息内容" => "メッセージ内容",
    "请求方法" => "リクエスト方法",
    "请求路径" => "リクエストパス",
    "日志列表" => "ログ一覧",
    "上传成功" => "アップロード成功",
    "媒体列表" => "メディアリスト",
    "开始上传" => "アップロードの開始",
    "等待上传" => "アップロードを待っています",
    "重新上传" => "再アップロード",
    "上传完成" => "アップロードが完了しました",
    "系统保留" => "システム予約済み",
    "发现重复" => "重複が見つかりました",
    "上传文件" => "ファイルをアップロードする",
    "个在用，" => "1つは使用中ですが、",
    "文件重名" => "ファイル名が重複している",
    "删除文件" => "ファイルの削除",
    "创建页面" => "ページを作成",
    "编辑页面" => "編集ページ",
    "删除页面" => "ページを削除する",
    "角色列表" => "役割リスト",
    "角色名称" => "役割名",
    "权限分配" => "権限の割り当て",
    "创建角色" => "ロールの作成",
    "编辑角色" => "役割の編集",
    "删除角色" => "役割の削除",
    "基础设置" => "基本設定",
    "高级设置" => "高度な設定",
    "其他设置" => "その他の設定",
    "上传类型" => "アップロードタイプ",
    "限制大小" => "制限サイズ",
    "默认最大" => "デフォルトの最大値",
    "单点登录" => "サインイン",
    "版权信息" => "著作権情報",
    "链接地址" => "リンクアドレス",
    "热门文章" => "人気の記事",
    "首页评论" => "ホームページのコメント",
    "内容相关" => "コンテンツ関連",
    "图片分页" => "画像のページング",
    "友情链接" => "リンク",
    "语音朗读" => "音声読み上げ",
    "评论登录" => "コメントログイン",
    "评论待审" => "コメント保留中",
    "保存成功" => "正常に保存",
    "创建标签" => "タグの作成",
    "编辑标签" => "タグを編集する",
    "删除标签" => "タグの削除",
    "用户列表" => "ユーザーリスト",
    "注册时间" => "登録時間",
    "首次登陆" => "初回ログイン",
    "最后登陆" => "前回のログイン",
    "语言权限" => "言語の許可",
    "创建用户" => "ユーザーを作成",
    "不能修改" => "変更できません",
    "编辑用户" => "ユーザーの編集",
    "删除用户" => "ユーザーを削除する",
    "最新文章" => "最新記事",
    "搜索结果" => "の検索結果",
    "选择语言" => "言語を選択してください",
    "分类为空" => "カテゴリが空です",
    "收藏文章" => "記事を集める",
    "收藏成功" => "収集に成功しました",
    "取消收藏" => "お気に入りをキャンセルする",
    "取消回复" => "返信をキャンセル",
    "发表评论" => "コメント",
    "语音播放" => "音声再生",
    "回到顶部" => "トップに戻る",
    "分类目录" => "カテゴリー",
    "下载内容" => "ダウンロードコンテンツ",
    "相关文章" => "関連記事",
    "媒体合成" => "メディア合成",
    "语音合成" => "音声合成",
    "前端登录" => "フロントエンドログイン",
    "前端注册" => "フロントエンド登録",
    "前端登出" => "フロントエンドのログアウト",
    "后台首页" => "背景ホーム",
    "欢迎页面" => "ウェルカムページ",
    "权限管理" => "権限管理",
    "用户管理" => "ユーザー管理",
    "角色管理" => "役割管理",
    "权限菜单" => "権限メニュー",
    "分类管理" => "分類管理",
    "标签管理" => "タグ管理",
    "文章管理" => "記事管理",
    "页面管理" => "ページ管理",
    "媒体管理" => "メディア管理",
    "上传接口" => "アップロードインターフェース",
    "链接管理" => "リンク管理",
    "评论管理" => "コメント管理",
    "审核通过" => "試験に合格しました",
    "机器评论" => "マシンレビュー",
    "系统管理" => "システムマネジメント",
    "系统设置" => "システム設定",
    "保存设置" => "設定を保存する",
    "恢复出厂" => "工場を復元する",
    "操作日志" => "操作ログ",
    "删除日志" => "ログの削除",
    "科西嘉语" => "コルシカ島",
    "瓜拉尼语" => "グアラニー",
    "卢旺达语" => "ルワンダ",
    "约鲁巴语" => "ヨルバ語",
    "尼泊尔语" => "ネパール語",
    "夏威夷语" => "ハワイアン",
    "意第绪语" => "イディッシュ語",
    "爱尔兰语" => "アイルランド人",
    "希伯来语" => "ヘブライ語",
    "卡纳达语" => "カンナダ語",
    "匈牙利语" => "ハンガリー人",
    "泰米尔语" => "タミル語",
    "阿拉伯语" => "アラビア語",
    "孟加拉语" => "ベンガル語",
    "萨摩亚语" => "サモア語",
    "班巴拉语" => "バンバラ",
    "立陶宛语" => "リトアニア語",
    "马耳他语" => "マルタ語",
    "土库曼语" => "トルクメン語",
    "阿萨姆语" => "アッサム語",
    "僧伽罗语" => "シンハラ語",
    "乌克兰语" => "ウクライナ語",
    "威尔士语" => "ウェールズ語",
    "菲律宾语" => "フィリピン人",
    "艾马拉语" => "アイマラ",
    "泰卢固语" => "テルグ語",
    "多格来语" => "ドグライ",
    "迈蒂利语" => "マイティリ語",
    "普什图语" => "パシュトゥー語",
    "迪维希语" => "ディベヒ語",
    "卢森堡语" => "ルクセンブルク語",
    "土耳其语" => "トルコ語",
    "马其顿语" => "マケドニアの",
    "卢干达语" => "ルガンダ",
    "马拉地语" => "マラーティー語",
    "乌尔都语" => "ウルドゥー語",
    "葡萄牙语" => "ポルトガル語",
    "奥罗莫语" => "オロモ",
    "西班牙语" => "スペイン語",
    "弗里西语" => "フリジア語",
    "索马里语" => "ソマリ語",
    "齐切瓦语" => "チチェワ",
    "旁遮普语" => "パンジャブ語",
    "巴斯克语" => "バスク語",
    "意大利语" => "イタリアの",
    "塔吉克语" => "タジク語",
    "克丘亚语" => "ケチュア語",
    "奥利亚语" => "おりや",
    "哈萨克语" => "カザフ語",
    "林格拉语" => "リンガラ",
    "塞佩蒂语" => "セペティ",
    "塞索托语" => "セソト",
    "维吾尔语" => "ウイグル語",
    "ICP No.001" => "ICP No.001",
    "用户名" => "ユーザー名",
    "验证码" => "検証コード",
    "记住我" => "私を覚えてますか",
    "手机号" => "電話番号",
    "请输入" => "入ってください",
    "请选择" => "選んでください",
    "浏览量" => "ビュー",
    "回收站" => "ゴミ箱",
    "菜单树" => "メニューツリー",
    "控制器" => "コントローラ",
    "第一页" => "一ページ目",
    "最后页" => "最後のページ",
    "筛选列" => "列をフィルタリングする",
    "删除行" => "行の削除",
    "还原行" => "行を元に戻す",
    "重命名" => "名前の変更",
    "国际化" => "グローバリゼーション",
    "文章数" => "記事数",
    "机器人" => "ロボット",
    "星期日" => "日曜日",
    "星期一" => "月曜日",
    "星期二" => "火曜日",
    "星期三" => "水曜日",
    "星期四" => "木曜日",
    "星期五" => "金曜日",
    "星期六" => "土曜日",
    "会员数" => "会員数",
    "评论数" => "コメント数",
    "文件名" => "ファイル名",
    "音视频" => "オーディオとビデオ",
    "扩展名" => "拡張子名",
    "未使用" => "未使用",
    "个无效" => "無効",
    "共删除" => "合計削除数",
    "个文件" => "ファイル",
    "备案号" => "事件番号",
    "轮播图" => "カルーセル",
    "条显示" => "バー表示",
    "上一页" => "前のページ",
    "下一页" => "次のページ",
    "未分类" => "未分類",
    "空标签" => "空のラベル",
    "无标题" => "無題",
    "控制台" => "コンソール",
    "登录中" => "ログイン",
    "注册中" => "登録する",
    "跳转中" => "リダイレクト中",
    "提交中" => "提出する",
    "审核中" => "検討中",
    "请稍后" => "お待ちください",
    "篇文章" => "記事",
    "客户端" => "クライアント",
    "没有了" => "何も残っていない",
    "后评论" => "コメントを投稿する",
    "未命名" => "名前のない",
    "软删除" => "ソフトデリート",
    "语言包" => "言語パック",
    "豪萨语" => "ハウサ語",
    "挪威语" => "ノルウェー語",
    "贡根语" => "ゴンガン語",
    "拉丁语" => "ラテン",
    "捷克语" => "チェコ語",
    "波斯语" => "ペルシア語",
    "印地语" => "ヒンディー語",
    "冰岛语" => "アイスランド語",
    "契维语" => "ツイ",
    "高棉语" => "カンボジア人",
    "丹麦语" => "デンマーク語",
    "修纳语" => "ショナ",
    "越南语" => "ベトナム語",
    "宿务语" => "セブアノ語",
    "波兰语" => "研磨",
    "鞑靼语" => "タタール語",
    "老挝语" => "ラオ語",
    "瑞典语" => "スウェーデンの",
    "缅甸语" => "ビルマ語",
    "信德语" => "シンド語",
    "马来语" => "マレー語",
    "伊博语" => "イボ語",
    "希腊语" => "ギリシャ語",
    "芬兰语" => "フィンランド語",
    "埃维语" => "エウェ",
    "毛利语" => "マオリ語",
    "荷兰语" => "オランダの",
    "蒙古语" => "モンゴル語",
    "米佐语" => "ミゾ",
    "世界语" => "エスペラント",
    "印尼语" => "インドネシア語",
    "宗加语" => "ゾンガ",
    "密码" => "パスワード",
    "登录" => "ログイン",
    "重置" => "リセット",
    "信息" => "情報",
    "成功" => "成功",
    "失败" => "失敗",
    "昵称" => "ニックネーム",
    "选填" => "オプション",
    "注册" => "登録する",
    "错误" => "間違い",
    "头像" => "アバター",
    "上传" => "アップロード",
    "保存" => "保存",
    "标题" => "タイトル",
    "别名" => "エイリアス",
    "描述" => "説明する",
    "封面" => "カバー",
    "分类" => "分類",
    "状态" => "州",
    "正常" => "普通",
    "禁用" => "無効にする",
    "作者" => "著者",
    "日期" => "日付",
    "添加" => "に追加",
    "刷新" => "リフレッシュする",
    "标签" => "ラベル",
    "浏览" => "ブラウズ",
    "点赞" => "のように",
    "评论" => "コメント",
    "操作" => "操作する",
    "阅读" => "読む",
    "编辑" => "編集",
    "删除" => "消去",
    "翻译" => "翻訳する",
    "内容" => "コンテンツ",
    "分割" => "セグメンテーション",
    "建议" => "提案",
    "非法" => "違法",
    "还原" => "削減",
    "方法" => "方法",
    "排序" => "選別",
    "图标" => "アイコン",
    "菜单" => "メニュー",
    "时间" => "時間",
    "匿名" => "匿名",
    "确定" => "もちろん",
    "取消" => "キャンセル",
    "跳转" => "ジャンプ",
    "页码" => "ページ番号",
    "共计" => "合計",
    "每页" => "1ページあたり",
    "导出" => "輸出",
    "打印" => "印刷する",
    "回复" => "返事",
    "路径" => "パス",
    "预览" => "プレビュー",
    "名称" => "名前",
    "行数" => "行",
    "相册" => "フォトアルバム",
    "文章" => "記事",
    "待审" => "保留中",
    "通过" => "合格",
    "垃圾" => "ごみ",
    "提取" => "抽出する",
    "生成" => "生成する",
    "定位" => "位置",
    "填充" => "充填",
    "帮助" => "ヘルプ",
    "返回" => "戻る",
    "语言" => "言語",
    "宽屏" => "ワイドスクリーン",
    "普通" => "普通",
    "后台" => "バックステージ",
    "前台" => "フロント",
    "登出" => "サインアウト",
    "版本" => "バージョン",
    "关于" => "について",
    "微信" => "微信",
    "会员" => "メンバー",
    "地址" => "住所",
    "类型" => "タイプ",
    "图片" => "写真",
    "文件" => "書類",
    "失效" => "無効",
    "大小" => "サイズ",
    "下载" => "ダウンロード",
    "导航" => "ナビゲーション",
    "站标" => "サイトのロゴ",
    "单位" => "ユニット",
    "加速" => "加速する",
    "角色" => "役割",
    "首页" => "表紙",
    "个数" => "番号",
    "我的" => "私の",
    "说道" => "言った",
    "手机" => "携帯電話",
    "可选" => "オプション",
    "搜索" => "検索",
    "页面" => "ページ",
    "查看" => "チェック",
    "创建" => "作成する",
    "更新" => "更新する",
    "英语" => "英語",
    "法语" => "フランス語",
    "俄语" => "ロシア",
    "梵语" => "サンスクリット",
    "日语" => "日本語",
    "泰语" => "タイ語",
    "苗语" => "モン族",
    "德语" => "ドイツ人",
    "韩语" => "韓国語",
    "请" => "お願いします",
];
