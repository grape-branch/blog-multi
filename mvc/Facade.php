<?php

namespace Mvc;

use Closure;

// 门面静态化
class Facade
{
    // 对象容器
    protected static $container = [];

    // 添加容器
    protected static function bind($alias, $process)
    {
        if (!isset(static::$container[$alias]))
            static::$container[$alias] = $process;
    }

    // 取出对象
    protected static function make($alias, $params = [])
    {
        $process = static::$container[$alias];
        // 如果是闭包
        if ($process instanceof Closure)
            return call_user_func_array(static::$container[$alias], $params);
        return $process;
    }
}
