<?php
return [
    // 设置时区
    'timezone' => 'Asia/Shanghai',

    // 数据库配置信息
    'db' => [
        'type' => 'mysql',
        'host' => '127.0.0.1',
        'dbname' => 'mvc',
        'port' => '3306',
        'charset' => 'utf8',
        'username' => 'root',
        'password' => '',
    ],

    // 调试模式
    'debug' => true,

    // 记录操作日志
    'log' => false,

    // 数据库零查询静态缓存周期(秒，0关闭)
    'cache' => 0,

    // 开启强制路由(推荐)
    'route' => true,

    // 强制路由规则
    'rules' => [

        // 前端路由
        'index' => [
            // 请求方法 => 路由规则集
            'get' => [
                // 路由规则(正则) => 控制器
                // 测试路由
                // http://localhost/test.html
                '/test.html' => 'TestController/index',
                // http://localhost/test/index/arg1/arg2/demo.html?a=1&b=2
                '/test/index/(\w+)/(\w+)/(\w+).html' => 'TestController/index',

                // 前端路由
                '/' => 'Index/index', //首页
                '/page-(\d+).html' => 'Index/index', //首页分页
                '/category/([a-z0-9_-]+).html' => 'Index/category', //分类页
                '/category/([a-z0-9_-]+)/page-(\d+).html' => 'Index/category', //分类分页
                '/tag/([a-z0-9_-]+).html' => 'Index/tag', //标签页
                '/tag/([a-z0-9_-]+)/page-(\d+).html' => 'Index/tag', //标签分页
                '/search.html' => 'Index/search', //搜索页
                '/search/page-(\d+).html' => 'Index/search', //搜索分页
                '/([a-z0-9_-]+).html' => 'Index/detail', //详情页
                '/([a-z0-9_-]+)/page-(\d+).html' => 'Index/detail', //详情分页
                '/page/([a-z0-9_-]+).html' => 'Index/page', //独立页面
                '/page/([a-z0-9_-]+)/page-(\d+).html' => 'Index/page', //独立页面分页
                '/sitemap.xml' => 'Index/sitemap', //网站地图
                '/fetch' => 'Index/fetch', // 获取接口
            ],
            'ajax' => [
                '/ajax/([a-z0-9_-]+).html' => 'Index/ajax', //ajax处理
                '/fetch' => 'Index/fetch', // 获取接口
            ]
        ],

        // 后端路由
        'admin' => [
            // 请求方法 - 控制器 - 方法
            'get' => [
                // 账号管理 无需权限验证
                '/admin/login.html' => 'Account/index', //登录
                '/admin/register.html' => 'Account/register', //注册
                '/admin/logout.html' => 'Account/logout', //登出
                '/admin/profile.html' => 'Account/profile', //修改资料
                '/admin/captcha.html' => 'Account/captcha', //验证码

                // 权限管理 用户 - 角色 - 权限
                '/admin/' => 'Index/index', //后端首页
                '/admin/index/welcome.html' => 'Index/welcome', //欢迎页面
                // 用户管理
                '/admin/user/index.html' => 'User/index', //列表
                '/admin/user/create.html' => 'User/create', //创建
                '/admin/user/edit/(\d+).html' => 'User/edit', //编辑
                // 角色管理
                '/admin/role/index.html' => 'Role/index', //列表
                '/admin/role/create.html' => 'Role/create', //创建
                '/admin/role/edit/(\d+).html' => 'Role/edit', //编辑
                // 权限菜单
                '/admin/auth/index.html' => 'Auth/index', //列表
                '/admin/auth/create.html' => 'Auth/create', //创建
                '/admin/auth/edit/(\d+).html' => 'Auth/edit', //编辑
                // 分类管理
                '/admin/category/index.html' => 'Category/index', //列表
                '/admin/category/create.html' => 'Category/create', //创建
                '/admin/category/edit/(\d+).html' => 'Category/edit', //编辑
                // 标签管理
                '/admin/tag/index.html' => 'Tag/index', //列表
                '/admin/tag/create.html' => 'Tag/create', //创建
                '/admin/tag/edit/(\d+).html' => 'Tag/edit', //编辑
                // 文章管理
                '/admin/article/index.html' => 'Article/index', //列表
                '/admin/article/create.html' => 'Article/create', //创建
                '/admin/article/edit/(\d+).html' => 'Article/edit', //编辑
                '/admin/article/recycle.html' => 'Article/recycle', //回收站
                // 页面管理
                '/admin/page/index.html' => 'Page/index', //列表
                '/admin/page/create.html' => 'Page/create', //创建
                '/admin/page/edit/(\d+).html' => 'Page/edit', //编辑
                // 媒体管理
                '/admin/media/index.html' => 'Media/index', //列表
                '/admin/media/file.html' => 'Media/file', //上传文件
                // 链接管理
                '/admin/link/index.html' => 'Link/index', //列表
                '/admin/link/create.html' => 'Link/create', //创建
                '/admin/link/edit/(\d+).html' => 'Link/edit', //编辑
                // 评论管理
                '/admin/comment/index.html' => 'Comment/index', //列表
                '/admin/comment/reply.html' => 'Comment/reply', //回复
                '/admin/comment/edit/(\d+).html' => 'Comment/edit', //编辑
                '/admin/comment/robot.html' => 'Comment/robot', //机器人评论
                // 系统管理
                '/admin/setting/index.html' => 'Setting/index', //系统设置
                '/admin/log/index.html' => 'Log/index', //操作日志
                // 国际化
                '/admin/i18n/index.html' => 'I18n/index', //谷歌翻译
                '/admin/i18n/category.html' => 'I18n/category', //分类
                '/admin/i18n/category/(\d+).html' => 'I18n/category_i18n', //分类国际化
                '/admin/i18n/category/help/(\d+).html' => 'I18n/category_help', //分类国际化帮助
                '/admin/i18n/tag.html' => 'I18n/tag', //标签
                '/admin/i18n/tag/(\d+).html' => 'I18n/tag_i18n', //标签国际化
                '/admin/i18n/tag/help/(\d+).html' => 'I18n/tag_help', //标签国际化帮助
                '/admin/i18n/article.html' => 'I18n/article', //文章
                '/admin/i18n/article/(\d+).html' => 'I18n/article_i18n', //文章国际化
                '/admin/i18n/article/help/(\d+).html' => 'I18n/article_help', //文章国际化帮助
                '/admin/i18n/page.html' => 'I18n/page', //页面
                '/admin/i18n/page/(\d+).html' => 'I18n/page_i18n', //页面国际化
                '/admin/i18n/page/help/(\d+).html' => 'I18n/page_help', //页面国际化帮助
            ],
            'post' => [
                '/admin/media/upload.html' => 'Media/upload', //上传
            ],
            'ajax' => [
                // 账号管理 无需权限验证
                '/admin/login.html' => 'Account/index', //保存登录
                '/admin/logout.html' => 'Account/logout', //登出
                '/admin/register.html' => 'Account/register', //保存注册
                '/admin/profile.html' => 'Account/profile', //保存资料
                '/admin/clear.html' => 'Account/clear', //清除缓存
                // 权限管理 用户 - 角色 - 权限
                // 用户管理
                '/admin/user/index.html' => 'User/index', //列表ajax请求
                '/admin/user/save.html' => 'User/save', //保存新建
                '/admin/user/update.html' => 'User/update', //保存更新
                '/admin/user/modify.html' => 'User/modify', //属性修改
                '/admin/user/delete.html' => 'User/delete', //删除
                // 角色管理
                '/admin/role/index.html' => 'Role/index', //列表ajax请求
                '/admin/role/save.html' => 'Role/save', //保存新建
                '/admin/role/update.html' => 'Role/update', //保存更新
                '/admin/role/modify.html' => 'Role/modify', //属性修改
                '/admin/role/delete.html' => 'Role/delete', //删除
                // 权限菜单
                '/admin/auth/index.html' => 'Auth/index', //列表ajax请求
                '/admin/auth/save.html' => 'Auth/save', //保存新建
                '/admin/auth/update.html' => 'Auth/update', //保存更新
                '/admin/auth/modify.html' => 'Auth/modify', //属性修改
                '/admin/auth/delete.html' => 'Auth/delete', //删除
                // 分类管理
                '/admin/category/index.html' => 'Category/index', //列表ajax请求
                '/admin/category/save.html' => 'Category/save', //保存新建
                '/admin/category/update.html' => 'Category/update', //保存更新
                '/admin/category/modify.html' => 'Category/modify', //属性修改
                '/admin/category/delete.html' => 'Category/delete', //删除
                // 标签管理
                '/admin/tag/index.html' => 'Tag/index', //列表ajax请求
                '/admin/tag/save.html' => 'Tag/save', //保存新建
                '/admin/tag/update.html' => 'Tag/update', //保存更新
                '/admin/tag/modify.html' => 'Tag/modify', //属性修改
                '/admin/tag/delete.html' => 'Tag/delete', //删除
                // 文章管理
                '/admin/article/index.html' => 'Article/index', //列表ajax请求
                '/admin/article/save.html' => 'Article/save', //保存新建
                '/admin/article/update.html' => 'Article/update', //保存更新
                '/admin/article/modify.html' => 'Article/modify', //属性修改
                '/admin/article/delete.html' => 'Article/delete', //删除
                '/admin/article/recycle.html' => 'Article/recycle', //回收站
                '/admin/article/restore.html' => 'Article/restore', //还原
                '/admin/article/destroy.html' => 'Article/destroy', //彻底删除
                // 页面管理
                '/admin/page/index.html' => 'Page/index', //列表ajax请求
                '/admin/page/save.html' => 'Page/save', //保存新建
                '/admin/page/update.html' => 'Page/update', //保存更新
                '/admin/page/modify.html' => 'Page/modify', //属性修改
                '/admin/page/delete.html' => 'Page/delete', //删除
                // 媒体管理
                '/admin/media/index.html' => 'Media/index', //列表
                '/admin/media/upload.html' => 'Media/upload', //上传
                '/admin/media/rename.html' => 'Media/rename', //重命名
                '/admin/media/delete.html' => 'Media/delete', //删除
                '/admin/media/calc.html' => 'Media/calc', //计算孤立文件
                // 链接管理
                '/admin/link/index.html' => 'Link/index', //列表ajax请求
                '/admin/link/save.html' => 'Link/save', //保存新建
                '/admin/link/update.html' => 'Link/update', //保存更新
                '/admin/link/modify.html' => 'Link/modify', //属性修改
                '/admin/link/delete.html' => 'Link/delete', //删除
                // 评论管理
                '/admin/comment/index.html' => 'Comment/index', //列表ajax请求
                '/admin/comment/save_reply.html' => 'Comment/save_reply', //保存回复
                '/admin/comment/update.html' => 'Comment/update', //保存更新
                '/admin/comment/pending.html' => 'Comment/pending', //待审
                '/admin/comment/passed.html' => 'Comment/passed', //审核通过
                '/admin/comment/spam.html' => 'Comment/spam', //垃圾评论
                '/admin/comment/delete.html' => 'Comment/delete', //删除
                '/admin/comment/robot.html' => 'Comment/robot', //机器人评论
                // 系统管理
                '/admin/setting/save_index.html' => 'Setting/save_index', //保存系统设置
                '/admin/setting/recovery.html' => 'Setting/recovery', //恢复出厂设置
                '/admin/log/index.html' => 'Log/index', //操作日志
                '/admin/log/delete.html' => 'Log/delete', //删除
                // 国际化
                '/admin/i18n/index.html' => 'I18n/index', //谷歌翻译
                '/admin/i18n/fetch.html' => 'I18n/fetch', //提取语言包
                '/admin/i18n/generate.html' => 'I18n/generate', //生成语言包
                '/admin/i18n/category.html' => 'I18n/category', //分类ajax请求
                '/admin/i18n/save_category.html' => 'I18n/save_category', //保存分类国际化
                '/admin/i18n/tag.html' => 'I18n/tag', //标签ajax请求
                '/admin/i18n/save_tag.html' => 'I18n/save_tag', //保存标签国际化
                '/admin/i18n/article.html' => 'I18n/article', //文章ajax请求
                '/admin/i18n/save_article.html' => 'I18n/save_article', //保存文章国际化
                '/admin/i18n/page.html' => 'I18n/page', //页面ajax请求
                '/admin/i18n/save_page.html' => 'I18n/save_page', //保存页面国际化
            ],
        ],

        // api接口路由
        'api' => [
            'get' => [
                '/api/fetch' => 'Index/fetch', //数据转发
                '/api/translate' => 'Index/translate', //谷歌翻译接口
                '/api/' => 'Index/index', //首页
                '/api/page-(\d+).html' => 'Index/index', //首页分页
                '/api/category/([a-z0-9_-]+).html' => 'Index/category', //分类页
                '/api/category/([a-z0-9_-]+)/page-(\d+).html' => 'Index/category', //分类分页
                '/api/tag/([a-z0-9_-]+).html' => 'Index/tag', //标签页
                '/api/tag/([a-z0-9_-]+)/page-(\d+).html' => 'Index/tag', //标签分页
                '/api/search.html' => 'Index/search', //搜索页
                '/api/search/page-(\d+).html' => 'Index/search', //搜索分页
                '/api/([a-z0-9_-]+).html' => 'Index/detail', //详情页
                '/api/([a-z0-9_-]+)/page-(\d+).html' => 'Index/detail', //详情分页
                '/api/page/([a-z0-9_-]+).html' => 'Index/page', //独立页面
                '/api/page/([a-z0-9_-]+)/page-(\d+).html' => 'Index/page', //独立页面分页
                '/api/ajax/([a-z0-9_-]+).html' => 'Index/ajax', //ajax处理
            ],
            'post' => [
                '/api/fetch' => 'Index/fetch', //数据转发
                '/api/translate' => 'Index/translate', //谷歌翻译接口
                '/api/ajax/([a-z0-9_-]+).html' => 'Index/ajax', //ajax处理
            ],
            'ajax' => [
                '/api/fetch' => 'Index/fetch', //数据转发
                '/api/translate' => 'Index/translate', //谷歌翻译接口
                '/api/' => 'Index/index', //首页
                '/api/page-(\d+).html' => 'Index/index', //首页分页
                '/api/category/([a-z0-9_-]+).html' => 'Index/category', //分类页
                '/api/category/([a-z0-9_-]+)/page-(\d+).html' => 'Index/category', //分类分页
                '/api/tag/([a-z0-9_-]+).html' => 'Index/tag', //标签页
                '/api/tag/([a-z0-9_-]+)/page-(\d+).html' => 'Index/tag', //标签分页
                '/api/search.html' => 'Index/search', //搜索页
                '/api/search/page-(\d+).html' => 'Index/search', //搜索分页
                '/api/([a-z0-9_-]+).html' => 'Index/detail', //详情页
                '/api/([a-z0-9_-]+)/page-(\d+).html' => 'Index/detail', //详情分页
                '/api/page/([a-z0-9_-]+).html' => 'Index/page', //独立页面
                '/api/page/([a-z0-9_-]+)/page-(\d+).html' => 'Index/page', //独立页面分页
                '/api/ajax/([a-z0-9_-]+).html' => 'Index/ajax', //ajax处理
            ],
        ],
    ],

    // 其它预留
];
