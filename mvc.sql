/*
Navicat MySQL Data Transfer

Source Server         : blog
Source Server Version : 50562
Source Host           : localhost:3306
Source Database       : mvc

Target Server Type    : MYSQL
Target Server Version : 50562
File Encoding         : 65001

Date: 2024-04-04 09:54:11
*/

SET FOREIGN_KEY_CHECKS=0;
-- ----------------------------
-- Table structure for `article`
-- ----------------------------
DROP TABLE IF EXISTS `article`;
CREATE TABLE `article` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `category_id` int(11) unsigned NOT NULL DEFAULT '0',
  `tag_ids` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `author` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `cover` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `seo_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `seo_description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `views` int(11) unsigned NOT NULL DEFAULT '0',
  `likes` int(11) unsigned NOT NULL DEFAULT '0',
  `status` int(1) unsigned NOT NULL DEFAULT '0' COMMENT '0正常1禁用2回收站',
  `can_comment` int(1) unsigned NOT NULL DEFAULT '0' COMMENT '0允许1禁止',
  `create_at` int(11) unsigned NOT NULL DEFAULT '0',
  `update_at` int(11) unsigned NOT NULL DEFAULT '0',
  `lang` varchar(8) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'zh-CN',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of article
-- ----------------------------
INSERT INTO `article` VALUES ('1', '2', '', '世界，你好！', 'hello-world', 'admin', '<p>\n	欢迎使用博客系统多国语言版。\n</p>\n<p>\n	这是您的第一篇文章。编辑或删除它，然后开始写作吧！\n</p>', '', '', '', '', '0', '0', '0', '0', '0', '0', 'zh-CN');

-- ----------------------------
-- Table structure for `auth`
-- ----------------------------
DROP TABLE IF EXISTS `auth`;
CREATE TABLE `auth` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `pid` int(11) unsigned NOT NULL DEFAULT '0',
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `controller` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `action` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `icon` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sort` int(11) NOT NULL DEFAULT '0',
  `is_menu` int(1) unsigned NOT NULL DEFAULT '0' COMMENT '0是1否',
  `status` int(1) unsigned NOT NULL DEFAULT '0' COMMENT '0正常1禁用',
  `time` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=115 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of auth
-- ----------------------------
INSERT INTO `auth` VALUES ('1', '0', '后台首页', 'Index', 'index', 'layui-icon layui-icon-home', '0', '1', '0', '0');
INSERT INTO `auth` VALUES ('2', '1', '欢迎页面', 'Index', 'welcome', '', '0', '1', '0', '0');
INSERT INTO `auth` VALUES ('3', '0', '权限管理', '', '', 'layui-icon layui-icon-auz', '0', '0', '0', '0');
INSERT INTO `auth` VALUES ('4', '3', '用户管理', 'User', 'index', '', '0', '0', '0', '0');
INSERT INTO `auth` VALUES ('5', '4', '查看', 'User', 'index', '', '0', '1', '0', '0');
INSERT INTO `auth` VALUES ('6', '4', '创建', 'User', 'create', '', '0', '1', '0', '0');
INSERT INTO `auth` VALUES ('7', '4', '保存', 'User', 'save', '', '0', '1', '0', '0');
INSERT INTO `auth` VALUES ('8', '4', '编辑', 'User', 'edit', '', '0', '1', '0', '0');
INSERT INTO `auth` VALUES ('9', '4', '更新', 'User', 'update', '', '0', '1', '0', '0');
INSERT INTO `auth` VALUES ('10', '4', '修改属性', 'User', 'modify', '', '0', '1', '0', '0');
INSERT INTO `auth` VALUES ('11', '4', '删除', 'User', 'delete', '', '0', '1', '0', '0');
INSERT INTO `auth` VALUES ('12', '3', '角色管理', 'Role', 'index', '', '0', '0', '0', '0');
INSERT INTO `auth` VALUES ('13', '12', '查看', 'Role', 'index', '', '0', '1', '0', '0');
INSERT INTO `auth` VALUES ('14', '12', '创建', 'Role', 'create', '', '0', '1', '0', '0');
INSERT INTO `auth` VALUES ('15', '12', '保存', 'Role', 'save', '', '0', '1', '0', '0');
INSERT INTO `auth` VALUES ('16', '12', '编辑', 'Role', 'edit', '', '0', '1', '0', '0');
INSERT INTO `auth` VALUES ('17', '12', '更新', 'Role', 'update', '', '0', '1', '0', '0');
INSERT INTO `auth` VALUES ('18', '12', '修改属性', 'Role', 'modify', '', '0', '1', '0', '0');
INSERT INTO `auth` VALUES ('19', '12', '删除', 'Role', 'delete', '', '0', '1', '0', '0');
INSERT INTO `auth` VALUES ('20', '3', '权限菜单', 'Auth', 'index', '', '0', '0', '0', '0');
INSERT INTO `auth` VALUES ('21', '20', '查看', 'Auth', 'index', '', '0', '1', '0', '0');
INSERT INTO `auth` VALUES ('22', '20', '创建', 'Auth', 'create', '', '0', '1', '0', '0');
INSERT INTO `auth` VALUES ('23', '20', '保存', 'Auth', 'save', '', '0', '1', '0', '0');
INSERT INTO `auth` VALUES ('24', '20', '编辑', 'Auth', 'edit', '', '0', '1', '0', '0');
INSERT INTO `auth` VALUES ('25', '20', '更新', 'Auth', 'update', '', '0', '1', '0', '0');
INSERT INTO `auth` VALUES ('26', '20', '修改属性', 'Auth', 'modify', '', '0', '1', '0', '0');
INSERT INTO `auth` VALUES ('27', '20', '删除', 'Auth', 'delete', '', '0', '1', '0', '0');
INSERT INTO `auth` VALUES ('28', '0', '分类管理', '', '', 'layui-icon layui-icon-app', '0', '0', '0', '0');
INSERT INTO `auth` VALUES ('29', '28', '分类列表', 'Category', 'index', '', '0', '0', '0', '0');
INSERT INTO `auth` VALUES ('30', '28', '创建', 'Category', 'create', '', '0', '0', '0', '0');
INSERT INTO `auth` VALUES ('31', '28', '保存', 'Category', 'save', '', '0', '1', '0', '0');
INSERT INTO `auth` VALUES ('32', '28', '编辑', 'Category', 'edit', '', '0', '1', '0', '0');
INSERT INTO `auth` VALUES ('33', '28', '更新', 'Category', 'update', '', '0', '1', '0', '0');
INSERT INTO `auth` VALUES ('34', '28', '修改属性', 'Category', 'modify', '', '0', '1', '0', '0');
INSERT INTO `auth` VALUES ('35', '28', '删除', 'Category', 'delete', '', '0', '1', '0', '0');
INSERT INTO `auth` VALUES ('36', '0', '标签管理', '', '', 'layui-icon layui-icon-note', '0', '0', '0', '0');
INSERT INTO `auth` VALUES ('37', '36', '标签列表', 'Tag', 'index', '', '0', '0', '0', '0');
INSERT INTO `auth` VALUES ('38', '36', '创建', 'Tag', 'create', '', '0', '0', '0', '0');
INSERT INTO `auth` VALUES ('39', '36', '保存', 'Tag', 'save', '', '0', '1', '0', '0');
INSERT INTO `auth` VALUES ('40', '36', '编辑', 'Tag', 'edit', '', '0', '1', '0', '0');
INSERT INTO `auth` VALUES ('41', '36', '更新', 'Tag', 'update', '', '0', '1', '0', '0');
INSERT INTO `auth` VALUES ('42', '36', '修改属性', 'Tag', 'modify', '', '0', '1', '0', '0');
INSERT INTO `auth` VALUES ('43', '36', '删除', 'Tag', 'delete', '', '0', '1', '0', '0');
INSERT INTO `auth` VALUES ('44', '0', '文章管理', '', '', 'layui-icon layui-icon-template-1', '0', '0', '0', '0');
INSERT INTO `auth` VALUES ('45', '44', '文章列表', 'Article', 'index', '', '0', '0', '0', '0');
INSERT INTO `auth` VALUES ('46', '44', '创建', 'Article', 'create', '', '0', '0', '0', '0');
INSERT INTO `auth` VALUES ('47', '44', '保存', 'Article', 'save', '', '0', '1', '0', '0');
INSERT INTO `auth` VALUES ('48', '44', '编辑', 'Article', 'edit', '', '0', '1', '0', '0');
INSERT INTO `auth` VALUES ('49', '44', '更新', 'Article', 'update', '', '0', '1', '0', '0');
INSERT INTO `auth` VALUES ('50', '44', '修改属性', 'Article', 'modify', '', '0', '1', '0', '0');
INSERT INTO `auth` VALUES ('51', '44', '软删除', 'Article', 'delete', '', '0', '1', '0', '0');
INSERT INTO `auth` VALUES ('52', '44', '回收站', 'Article', 'recycle', '', '0', '0', '0', '0');
INSERT INTO `auth` VALUES ('53', '44', '还原', 'Article', 'restore', '', '0', '1', '0', '0');
INSERT INTO `auth` VALUES ('54', '44', '彻底删除', 'Article', 'destroy', '', '0', '1', '0', '0');
INSERT INTO `auth` VALUES ('55', '0', '页面管理', '', '', 'layui-icon layui-icon-file', '0', '0', '0', '0');
INSERT INTO `auth` VALUES ('56', '55', '页面列表', 'Page', 'index', '', '0', '0', '0', '0');
INSERT INTO `auth` VALUES ('57', '55', '创建', 'Page', 'create', '', '0', '0', '0', '0');
INSERT INTO `auth` VALUES ('58', '55', '保存', 'Page', 'save', '', '0', '1', '0', '0');
INSERT INTO `auth` VALUES ('59', '55', '编辑', 'Page', 'edit', '', '0', '1', '0', '0');
INSERT INTO `auth` VALUES ('60', '55', '更新', 'Page', 'update', '', '0', '1', '0', '0');
INSERT INTO `auth` VALUES ('61', '55', '修改属性', 'Page', 'modify', '', '0', '1', '0', '0');
INSERT INTO `auth` VALUES ('62', '55', '删除', 'Page', 'delete', '', '0', '1', '0', '0');
INSERT INTO `auth` VALUES ('63', '0', '媒体管理', '', '', 'layui-icon layui-icon-camera', '0', '0', '0', '0');
INSERT INTO `auth` VALUES ('64', '63', '媒体列表', 'Media', 'index', '', '0', '0', '0', '0');
INSERT INTO `auth` VALUES ('65', '63', '上传文件', 'Media', 'file', '', '0', '0', '0', '0');
INSERT INTO `auth` VALUES ('66', '63', '上传接口', 'Media', 'upload', '', '0', '1', '0', '0');
INSERT INTO `auth` VALUES ('67', '63', '重命名', 'Media', 'rename', '', '0', '1', '0', '0');
INSERT INTO `auth` VALUES ('68', '63', '删除', 'Media', 'delete', '', '0', '1', '0', '0');
INSERT INTO `auth` VALUES ('69', '63', '计算孤立文件', 'Media', 'calc', '', '0', '1', '0', '0');
INSERT INTO `auth` VALUES ('70', '0', '链接管理', '', '', 'layui-icon layui-icon-link', '0', '0', '0', '0');
INSERT INTO `auth` VALUES ('71', '70', '链接列表', 'Link', 'index', '', '0', '0', '0', '0');
INSERT INTO `auth` VALUES ('72', '70', '创建', 'Link', 'create', '', '0', '0', '0', '0');
INSERT INTO `auth` VALUES ('73', '70', '保存', 'Link', 'save', '', '0', '1', '0', '0');
INSERT INTO `auth` VALUES ('74', '70', '编辑', 'Link', 'edit', '', '0', '1', '0', '0');
INSERT INTO `auth` VALUES ('75', '70', '更新', 'Link', 'update', '', '0', '1', '0', '0');
INSERT INTO `auth` VALUES ('76', '70', '修改属性', 'Link', 'modify', '', '0', '1', '0', '0');
INSERT INTO `auth` VALUES ('77', '70', '删除', 'Link', 'delete', '', '0', '1', '0', '0');
INSERT INTO `auth` VALUES ('78', '0', '评论管理', '', '', 'layui-icon layui-icon-dialogue', '0', '0', '0', '0');
INSERT INTO `auth` VALUES ('79', '78', '评论列表', 'Comment', 'index', '', '0', '0', '0', '0');
INSERT INTO `auth` VALUES ('80', '78', '回复', 'Comment', 'reply', '', '0', '1', '0', '0');
INSERT INTO `auth` VALUES ('81', '78', '保存', 'Comment', 'save_reply', '', '0', '1', '0', '0');
INSERT INTO `auth` VALUES ('82', '78', '编辑', 'Comment', 'edit', '', '0', '1', '0', '0');
INSERT INTO `auth` VALUES ('83', '78', '更新', 'Comment', 'update', '', '0', '1', '0', '0');
INSERT INTO `auth` VALUES ('84', '78', '待审', 'Comment', 'pending', '', '0', '1', '0', '0');
INSERT INTO `auth` VALUES ('85', '78', '审核通过', 'Comment', 'passed', '', '0', '1', '0', '0');
INSERT INTO `auth` VALUES ('86', '78', '垃圾评论', 'Comment', 'spam', '', '0', '1', '0', '0');
INSERT INTO `auth` VALUES ('87', '78', '机器评论', 'Comment', 'robot', '', '0', '0', '0', '0');
INSERT INTO `auth` VALUES ('88', '78', '删除', 'Comment', 'delete', '', '0', '1', '0', '0');
INSERT INTO `auth` VALUES ('89', '0', '系统管理', '', '', 'layui-icon layui-icon-set', '0', '0', '0', '0');
INSERT INTO `auth` VALUES ('90', '89', '系统设置', 'Setting', 'index', '', '0', '0', '0', '0');
INSERT INTO `auth` VALUES ('91', '89', '保存设置', 'Setting', 'save_index', '', '0', '1', '0', '0');
INSERT INTO `auth` VALUES ('92', '89', '恢复出厂', 'Setting', 'recovery', '', '0', '1', '0', '0');
INSERT INTO `auth` VALUES ('93', '89', '操作日志', 'Log', 'index', '', '0', '0', '0', '0');
INSERT INTO `auth` VALUES ('94', '89', '删除日志', 'Log', 'delete', '', '0', '1', '0', '0');
INSERT INTO `auth` VALUES ('95', '0', '国际化', '', '', 'layui-icon layui-icon-util', '0', '0', '0', '0');
INSERT INTO `auth` VALUES ('96', '95', '语言包', 'I18n', 'index', '', '0', '0', '0', '0');
INSERT INTO `auth` VALUES ('97', '95', '提取语言包', 'I18n', 'fetch', '', '0', '1', '0', '0');
INSERT INTO `auth` VALUES ('98', '95', '生成语言包', 'I18n', 'generate', '', '0', '1', '0', '0');
INSERT INTO `auth` VALUES ('99', '95', '分类', 'I18n', 'category', '', '0', '0', '0', '0');
INSERT INTO `auth` VALUES ('100', '95', '分类国际化', 'I18n', 'category_i18n', '', '0', '1', '0', '0');
INSERT INTO `auth` VALUES ('101', '95', '保存分类国际化', 'I18n', 'save_category', '', '0', '1', '0', '0');
INSERT INTO `auth` VALUES ('102', '95', '分类国际化帮助', 'I18n', 'category_help', '', '0', '1', '0', '0');
INSERT INTO `auth` VALUES ('103', '95', '标签', 'I18n', 'tag', '', '0', '0', '0', '0');
INSERT INTO `auth` VALUES ('104', '95', '标签国际化', 'I18n', 'tag_i18n', '', '0', '1', '0', '0');
INSERT INTO `auth` VALUES ('105', '95', '保存标签国际化', 'I18n', 'save_tag', '', '0', '1', '0', '0');
INSERT INTO `auth` VALUES ('106', '95', '标签国际化帮助', 'I18n', 'tag_help', '', '0', '1', '0', '0');
INSERT INTO `auth` VALUES ('107', '95', '文章', 'I18n', 'article', '', '0', '0', '0', '0');
INSERT INTO `auth` VALUES ('108', '95', '文章国际化', 'I18n', 'article_i18n', '', '0', '1', '0', '0');
INSERT INTO `auth` VALUES ('109', '95', '保存文章国际化', 'I18n', 'save_article', '', '0', '1', '0', '0');
INSERT INTO `auth` VALUES ('110', '95', '文章国际化帮助', 'I18n', 'article_help', '', '0', '1', '0', '0');
INSERT INTO `auth` VALUES ('111', '95', '页面', 'I18n', 'page', '', '0', '0', '0', '0');
INSERT INTO `auth` VALUES ('112', '95', '页面国际化', 'I18n', 'page_i18n', '', '0', '1', '0', '0');
INSERT INTO `auth` VALUES ('113', '95', '保存页面国际化', 'I18n', 'save_page', '', '0', '1', '0', '0');
INSERT INTO `auth` VALUES ('114', '95', '页面国际化帮助', 'I18n', 'page_help', '', '0', '1', '0', '0');

-- ----------------------------
-- Table structure for `category`
-- ----------------------------
DROP TABLE IF EXISTS `category`;
CREATE TABLE `category` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `pid` int(11) unsigned NOT NULL DEFAULT '0',
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `num` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '文章数量',
  `sort` int(11) NOT NULL DEFAULT '0',
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cover` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `is_gallery` int(1) unsigned NOT NULL DEFAULT '0' COMMENT '0普通1相册',
  `seo_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `seo_description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` int(1) unsigned NOT NULL DEFAULT '0' COMMENT '0正常1禁用',
  `time` int(11) unsigned NOT NULL DEFAULT '0',
  `lang` varchar(8) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'zh-CN',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of category
-- ----------------------------
INSERT INTO `category` VALUES ('1', '0', '分类目录', 'categories', '0', '0', '', '', '0', '', '', '0', '0', 'zh-CN');
INSERT INTO `category` VALUES ('2', '1', '未分类', 'uncategorized', '1', '0', '', '', '0', '', '', '0', '0', 'zh-CN');

-- ----------------------------
-- Table structure for `comment`
-- ----------------------------
DROP TABLE IF EXISTS `comment`;
CREATE TABLE `comment` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `pid` int(11) unsigned NOT NULL DEFAULT '0',
  `article_id` int(11) unsigned NOT NULL DEFAULT '0',
  `comment` text COLLATE utf8_unicode_ci NOT NULL,
  `author` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sort` int(11) NOT NULL DEFAULT '0',
  `status` int(1) unsigned NOT NULL DEFAULT '0' COMMENT '0正常1待审2垃圾评论3机器人评论',
  `time` int(11) unsigned NOT NULL DEFAULT '0',
  `lang` varchar(8) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'zh-CN',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of comment
-- ----------------------------
INSERT INTO `comment` VALUES ('1', '0', '1', '您好，这是一条评论。若需要审核、编辑或删除评论，请访问控制台的评论管理。', 'admin', '0', '0', '0', 'zh-CN');

-- ----------------------------
-- Table structure for `link`
-- ----------------------------
DROP TABLE IF EXISTS `link`;
CREATE TABLE `link` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `logo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sort` int(11) NOT NULL DEFAULT '0',
  `status` int(1) unsigned NOT NULL DEFAULT '0' COMMENT '0正常1禁用',
  `time` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of link
-- ----------------------------

-- ----------------------------
-- Table structure for `log`
-- ----------------------------
DROP TABLE IF EXISTS `log`;
CREATE TABLE `log` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `author` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `request_method` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `message` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ip` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` int(1) unsigned NOT NULL DEFAULT '0' COMMENT '0成功1失败',
  `time` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of log
-- ----------------------------

-- ----------------------------
-- Table structure for `media`
-- ----------------------------
DROP TABLE IF EXISTS `media`;
CREATE TABLE `media` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ext` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `path` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` int(1) unsigned NOT NULL DEFAULT '0' COMMENT '0图片1音视频2Flash3文件',
  `status` int(1) unsigned NOT NULL DEFAULT '0' COMMENT '0正常1未使用2失效',
  `time` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of media
-- ----------------------------

-- ----------------------------
-- Table structure for `page`
-- ----------------------------
DROP TABLE IF EXISTS `page`;
CREATE TABLE `page` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `pid` int(11) unsigned NOT NULL DEFAULT '0',
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sort` int(11) unsigned NOT NULL DEFAULT '0',
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `cover` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `is_full` int(1) unsigned NOT NULL DEFAULT '0' COMMENT '0普通1宽屏',
  `seo_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `seo_description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `is_nav` int(1) unsigned NOT NULL DEFAULT '0',
  `status` int(1) unsigned NOT NULL DEFAULT '0' COMMENT '0正常1禁用',
  `time` int(11) unsigned NOT NULL DEFAULT '0',
  `lang` varchar(8) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'zh-CN',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of page
-- ----------------------------
INSERT INTO `page` VALUES ('1', '0', '页面', 'page', '0', '', '', '', '0', '', '', '0', '0', '0', 'zh-CN');
INSERT INTO `page` VALUES ('2', '1', '我的点赞', 'page-likes', '0', '', '', '', '0', '', '', '0', '0', '0', 'zh-CN');
INSERT INTO `page` VALUES ('3', '1', '我的收藏', 'page-favorites', '0', '', '', '', '0', '', '', '0', '0', '0', 'zh-CN');

-- ----------------------------
-- Table structure for `role`
-- ----------------------------
DROP TABLE IF EXISTS `role`;
CREATE TABLE `role` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sort` int(11) NOT NULL DEFAULT '0',
  `auth_ids` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'json数据',
  `status` int(1) unsigned NOT NULL DEFAULT '0' COMMENT '0正常1禁用',
  `time` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of role
-- ----------------------------
INSERT INTO `role` VALUES ('1', '管理员', '0', '[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,88,89,90,91,92,93,94,95,96,97,98,99,100,101,102,103,104,105,106,107,108,109,110,111,112,113,114]', '0', '0');
INSERT INTO `role` VALUES ('2', '游客', '0', '[1,2,3,4,5,6,8,12,13,14,16,20,21,22,24,28,29,30,32,36,37,38,40,44,45,46,48,52,55,56,57,59,63,64,65,70,71,72,74,78,79,80,82,87,89,90,93,95,96,97,99,100,102,103,104,106,107,108,110,111,112,114]', '0', '0');
INSERT INTO `role` VALUES ('3', '翻译', '0', '[1,2,3,4,5,6,8,12,13,14,16,20,21,22,24,28,29,30,32,36,37,38,40,44,45,46,48,52,55,56,57,59,63,64,65,70,71,72,74,78,79,80,82,87,89,90,93,95,96,97,98,99,100,101,102,103,104,105,106,107,108,109,110,111,112,113,114]', '0', '0');

-- ----------------------------
-- Table structure for `setting`
-- ----------------------------
DROP TABLE IF EXISTS `setting`;
CREATE TABLE `setting` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `value` text COLLATE utf8_unicode_ci NOT NULL,
  `time` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of setting
-- ----------------------------

-- ----------------------------
-- Table structure for `tag`
-- ----------------------------
DROP TABLE IF EXISTS `tag`;
CREATE TABLE `tag` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `num` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '文章数量',
  `sort` int(11) NOT NULL DEFAULT '0',
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `seo_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `seo_description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` int(1) unsigned NOT NULL DEFAULT '0',
  `time` int(11) unsigned NOT NULL DEFAULT '0',
  `lang` varchar(8) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'zh-CN',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of tag
-- ----------------------------

-- ----------------------------
-- Table structure for `user`
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `role_id` int(11) unsigned NOT NULL DEFAULT '0',
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nickname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `avatar` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `reg_time` int(11) unsigned NOT NULL DEFAULT '0',
  `first_login` int(11) unsigned NOT NULL DEFAULT '0',
  `last_login` int(11) unsigned NOT NULL DEFAULT '0',
  `lang_slugs` text COLLATE utf8_unicode_ci NOT NULL,
  `status` int(1) unsigned NOT NULL DEFAULT '0' COMMENT '0正常1禁用',
  `remember_token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('1', '1', 'admin', '', '', 'db69fc039dcbd2962cb4d28f5891aae1', '', '0', '0', '0', '[\"co\",\"gn\",\"rw\",\"ha\",\"no\",\"af\",\"yo\",\"en\",\"gom\",\"la\",\"ne\",\"fr\",\"cs\",\"haw\",\"ka\",\"ru\",\"zh-CN\",\"fa\",\"bho\",\"hi\",\"be\",\"sw\",\"is\",\"yi\",\"ak\",\"ga\",\"gu\",\"km\",\"sk\",\"iw\",\"kn\",\"hu\",\"ta\",\"ar\",\"bn\",\"az\",\"sm\",\"su\",\"da\",\"sn\",\"bm\",\"lt\",\"vi\",\"mt\",\"tk\",\"as\",\"ca\",\"si\",\"ceb\",\"gd\",\"sa\",\"pl\",\"gl\",\"lv\",\"uk\",\"tt\",\"cy\",\"ja\",\"tl\",\"ay\",\"lo\",\"te\",\"ro\",\"ht\",\"doi\",\"sv\",\"mai\",\"th\",\"hy\",\"my\",\"ps\",\"hmn\",\"dv\",\"zh-TW\",\"lb\",\"sd\",\"ku\",\"tr\",\"mk\",\"bg\",\"ms\",\"lg\",\"mr\",\"et\",\"ml\",\"de\",\"sl\",\"ur\",\"pt\",\"ig\",\"ckb\",\"om\",\"el\",\"es\",\"fy\",\"so\",\"am\",\"ny\",\"pa\",\"eu\",\"it\",\"sq\",\"ko\",\"tg\",\"fi\",\"ky\",\"ee\",\"hr\",\"qu\",\"bs\",\"mi\",\"or\",\"ti\",\"kk\",\"nl\",\"kri\",\"ln\",\"mg\",\"mn\",\"lus\",\"xh\",\"zu\",\"sr\",\"nso\",\"st\",\"eo\",\"mni-Mtei\",\"ug\",\"uz\",\"ilo\",\"id\",\"jw\",\"ts\"]', '0', '');
INSERT INTO `user` VALUES ('2', '2', 'anonymous', '匿名', '', '87d9bb400c0634691f0e3baaf1e2fd0d', '', '0', '0', '0', '', '0', '');
